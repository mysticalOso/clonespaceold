/*
 * Vessel.cpp
 *
 *  Created on: 8 Feb 2015
 *      Author: mysticalOso
 */

#include <core/Vessel.h>
#include <osogine/game/EntityList.h>
#include <osogine/game/Game.h>
#include <osogine/game/Hub.h>
#include <races/Race.h>
#include <ship/EnergyCore.h>
#include <ship/GemTractor.h>
#include <ship/Module.h>
#include <ship/ModuleList.h>
#include <ship/Shield.h>
#include <ship/ShipTractor.h>
#include <ship/Steering.h>
#include <weapons/Weapon.h>
#include <string>


Vessel::Vessel(Hub* parent) : Entity(parent)
{
	init();
}

Vessel::Vessel(Game* game) : Entity(game)
{
	init();
}

Vessel::~Vessel() {
}


void Vessel::addEngine(Module* engine)
{
	engines->add(engine);
	addChild(engine);
}

void Vessel::addPrimaryWeapon(Module* weapon)
{
	primaryWeapons->add(weapon);
	addChild(weapon);
}


void Vessel::addSecondaryWeapon(Module* weapon)
{
	secondaryWeapons->add(weapon);
	addChild(weapon);
}

void Vessel::addPassiveModule(Module* module)
{
	passiveModules->add(module);
	addChild(module);
}

void Vessel::setSteering(Steering* steering1)
{
	if (steering!=0) children->remove(steering);
	steering = steering1;
	addChild(steering);
}

void Vessel::setEnergyCore(EnergyCore* core)
{
	if (energyCore!=0) children->remove(energyCore);
	energyCore = core;
	addChild(energyCore);
}

void Vessel::setShield(Shield* shield1)
{
	if (shield!=0) children->remove(shield);
	shield = shield1;
	addChild(shield);
}

void Vessel::setGemTractor(GemTractor* gemTractor1)
{
	if (gemTractor!=0) children->remove(gemTractor);
	gemTractor = gemTractor1;
	addChild(gemTractor);
}


Weapon* Vessel::getPrimaryWeapon()
{
	if (primaryWeapons->size()>0) return (Weapon*) primaryWeapons->get(0);
	return 0;
}


void Vessel::turnOnPrimaryWeapons(bool isOn)
{
	primaryWeapons->turnOn(isOn);
}

void Vessel::turnOnSecondaryWeapons(bool isOn)
{
	secondaryWeapons->turnOn(isOn);
}

void Vessel::turnOnShield(bool isOn)
{
	if (shield!=0) shield->turnOn(isOn);
}


void Vessel::setMoveDir(string dir, bool isOn)
{
	engines->setThrust(dir, isOn);
}

void Vessel::setTarget(float x, float y, bool isMouse)
{
	if (steering != 0) steering->setTarget(x,y,isMouse);
}


Polygon* Vessel::getOutline()
{
	if ( shieldIsOn() ) return shield->getOutline();
	else return outline;
}



bool Vessel::shieldIsOn()
{
	return (shield!=0 && shield->getIsOn());
}

int Vessel::getAmmo()
{
	return secondaryWeapons->getAmmo();
}

EntityList* Vessel::getGems()
{
	if (gemTractor!=0) return gemTractor->getGems();
	else return 0;
}

void Vessel::addAmmo()
{
	secondaryWeapons->addAmmo();
}

bool Vessel::needsAmmo()
{
	return secondaryWeapons->needsAmmo();
}

void Vessel::setShipTractor(ShipTractor* shipTractor1)
{
	if (shipTractor!=0) children->remove(shipTractor);
	shipTractor = shipTractor1;
	addChild(shipTractor);
}

EntityList* Vessel::getShipGems()
{
	if (shipTractor!=0) return shipTractor->getGems();
	else return 0;
}

Module* Vessel::getPassiveModule(string name)
{
	return passiveModules->getModule(name);
}

void Vessel::init()
{
	engines = new ModuleList();
	primaryWeapons = new ModuleList();
	secondaryWeapons = new ModuleList();
	passiveModules = new ModuleList();
	steering = 0;
	energyCore = 0;
	shield = 0;
	gemTractor = 0;
	isDocked = false;
}

void Vessel::killEngines()
{
	engines->turnOn(false);
}

bool Vessel::isEnemyOf(Vessel* vessel)
{
	Race* r1 = getRace();
	Race* r2 = vessel->getRace();
	return r1->isEnemy(r2);
}
