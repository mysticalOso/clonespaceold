/*
 * ExplosionGfxP.cpp
 *
 *  Created on: 4 Dec 2014
 *      Author: mysticalOso
 */

#include <core/ExplosionGfxP.h>
#include <osogine/gfx/GfxObject.h>
#include <space/Explosion.h>
#include <osogine/utils/Printer.h>

ExplosionGfxP::ExplosionGfxP() {
	// TODO Auto-generated constructor stub

}

ExplosionGfxP::~ExplosionGfxP() {
	// TODO Auto-generated destructor stub
}

void ExplosionGfxP::init()
{
	addUniform( "MVP" );
	addUniform( "time" );
	addUniform( "alpha" );

	addUniform( "M" );
	addUniform( "lightColour" );
	addUniform( "lightDirection" );
	addUniform( "numLights" );

}

void ExplosionGfxP::execute()
{
	Explosion* exp = (Explosion*) entity;
	//clearDepthBit();
	toggleDepthTest(false);
	setUniformValue( "MVP", getMVP() );
	setUniformValue( "M", entity->getMatrix() );

	vector<vec3> colours;

	colours.push_back(vec3(1.0,1.0,1.0));
	colours.push_back(vec3(0.1 ,0 , 0.2));
	colours.push_back(vec3(0,0.1, 0.2 ));

	vector<vec3> directions;
	directions.push_back(vec3(-0.8,0.8,0.5));
	directions.push_back(vec3(1,0,0));
	directions.push_back(vec3(0,-1,0));


	setUniformValue( "lightColour", colours, 3 );
	setUniformValue( "lightDirection", directions, 3 );
	setUniformValue( "numLights", 3 );

	activateBuffer( gfx->getVertexBuffer(), 0 );
	activateBuffer( gfx->getColourBuffer(), 1 );
	activateBuffer( exp->getVelocityBuffer(), 2 );
	activateBuffer( exp->getForceBuffer(), 3 );
	activateBuffer( exp->getAxisBuffer(), 4 );
	activateBuffer( exp->getSpinBuffer(), 5 );
	activateBuffer( exp->getLifeBuffer(), 6 );
	activateBuffer( exp->getDeathBuffer(), 7 );
	activateBuffer( exp->getOffsetBuffer(), 8 );
	activateBuffer( gfx->getNormalBuffer(), 9 );
	activateBuffer( gfx->getIndexBuffer(), 0 );


	int k = 5;
	int time = exp->getTime();
	//Printer::print("Time = ",time,"\n");
	if (time < 5) k = time;
	for (int i = 0 ; i < k; i++)
	{
		float alpha = (float)(k-i)/(float)k;
		if (i>0) alpha *= 0.3;
//		if (time>100)
//		{
//			alpha *= (200 - time)*0.01;
//		}
		setUniformValue( "time", (float)(time - i) );
		setUniformValue( "alpha", alpha );

		if (!exp->getIsLines())
		{
			draw( );
		}
		else
		{
			//toggleDepthTest(false);
			drawLines();
		}
	}

	deactivateBuffer( 0 );
	deactivateBuffer( 1 );
	deactivateBuffer( 2 );
	deactivateBuffer( 3 );
	deactivateBuffer( 4 );
	deactivateBuffer( 5 );
	deactivateBuffer( 6 );
	deactivateBuffer( 7 );
	deactivateBuffer( 8 );
	deactivateBuffer( 9 );
	//deactivateBuffer( 4 );



}
