/*
 * Constants.h
 *
 *  Created on: 16 Oct 2013
 *      Author: Adam Nasralla
 */

#ifndef CONSTANTS_H_
#define CONSTANTS_H_

#define _USE_MATH_DEFINES
#include <math.h>


extern const int SCREEN_WIDTH;
extern const int SCREEN_HEIGHT;
extern const bool FULL_SCREEN;
extern const int WIDTH;
extern const int HEIGHT;
extern const int WIDTH2;
extern const int HEIGHT2;
extern const float PI;
extern const int NO_STARS;
extern const int GAME_MODE;
extern const bool SOUND_ON;
extern const bool HALL_LIGHTS;
extern const bool LIGHTS;
extern const bool TEST_MODE;
extern const float MISSILE_DAMAGE;
extern const bool INTERIOR_COLLISIONS;

#endif /* CONSTANTS_H_ */
