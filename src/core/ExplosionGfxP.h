/*
 * ExplosionGfxP.h
 *
 *  Created on: 4 Dec 2014
 *      Author: mysticalOso
 */

#ifndef SRC_OSOGINE_GFX_EXPLOSIONGFXP_H_
#define SRC_OSOGINE_GFX_EXPLOSIONGFXP_H_

#include <osogine/gfx/GfxProgram.h>

class ExplosionGfxP: public GfxProgram {
public:
	ExplosionGfxP();
	virtual ~ExplosionGfxP();

	void init();
	void execute();
};

#endif /* SRC_OSOGINE_GFX_EXPLOSIONGFXP_H_ */
