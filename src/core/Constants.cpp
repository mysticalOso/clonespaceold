/*
 * Constants.cpp
 *
 *  Created on: 21 Mar 2014
 *      Author: Adam Nasralla
 */

#include <core/Constants.h>

const int SCREEN_WIDTH = 1920;
const int SCREEN_HEIGHT = 1080;
const bool FULL_SCREEN = true;

//const int SCREEN_WIDTH = 1600;
//const int SCREEN_HEIGHT = 900;
//const bool FULL_SCREEN = false;

const int WIDTH = 1920;
const int HEIGHT = 1080;
const int WIDTH2 = (WIDTH/2);
const int HEIGHT2 = (HEIGHT/2);
const int NO_STARS = 400;
const int GAME_MODE = 0;
const bool SOUND_ON = true;
const bool HALL_LIGHTS = false;

const bool INTERIOR_COLLISIONS = true;
const bool LIGHTS = true;
const bool TEST_MODE = false;

//TODO remove missile damage global!
const float MISSILE_DAMAGE = 5;
