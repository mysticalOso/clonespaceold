/*
 * Camera.h
 *
 *  Created on: 8 Oct 2014
 *      Author: Adam Nasralla
 */

#ifndef CAMERACS_H_
#define CAMERACS_H_

#define GLM_FORCE_RADIANS

#include <osogine/game/Camera.h>

class Game;
class ForceMover;
class Clone;
class Sentient;
class Ship;

class CameraCS: public Camera
{
public:
	CameraCS(Game* game);
	virtual ~CameraCS();

	mat4 getViewProjectionMatrix();

	void update();

	void increaseZoom();
	void decreaseZoom();

	float getCurrentZoom();

	void targetNextClone();

	void setTargetPosition(float x, float y, float z, float speed, float xySpeed);

	void setTarget(Entity* e){ targetEntity = e; targetClone = 0;}

	void setTargetClone(Clone* c);
	void setSentient(Sentient* s){ sentient = s; }

	void clearFixedTarget();

	mat4 getProjectionMatrix();

	Clone* getOldTargetClone(){ return oldTargetClone; }

	Ship* getOldTargetShip(){ return oldTargetShip; }

	bool isTransitioning(){ return count<100; }

	void setOldTargetShip(Ship* ship){ oldTargetShip = ship; }


	void setZoom(float z);

	void setSpeed(float s);

	void setPosition(float x, float y, float z);

private:

	void updateTarget();

	bool targetHasChanged();

	void updateCameraMovement();

	void initZoomLevels();

	bool isDriver();


	bool lockOnTarget;
	Clone* targetClone;
	Ship* targetShip;
	Entity* targetEntity;
	Sentient* sentient;

	float zoomIncrement;
	ForceMover* mover;
	List<int> zoomLevels;
	int currentZoomLevel;
	int maxZoomLevel;

	float currentShipZoom;
	float currentCloneZoom;

	Entity* oldTarget;
	Ship* oldTargetShip;

	Clone* oldTargetClone;

	bool fixedTarget;
	bool wasDriver;

	int count;
};

#endif /* CAMERACS_H_ */
