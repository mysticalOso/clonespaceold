/*
 * InputHubCS.h
 *
 *  Created on: 28 Oct 2014
 *      Author: Adam Nasralla
 */

#ifndef INPUTHUBCS_H_
#define INPUTHUBCS_H_

#include <osogine/input/InputHub.h>

class IInput;
class Game;

class InputHubCS: public InputHub
{
public:
	InputHubCS();
	~InputHubCS();

	void init(IInput*, Game*);

	void initControllers(Game*);
};

#endif /* INPUTHUBCS_H_ */
