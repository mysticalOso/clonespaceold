/*
 * Camera.cpp
 *
 *  Created on: 8 Oct 2014
 *      Author: Adam Nasralla
 */
#include <clone/Clone.h>
#include <osogine/utils/ForceMover.h>
#include <osogine/utils/Printer.h>
#include <races/Sentient.h>
#include <ship/Ship.h>

#define GLM_FORCE_RADIANS
#define degreesToRadians(x) x*(3.141592f/180.0f)

#include <core/CameraCS.h>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include <core/Constants.h>
#include <osogine/utils/Transform.h>

CameraCS::CameraCS(Game* game) : Camera(game)
{
	targetClone = 0;
	mover = new ForceMover(game);
	lockOnTarget = true;
	zoomIncrement = 5000;
	currentZoomLevel = 7;
	currentShipZoom = 50000;
	currentCloneZoom = 400;
	maxZoomLevel = 0;
	updateOrder = 2;
	fixedTarget = false;
	mover->setTargetZ(50000);
	mover->setSubjectEntity(this);
	initZoomLevels();
}

CameraCS::~CameraCS()
{

}

void CameraCS::update()
{
	if (count<100) count++;
	updateTarget();

	updateCameraMovement();

	//mover->update();
	Entity::update();


}


glm::mat4 CameraCS::getViewProjectionMatrix()
{
	float far = getZ() + 100;
	if (far < 200) far = 200;
	glm::mat4 Projection = glm::perspective( degreesToRadians(zoom) , 16.0f / 9.0f, 100.f, far);
	//glm::scale(glm::vec3(2.0f/WIDTH, 2.0f/HEIGHT,0.001f)) ;
			//
	// Camera matrix
//	glm::mat4 View       = glm::lookAt(
//								pos, // Camera is at (4,3,-3), in World Space
//								glm::vec3(0,0,0), // and looks at the origin
//								glm::vec3(0,1,0)  // Head is up (set to 0,-1,0 to look upside-down)
//						   );

	glm::mat4 View =  glm::inverse( getTransform()->getMatrix() );

	return Projection * View;
}

void CameraCS::increaseZoom()
{
	setSpeed(1);
	if (isDriver())
	{
		currentShipZoom /= 1.2;
		if (currentShipZoom < 1000) currentShipZoom = 1000;
		setZoom(currentShipZoom);
	}
	else
	{
		currentCloneZoom /= 1.2;
		if (currentCloneZoom < 100) currentCloneZoom = 100;
		setZoom(currentCloneZoom);
	}
}

void CameraCS::decreaseZoom()
{
	setSpeed(0.5);
	if (isDriver())
	{
		currentShipZoom *= 1.2;
		if (currentShipZoom > 75000) currentShipZoom = 75000;
		setZoom(currentShipZoom);
	}
	else
	{
		currentCloneZoom *= 1.2;
		if (currentCloneZoom > 200000) currentCloneZoom = 200000;
		setZoom(currentCloneZoom);
	}
}

float CameraCS::getCurrentZoom()
{
	if (currentZoomLevel > maxZoomLevel) currentZoomLevel = maxZoomLevel;
	if (currentZoomLevel < 0) currentZoomLevel = 0;
	return zoomLevels.get(currentZoomLevel);
}



void CameraCS::initZoomLevels()
{
	zoomLevels.add(1250);
	zoomLevels.add(2500);
	zoomLevels.add(5000);
	zoomLevels.add(10000);
	zoomLevels.add(20000);
	zoomLevels.add(30000);
	zoomLevels.add(40000);
	zoomLevels.add(50000);
	zoomLevels.add(60000);
	zoomLevels.add(70000);
	zoomLevels.add(80000);
	zoomLevels.add(90000);
	zoomLevels.add(100000);
	maxZoomLevel = zoomLevels.size()-1;
}



bool CameraCS::isDriver()
{
	if (targetClone==0) return false;
	else
	{
		wasDriver = targetClone->getIsDriver();
		return wasDriver;
	}
}



void CameraCS::targetNextClone()
{
	oldTargetClone = targetClone;
	count = 0;
	fixedTarget = false;
	setTargetClone( sentient->getNextClone() );
}

void CameraCS::updateTarget()
{
	if (targetClone!=0)
	{

		if (isDriver()) targetEntity = targetClone->getShip();
		else targetEntity = targetClone;
		targetShip = targetClone->getShip();

	}
}

bool CameraCS::targetHasChanged()
{
	return (targetEntity!=oldTarget || targetShip!=oldTargetShip);
}

void CameraCS::updateCameraMovement()
{
	if (targetHasChanged())
	{
		count = 0;
//		Printer::print("\nTarget Entity = ");
//		Printer::print(targetEntity->getName());
//		Printer::print("\nTarget Clone = ");
//		Printer::print(targetClone->getName());
//		Printer::print("\nTarget Ship = ");
//		Printer::print(targetShip->getName());
		mover->setTargetEntity(targetEntity);
		mover->setTargetParent(targetShip);
		if (isDriver())
		{
			if (!fixedTarget) setSpeed(0.4);
			targetShip->fadeRoofIn();
			if (!fixedTarget) setZoom(currentShipZoom);
			mover->setTargetR(0);
		}
		else
		{
			if (!fixedTarget)setSpeed(1.5);
			targetShip->fadeRoofOut();
			if (!fixedTarget) setZoom(currentCloneZoom);
			mover->setLockEntityR(true);
		}
	}

	mover->update();

}

void CameraCS::clearFixedTarget()
{
	fixedTarget = false;
	oldTarget = 0;
	setTargetClone(targetClone);
	mover->setTargetEntity(targetClone);
	mover->setLockEntityX(true);
	mover->setLockEntityY(true);
	mover->setTargetZ(currentCloneZoom);
//	mover->setTargetX(targetClone->getX());
//	mover->setTargetY(targetClone->getY());
//	mover->setTargetZ(currentCloneZoom);
	//mover->setTargetEntity(targetClone);
	//mover->setTargetParent(targetClone->getShip());
	//mover->setTargetX(targetClone)
	//mover->setTargetParent(targetShip);
	//mover->setTargetEntity(targetEntity);
}

void CameraCS::setTargetClone(Clone* c)
{
	count = 0;
	oldTarget = targetEntity;
	oldTargetShip = targetShip;

	targetClone = c;
	targetShip = c->getShip();
}

void CameraCS::setZoom(float z)
{
	//Printer::print("ZOOM = ",z,"\n");
	mover->setTargetZ(  z  );
}

mat4 CameraCS::getProjectionMatrix()
{
	return Camera::getProjectionMatrix();
	//return  glm::scale(vec3(2.0f/WIDTH, 2.0f/HEIGHT, 1.0f));
}

void CameraCS::setTargetPosition(float x, float y, float z, float speed, float xySpeed)
{

	mover->setXYSpeed(xySpeed);


	setSpeed(speed);
	targetEntity = 0;
	targetClone = 0;
	fixedTarget = true;
	mover->setTargetEntity(0);
	mover->setTargetX(x);
	mover->setTargetY(y);
	mover->setTargetZ(z);
}

void CameraCS::setSpeed(float s)
{
	mover->setSpeed(s);
}

void CameraCS::setPosition(float x, float y, float z)
{
	Entity::setPosition(x,y,z);
	mover->setPosition(x,y,z);
}
