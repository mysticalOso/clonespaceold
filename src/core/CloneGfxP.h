/*
 * CloneGfxP.h
 *
 *  Created on: 22 Feb 2015
 *      Author: Adam Nasralla
 */

#ifndef SRC_CORE_CLONEGFXP_H_
#define SRC_CORE_CLONEGFXP_H_

#include <osogine/gfx/GfxProgram.h>

class Polygon;

class CloneGfxP: public GfxProgram
{
public:
	CloneGfxP();
	~CloneGfxP();

	void init();
	void execute();

	void setLightMap(Polygon* p){ lightMap = p; }

private:

	Polygon* lightMap;



};

#endif /* SRC_CORE_CLONEGFXP_H_ */
