/*
 * EntityHubCL.cpp
 *
 *  Created on: 9 Oct 2014
 *      Author: Adam Nasralla
 */

#include <builders/AIBuilder.h>
#include <builders/AsteroidBuilder.h>
#include <builders/CloneBuilder.h>
#include <builders/CloudBuilder.h>
#include <builders/ExplosionBuilder.h>
#include <builders/GemBuilder.h>
#include <builders/InteriorBuilder.h>
#include <builders/ModuleBuilder.h>
#include <builders/RaceBuilder.h>
#include <builders/ShieldBuilder.h>
#include <builders/ShipBuilder.h>
#include <builders/UIBuilder.h>
#include <core/CameraCS.h>
#include <core/EntityHubCS.h>
#include <osogine/game/Game.h>
#include <osogine/gfx/GfxHub.h>
#include <hubs/RaceHub.h>
#include <space/Space.h>
#include <core/Constants.h>
#include <hubs/UIHub.h>
#include <ui/Tutorial.h>


void EntityHubCS::initEntities()
{

	Space* space = new Space(game);
	addEntity( "space", space );

	RaceHub* raceHub = new RaceHub(game);
	addEntity( "raceHub", raceHub );

	UIHub* uiHub = new UIHub(game);
	addEntity( "uiHub", uiHub );

	Tutorial* tutorial = new Tutorial(game);
	if (!TEST_MODE) addEntity( "tutorial", tutorial );


	space->init();

	if (!TEST_MODE) raceHub->init();

	if (!TEST_MODE) tutorial->init();

	//addEntity( "world", new World(game) );
}

void EntityHubCS::initCamera()
{
	if (!TEST_MODE)
	{
		camera = new CameraCS(game);
		camera->setController("camera");
	}
	else
	{
		camera = new Camera(game);
	}
}

void EntityHubCS::initBuilders()
{
	//TODO add inits for builder sharing
	addBuilder( "module", new ModuleBuilder(game) );
	addBuilder( "asteroid", new AsteroidBuilder(game) );
	addBuilder( "ship", new ShipBuilder(game) );
	addBuilder( "cloud", new CloudBuilder(game) );
	addBuilder( "explosion", new ExplosionBuilder(game) );
	addBuilder( "ai", new AIBuilder(game) );
	addBuilder( "race", new RaceBuilder(game) );
	addBuilder( "clone", new CloneBuilder(game) );
	addBuilder( "gem", new GemBuilder(game) );
	addBuilder( "shield", new ShieldBuilder(game) );
	addBuilder( "interior", new InteriorBuilder(game) );
	addBuilder( "ui", new UIBuilder(game) );

}

void EntityHubCS::update()
{
	children->update(0);
	children->update(1);
	children->update(2);
}

void EntityHubCS::render()
{
	for (int i = 0; i < 10; i++)
	{
		children->render(i);
	}
}

void EntityHubCS::event(string name)
{
	children->event(name);
}
