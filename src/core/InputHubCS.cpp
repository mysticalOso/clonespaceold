/*
 * InputHubCS.cpp
 *
 *  Created on: 28 Oct 2014
 *      Author: Adam Nasralla
 */

#include <osogine/utils/Printer.h>
#include <controllers/CameraCtrl.h>
#include <controllers/CloneCtrl.h>
#include <controllers/CursorCtrl.h>
#include <controllers/RaceCtrl.h>
#include <controllers/ShipCtrl.h>
#include <controllers/UIController.h>
#include <core/InputHubCS.h>
#include <core/Constants.h>
#include <osogine/input/SimpleController.h>


InputHubCS::InputHubCS()
{
}

InputHubCS::~InputHubCS()
{
}

void InputHubCS::initControllers(Game* game)
{


	if (!TEST_MODE)
	{
		controllers["camera"] = new CameraCtrl(game);
		controllers["ship"] = new ShipCtrl(game);
		controllers["clone"] = new CloneCtrl(game);
		controllers["ui"] = new UIController(game);
		controllers["race"] = new RaceCtrl(game);
		controllers["cursor"] = new CursorCtrl(game);
	}
	else
	{
		controllers["camera"] = new CameraCtrl(game);
		controllers["simple"] = new SimpleController(game);
		controllers["ui"] = new UIController(game);
	}
}

void InputHubCS::init(IInput* inputI, Game* game)
{
	InputHub::init(inputI, game);
	setMouseVisible(true);
	setMouseRelative(false);
	setMouseCentre(SCREEN_WIDTH/2, SCREEN_HEIGHT/2);
	//Printer::print("InputHubCS\n");
}
