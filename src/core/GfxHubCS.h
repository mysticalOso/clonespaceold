/*
 * GfxHubCS.h
 *
 *  Created on: 15 Oct 2014
 *      Author: Adam Nasralla
 */

#ifndef GFXHUBCS_H_
#define GFXHUBCS_H_

#include <osogine/gfx/GfxHub.h>
#include <osogine/utils/List.h>

class GfxHubCS: public GfxHub
{
public:
	GfxHubCS();
	~GfxHubCS();

	void initObjects();
	void initPrograms();


};

#endif /* GFXHUBCS_H_ */
