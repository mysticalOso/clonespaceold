/*
 * EntityHubCL.h
 *
 *  Created on: 9 Oct 2014
 *      Author: Adam Nasralla
 */

#ifndef ENTITYHUBCS_H_
#define ENTITYHUBCS_H_

#include <osogine/game/EntityHub.h>

class EntityHubCS : public EntityHub
{
public:
	EntityHubCS(){}
	~EntityHubCS(){}

	void initBuilders();
	void initCamera();
	void initEntities();

	void event(string name);
	void update();

	void render();
};

#endif /* ENTITYHUBCS_H_ */
