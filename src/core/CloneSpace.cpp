/*
 * CrashLand.cpp
 *
 *  Created on: 9 Oct 2014
 *      Author: Adam Nasralla
 */

#include <osogine/audio/AudioHub.h>
#include <core/CloneSpace.h>
#include <core/Constants.h>
#include <core/EntityHubCS.h>
#include <core/GfxHubCS.h>
#include <core/InputHubCS.h>
#include <osogine/platform/DesktopPlatform.h>

CloneSpace::CloneSpace()
{
	initPlatform( new DesktopPlatform( "CloneSpace", SCREEN_WIDTH, SCREEN_HEIGHT, FULL_SCREEN ) );
	initGfxHub( new GfxHubCS() );
	initAudioHub( new AudioHub() );
	initInputHub( new InputHubCS() );
	initEntityHub( new EntityHubCS() );

	run();
}

