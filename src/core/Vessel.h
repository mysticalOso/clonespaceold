/*
 * Vessel.h
 *
 *  Created on: 8 Feb 2015
 *      Author: mysticalOso
 */

#ifndef SRC_CORE_VESSEL_H_
#define SRC_CORE_VESSEL_H_

#include <osogine/game/Entity.h>

class EnergyCore;
class GemTractor;
class Module;
class ModuleList;
class Shield;
class Steering;
class Weapon;
class Race;
class Hub;
class ShipTractor;

class Vessel: public Entity {
public:
	Vessel(Hub* parent);
	Vessel(Game* game);
	virtual ~Vessel();

	void addEngine(Module* engine);
	void addPrimaryWeapon(Module* weapon);
	void addSecondaryWeapon(Module* weapon);
	void addPassiveModule(Module* module);
	void setGemTractor(GemTractor* gemTractor);
	void setSteering(Steering* steering1);
	void setEnergyCore(EnergyCore* core);
	void setShield(Shield* shield);

	Shield* getShield(){ return shield; }
	EnergyCore* getEnergyCore(){ return energyCore; }
	Weapon* getPrimaryWeapon();

	virtual void turnOnPrimaryWeapons(bool isOn);
	void turnOnSecondaryWeapons(bool isOn);
	void turnOnShield(bool isOn);

	void setMoveDir(string dir, bool isOn);
	void setTarget(float x, float y, bool isMouse);

	void killEngines();
	Polygon* getOutline();
	bool shieldIsOn();

	bool getIsDocked(){ return isDocked; }
	void setIsDocked(bool b){ isDocked = b; }

	virtual Hub* getBulletHub(){return 0;}

	virtual void setRace(Race* race){ this->race = race; }
	virtual Race* getRace(){ return race; }

	//TODO move health to hull module
	void heal(float amount)
	{
		health += amount;
		if (health>maxHealth) health = maxHealth;
	}

	bool hasMaxHealth(){ return health >= maxHealth; }
	float getHealth(){ return health; }
	float getHealthRatio(){ return health/maxHealth; }

	void setMaxHealth(float maxHealth){
		this->maxHealth = health = maxHealth; }

	void setHealth(float h){ health = h; }

	void setName(string n){ name = n; }

	string getName(){ return name; }

	bool hasGemTractor(){ return gemTractor!=0; }

	bool isEnemyOf(Vessel* vessel);

	int getAmmo();

	void setShipTractor(ShipTractor* shipTractor);
	EntityList* getShipGems();

	void addAmmo();

	bool needsAmmo();
	EntityList* getGems();

	Module* getPassiveModule(string name);
protected:

	void init();

	bool isDocked;
	GemTractor* gemTractor;
	ShipTractor* shipTractor;
	Steering* steering;
	EnergyCore* energyCore;
	Shield* shield;
	ModuleList* engines;
	ModuleList* primaryWeapons;
	ModuleList* secondaryWeapons;
	ModuleList* passiveModules;
	float maxHealth;
	float health;
	Race* race;
	string name;

};

#endif /* SRC_CORE_VESSEL_H_ */
