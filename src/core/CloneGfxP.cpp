/*
 * CloneGfxP.cpp
 *
 *  Created on: 22 Feb 2015
 *      Author: Adam Nasralla
 */

#include <clone/Clone.h>
#include <clone/CloneModel.h>
#include <core/CloneGfxP.h>
#include <glm/mat4x4.hpp>
#include <glm/vec2.hpp>
#include <glm/vec4.hpp>
#include <hubs/CloneHub.h>
#include <hubs/LightHub.h>
#include <osogine/game/Camera.h>
#include <osogine/game/EntityList.h>
#include <osogine/game/Game.h>
#include <osogine/game/Light.h>
#include <osogine/gfx/GfxObject.h>
#include <osogine/gfx/IGfx.h>
#include <osogine/utils/List.h>
#include <osogine/utils/Polygon.h>
#include <osogine/utils/Printer.h>
#include <osogine/utils/Transform.h>
#include <vector>

CloneGfxP::CloneGfxP() : GfxProgram()
{
	lightMap = 0;

}

CloneGfxP::~CloneGfxP()
{
	// TODO Auto-generated destructor stub
}

void CloneGfxP::init()
{
	addUniform( "MVP" );
	addUniform( "M" );
	addUniform( "lightColour" );
	addUniform( "lightPosition" );
	addUniform( "lightMap" );
	addUniform( "lightMapSize" );
	addUniform( "isBlack" );
}

void CloneGfxP::execute()
{

	if (gfx->getDepthEnabled())
	{
		//clearDepthBit();
		toggleDepthTest(true);
	}
	else
	{
		toggleDepthTest(false);
	}
	//clearDepthBit();
	toggleDepthTest(true);
	g->enableAdditiveBlend();



	vec3 lightColour;
	vec3 lightPosition;
	int lightMapSize = 0;
	int isBlack = 0;
	vector<vec2> newLightMap;
	mat4 lightMVP;
	lightMap = new Polygon();

	Clone* clone = (Clone*) entity;
	CloneModel* model = clone->getModel();
	EntityList* parts = model->getParts();
	LightHub* lightHub = clone->getHub()->getLightHub();

	for (int k=0; k<parts->size(); k++)
	{
		entity = (Entity*) parts->get(k);
		gfx = entity->getGfx();
		if (gfx!=0)
		{
			setUniformValue( "MVP", getMVP() );
			setUniformValue( "M", entity->getMatrix() );

			activateBuffer( gfx->getVertexBuffer(), 0 );
			activateBuffer( gfx->getNormalBuffer(), 1 );
			activateBuffer( gfx->getColourBuffer(), 2 );
			activateBuffer( gfx->getIndexBuffer(), 0 );

			for (int i=-1; i<lightHub->size(); i++)
			{
				if (i>=0)
				{
					Light* light = lightHub->getLight(i);
					lightColour = light->getColour() ;
					lightPosition = light->getLightPosition() ;
					Polygon* outline = light->getOutline();
					if (outline!=0) lightMapSize = light->getOutline()->size() ;
					else lightMapSize = 0;
					Transform* trans = light->getTransform();
					lightMVP = camera->getViewProjectionMatrix();
					newLightMap.clear();
					lightMap = light->getOutline();
					isBlack = 0;
					g->enableAdditiveBlend();
				}
				else
				{
					isBlack = 1;
					g->enableAlphaBlend();
				}
				for (int i=0; i<256; i++)
				{
					if (i<lightMap->size())
					{
						vec2 p= lightMap->get(i);
						vec4 p2 = lightMVP * vec4(p,0,1);
						p = vec2(p2) / p2.a;
						newLightMap.push_back(p);
					}
					else
					{
						newLightMap.push_back(vec2(0,0));
					}
				}
				setUniformValue( "lightMap", newLightMap, 256);
				setUniformValue( "lightColour", lightColour);
				setUniformValue( "lightPosition", lightPosition);
				setUniformValue( "lightMapSize", lightMapSize);
				setUniformValue( "isBlack" , isBlack);

				draw();
			}
		}
	}










	//g->enableAdditiveBlend();


	deactivateBuffer( 0 );
	deactivateBuffer( 1 );
	deactivateBuffer( 2 );

	g->enableAlphaBlend();

//	Game* game = clone->getGame();
//	GfxProgram* prog = game->getGfxProgram("outline");
//	prog->render(clone);

}
