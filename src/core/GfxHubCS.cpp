/*
 * GfxHubCS.cpp
 *
 *  Created on: 15 Oct 2014
 *      Author: Adam Nasralla
 */

#include <core/CloneGfxP.h>
#include <core/ExplosionGfxP.h>
#include <core/GfxHubCS.h>
#include <osogine/gfx/FaceGfxP.h>

#include <osogine/utils/List.h>
#include <osogine/gfx/GfxBuilder.h>
#include <osogine/gfx/GfxIndexer.h>
#include <osogine/gfx/GfxObject.h>

GfxHubCS::GfxHubCS() : GfxHub()
{

}

GfxHubCS::~GfxHubCS()
{
}

void GfxHubCS::initObjects()
{
	GfxHub::initObjects();

	//objects["ship"] = builder->buildTriMesh(getShipOutline(), 20, glm::vec3(0.7,0.8,1.0), glm::vec3(0.7,1.0,1.0), true, false);

	//builder->getIndexer()->setupForFlatShading( objects["ship"] );

	//objects["shipNormals"] = builder->buildTriMesh(getShipOutline(), 1, glm::vec3(0.7,0.8,1.0), glm::vec3(0.7,1.0,1.0), true, false);

	//builder->getUtils()->setupForDrawingNormals( objects["shipNormals"], 1 );

	//objects["ship"]->add( objects["shipNormals"] );


}

void GfxHubCS::initPrograms()
{
	GfxHub::initPrograms();
	programs[ "explosion" ] = new ExplosionGfxP();
	programs[ "explosion" ]->load( gfxI, "explosion.vs", "explosion.fs" );

	programs[ "face" ] = new FaceGfxP();
	programs[ "face" ]->load( gfxI, "flatshading.vs", "face.fs" );

	programs[ "clone" ] = new CloneGfxP();
	programs[ "clone" ]->load( gfxI, "clone.vs", "clone.fs" );
}
