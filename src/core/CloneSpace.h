/*
 * CrashLand.h
 *
 *  Created on: 9 Oct 2014
 *      Author: Adam Nasralla
 */

#ifndef CLONESPACE_H_
#define CLONESPACE_H_

#include <osogine/game/Game.h>

class CloneSpace: public Game
{
public:
	CloneSpace();
	~CloneSpace(){}
};

#endif /* CLONESPACE_H_ */
