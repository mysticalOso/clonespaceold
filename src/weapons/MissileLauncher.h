/*
 * Weapon.h
 *
 *  Created on: 30 Jan 2014
 *      Author: Adam Nasralla
 */

#ifndef MISSILELAUNCHER_H_
#define MISSILELAUNCHER_H_

#include <weapons/Weapon.h>

class CountTimer;
class Bullet;

//TODO rename to AmmoWeapon
class MissileLauncher : public Weapon
{
public:
	MissileLauncher( int rate, int ammo1, Bullet* bullet, Game* game );

	~MissileLauncher();
	void update();

	int getAmmo() { return ammo; }
	void addAmmo() {if (ammo<maxAmmo) ammo++;}
	bool needsAmmo() { return ammo<maxAmmo; }
	void install(Ship* s);
private:
	CountTimer *timer;
	int ammo;
	int maxAmmo;


};

#endif /* WEAPON_H_ */
