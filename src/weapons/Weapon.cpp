/*
 * Weapon.cpp
 *
 *  Created on: 31 Oct 2014
 *      Author: Adam Nasralla
 */

#include <clone/Clone.h>
#include <core/Vessel.h>
#include <hubs/BulletHub.h>
#include <hubs/BulletHubInt.h>
#include <hubs/CloneHub.h>
#include <hubs/ShipHub.h>
#include <osogine/game/Hub.h>
#include <osogine/utils/Transform.h>
#include <ship/Ship.h>
#include <weapons/Bullet.h>
#include <weapons/Weapon.h>
#include <glm/vec4.hpp>


Weapon::Weapon(Bullet* bullet, Game* game) : Module(game)
{
	this->bullet = bullet;
	bulletHub = 0;
	vessel = 0;
	isCloneWeapon = false;
}

Weapon::~Weapon()
{
	// TODO Auto-generated destructor stub
}

void Weapon::fire()
{
	bulletHub = vessel->getBulletHub();
	Bullet* newBullet = bullet->clone();
	newBullet->setRace(vessel->getRace());
	newBullet->fire(this);
}

void Weapon::install(Ship* ship1)
{
	ship = ship1;
	vessel = ship;
}

float Weapon::getSpeed()
{
	return bullet->getSpeed();
}

void Weapon::install(Clone* clone)
{
	vessel = clone;
	isCloneWeapon = true;
}

vec3 Weapon::getBulletPosition()
{
	Transform* vesselTrans = vessel->getTransform();
	vec3 localPos = getLocalPosition();
	if(!isCloneWeapon) localPos.y += 20;
	vec3 pos = vec3(vesselTrans->getLocalRotation() * vec4(localPos,1));

	return vessel->getLocalPosition() + pos;
}

vec3 Weapon::getBulletDirection()
{
	return vessel->getLocalDirection();
}

mat4 Weapon::getBulletRotation()
{
	return vessel->getLocalRotation();
}

vec3 Weapon::getVelocity()
{
	if (!isCloneWeapon) return vessel->getVelocity();
	else return vec3(0,0,0);
}
