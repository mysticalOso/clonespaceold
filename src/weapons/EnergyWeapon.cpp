#include <osogine/game/Hub.h>
#include <hubs/BulletHub.h>
#include <hubs/ShipHub.h>
#include <osogine/audio/SoundInterface.h>
#include <osogine/game/Game.h>
#include <ship/EnergyCore.h>
#include <ship/Ship.h>
#include <osogine/utils/CountTimer.h>
#include <weapons/Bullet.h>
#include <weapons/EnergyWeapon.h>


//TODO change to EnergyWeapon and MissileWeapon with prototype bullets when adding more weapons
EnergyWeapon::EnergyWeapon(int rate, float energyCost1, Bullet* bullet, Game* game) : Weapon(bullet, game)
{
	ship = 0;
	core = 0;
	timer = new CountTimer(rate);
	energyCost = energyCost1;
}

EnergyWeapon::~EnergyWeapon()
{
	//delete timer;
}

void EnergyWeapon::update()
{
	timer->update();

	if (core==0) core = vessel->getEnergyCore();

	if (core!=0)
	{

		if ( isOn && timer->isReady() && core->drain( energyCost ) )
		{
			game->event("lazerFired");
			if (isCloneWeapon) SoundInterface::fireLazer();
			fire();
			timer->reset();
		}

	}
}

void EnergyWeapon::install(Ship* ship1)
{
	Weapon::install(ship1);
	ship->addPrimaryWeapon(this);
	core = ship->getEnergyCore();

}

void EnergyWeapon::install(Clone* clone)
{
	Weapon::install(clone);
	vessel->addPrimaryWeapon(this);
	core = vessel->getEnergyCore();
}
