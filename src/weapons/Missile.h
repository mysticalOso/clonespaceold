/*
 * Missile.h
 *
 *  Created on: 3 Dec 2013
 *      Author: Adam Nasralla
 */

#ifndef MISSILE_H_
#define MISSILE_H_

#include <weapons/Bullet.h>

class EntityList;
class Space;
class Ship;
class GfxObject;
class GfxInterface;
class MissileLauncher;
class Thrust;

class Missile : public Bullet
{
public:
	Missile(float turning1, float thrust1, float resist1, float damage, float speed, float scale, Game* game );
	~Missile(void);

	void update();
	void render();
	Bullet* clone();

	float getDx(){return dx;}
	float getDy(){return dy;}

private:
	float dx,dy;
	float fx,fy;
	float resist;
	float fp;
	float dr;				//change in rotation
	float tx,ty,ta;			//targets
	int count;
	int tailSize;
	EntityList *tail;
	Ship *target;
	float pi;
	Thrust* thrust;

	void initGfx();
	void setTargetShip();
	void calcTarget();
	void updateRotation();
	void updateThrust();
	void updatePosition();
	void updateTail();
};

#endif /* MISSILE_H_ */
