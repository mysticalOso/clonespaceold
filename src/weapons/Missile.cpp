/*
 * Missile.cpp
 *
 *  Created on: 3 Dec 2013
 *      Author: Adam Nasralla
 */

#include <glm/vec2.hpp>
#include <ship/Ship.h>
#include <hubs/ShipHub.h>
#include <osogine/game/EntityList.h>
#include <osogine/game/Game.h>
#include <osogine/gfx/GfxBuilder.h>
#include <osogine/gfx/GfxObject.h>
#include <osogine/utils/Polygon.h>
#include <osogine/utils/Printer.h>
#include <osogine/utils/Targeter.h>
#include <ship/Thrust.h>
#include <weapons/Missile.h>
#include <weapons/Weapon.h>


//TODO find out why PI was wrong here??
//TODO add Missile builder

Missile::Missile(float turning1, float thrust1, float resist1,
		float speed, float scale, float damage, Game* game )
	: Bullet( speed, scale, damage, game )
{

	gfx = new GfxObject();

	tail = new EntityList();

	thrust = new Thrust(this);
	thrust->turnOn(true);
	thrust->setMaxPower(15);
	thrust->setPropel(5);
	thrust->setStartPower(thrust->getMaxPower());
	thrust->setWidth(2);
	thrust->setColour1(vec4(1,0.2,0.2,1));
	thrust->setColour2(vec4(0.5,0,0,0.5));
	ta = tx = ty = 0;

	fx = fy = 0;
	tailSize = 0;
	count = 0;

	dr = turning1;
	fp = thrust1;
	resist = resist1;

	target = 0;



	setFriction(resist1);


	//setTargetShip();
	initGfx();

}


Missile::~Missile(void)
{
	//tail->destroy();
	//delete tail;
}


void Missile::update()
{
	Entity::update();
	setTargetShip();

	if (target!=0)
	{
		calcTarget();
		updateRotation();
	}


	updateThrust();
	//updatePosition();
	//updateTail();
	count++;
}

//TODO remove sqrroot from missile
void Missile::setTargetShip()
{
	ShipHub* shipHub = weapon->getShipHub();
	target = (Ship*) shipHub->getClosestEnemy(this, race);
}

//calc interception point assuming fixed velocity for ship & fixed speed for missile (quadratic)
void Missile::calcTarget()
{
//	Entity* dummy = new Entity();
//	dummy->setPosition(getPosition());
//	vec2 t = Targeter::getTarget(dummy,this,target,0,0.8);
//	//delete dummy;
//	tx = t.x;
//	ty = t.y;
//
//	float x = getPosition().x;
//	float y = getPosition().y;
//	float dtx = tx-x;
//	float dty = ty-y;
//
//	float targetDist = glm::sqrt(dtx*dtx + dty*dty);
//	float speed = Entity::getSpeed();
//
//	float divisor = speed/targetDist;
//	dtx = dtx * divisor;
//	dty = dty * divisor;
//	tx = x + dtx*5 - dx*4;
//	ty = y + dty*5 - dy*4;

	float dx = getVelocity().x;
	float dy = getVelocity().y;
	float x = getPosition().x;
	float y = getPosition().y;
	float sdx = target->getVelocity().x;
	float sdy = target->getVelocity().y;
	float sx = target->getPosition().x - x;
	float sy = target->getPosition().y - y;
	float s2 = dx*dx+dy*dy;
	float a,b,c,d,t;


	c = sx*sx + sy*sy;
	b = 2*sx*sdx + 2*sy*sdy;
	a = sdx*sdx + sdy*sdy - s2;

	t = 50;

	d = b*b - 4*a*c;

	if (d<0)
	{
		t = 50; //missile travelling slower than ship assume it will take one sec to hit
	}
	else if (d==0)
	{
		t = -b / (2*a);
		if (t < 0) t = 50;
	}
	else if (d>0)
	{
		float t1,t2;
		t1 = (-b + glm::sqrt(d)) / (2*a);
		t2 = (-b - glm::sqrt(d)) / (2*a);

		//pick smallest positive time
		if (t1 < 0)
		{
			if (t2 < 0) t = 50;
			else t = t2;
		}
		else
		{
			if (t2 < 0) t = t1;
			else
			{
				if (t1 < t2) t = t1;
				else t = t2;
			}
		}
	}

	t = t * 0.8;
	tx = sx + t*sdx + x ;//- dx*10;
	ty = sy + t*sdy + y ;//- dy*10;



	float dtx = tx-x;
	float dty = ty-y;

	float targetDist = glm::sqrt(dtx*dtx + dty*dty);
	float speed = glm::sqrt(dx*dx + dy*dy);

		float divisor = speed/targetDist;
		dtx = dtx * divisor;
		dty = dty * divisor;
		tx = x + dtx*5 - dx*4;
		ty = y + dty*5 - dy*4;




}


void Missile::updateThrust()
{

	setForce( getDirection()*fp );
	//fx += cos(angle)*fp;
	//fy += sin(angle)*fp;

}

void Missile::updatePosition()
{


}

void Missile::updateRotation()
{
	float dx = tx-getPosition().x;
	float dy = ty-getPosition().y;
	//targetA = atan2 ( dy , dx );
	vec2 dir = vec2(dx,dy);

	if (dir != vec2(0,0))
	{
		float da = angBetween(dir);
		bool isClockwise = isClockwiseTo(dir);

		if (isClockwise)
		{
			if (da>dr) rotateZ(dr);
			else rotateZ(da);
		}
		else
		{
			if (da>dr) rotateZ(-dr);
			else rotateZ(-da);
		}
	}


}



Bullet* Missile::clone()
{

	Missile* clone = new Missile(dr, fp, resist, damage, speed, scale, game);
	clone->setGfx(getGfx());
	clone->setProgram(getProgram());
	clone->setOutline(outline->clone());
	return clone;

}

void Missile::render()
{
	Bullet::render();
	//gfx->print();
}


//TODO change missile tail to glm::vec2
void Missile::updateTail()
{
//	if (tailSize<10)
//	{
//		tail->add(new MissileTail());
//		tailSize++;
//	}
//	for (int i=tailSize-1; i>=0; i--)
//	{
//		MissileTail *t = (MissileTail *) tail->get(i);
//		if (i==0)
//		{
//			t->x = x;
//			t->y = y;
//		}
//		else
//		{
//			MissileTail *t2 = (MissileTail *) tail->get(i-1);
//			t->x = t2->x;
//			t->y = t2->y;
//		}
//	}

}

void Missile::initGfx()
{

//	setProgram("flat");
//
////	gfx->addVertex(-1,3,0);
////	gfx->addVertex(-1,-3,0);
////	gfx->addVertex(1,-3,0);
////	gfx->addVertex(1,3,0);
////	gfx->addVertex(0,4,0);
////
////	gfx->addIndex(0);
////	gfx->addIndex(1);
////	gfx->addIndex(2);
////	gfx->addIndex(2);
////	gfx->addIndex(3);
////	gfx->addIndex(0);
////	gfx->addIndex(0);
////	gfx->addIndex(3);
////	gfx->addIndex(4);
////
////	for (int i=0; i<5; i++)
////	{
////		gfx->addNormal(0,0,1);
////		gfx->addColour(0.8,0.8,0.8,1);
////	}
//
	setProgram("flat");

	Polygon* outline = new Polygon();
	outline->add(vec2(-1,3));
	outline->add(vec2(-1,-3));
	outline->add(vec2(1,-3));
	outline->add(vec2(1,3));
	outline->add(vec2(0,4));

	setOutline(outline);

	GfxBuilder* builder = game->getGfxBuilder();

	gfx = builder->buildTriMesh(outline->getOutline(),100,false,false);

	gfx->reColour(vec4(0.8,0.8,0.8,1),vec4(0.8,0.8,0.8,1));

//
////	gfx->hasTexture = false;
////	gfx->hasBump = false;
////	gfx->hasShading  = false;
////	gfx->drawTail = true;
////	gfx->tail = tail;
}
