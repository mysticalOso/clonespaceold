/*
 * Bullet.cpp
 *
 *  Created on: 31 Oct 2014
 *      Author: Adam Nasralla
 */

#include <weapons/Bullet.h>
#include <weapons/Weapon.h>
#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>
#include <osogine/game/Hub.h>
#include <osogine/game/Light.h>
#include <osogine/utils/Polygon.h>

Bullet::Bullet(float damage1, float speed1, float scale1, Game* game) : Entity(game)
{
	weapon = 0;
	race = 0;
	damage = damage1;
	speed = speed1;
	scale = scale1;
	setScale(scale1,scale1,scale1);
	lifetime = 200;
	hub = 0;
	light = 0;
}

Bullet::~Bullet()
{
}


void Bullet::update()
{
	Entity::update();
	lifetime--;
	if (light!=0)
	{
		light->setLightPosition( getLocalPosition() );
	}
	if (lifetime==0) detonate();

}

void Bullet::fire(Weapon* weapon)
{
	this->weapon = weapon;
	hub = weapon->getBulletHub();

	setRotation( weapon->getBulletRotation() );
	setPosition( weapon->getBulletPosition() );

	setVelocity( weapon->getVelocity() + weapon->getBulletDirection()*speed );
	hub->add(this);
}

Bullet* Bullet::clone()
{
	Bullet* clone = new Bullet(damage, speed, scale, game);
	clone->setGfx(getGfx());
	clone->setProgram(getProgram());
	clone->setOutline(outline->clone());
	return clone;
}

void Bullet::detonate()
{
	hub->remove(this);
	//delete this;
}


