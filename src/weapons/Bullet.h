/*
 * Bullet.h
 *
 *  Created on: 31 Oct 2014
 *      Author: Adam Nasralla
 */

#ifndef SRC_WEAPONS_BULLET_H_
#define SRC_WEAPONS_BULLET_H_

#include <osogine/game/Entity.h>

class Hub;
class Weapon;
class Race;
class Light;

class Bullet: public Entity
{
public:
	Bullet(float damage, float speed, float scale, Game* game);
	virtual ~Bullet();
	virtual void fire(Weapon* weapon);
	virtual void update();
	virtual Bullet* clone();
	void setRace(Race* race){ this->race = race; }
	Race* getRace(){ return race; }
	float getSpeed(){ return speed; }
	float getDamage(){ return damage; }
	void setLifetime(int lifetime){ this->lifetime = lifetime; }
	bool collideWith(Entity* e){ return intersectWith(e); }
	void setLight(Light* light){this->light = light; }
	Light* getLight(){ return light; }


protected:
	virtual void detonate();
	int lifetime;
	Hub* hub;
	Race* race;
	Weapon* weapon;
	Light* light;

	float damage;
	float speed;
	float scale;
};

#endif /* SRC_WEAPONS_BULLET_H_ */
