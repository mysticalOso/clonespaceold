/*
 * Weapon.h
 *
 *  Created on: 31 Oct 2014
 *      Author: Adam Nasralla
 */

#ifndef SRC_WEAPONS_WEAPON_H_
#define SRC_WEAPONS_WEAPON_H_

#include <ship/Module.h>

class Hub;
class Bullet;
class Vessel;
class Clone;

class Weapon: public Module
{
public:
	Weapon(Bullet* bullet, Game* game);
	virtual ~Weapon();

	virtual void fire();

	Bullet* getBullet(){return bullet;}

	float getSpeed();

	vec3 getBulletPosition();
	vec3 getBulletDirection();
	mat4 getBulletRotation();
	vec3 getVelocity();

	void setIsCloneWeapon(bool b){isCloneWeapon = b;}
	bool getIsCloneWeapon(){ return isCloneWeapon; }

	Hub* getBulletHub(){ return bulletHub; }

	virtual void install(Ship* ship1);
	virtual void install(Clone* clone);

protected:
	Hub* bulletHub;
	Bullet* bullet;
	Vessel* vessel;
	bool isCloneWeapon;
};

#endif /* SRC_WEAPONS_WEAPON_H_ */
