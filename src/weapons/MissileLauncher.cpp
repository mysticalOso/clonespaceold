/*
 * Weapon.cpp
 *
 *  Created on: 30 Jan 2014
 *      Author: Adam Nasralla
 */

#include <osogine/audio/SoundInterface.h>
#include <osogine/utils/CountTimer.h>
#include <ship/Ship.h>
#include <weapons/MissileLauncher.h>


//TODO decide how modules will be created

MissileLauncher::MissileLauncher(int rate, int ammo1,
		Bullet* bullet, Game* game) : Weapon(bullet, game)
{
	ship = 0;
	timer = new CountTimer(rate);
	ammo = maxAmmo = ammo1;
}

MissileLauncher::~MissileLauncher()
{
	//delete timer;
}


void MissileLauncher::update()
{
	timer->update();
	if ( isOn && timer->isReady() && ammo>0 )
	{
		fire();
		SoundInterface::fireMissile();
		timer->reset();
		ammo--;
	}
}

void MissileLauncher::install(Ship* s)
{
	Weapon::install(s);
	ship->addSecondaryWeapon(this);
}
