#ifndef ENERGYWEAPON_H_
#define ENERGYWEAPON_H_

#include <weapons/Weapon.h>

class CountTimer;
class EnergyCore;
class Bullet;

class EnergyWeapon : public Weapon
{
public:
	EnergyWeapon(int rate, float energyCost, Bullet* bullet, Game* game);
	~EnergyWeapon();
	void update();
	void install(Ship* ship);
	void install(Clone* clone);



private:
	CountTimer *timer;
	EnergyCore *core;
	float energyCost;


};

#endif /* ENERGYWEAPON_H_ */
