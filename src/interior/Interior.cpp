/*
 * Interior.cpp
 *
 *  Created on: 3 Feb 2015
 *      Author: mysticalOso
 */

#include <clone/Clone.h>
#include <hubs/BulletHubInt.h>
#include <hubs/CloneHub.h>
#include <hubs/ExplosionHub.h>
#include <hubs/LightHub.h>
#include <hubs/RoomHub.h>
#include <hubs/ShipHubInt.h>
#include <interior/Interior.h>
#include <interior/Room.h>
#include <osogine/game/EntityList.h>
#include <ship/Ship.h>

Interior::Interior(Ship* ship) : Entity(ship)
{
	this->ship = ship;
	ship->setInterior(this);
	renderOrder = 0;

	init();
}
//
//Interior::Interior(Entity* parent, bool test) : Entity(parent)
//{
//
//	ship = 0;
//	init();
//}

Interior::~Interior()
{
	//delete shipHubInt;
	//delete cloneHub;
	//delete roomHub;
}

void Interior::update()
{
	if (cloneHub->containsActivePlayer())
	{
		Entity::update();
	}
}

void Interior::update(int order)
{
	if (cloneHub->containsActivePlayer())
	{
		Entity::update(order);
	}
}

void Interior::render()
{
	if (cloneHub->containsActivePlayer() || getShip()->getName()=="station")
	{
		Entity::render();
	}
}

void Interior::render(int order)
{
	if (cloneHub->containsActivePlayer() || getShip()->getName()=="station")
	{
		Entity::render(order);
	}
}

void Interior::init()
{
	cloneHub = new CloneHub(this);
	roomHub = new RoomHub(this);
	shipHubInt = new ShipHubInt(this);
	bulletHubInt = new BulletHubInt(this);
	explosionHub = new ExplosionHub(this);
	lightHub = new LightHub(this);

	renderOrder = 1;

	cloneHub->init();
	roomHub->init();
	shipHubInt->init();
	lightHub->init();
	bulletHubInt->init();
}

Clone* Interior::getClone(string name)
{
	return cloneHub->getClone(name);
}
