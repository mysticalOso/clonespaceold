/*
 * Block.h
 *
 *  Created on: 4 Jun 2014
 *      Author: Adam Nasralla
 */

#ifndef WALLS_H_
#define WALLS_H_

#include <glm/vec2.hpp>
#include <vector>
#include <osogine/game/Entity.h>
#include <interior/WallGap.h>
#include <osogine/utils/Polygon.h>

using namespace std;

class ShipInterior;
class Room;
class Polygon;

class Walls : public Entity
{
public:
	Walls(Room* room);
	Walls(Entity* parent);

	void addExternal(float x1, float y1);

	~Walls();

	void addGap(float index1, float ratio1, float length1);

	WallGap getGap(int i){return gaps[i];}

	void init();

	void update();


	Polygon* getInternals(){ return internal; }
	Polygon* getExternals(){ return external; }


	EntityList* getSections(){return sections;}

	bool checkCollision(Polygon* polygon){ return false; }


	float getWidth(){return width;}

	int getNoPoints(){return external->size();}


	vec2 getInternal(int i){return internal->get(i);}
	vec2 getExternal(int i){return external->get(i);}

	bool collideWith(Entity* entity);

	void render();

	void setCollisionOutline(Polygon* p);

	void setWidth(float w){ width = w; }

private:

	float width;

	Polygon* external; //external
	Polygon* internal;	//internal
	EntityList* sections;
	vector<WallGap> gaps;
	Room* room;

	void initVariables();
	void initInternals();
	void initSections();
	void initGfx();
	void initGaps();

};

#endif /* WALLS_H_ */
