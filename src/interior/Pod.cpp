/*
 * Pod.cpp
 *
 *  Created on: 20 Feb 2015
 *      Author: Adam Nasralla
 */

#include <clone/Clone.h>
#include <glm/detail/type_int.hpp>
#include <glm/detail/type_vec.hpp>
#include <interior/Pod.h>
#include <osogine/game/EntityList.h>
#include <osogine/game/Game.h>
#include <osogine/game/IEntity.h>
#include <osogine/game/TextureEntity.h>
#include <osogine/gfx/GfxObject.h>
#include <osogine/gfx/ObjLoader/ObjLoader.h>
#include <osogine/utils/Dice.h>
#include <osogine/utils/Polygon.h>
#include <osogine/utils/Printer.h>
#include <osogine/utils/Utils.h>
#include <ship/Module.h>
#include <ui/UIEntity.h>

Pod::Pod(Entity* parent) : Entity(parent)
{
	isOn = false;
	isChanging = false;
	delayTime = 100;
	delayCount = 0;
	spinTime = 50;
	spinCount = spinTime;
	renderOrder = 2;
	flipped = false;
	cloneNear = false;
	initGfx();
	trailSize = 0;
	module = 0;
	ui = 0;
	setScale(0.02,0.02,0.02);
	setPosition(0,0,0.15);

	targeter = new Entity(this);
	targeter->setProgram("outline");

	targeted = false;

	initTargeter();

	//initTrail();
}

Pod::~Pod()
{

}

void Pod::update()
{
	if (targeted)
	{
		targeter->setIsVisible(true);
		updateTargeter();
	}
	else
	{
		targeter->setIsVisible(false);
	}
//	if (!cloneNear && !cloneVeryNear) turnOn();
//	if (cloneNear && !cloneVeryNear) turnOff();
	if (cloneVeryNear) turnOn();
	else turnOff();

	Entity::update();

		updateSpin();


	updateColour();
	cloneNear = false;
	cloneVeryNear = false;
	//updateTrail();
}

void Pod::turnOn()
{
	isOn = true;
}

void Pod::turnOff()
{

	isOn = false;
}

void Pod::updateDelay()
{
	delayCount++;
	if (delayCount==delayTime)
	{
		delayCount = 0;
		if (isOn) turnOff();
		else turnOn();
	}
}

void Pod::updateSpin()
{

	//if (isOn) Printer::print("Is on!\n");
	//Printer::print("Spin count = ",spinCount,"\n");
	if (isOn) spinCount++;
	else spinCount--;
	if (spinCount>spinTime) spinCount = spinTime;
	if (spinCount<0) spinCount = 0;

	float rot = 0;
	float ratio = (float)spinCount / (float)spinTime;


	rot = Utils::sinInterpolate(0,PI,ratio);

	if (!flipped) setRotation(0,rot,0);
	else setRotation(0,rot,PI);
}

void Pod::initGfx()
{
	setGfx(ObjLoader::getObject("pod")->clone());
	setProgram("pod");
	setDisableCamera(false);
	setScale(5,5,5);
	setAlpha(0.25);
	gfx->setDepthMask(false);

	for (uint i=0; i<gfx->getNoVertices(); i++)
	{
		colourDir.add(Dice::chance(2));
		colourRatio.add(Dice::roll(0.0,1.0));
	}
}

void Pod::checkClone(Clone* clone)
{

	if (clone->distTo(this) < 0.8)
	{
		cloneVeryNear = true;
		if (module!=0)
		{
			if ( module->getCode()==vec2(2,0) || module->getCode()==vec2(2,1) )
			{
				//Printer::print("NEARCOCKPIT");
				game->event("nearCockPit");
			}
		}
	}
	if (clone->distTo(this) < 2)
	{
		cloneNear = true;
	}
}

void Pod::use(Clone* clone)
{
	//if (module!=0) module->use(clone);
	checkClone(clone);
	if (cloneVeryNear) {}//Printer::print("\nPODUSE\n");
	if (cloneVeryNear && ui!=0)
	{
		ui->toggleActive(true);
	}
	if (cloneVeryNear && module!=0)
	{
		module->use(clone);
	}
}

void Pod::toggleTargeted(bool b)
{
	targeted = b;
}

void Pod::updateColour()
{
	for (uint i=0; i<gfx->getNoVertices(); i++)
	{
		float r = Dice::roll(0.00,0.03);
		vec4 c = gfx->getColour(i);

		bool dir = colourDir.get(i);
		float ratio = colourRatio.get(i);

		if (dir) ratio += r;
		else ratio -= r;
		if (ratio>1)
		{
			colourDir.set(i, !dir);
			ratio = 1;
		}
		if (ratio<0)
		{
			colourDir.set(i, !dir);
			ratio = 0;
		}

		float c1 = Utils::sinInterpolate(0,1,ratio);
		c.r = c1*0.25 + 0.25;
		c.g = c1*0.25 + 0.25;
		c.b = c1*0.25 + 0.25;

		colourRatio.set(i,ratio);

		gfx->setColour(i,c);

	}
}

vec3 Pod::getLocalPosition()
{
	vec3 p = Entity::getLocalPosition();
	p += ((Entity*)parent)->getLocalPosition();
	return p;
}

void Pod::updateTargeter()
{
	targeterCount++;
	if (targeterCount > 100)
	{
		targeterCount = 0;
	}
	float scale = Utils::sinInterpolate(1,2,(float)targeterCount/100.0f);
	targeter->setScale(scale);
}

void Pod::initTargeter()
{
	Polygon* outline = new Polygon();
	float size = 0.1;

	outline->add(vec2(-24,0) * size);
	outline->add(vec2(-12,20) * size);
	outline->add(vec2(12,20) * size);
	outline->add(vec2(24,0) * size);
	outline->add(vec2(12,-20) * size);
	outline->add(vec2(-12,-20) * size);

	targeter->setOutline(outline);
}
