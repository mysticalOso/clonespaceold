/*
 * Room.h
 *
 *  Created on: 28 Jun 2014
 *      Author: Adam Nasralla
 */

#ifndef ROOM_H_
#define ROOM_H_

#include <osogine/game/Entity.h>

class RoomHub;
class Walls;
class Door;
class Clone;
class TextureEntity;
class Pod;
class Module;

class Room: public Entity
{
public:
	Room(RoomHub* hub);
	~Room();
	void setWalls(Walls* walls1) { walls = walls1;  }
	Walls* getWalls(){ return walls; }
	EntityList* getDoors(){ return doors; }
	//void addFloor(std::string tileSize);

	void addDoor(Door* door);
	//void addLight(Light* lights);

	void check(Clone* clone);
	bool collideWith(Entity* entity);

	void setIsExternal(bool b);
	bool getIsExternal(){ return isExternal; }
	//bool checkLazerWithWalls(Lazer* lazer);

	void unlockDoors();
	void lockDoors();

	void use(Clone* clone);

	Module* getModule(){ return module; }
	void setModule(Module* module);
	Pod* getPod(){ return pod; }
	Pod* getPod2(){ return pod2; }

	void positionPod(float x, float y);

	RoomHub* getOwner(){ return ownerHub; }

	TextureEntity* getIcon(){ return icon; }

	vec3 getLightColour();



	//TODO remove Pod2
	void changePod(Pod* pod, Pod* pod2);

private:


	void initIcon();
	void checkCloneWithDoors(Clone* clone);
	//bool checkCloneWithWalls(Clone* clone);

	bool isExternal;
	Walls* walls;
	EntityList* doors;
	RoomHub* ownerHub;
	TextureEntity* icon;
	Pod* pod;
	Pod* pod2;
	Module* module;
	//EntityList* lights;

};

#endif /* ROOM_H_ */
