/*
 * Door.h
 *
 *  Created on: 29 Jun 2014
 *      Author: Adam Nasralla
 */

#ifndef DOOR_H_
#define DOOR_H_

#include <glm/vec2.hpp>
#include <osogine/game/Entity.h>

class Polygon;
class WallGap;
class Room;
class Interior;
class Clone;

class Door: public Entity
{
public:
	Door(WallGap g, Room* room);
	~Door();

	void checkClone(Clone* c);
	void lock(bool b){isLocked = b;}
	bool checkCollision(Polygon* polygon);

	Polygon* getLeftSection(){ return lSection; }
	Polygon* getRightSection(){ return rSection; }
	void update();

	void setOffset(float x1, float y1, float r1);
	void setPos(float x1, float y1);
	void setRotation(float r);

	bool getIsLocked(){ return isLocked; }
	void setIsLocked( bool b ) { isLocked = b; }
	bool getIsExternal(){ return isExternal; }
	void setIsExternal( bool b ) { isExternal = b; }


	void updateSections();

private:
	float length;

	vec2 inL;
	vec2 inR;
	vec2 exL;
	vec2 exR;

	vec2 inL2;
	vec2 inR2;
	vec2 exL2;
	vec2 exR2;

	vec2 inC;
	vec2 exC;

	vec2 centre;

	int openCount;
	int maxCount;

	bool isOpening;
	bool isLocked;
	bool isExternal;

	Polygon* lSection;
	Polygon* rSection;

	void updateGfx();
	void initGfx();


	bool collideWith(Entity* e);

};

#endif /* DOOR_H_ */
