/*
 * WallSection.h
 *
 *  Created on: 1 Jul 2014
 *      Author: Adam Nasralla
 */

#ifndef WALLSECTION_H_
#define WALLSECTION_H_

#include <glm/vec2.hpp>
#include <osogine/game/Entity.h>
#include <vector>

class Polygon;
class WallGap;

using namespace std;
using namespace glm;

class Walls;
class GfxObject;
class Entity;

class WallSection : public Entity
{
public:


	WallSection(WallGap start, WallGap finish, Walls* walls);
	WallSection(Walls* walls);

	void addToGfx(GfxObject* gfx);

	void updateVerts();

	bool collideWith(Entity* e);

	~WallSection(){}

private:

	vector<vec2> ins;
	vector<vec2> exs;

	bool isLoop;

	Walls* walls;

};



#endif /* WALLSECTION_H_ */
