/*
 * Pod.h
 *
 *  Created on: 20 Feb 2015
 *      Author: Adam Nasralla
 */

#ifndef SRC_INTERIOR_POD_H_
#define SRC_INTERIOR_POD_H_

#include <osogine/game/Entity.h>
#include <osogine/utils/List.h>

class Clone;
class UIEntity;
class Pod;
class Module;

class Pod : public Entity
{
public:
	Pod(Entity* parent);
	virtual ~Pod();

	void update();

	void turnOn();
	void turnOff();

	void checkClone(Clone* clone);

	void flip(){ flipped = true;  }

	void setCloneNear(bool b){ cloneNear = b; }

	virtual void use(Clone* clone);

	vec3 getLocalPosition();

	void toggleTargeted(bool b);


	Module* getModule(){ return module; }

	//TODO change pods to just use modules
	void setUI(UIEntity* ui){this->ui = ui;}

	UIEntity* getUI(){ return ui; }

	void setModule(Module* module){this->module = module; }

protected:
	bool isOn;
	int delayCount;
	int spinCount;
	int spinTime;
	int delayTime;
	bool targeted;
	bool flipped;
	int targeterCount;

	void updateDelay();
	void updateSpin();

	void initGfx();
	void updateColour();

	void updateTargeter();
	void initTargeter();
	bool isChanging;

	bool cloneNear;
	bool cloneVeryNear;
	List<float> colourRatio;
	List<bool> colourDir;
	int trailSize;

	Module* module;
	UIEntity* ui;

	Entity* targeter;
};

#endif /* SRC_INTERIOR_POD_H_ */
