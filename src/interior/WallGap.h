/*
 * WallGap.h
 *
 *  Created on: 1 Jul 2014
 *      Author: Adam Nasralla
 */

#ifndef WALLGAP_H_
#define WALLGAP_H_

#include <glm/vec2.hpp>

using namespace glm;

class Walls;

class WallGap
{
public:
	WallGap(float index1, float ratio1, float length1);
	void build(Walls* walls);
	~WallGap(){}

	int index;
	float ratio;
	float length;
	vec2 inL;
	vec2 inR;
	vec2 exL;
	vec2 exR;
};

#endif /* WALLGAP_H_ */
