/*
 * Room.cpp
 *
 *  Created on: 28 Jun 2014
 *      Author: Adam Nasralla
 */

#include <glm/vec2.hpp>
#include <hubs/RoomHub.h>
#include <interior/Door.h>
#include <interior/Pod.h>
#include <interior/Room.h>
#include <interior/Walls.h>
#include <osogine/game/EntityList.h>
#include <osogine/game/Game.h>
#include <osogine/game/IEntity.h>
#include <osogine/game/TextureEntity.h>
#include <osogine/gfx/GfxBuilder.h>
#include <osogine/gfx/GfxObject.h>
#include <ship/Module.h>



Room::Room(RoomHub* hub) : Entity(hub)
{
	walls = 0;
	module = 0;
	doors = new EntityList();
	ownerHub = hub;
	icon = new TextureEntity(this);
	pod = new Pod(this);

	pod2 = new Pod(this);
	pod2->flip();
	initIcon();
}

Room::~Room()
{
	//delete walls;

	/*
	//delete doors;	//doors must be destroyed in ShipInterior

	//delete lights;*/
}

void Room::initIcon()
{
	icon->setProgram("texture");
	icon->setTexture("shieldRoom");
	icon->setTextureAlpha(1);

	GfxBuilder* builder = game->getGfxBuilder();
	GfxObject* gfx = new GfxObject();
	gfx->setDepthEnabled(false);
	builder->addQuad(gfx, vec2(0,0), vec2(5.1,5.15), vec2(0,0), vec2(1,1) );
	icon->setGfx(gfx);

	icon->setRenderOrder(1);
	icon->setPosition(-2.53,-3.05,0);
}

void Room::changePod(Pod* newPod, Pod* newPod2)
{
	//delete pod;
	//delete pod2;
	pod = newPod;
	pod2 = newPod2;
	newPod2->flip();
}

//void Room::addLight(Light* light)
//{
//	lights->add(light);
//}




void Room::checkCloneWithDoors(Clone* clone)
{
	for(int i=0; i<doors->size(); i++)
	{
		Door* d = (Door*) doors->get(i);
		d->checkClone(clone);
	}
	pod->checkClone(clone);
	pod2->checkClone(clone);
}



/*
bool Room::checkCloneWithWalls(Clone* clone)
{
	return (walls->checkCollision(clone->getBoundary()) || doors->checkCollision(clone->getBoundary()));
}

bool Room::checkLazerWithWalls(Lazer* lazer)
{
	return walls->checkLazerCollision(lazer->getBoundary()) || doors->checkCollision(lazer->getBoundary());
}
*/






void Room::unlockDoors()
{
	/*
	for (int i = 0; i < doors->size(); i++)
	{
		Door* d = (Door*) doors->get(i);
		if (d->getIsLocked())
		{
			d->setIsLocked(false);
			d->setWasLocked(true);
		}
	}*/
}

void Room::lockDoors()
{
	/*
	for (int i = 0; i < doors->size(); i++)
	{
		Door* d = (Door*) doors->get(i);
		if (d->getWasLocked())
		{
			d->setIsLocked(true);
			d->setWasLocked(false);
		}
	}*/
}

void Room::addDoor(Door* door)
{
	doors->add(door);
}

void Room::check(Clone* clone)
{
	checkCloneWithDoors(clone);

}

bool Room::collideWith(Entity* entity)
{
	return walls->collideWith(entity); //|| doors->collideWith(entity);
}

void Room::setIsExternal(bool b)
{
	isExternal = b;
	if (b)
	{
		icon->setIsVisible(false);
		pod->setIsVisible(false);
		pod2->setIsVisible(false);
		removeChild(pod);
		removeChild(pod2);
		removeChild(icon);
	}

}

void Room::use(Clone* clone)
{
	pod->use(clone);
}

void Room::positionPod(float x, float y)
{
	pod->setPosition(x,y,0.15);
	pod2->setPosition(x,y,0.15);
	icon->setPosition(x-2.2,y-2.5,0);
}

void Room::setModule(Module* module)
{
	this->module = module;
	pod->setModule(module);
	icon->setTexture(module->getIconName());
}

vec3 Room::getLightColour()
{
	if (module!=0) return module->getColour();
	return vec3(0.5,0.5,0.5);
}
