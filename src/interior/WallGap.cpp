
#include <glm/vec2.hpp>
#include <interior/WallGap.h>
#include <interior/Walls.h>
#include <osogine/utils/Utils.h>



using namespace glm;

WallGap::WallGap(float index1, float ratio1, float length1)
{
	index = index1;
	ratio = ratio1;
	length = length1;
}

void WallGap::build(Walls* walls)
{
	vec2 prev = walls->getInternal(index);

	vec2 next = walls->getInternal((index+1) % walls->getInternals()->size());
	float width = walls->getWidth();
	vec2 centre = prev + (next-prev)*ratio;

	vec2 unit = Utils::getUnitDirection(prev,next);
	vec2 halfWidth = unit*length*0.5f;
	inL = centre - halfWidth;
	inR = centre + halfWidth;

	vec2 perp = Utils::getPerpendicular(unit);
	exL = inL + perp * width;
	exR = inR + perp * width;

}
