/*
 * Door.cpp
 *
 *  Created on: 29 Jun 2014
 *      Author: Adam Nasralla
 */

#include <clone/Clone.h>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <interior/Door.h>
#include <interior/Room.h>
#include <interior/WallGap.h>
#include <interior/Walls.h>
#include <osogine/gfx/GfxObject.h>
#include <osogine/utils/Polygon.h>
#include <osogine/utils/Transform.h>
#include <osogine/utils/Utils.h>
#include <vector>


using namespace std;

Door::Door(WallGap g, Room* room) : Entity(room)
{
	room->addDoor(this);
	length = g.length;
	isLocked = false;
	inL = g.inL;
	inR = g.inR;
	exR = g.exR;
	exL = g.exL;

	inC = inL2 = inR2 = inL + (inR-inL)*0.5f;
	exC = exL2 = exR2 = exL + (exR-exL)*0.5f;

	centre = (inL + inR + exR + exL) / 4.0f;

	isOpening = false;

	isExternal = false;

	openCount = 0;
	maxCount = 20;

	lSection = new Polygon();
	rSection = new Polygon();

	renderOrder = 2;

	setProgram("simple");

	initGfx();
	updateSections();
}

Door::~Door()
{
//delete gfx;
	//delete lSection;
	//delete rSection;
}



void Door::update()
{

	int oldOpenCount = openCount;

	if (isOpening) {if (openCount < maxCount) openCount++;}
	else if (openCount > 0) openCount--;

	if (oldOpenCount != openCount)
	{

		float ratio = Utils::sinInterpolate(1,0,(float)openCount/(float)maxCount);

		inL2 = inL + (inC - inL) * ratio;
		exL2 = exL + (exC - exL) * ratio;
		inR2 = inR + (inC - inR) * ratio;
		exR2 = exR + (exC - exR) * ratio;

		updateGfx();
		updateSections();
	}

	isOpening = false;
}


void Door::updateGfx()
{
	gfx->setVertex(2,vec3(exL2.x,exL2.y,0));
	gfx->setVertex(3,vec3(inL2.x,inL2.y,0));
	gfx->setVertex(4,vec3(inR2.x,inR2.y,0));
	gfx->setVertex(5,vec3(exR2.x,exR2.y,0));

}


void Door::initGfx()
{
	gfx = new GfxObject();

	gfx->addQuad(inL,exL,exL2,inL2,vec4(0.2,0.3,0.4,1));
	gfx->addQuad(inR2,exR2,exR,inR,vec4(0.2,0.3,0.4,1));
}



void Door::checkClone(Clone* c)
{
	vec2 pos = transform->applyTo(centre);
	float d = Utils::dist2( pos, vec2(c->getPosition()));
	if (d < 2 && !isLocked)
	{
		isOpening = true;
	}
}

bool Door::checkCollision(Polygon* polygon)
{
//
//	updateSections();
//
////	for (uint i=0; i<polygon.size(); i++)
////	{
////		//printf("Poly x = %f  y = %f\n",polygon[i].x,polygon[i].y);
////	}
////	//fflush(stdout);
////	//printf("polyFinish\n");
////	return false;
//	bool result = Utils::polyOverlap(polygon, Utils::transformVerts(lSection,gfx->getLocalTransform() ))
//	|| Utils::polyOverlap(polygon,Utils::transformVerts(rSection,gfx->getLocalTransform() ));
//	if (result)
//	{
//		result = true;
//	}
//	return result;
	return false;
}

void Door::updateSections()
{
	lSection->clear();
	rSection->clear();
	lSection->add(inL);
	lSection->add(exL);
	lSection->add(exL2);
	lSection->add(inL2);
	rSection->add(inR);
	rSection->add(exR);
	rSection->add(exR2);
	rSection->add(inR2);
}

bool Door::collideWith(Entity* e)
{
	return e->collideWith(lSection) || e->collideWith(rSection);
}
