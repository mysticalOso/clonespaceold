/*
 * Interior.h
 *
 *  Created on: 3 Feb 2015
 *      Author: mysticalOso
 */

#ifndef SRC_INTERIOR_INTERIOR_H_
#define SRC_INTERIOR_INTERIOR_H_

#include <osogine/game/Entity.h>

class Ship;
class Clone;
class Room;
class RoomHub;
class CloneHub;
class ShipHubInt;
class BulletHubInt;
class ExplosionHub;
class LightHub;

class Interior: public Entity {
public:
	//Interior(Entity* parent, bool test);
	Interior(Ship* ship);
	virtual ~Interior();

	Clone* getClone(string name);

	CloneHub* getCloneHub(){ return cloneHub; }
	RoomHub* getRoomHub(){ return roomHub; }
	ShipHubInt* getShipHubInt(){ return shipHubInt; }
	BulletHubInt* getBulletHubInt(){ return bulletHubInt; }
	ExplosionHub* getExplosionHub(){ return explosionHub; }
	LightHub* getLightHub(){ return lightHub; }

	void render();
	void render(int order);
	void update();
	void update(int order);
	Ship* getShip(){ return ship; }


private:

	void init();

	CloneHub* cloneHub;
	RoomHub* roomHub;
	ShipHubInt* shipHubInt;
	BulletHubInt* bulletHubInt;
	ExplosionHub* explosionHub;
	LightHub* lightHub;

	Ship* ship;
};

#endif /* SRC_INTERIOR_INTERIOR_H_ */
