/*
 * Block.cpp
 *
 *  Created on: 4 Jun 2014
 *      Author: Adam Nasralla
 */

#include <glm/vec2.hpp>
#include <interior/Room.h>
#include <interior/Walls.h>
#include <interior/WallSection.h>
#include <osogine/gfx/GfxObject.h>
#include <osogine/utils/Polygon.h>
#include <osogine/utils/Utils.h>



Walls::Walls(Room* room) : Entity(room)
{
	room->setWalls(this);
	this->room = room;
	initVariables();
}

Walls::Walls(Entity* parent) : Entity(parent)
{
	room = 0;
	initVariables();
}


Walls::~Walls()
{
	//delete gfx;
	//delete internal;
	//delete external;
	//delete sections;	//TODO add EntityList destroy
}



void Walls::initInternals()
{
	for (int i=0; i<external->size(); i++)
	{
		int j = (i+1);
		if (j==external->size()) j=0;
		int k = (i-1);
		if (k==-1) k = external->size()-1;

		vec2 dotBi = Utils::getAdjustedBisector(k,i,j,external);
		vec2 p = external->get(i) + width * dotBi;
		internal->add(p);
	}
}

void Walls::initSections()
{
	if (gaps.size()==0)
	{
		sections->add(new WallSection(this));
	}
	else
	{
		for (unsigned int i=0; i<gaps.size(); i++)
		{
			int j = (i+1) % gaps.size();
			sections->add(new WallSection( gaps[i], gaps[j], this) );
 		}
	}
}

void Walls::initGfx()
{
	gfx = new GfxObject();

	for (int i=0; i<sections->size(); i++)
	{
		WallSection* section = (WallSection*) sections->get(i);
		section->addToGfx(gfx);
	}

}

void Walls::init()
{
	initInternals();
	initGaps();
	initSections();
	initGfx();
}

void Walls::addGap(float index1, float ratio1, float length1)
{
	gaps.push_back(WallGap(index1,ratio1,length1));
}

void Walls::render()
{
	Entity::render();
}

bool Walls::collideWith(Entity* entity)
{
	return sections->intersectWith(entity);
}

void Walls::initVariables()
{
	width = 0.15;
	gfx = 0;
	internal = new Polygon();
	external = new Polygon();
	sections = new EntityList();
	renderOrder = 2;
	setProgram("simple");
	outline = new Polygon();
}

void Walls::initGaps()
{
	for (unsigned int i=0; i<gaps.size(); i++)
	{
		gaps[i].build(this);
	}
}


void Walls::addExternal(float x1, float y1)
{
	vec2 p = vec2(x1,y1);
	external->add(p);
}

void Walls::update()
{
	Entity::update();
}

void Walls::setCollisionOutline(Polygon* p)
{
	if (room!=0) room->setOutline(p);
}
