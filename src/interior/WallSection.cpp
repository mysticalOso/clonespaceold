/*
 * Room.cpp
 *
 *  Created on: 28 Jun 2014
 *      Author: Adam Nasralla
 */


#include <interior/WallGap.h>
#include <interior/Walls.h>
#include <interior/WallSection.h>
#include <osogine/gfx/GfxObject.h>
#include <osogine/utils/List.h>
#include <osogine/utils/Polygon.h>
#include <glm/vec2.hpp>
#include <osogine/utils/Printer.h>

using namespace glm;

WallSection::WallSection(WallGap start, WallGap finish, Walls* walls) : Entity(walls)
{
	ins.push_back(start.inR);
	exs.push_back(start.exR);

	renderOrder = 2;

	int i = start.index;

	do
	{
		i = (i + 1)%walls->getNoPoints();
		ins.push_back(walls->getInternal(i));
		exs.push_back(walls->getExternal(i));
	}
	while(i!=finish.index);

	ins.push_back(finish.inL);
	exs.push_back(finish.exL);

	isLoop = false;
	updateVerts();

	this->walls = walls;

	setProgram("outline");
}

WallSection::WallSection(Walls* walls) : Entity(walls)
{
	for (int i = 0; i < walls->getNoPoints(); i++)
	{
		ins.push_back(walls->getInternal(i));
		exs.push_back(walls->getExternal(i));
	}

	isLoop = true;
	updateVerts();
}

void WallSection::addToGfx(GfxObject* gfx)
{
	int offset = gfx->getNoVertices();

	for (unsigned int i=0; i<exs.size(); i++)
	{
		gfx->addVertex(exs[i].x,exs[i].y,0);
		gfx->addColour(0.1,0.1,0.1,1);
	}
	for (unsigned int i=0; i<ins.size(); i++)
	{
		gfx->addVertex(ins[i].x,ins[i].y,0);
		gfx->addColour(0.1,0.1,0.1,1);
	}

	int j,k,l,s;

	if (isLoop) s = exs.size();
	else s = exs.size()-1;

	for (int i=0; i<s; i++)
	{
		if (isLoop) j = (i + 1)%exs.size();
		else j = (i + 1);
		k = i + exs.size();
		l = j + exs.size();
		gfx->addTriangle(i+offset,l+offset,k+offset);
		gfx->addTriangle(i+offset,j+offset,l+offset);
	}
}


void WallSection::updateVerts()
{
	std::vector<vec2> verts;
	verts.insert(verts.begin(),exs.begin(),exs.end());
	std::vector<vec2> ins2 = std::vector<vec2>(ins);
	std::reverse(ins2.begin(), ins2.end());
	verts.insert(verts.end(),ins2.begin(),ins2.end());

	List<vec2>* list = new List<vec2>();
	list->setVector(verts);
	outline = new Polygon( list );

}

bool WallSection::collideWith(Entity* e)
{
	bool result = Entity::collideWith(e);
	if (result)
	{
		//pass up section outline with collision
		walls->setCollisionOutline(getOutline());
	}
	return result;
}
