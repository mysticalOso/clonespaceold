/*
 * AmmoBar.cpp
 *
 *  Created on: 15 Apr 2014
 *      Author: Adam Nasralla
 */

#include <osogine/gfx/GfxObject.h>
#include <ui/AmmoBar.h>


AmmoBar::AmmoBar(Entity* parent, float x1, float y1, int maxAmmo1, float width1) : Entity(parent)
{
	setPosition(x1,y1,0);
	ammo = maxAmmo = maxAmmo1;
	width = width1;
//	if (maxAmmo>1) spacing = width / (float)maxAmmo-1;
//	else spacing = 0;
	colour = vec3( 104.f / 256.f, 159.f / 256.f, 219.f / 256.f );
	setDisableCamera(true);
	spacing = 24;
	iconHeight = 16;
	iconWidth = 18;

	initGfx();
	updateGfx();
}

AmmoBar::~AmmoBar()
{

}

void AmmoBar::setAmmo(int a)
{
	ammo = a;
	updateGfx();
}


void AmmoBar::updateGfx()
{


	gfx->clearAll();

	for (int i = 0; i < ammo; i++)
	{
		float x1 = i*spacing;

		gfx->addVertex( x1, 0, 0 );
		gfx->addVertex( x1+iconWidth, 0, 0 );
		gfx->addVertex( x1+iconWidth*0.5, iconHeight, 0 );

		gfx->addColour(colour.r, colour.g, colour.b, 0.7);
		gfx->addColour(colour.r, colour.g, colour.b, 0.7);
		gfx->addColour(colour.r, colour.g, colour.b, 0.7);

		int a = i*3;
		int b = a+1;
		int c = a+2;
		gfx->addTriangle(a,b,c);
	}

}

void AmmoBar::initGfx()
{
	gfx = new GfxObject();
	setProgram("simple");
}
