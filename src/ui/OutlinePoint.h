/*
 * OutlinePoint.h
 *
 *  Created on: 27 Feb 2015
 *      Author: Adam Nasralla
 */

#ifndef SRC_UI_OUTLINEPOINT_H_
#define SRC_UI_OUTLINEPOINT_H_

#include <ui/Button.h>

class OutlineEditor;

class OutlinePoint: public Button
{
public:
	OutlinePoint(UIEntity* parent);
	~OutlinePoint();
	void setIsCursor(bool b){ isCursor = b; }
	void mouseMoved(float mx, float my);



private:
	bool isCursor;
	OutlineEditor* editor;

};

#endif /* SRC_UI_OUTLINEPOINT_H_ */
