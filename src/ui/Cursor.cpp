/*
 * Cursor.cpp
 *
 *  Created on: 12 Mar 2015
 *      Author: mysticalOso
 */

#include <ui/Cursor.h>



Cursor::Cursor(Entity* parent) : UIEntity(parent)
{
	setProgram("texture");
	setTexture("uiIcons",1024);
	setTextureAlpha(0.6);
	renderOrder = 9;
	initQuad(5,350,28,23);
	//initQuad(0,0,1000,1000);
	setController("cursor");
}

Cursor::~Cursor() {
	// TODO Auto-generated destructor stub
}

