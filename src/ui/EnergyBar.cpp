/*
 * EnergyBar.cpp
 *
 *  Created on: 14 Apr 2014
 *      Author: Adam Nasralla
 */

#include <osogine/gfx/GfxObject.h>
#include <ui/EnergyBar.h>


EnergyBar::EnergyBar(Entity* parent, float x1, float y1, float width1, float height1, int noSegments1) : TextureEntity(parent)
{
	setPosition(x1,y1,0);
	width = width1;
	height = height1;
	noSegments = noSegments1;
	segWidth = width / (float)(noSegments-1);
	setDisableCamera(true);
	ratio = 1;
	initGfx();
	updateGfx();
}

EnergyBar::~EnergyBar()
{
}


void EnergyBar::updateGfx()
{


	int halfSegs = (noSegments / 2)-1;
	int j = noSegments-1;

	gfx->clearAll();

	for (int i=0; i<noSegments; i++)
	{

		float x1 = i*segWidth;

		if (x1 > (ratio*width))
		{
			x1 = ratio*width;
		}


		gfx->addVertex(x1,0,0);
		gfx->addVertex(x1+12,height,0);

		vec3 c;

//		if (i < halfSegs)
//		{
//			c.r = 1;
//			c.g = (float)i / (float)halfSegs;
//			c.b = 0;
//		}
//		else
//		{
//			c.r = 1 - (float)(i-halfSegs) / (float)halfSegs;
//			c.g = 1;
//			c.b = 0;
//		}
		c.r = c.g = c.b = 0;

		if (i==0) {c.r = 0.8; c.g = 0;}
		if (i==1) {c.r = 0.8; c.g = 0.4;}
		if (i==2) {c.r = 0.76; c.g = 0.56;}
		if (i==3) {c.r = 0.5; c.g = 0.6;}
		if (i==4) {c.r = 0; c.g = 0.6;}

//		c.r = c.r * 0.8f;
//		c.g = c.g  * 0.6f;
//		c.b = 0.f;

		//printf("i = %d  red = %f  green = %f\n",i,c.r,c.g);
		fflush(stdout);
		gfx->addColour(c.r,c.g,c.b,0.7);
		gfx->addColour(c.r,c.g,c.b,0.7);

		if (x1 == ratio*width)
		{
			j=i;
			i=noSegments;
		}
	}

	for (int i=0; i<j; i++)
	{
		int a = i*2;
		int b = a+1;
		int c = a+2;
		int d = a+3;
		gfx->addTriangle(a,b,c);
		gfx->addTriangle(b,c,d);
	}

}

void EnergyBar::initGfx()
{
	gfx = new GfxObject();
	setProgram("simpleFlat");
}

void EnergyBar::setRatio(float r)
{
	ratio = r;
	updateGfx();
}
