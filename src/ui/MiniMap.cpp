/*
 * MiniMap.cpp
 *
 *  Created on: 16 Apr 2014
 *      Author: Adam Nasralla
 */

#include <hubs/AsteroidHub.h>
#include <hubs/RaceHub.h>
#include <hubs/ShipHub.h>
#include <osogine/game/EntityList.h>
#include <osogine/game/Game.h>
#include <osogine/game/Hub.h>
#include <osogine/gfx/GfxObject.h>
#include <osogine/utils/Utils.h>
#include <races/Sentient.h>
#include <ship/Ship.h>
#include <space/Space.h>
#include <ui/MiniMap.h>


MiniMap::MiniMap(Entity* parent, float x1, float y1, float size1, Space* space1) : Entity(parent)
{
	setPosition(x1,y1,0);
	size = size1;


	showEnemy = true;
	showHome = true;



	setDisableCamera(true);

	space = space1;
	//gfx=0;
	centre = vec2(x1+size1*0.5, y1+size1*0.5);
	init();

	//addTarget(vec2(0,0),vec3(1,0,0));
}

MiniMap::~MiniMap()
{

}

void MiniMap::update()
{

	ShipHub* shipHub = space->getShipHub();
	AsteroidHub* asteroidHub = space->getAsteroidHub();

	EntityList* asteroids = asteroidHub->getChildren();
	EntityList* ships = shipHub->getChildren();

	Ship* player = shipHub->getPlayerShip();



	gfx->clearAll();

	if (player!=0)
	{

		float d = (size/2.0)-10;
		float d2 = (d*d);

		for (int i=0; i<asteroids->size(); i++)
		{
			Entity* asteroid = (Entity*) asteroids->get(i);
			vec3 pos = asteroid->getPosition() - player->getPosition();
			pos = pos * 0.05f * (size/300.0f);
			if (Utils::dist2(vec2(pos),vec2(0,0)) < d2)
			{
				gfx->addQuad(pos.x-2,pos.y-2,4,4,vec4(0.6,0.6,0.6,1));
			}
		}

		for (int i=0; i<ships->size(); i++)
		{
			Ship* ship = (Ship*)ships->get(i);
			vec3 pos = ship->getPosition() - player->getPosition();
			//float a = ship->getAngle();
			pos = pos * 0.05f * (size/300.0f);

			if (Utils::dist2(vec2(pos),vec2(0,0)) < d2)
			{
				vec4 c;
				if (ship->isEnemyOf(player)) c = vec4(1,0,0,1);
				else c = vec4(0,0.8,0,1);
				gfx->addQuad(pos.x-2,pos.y-2,4,4,c);
				//gfx->addTriangle(pos.x,pos.y,4,a,c);
			}

//			if (ship->hasDockingBay() && !ship->isEnemyOf(player))
//			{
//				target = ship->getPos();
//			}
//			if (ship->hasDockingBay() && ship->isEnemyOf(player))
//			{
//				enemyTarget = ship->getPos();
//			}
		}

		updateCompass();
	}
}


void MiniMap::init()
{
	gfx = new GfxObject();
	setProgram("simple");
}

void MiniMap::addTarget(vec2 pos, vec3 colour)
{
	targetEntity.add(0);
	targets.add(pos);
	targetColours.add(colour);
}

void MiniMap::removeTarget(int index)
{
	if (targetColours.size() > index)
	{
		targetColours.erase(index);
		targets.erase(index);
		targetEntity.erase(index);
	}
}

void MiniMap::updateGfx()
{
}

void MiniMap::updateCompass()
{
	RaceHub* raceHub = (RaceHub*) game->getEntity("raceHub");
	float d = size + 2;
	vec2 playerPos = raceHub->getPlayerPos();



	//Printer::print("Player Pos = ",playerPos,"\n");

	for (int i=0; i<targets.size(); i++)
	{
		if (targetEntity.get(i)!=0)
		{
			vec2 t = vec2(targetEntity.get(i)->getLocalPosition());
			targets.set(0,t);
		}
		vec2 target = targets.get(i);
		vec3 colour = targetColours.get(i);
		float dist = Utils::dist2(playerPos,target);
		if (dist < (0.5*0.5)) game->event("cloneNearTarget");
		else
		{
			float ang = Utils::ang2(playerPos,target);
			vec2 arrowPos = Utils::getPointAwayFrom(vec2(0,0),ang,d) - vec2(getPosition());
			vec4 c = vec4(colour.r,colour.g,colour.b,0.7);
			gfx->addTriangle(arrowPos.x,arrowPos.y,15,0.7,ang,c);
		}
		if (dist < (800*800)) game->event("nearTarget");
		if (playerPos.y > 3500 && playerPos.x < 6000) game->event("nearAsteroids");
	}


//	float d = size*0.5 + 2;
//	Ship* player = space->getPlayer();
//
//	vec3 colour = vec3( 104.f / 256.f, 159.f / 256.f, 219.f / 256.f );
//	if (player!=0)
//	{
//		if (showHome)
//		{
//			float ang = Utils::ang2(player->getPos(),target);
//			vec2 arrowPos = Utils::getPointAwayFrom(vec2(0,0),ang,d);
//			vec4 c = vec4(colour.r,colour.g,colour.b,0.7);
//			gfx->addTriangle(arrowPos.x,arrowPos.y,10,0.4,ang,c);
//		}
//		if (showEnemy)
//		{
//			float ang = Utils::ang2(player->getPos(),enemyTarget);
//			vec2 arrowPos = Utils::getPointAwayFrom(vec2(0,0),ang,d);
//			vec4 c = vec4(1,0,0,0.7);
//			gfx->addTriangle(arrowPos.x,arrowPos.y,10,0.4,ang,c);
//		}
//	}

	//gfx->addTriangle(arrowPos.x,arrowPos.y,7,0.4,ang,c);


}

void MiniMap::addTarget(Entity* target, vec3 colour)
{
	targetEntity.add(target);
	targets.add(vec2(target->getPosition()));
	targetColours.add(colour);
}
