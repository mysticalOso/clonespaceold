/*
 * TriCoords.h
 *
 *  Created on: 17 Jun 2014
 *      Author: Adam Nasralla
 */

#ifndef TRICOORDS_H_
#define TRICOORDS_H_

class TriCoords
{
public:
	TriCoords(){col = 0; row = 0; isUp = true;}
	TriCoords(int col1, int row1, bool isUp1){col = col1; row = row1; isUp = isUp1;}
	~TriCoords(){}
    bool operator <( const TriCoords &rhs ) const
    {
       return ( row < rhs.row || (row == rhs.row && col < rhs.col) || (row == rhs.row && col == rhs.col && isUp > rhs.isUp) );
    }

    bool operator >( const TriCoords &rhs ) const
    {
       return ( row > rhs.row || (row == rhs.row && col > rhs.col) || (row == rhs.row && col == rhs.col && isUp < rhs.isUp) );
    }

    bool operator ==( const TriCoords &rhs ) const
    {
       return ( row == rhs.row && col == rhs.col && isUp == rhs.isUp );
    }
    void next()
	{
		if (isUp)
		{
			isUp = false;
		}
		else
		{
			isUp = true;
		    col++;
		}
    }
	bool isUp;
	int row;
	int col;
};

#endif /* TRICOORDS_H_ */
