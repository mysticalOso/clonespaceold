/*
 * CommLink.h
 *
 *  Created on: 12 Feb 2015
 *      Author: Adam Nasralla
 */

#ifndef SRC_UI_COMMLINK_H_
#define SRC_UI_COMMLINK_H_

#include <osogine/game/Entity.h>

class TextureEntity;
class TextEntity;
class TriGrid;
class SentientFace;
class Xaccy;

//TODO super class UIPanel

class CommLink: public Entity
{
public:
	CommLink(Entity* parent);
	~CommLink();

	void showMessage(string msg, float displayTime);
	void changeMessage(string msg);
	void turnOff();

	void toggleFace();

	void update();

private:

	void init();
	void initGrid();
	void initTextBox();
	void initBack();

	float displayTime;

	float fadeDelay;
	bool fadingIn;

	TriGrid* grid;
	TextEntity* textBox;
	TextureEntity* back;
	TextureEntity* face;
	TextureEntity* sentientFace;
	Xaccy* xaccy;


};

#endif /* SRC_UI_COMMLINK_H_ */
