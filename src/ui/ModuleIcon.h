/*
 * ModuleIcon.h
 *
 *  Created on: 27 Feb 2015
 *      Author: Adam Nasralla
 */

#ifndef SRC_UI_MODULEICON_H_
#define SRC_UI_MODULEICON_H_

#include <ui/Button.h>
#include <glm/vec2.hpp>

class Module;
class ModuleEditor;
class OutlineEditor;
class ShipDesigner;

class ModuleIcon: public Button
{
public:
	ModuleIcon(UIEntity* parent, vec2 code);
	~ModuleIcon();
	void update();
	void showEdge();
	void hideEdge();
	void setEdge(TextureEntity* edge){ this->edge = edge; }
	vec2 getCode(){ return code; }
	void mouseDown();
	void mouseMoved(float mx, float my);
	void setGlowColour(vec3 c){glowColour = (c+textureColour)*0.5f; startColour = textureColour; }

	void setName(string s){ name = s; }
	string getName(){ return name; }

private:

	void updateGlow();

	ShipDesigner* designer;
	ModuleEditor* moduleEditor;
	OutlineEditor* outlineEditor;
	float glowCount;
	TextureEntity* edge;
	vec2 code;
	vec3 glowColour;
	vec3 startColour;
	bool glowUp;

};

#endif /* SRC_UI_MODULEICON_H_ */
