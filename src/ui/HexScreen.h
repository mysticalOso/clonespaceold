/*
 * HexScreen.h
 *
 *  Created on: 6 Mar 2015
 *      Author: Adam Nasralla
 */

#ifndef SRC_UI_HEXSCREEN_H_
#define SRC_UI_HEXSCREEN_H_

#include <ui/UIEntity.h>

class TriGrid;

class HexScreen: public UIEntity
{
public:
	HexScreen(Entity* parent);
	~HexScreen();

	void show();
	void hide();
	void update();

private:
	void initGrid();
	TriGrid* grid;
	TextureEntity* face;
	float fadeDelay;
	bool fadingIn;
	TextureEntity* back;
};

#endif /* SRC_UI_HEXSCREEN_H_ */
