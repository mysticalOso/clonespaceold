/*
 * TriGrid.cpp
 *
 *  Created on: 13 Jun 2014
 *      Author: mysticalOso
 */

#include <core/Constants.h>
#include <osogine/game/EntityList.h>
#include <osogine/gfx/GfxObject.h>
#include <osogine/utils/Dice.h>
#include <osogine/utils/Printer.h>
#include <ui/TriGrid.h>
#include <ui/TriGridEdge.h>



using namespace std;

TriGrid::TriGrid(Entity* parent) : Entity(parent)
{
	gfx = 0;
	triHeight = 20;
	triWidth = 24;
	renderOrder = 1;
	tris = new EntityList();
	edges = new EntityList();
	start = TriCoords(9999999,9999999,false);
	end = TriCoords(9999999,9999999,false);
	setPosition(-WIDTH2,-HEIGHT2,0);
	tsWidth = WIDTH / triWidth;
	tsHeight = HEIGHT / triHeight;
	mouseDown = false;
	rightDown = false;
	count = 0;
	alpha = 0;
	mx = 0;
	my = 0;
}

TriGrid::~TriGrid()
{

}

void TriGrid::update()
{
	tris->update();
	tris->update(1);
	updateCursor();
	updateUI();
}

void TriGrid::initGfx()
{
	gfx = new GfxObject();
	setProgram("flat");
	gfx->setDepthEnabled(false);
	setDisableCamera(true);
	//icon = new Icon(148,183,vec2(0,0));
}

//TODO learn about enums

//type 0 = ship designer
//type 1 = hud
void TriGrid::init(int type)
{
	initGfx();
	initEdges(type);
	initTris();
	initNeighbours();

	for (uint i=0; i<gfx->getNoVertices(); i++)
	{
		gfx->addNormal(0,0,1);
	}
}

int TriGrid::addVertex(int row, int col, float x1, float y1, vec4 colour, bool isLead)
{
	pair<int,int> coords = make_pair(col,row);
	if (lookup.count(coords))
	{
		if (!isLead)
		{
			return lookup[coords];
		}
		else
		{
			if (leadLookup.count(coords))
			{
				return addNewVertex(coords,x1,y1,colour,isLead);
			}
			else
			{
				leadLookup[coords] = isLead;
				int index = lookup[coords];
				gfx->setColour(index,colour);
				return index;
			}
		}
	}
	else
	{
		return addNewVertex(coords,x1,y1,colour,isLead);
	}

}



int TriGrid::addNewVertex(pair<int, int> coords, float x1, float y1, vec4 colour, bool isLead)
{
	gfx->addVertex(x1,y1,0);
	gfx->addColour(colour.r,colour.g,colour.b,colour.a);
	int index = gfx->getNoVertices()-1;
	lookup[coords] = index;
	if (isLead)
	{
		leadLookup[coords] = isLead;
	}
	return index;
}

void TriGrid::addTriangle(Tri* tri)
{
	gfx->addIndex(tri->getVert(0));
	gfx->addIndex(tri->getVert(1));
	gfx->addIndex(tri->getVert(2));
}

void TriGrid::updateColour(int index, vec4 c)
{
	gfx->setColour(index,c);
}

void TriGrid::initTris()
{
	findStart();
	do
	{
		findNextEnd();
		for (TriCoords c=start; c<end; c.next())
		{
			Tri* t = new Tri(c,this);
			tris->add(t);
			triLookup[t->getCoords()] = t;
		}

	}while (findNextStart() || findNextRowStart());
//	for (int i=0; i<20; i++)
//		for (int j=0; j<20; j++)
//		{
//			Tri* t = new Tri(i,j,true,this);
//			tris->add(t);
//			triLookup[t->getCoords()] = t;
//
//			t = new Tri(i,j,false,this);
//			tris->add(t);
//			triLookup[t->getCoords()] = t;
//		}
}

void TriGrid::initNeighbours()
{
	for (int i = 0; i < tris->size(); i++)
	{
		Tri* t = (Tri*) tris->get(i);
		t->initNeighbours();
	}

}

void TriGrid::startGrow(int col, int row, bool g)
{
	int s = tris->size();
	int i = Dice::roll(0,s-1);
	Tri* t = (Tri*) tris->get(i);
//	Tri* t = getTriangle(TriCoords(col,row,true));

	if (t!=0) t->startGrow(g);
}


void TriGrid::initEdges(int type)
{

	TriGridEdge* edge;

	if (type==0)
	{

		edge = new TriGridEdge(1,42,0,5,true);
		edges->add(edge);
		edge = new TriGridEdge(0,53,0,5,false);
		edges->add(edge);
		edge = new TriGridEdge(1,30,5,17,true);
		edges->add(edge);
		edge = new TriGridEdge(0,63,5,29,false);
		edges->add(edge);

		edge = new TriGridEdge(1,7,5,17,true);
		edges->add(edge);
		edge = new TriGridEdge(0,18,5,17,false);
		edges->add(edge);
		edge = new TriGridEdge(1,6,5,17,true);
		edges->add(edge);
		edge = new TriGridEdge(0,-6,17,29,true);
		edges->add(edge);

		edge = new TriGridEdge(0,6,29,37,true);
		edges->add(edge);
		edge = new TriGridEdge(1,61,29,48,false);
		edges->add(edge);

		edge = new TriGridEdge(1,-19,37,53,true);
		edges->add(edge);
		edge = new TriGridEdge(1,39,48,53,false);
		edges->add(edge);

	}

	if (type==1)
	{

		edge = new TriGridEdge(0,-30,42,54,true);
		edges->add(edge);
		edge = new TriGridEdge(0,-13,42,51,false);
		edges->add(edge);
		edge = new TriGridEdge(0,33,51,54,false);
		edges->add(edge);

	}

	if (type==2)
	{

		edge = new TriGridEdge(1,23,1,7,true);
		edges->add(edge);
		edge = new TriGridEdge(0,56,1,7,false);
		edges->add(edge);
		edge = new TriGridEdge(0,17,7,13,true);
		edges->add(edge);
		edge = new TriGridEdge(1,56,7,13,false);
		edges->add(edge);

	}

	if (type==3)
	{
		edge = new TriGridEdge(1,0,0,70,true);
		edges->add(edge);
		edge = new TriGridEdge(1,120,0,70,false);
		edges->add(edge);
	}

	if (type==4)
	{
		edge = new TriGridEdge(1,30,0,4,true);
		edges->add(edge);
		edge = new TriGridEdge(1,39,0,2,false);
		edges->add(edge);
		edge = new TriGridEdge(0,37,2,4,false);
		edges->add(edge);
		edge = new TriGridEdge(0,41,0,2,true);
		edges->add(edge);
		edge = new TriGridEdge(1,41,2,4,true);
		edges->add(edge);
		edge = new TriGridEdge(0,50,0,26,false);
		edges->add(edge);
		edge = new TriGridEdge(1,14,4,30,true);
		edges->add(edge);
		edge = new TriGridEdge(0,80,26,54,false);
		edges->add(edge);

		vec2 coord = getCoordFromPos(408,600);
		edge = new TriGridEdge(0,coord.x,coord.y,coord.y+6,true);
		edges->add(edge);

		coord = getCoordFromPos(0,720);
		edge = new TriGridEdge(1,coord.x,coord.y,coord.y+16,true);
		edges->add(edge);

		coord = getCoordFromPos(1248,1040);
		edge = new TriGridEdge(0,coord.x,coord.y,coord.y+2,true);
		edges->add(edge);

		coord = getCoordFromPos(1920,1080-320);
		edge = new TriGridEdge(1,coord.x,coord.y,coord.y+16,false);
		edges->add(edge);
	}
	if (type==5)
	{
		edge = new TriGridEdge(1,0,0,23,true);
		edges->add(edge);
		edge = new TriGridEdge(0,-23,23,46,true);
		edges->add(edge);
		edge = new TriGridEdge(0,23,0,23,false);
		edges->add(edge);
		edge = new TriGridEdge(1,23,23,46,false);
		edges->add(edge);
	}
}

void TriGrid::findStart()
{
	start = TriCoords(999999,999999,false);
	for (int i=0; i<edges->size(); i++)
	{
		TriGridEdge* edge = (TriGridEdge*) edges->get(i);
		if (edge->isOpener())
		{
			TriCoords coords = edge->getStartCoords();
			if (coords<start) start=coords;
		}
	}
}

bool TriGrid::findNextRowStart()
{
	start.row++;
	start.col = 999999;
	bool found = false;
	for (int i=0; i<edges->size(); i++)
	{
		TriGridEdge* edge = (TriGridEdge*) edges->get(i);
		if (edge->isOpener() && edge->inRange(start.row))
		{
			TriCoords coords = edge->getCoords(start.row);
			if (coords<start || coords==start)
			{
				found = true;
				start=coords;
			}
		}
	}
	return found;
}

bool TriGrid::findNextStart()
{
	bool found = false;
	start.col = 999999;
	for (int i=0; i<edges->size(); i++)
	{
		TriGridEdge* edge = (TriGridEdge*) edges->get(i);
		if (edge->isOpener() && edge->inRange(start.row))
		{
			TriCoords coords = edge->getCoords(start.row);
			if ((coords<start || coords==start) && (coords>end || coords==end))
			{
				found = true;
				start=coords;
			}
		}
	}
	return found;
}

void TriGrid::findNextEnd()
{
	end = TriCoords(999999,999999,false);
	for (int i=0; i<edges->size(); i++)
	{
		TriGridEdge* edge = (TriGridEdge*) edges->get(i);
		if (!edge->isOpener() && edge->inRange(start.row))
		{
			TriCoords coords = edge->getCoords(start.row);
			if (coords<end && coords>start) end=coords;
		}
	}
}

vec2 TriGrid::findNearestCoord(float x1, float y1)
{
	x1 -= getX();
	y1 -= getY();
	vec2 coord = getCoordFromPos(x1,y1);
	coord.x = floor(coord.x + 0.5);
	coord.y = floor(coord.y + 0.5);
	return coord;
}

void TriGrid::updateMousePos(float mx1, float my1)
{
	mx = mx1;
	my = my1;
	//printf("Col = %f Row = %f\n",coord.x,coord.y);
	//fflush(stdout);
}

void TriGrid::updateCursor()
{
	vec2 c = findNearestCoord(mx,my);

	overTri(c.x,c.y,true);
	overTri(c.x-1,c.y,true);
	overTri(c.x-1,c.y,false);
	overTri(c.x,c.y-1,true);
	overTri(c.x,c.y-1,false);
	overTri(c.x-1,c.y-1,false);

	if (mouseDown)
	{
		lightTri(c.x,c.y,true,true);
		lightTri(c.x-1,c.y,true,true);
		lightTri(c.x-1,c.y,false,true);
		lightTri(c.x,c.y-1,true,true);
		lightTri(c.x,c.y-1,false,true);
		lightTri(c.x-1,c.y-1,false,true);
	}
	if (rightDown)
	{
		lightTri(c.x,c.y,true,false);
		lightTri(c.x-1,c.y,true,false);
		lightTri(c.x-1,c.y,false,false);
		lightTri(c.x,c.y-1,true,false);
		lightTri(c.x,c.y-1,false,false);
		lightTri(c.x-1,c.y-1,false,false);
	}
}




void TriGrid::lightTri(int x, int y, bool isUp, bool isLit)
{
	TriCoords coords = TriCoords(x,y,isUp);
	Tri* tri = triLookup[coords];
	if (tri!=0) tri->light(isLit);
}

void TriGrid::overTri(int x, int y, bool isUp)
{
	TriCoords coords = TriCoords(x,y,isUp);
	Tri* tri = triLookup[coords];
	if (tri!=0) tri->over(true);
}
vec2 TriGrid::getPosFromCoord(float col, float row)
{
	vec2 p1;
	p1.x = (col + row*0.5) * triWidth;
	p1.y = row * triHeight;
	return p1;
}

vec2 TriGrid::getCoordFromPos(float x1, float y1)
{
	vec2 coord;
	coord.y = y1 / triHeight;
	coord.x = (x1 / triWidth) - coord.y*0.5;
	return coord;

}

void TriGrid::updateUI()
{

}
