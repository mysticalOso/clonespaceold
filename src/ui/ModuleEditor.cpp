/*
 * ModuleEditor.cpp
 *
 *  Created on: 28 Feb 2015
 *      Author: Adam Nasralla
 */

#include <builders/UIBuilder.h>
#include <osogine/file/FileInterface.h>
#include <osogine/utils/Polygon.h>
#include <osogine/utils/Printer.h>
#include <ui/ModuleEditor.h>
#include <ui/ModuleIcon.h>
#include <ui/OutlineEditor.h>
#include <ui/ShipDesigner.h>


ModuleEditor::ModuleEditor(Entity* parent) : UIEntity(parent)
{
	shipDesigner = (ShipDesigner*) parent;
	outlineEditor = 0;
	addedModules = new EntityList();
	currentModule = 0;
	builder->buildModuleIcons(this);
	saveLoad(0,false);

	for (int i=0; i<addedModules->size(); i++)
	{
		ModuleIcon* icon = (ModuleIcon*) addedModules->get(i);
		icon->hideEdge();
	}
	for (int i=0; i<children->size(); i++)
	{
		ModuleIcon* icon = (ModuleIcon*) children->get(i);
		icon->hideEdge();
	}
}

ModuleEditor::~ModuleEditor()
{

}

void ModuleEditor::clicked(Button* icon)
{
	ModuleIcon* m = (ModuleIcon*) icon;
	currentModule = m;
	if (!addedModules->contains(currentModule))
	{
		ModuleIcon* newIcon = builder->buildModuleIcon(this,m->getCode());
		newIcon->showEdge();
	}
}

void ModuleEditor::mouseUp()
{
	UIEntity::mouseUp();
	if (currentModule!=0)
	{
		currentModule->snapPosToGrid();
		vec2 pos = vec2(currentModule->getPosition());
		outlineEditor = shipDesigner->getOutlineEditor();
		if (shouldAddModule())
		{
			if (!addedModules->contains(currentModule)) addedModules->add(currentModule);
		}
		else
		{
			if (addedModules->contains(currentModule)) addedModules->remove(currentModule);
			if (children->contains(currentModule)) removeChild(currentModule);
		}

		currentModule = 0;
	}
}

void ModuleEditor::saveLoad(int i, bool isSaving)
{
	vector<vec4> points;
	if (isSaving)
	{
		for (int i=0; i<addedModules->size(); i++)
		{
			ModuleIcon* icon = (ModuleIcon*) addedModules->get(i);
			vec2 code = icon->getCode();
			vec2 pos = vec2(icon->getPosition());
			points.push_back(vec4(code.x,code.y,pos.x,pos.y));
			//Printer::print("Saving Code",code,"\n");
			//Printer::print("Saving Pos",pos,"\n");
		}
		FileInterface::saveModules(i,points);
	}
	else
	{
		reset();
		points = FileInterface::loadModules(i);
		for (uint i=0; i<points.size(); i++)
		{
			vec4 point = points[i];
			ModuleIcon* icon = builder->buildModuleIcon(this, vec2(point.x, point.y));
			icon->setPosition(point.b, point.a, 0);
			icon->showEdge();
			//Printer::print("Loading ",point,"\n");
			addedModules->add(icon);
		}
	}
}

void ModuleEditor::reset()
{
	addedModules->clear();
	children->clear();
	currentModule = 0;
	builder->buildModuleIcons(this);
	for (int i=0; i<children->size(); i++)
	{
		ModuleIcon* icon = (ModuleIcon*) children->get(i);
		icon->showEdge();
	}
}

void ModuleEditor::mouseDown()
{
	UIEntity::mouseDown();
	bool mouseOverIcon = false;
	for (int i=0; i<children->size(); i++)
	{
		ModuleIcon* icon = (ModuleIcon*) children->get(i);
		if (icon->isMouseOver()) mouseOverIcon = true;
	}
	//if (mouseOverIcon) shipDesigner->setText("Photon Lazer Cannon");

}

bool ModuleEditor::shouldAddModule()
{
	bool result = true;
	if (checkTouchingShipOutline()) result = false;
	if (!isInsideShipOutline()) result = false;
	if (touchingAnotherModule()) result = false;
	if (illegalDouble()) result = false;
	return result;
}

bool ModuleEditor::checkTouchingShipOutline()
{
	return (currentModule->intersectWith(outlineEditor->getOutline()));
}

bool ModuleEditor::isInsideShipOutline()
{
	vec2 pos = vec2(currentModule->getPosition());
	return (outlineEditor->getOutline()->isInside(pos));
}

bool ModuleEditor::touchingAnotherModule()
{
	bool result = false;
	for (int i=0; i<children->size(); i++)
	{
		ModuleIcon* icon = (ModuleIcon*) children->get(i);
		if (icon!=currentModule)
		{
			if (icon->collideWith(currentModule))
			{
				result = true;
			}
		}
	}
	return result;
}

bool ModuleEditor::illegalDouble()
{
	bool result = false;
	vec2 code = currentModule->getCode();
	if (code.x==2 || code.x==4 || code.x==5 || code.x==6 || code.x==7)
	{
		for (int i=0; i<addedModules->size(); i++)
		{
			ModuleIcon* icon = (ModuleIcon*) addedModules->get(i);
			if (icon!=currentModule)
			{
				vec2 code2 = icon->getCode();
				if (code.x == code2.x) result = true;
			}
		}
	}
	return result;
}

bool ModuleEditor::hasNeeded()
{
	bool hasEngine = false;
	bool hasCore = false;
	bool hasSteering = false;
	for (int i=0; i<addedModules->size(); i++)
	{
		ModuleIcon* icon = (ModuleIcon*) addedModules->get(i);
		vec2 code = icon->getCode();
		if (code.x==2) hasSteering = true;
		if (code.x==3) hasEngine  = true;
		if (code.x==4) hasCore = true;
	}
	if (!(hasSteering && hasEngine && hasCore)) shipDesigner->setMessage("Thruster Energy Core and Cockpit needed!");
	return (hasSteering && hasEngine && hasCore);
}
