/*
 * CostPanel.h
 *
 *  Created on: 27 Feb 2015
 *      Author: Adam Nasralla
 */

#ifndef SRC_UI_COSTPANEL_H_
#define SRC_UI_COSTPANEL_H_

#include <ui/UIEntity.h>

class CostBar;

class CostPanel: public UIEntity
{
public:
	CostPanel(Entity* parent);
	~CostPanel();

	void update();

	void setTextureAlpha(float a);


	void randomCost();

	void initCosts();

	void setStock(EntityList* gems);

private:
	EntityList* stocks;
	EntityList* costs;
	bool first;
	int count;
};

#endif /* SRC_UI_COSTPANEL_H_ */
