/*
 * OutlineEditor.h
 *
 *  Created on: 27 Feb 2015
 *      Author: Adam Nasralla
 */

#ifndef SRC_UI_OUTLINEEDITOR_H_
#define SRC_UI_OUTLINEEDITOR_H_

#include <ui/UIEntity.h>

class OutlinePoint;
class ShipDesigner;
class ModuleEditor;

class OutlineEditor: public UIEntity
{
public:
	OutlineEditor(Entity* parent);
	~OutlineEditor();

	OutlinePoint* getPoint(int i){ return (OutlinePoint*) children->get(i); }
	void removePoint(OutlinePoint* p){ removeChild((IEntity*)p); }
	int noPoints(){ return children->size(); }
	vec2 getPointPos(int i);
	void update();

	void setTextureAlpha(float a);
	void mouseMoved(float mx, float my);

	void mouseDown();
	void mouseUp();
	void setOverPoint(bool b){ overPoint = b; }

	void saveLoad(int i, bool b);

	void reset();

	void toggleAction(){ symmetryOn = !symmetryOn ; }



private:
	void initGfx();
	void updateGfx();
	void updateMouseHex();

	void restoreOldOutline();

	void addNewPoint();
	void addNewMirrorPoint();

	void mirrorMouseMove();
	void mirrorMouseDown();

	bool mouseHexCollideWithPoints();
	bool mirrorHexCollideWithPoints();

	bool outlineCrossesIcons();

	OutlinePoint* mouseHex;
	OutlinePoint* mirrorHex;
	ModuleEditor* moduleEditor;
	ShipDesigner* designer;
	bool overPoint;
	bool overLine;
	bool mirrorOverLine;
	bool symmetryOn;
	Polygon* oldOutline;
	Polygon* boundary;
};

#endif /* SRC_UI_OUTLINEEDITOR_H_ */
