/*
 * CostBar.cpp
 *
 *  Created on: 27 Feb 2015
 *      Author: Adam Nasralla
 */


#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <osogine/gfx/GfxObject.h>
#include <osogine/utils/Dice.h>
#include <osogine/utils/Utils.h>
#include <ui/CostBar.h>

CostBar::CostBar(Entity* parent) : Entity(parent)
{
	maxCount = 50;

	count = 50;
	value = 0;
	setDisableCamera(true);
}

CostBar::~CostBar()
{

}



void CostBar::setValue(float newValue)
{

	value = newValue;
	oldR = r;
	oldL = l;
	newR = Utils::interpolate(bottomR,topR,value);
	newL = Utils::interpolate(bottomL,topL,value);
	count = 0;

}

void CostBar::update()
{

	if (count<maxCount)
	{
		count++;
		float ratio = (float)count / (float)maxCount;

		l = Utils::sinInterpolate(oldL,newL, ratio);
		r = Utils::sinInterpolate(oldR, newR, ratio);
	}

	updateGfx();

}

void CostBar::initPoints()
{
	topL = vec2(gfx->getVertex(0));
	topR = vec2(gfx->getVertex(1));
	bottomR= vec2(gfx->getVertex(2));
	bottomL = vec2(gfx->getVertex(3));
	l = bottomL;
	r = bottomR;
}

void CostBar::updateGfx()
{
	gfx->setVertex(0,vec3(l,0));
	gfx->setVertex(1,vec3(r,0));
}

void CostBar::setGfx(GfxObject* gfx)
{
	Entity::setGfx(gfx);
	initPoints();
	setAlpha(0);
}

void CostBar::addValue()
{
	value += 0.1;
	setValue(value);
}
