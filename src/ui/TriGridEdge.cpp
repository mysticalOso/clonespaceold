/*
 * TriGridEdge.cpp
 *
 *  Created on: 18 Jun 2014
 *      Author: Adam Nasralla
 */

#include <ui/TriCoords.h>
#include <ui/TriGridEdge.h>



TriGridEdge::TriGridEdge(int dir1, int colStart1, int rowStart1, int rowFinish1, bool opener1)
{
	dir = dir1;
	colStart  = colStart1;
	rowStart = rowStart1;
	rowFinish = rowFinish1;
	opener = opener1;
}


TriGridEdge::~TriGridEdge()
{
	// TODO Auto-generated destructor stub
}

TriCoords TriGridEdge::getStartCoords()
{
	return getCoords(rowStart);
}

TriCoords TriGridEdge::getCoords(int row)
{
	if (dir==0)
	{
		return TriCoords(colStart,row,true);
	}
	else
	{
		int col = (colStart-1) - (row-rowStart) ;
		return TriCoords(col,row,false);
	}
}
