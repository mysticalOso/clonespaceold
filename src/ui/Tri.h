/*
 * Tri.h
 *
 *  Created on: 17 Jun 2014
 *      Author: Adam Nasralla
 */

#ifndef TRI_H_
#define TRI_H_

#include <osogine/game/IEntity.h>

class TriGrid;
class TriCoords;
class EntityList;

class Tri : public IEntity
{
public:
	Tri(int row1, int col1, bool isUp, TriGrid* grid1);
	Tri(TriCoords t, TriGrid* grid1);
	int getVert(int i) {return verts[i];}
	void initNeighbours();
	void update();
	void update(int order);
	void light(bool b){ isLit = b;}
	void over(bool b){ isOver = b;}
	void startGrow(bool g);
	bool getIsOn(){return isOn;}

	TriCoords getCoords();
	~Tri();

private:
	int verts[3];
	int row;
	int col;
	bool isUp;
	EntityList* neighbours;
	float colour;
	bool colourDir;
	TriGrid* grid;
	bool isGrowing;
	bool shouldGrow;
	int firstGrow;
	bool shrink;
	int growCount;
	int firstFrame;
	bool isOn;
	bool isOver;
	bool isLit;
	void init();
	void grow();

};

#endif /* TRI_H_ */
