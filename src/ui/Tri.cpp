/*
 * Tri.cpp
 *
 *  Created on: 17 Jun 2014
 *      Author: Adam Nasralla
 */

#include <glm/vec4.hpp>
#include <glm/vec2.hpp>
#include <osogine/game/EntityList.h>
#include <osogine/utils/Dice.h>
#include <osogine/utils/Printer.h>
#include <ui/Tri.h>
#include <ui/TriCoords.h>
#include <ui/TriGrid.h>

Tri::Tri(int row1, int col1, bool isUp1, TriGrid* grid1)
{
	row = row1;
	col = col1;
	isUp = isUp1;
	grid = grid1;
	isOn = false;
	neighbours = new EntityList();
	growCount = 0;
	isGrowing = false;
	colour = 0;
	colourDir = false;
	shouldGrow = false;
	isLit = false;
	isOver = false;
	firstFrame = 0;
	shrink = false;
	firstGrow = true;
	init();
}

Tri::Tri(TriCoords t, TriGrid* grid1)
{
	row = t.row;
	col = t.col;
	isUp = t.isUp;
	grid = grid1;
	isOn = false;
	neighbours = new EntityList();
	growCount = 0;
	isGrowing = false;
	colour = 0;
	colourDir = false;
	shouldGrow = false;
	isLit = false;
	isOver = false;
	firstFrame = 0;
	shrink = false;
	firstGrow = true;
	init();
}

Tri::~Tri()
{

}

TriCoords Tri::getCoords()
{
	return TriCoords(col,row,isUp);
}

void Tri::initNeighbours()
{
	Tri* t;
	if (isUp)
	{
		t = grid->getTriangle(TriCoords(col-1,row,false));
		if (t!=0) neighbours->add(t);
		t = grid->getTriangle(TriCoords(col,row,false));
		if (t!=0) neighbours->add(t);
		t = grid->getTriangle(TriCoords(col,row-1,false));
		if (t!=0) neighbours->add(t);
	}
	else
	{
		t = grid->getTriangle(TriCoords(col,row,true));
		if (t!=0) neighbours->add(t);
		t = grid->getTriangle(TriCoords(col,row+1,true));
		if (t!=0) neighbours->add(t);
		t = grid->getTriangle(TriCoords(col+1,row,true));
		if (t!=0) neighbours->add(t);
	}

}

void Tri::init()
{
	vec2 p1,p2,p3;
	float width = grid->getTriWidth();
	float height = grid->getTriHeight();
	float xShift = row * width * 0.5;
	colour = 0;
	colourDir = false;
	firstFrame = 0;
	vec4 c = vec4(colour,colour,colour,0);
	if (isUp)
	{
		p1.x = col * width + xShift;
		p1.y = row * height;
		p2.x = (col+0.5) * width  + xShift;
		p2.y = (row+1) * height;
		p3.x = (col+1) * width  + xShift;
		p3.y = p1.y;
		verts[0] = grid->addVertex(col,row,p1.x,p1.y,c,true);
		verts[1] = grid->addVertex(col,row+1,p2.x,p2.y,c,false);
		verts[2] = grid->addVertex(col+1,row,p3.x,p3.y,c,false);
	}
	else
	{
		p1.x = (col+1) * width  + xShift;
		p1.y = row * height;
		p2.x = (col+0.5) * width  + xShift;
		p2.y = (row+1) * height;
		p3.x = (col+1.5) * width  + xShift;
		p3.y = p2.y;
		verts[0] = grid->addVertex(col+1,row,p1.x,p1.y,c,true);
		verts[1] = grid->addVertex(col,row+1,p2.x,p2.y,c,false);
		verts[2] = grid->addVertex(col+1,row+1,p3.x,p3.y,c,false);
	}

//	printf("%f %f\n",p1.x,p1.y);
//	printf("%f %f\n",p2.x,p2.y);
//	printf("%f %f\n\n",p3.x,p3.y);

	grid->addTriangle(this);

//	fflush(stdout);

}

void Tri::update()
{
	float alpha = 0.8;
	if (isOn)
	{
		//if (firstGrow==1) colour = Dice::roll(0.0,0.5);
		//if (firstGrow>0) firstGrow--;

		if (colourDir) colour += Dice::roll(-0.005,0.03);
		else colour -= Dice::roll(-0.005,0.03);


		if (colour <= 0.5 && !colourDir)
		{
			colour = 0.5;
			colourDir = !colourDir;
		}
		if (colour >= 1)
		{
			colour = 1;
			firstGrow = false;
			colourDir = !colourDir;
		}
	}
	else if (colour > 0)
	{
		colour -= Dice::roll(-0.005,0.03);
	}


	if (colour <= 0)
	{
		alpha = 0;
	}


	vec4 c;
	float c1 = 0;
	if (colour <= 0) c1 = 0;
	else c1 = colour * 0.3;


	c = vec4(c1,c1+0.05,c1+0.1,alpha);

	if (isOver)
	{
		c = vec4(c1+0.07,c1+0.12,c1+0.17,alpha);
	}

	if (isLit)
	{
		c = vec4(c1+0.3,c1+0.4,c1+0.5,alpha);
	}

	isOver = false;

	if (firstFrame<5)
	{
		c = vec4(0,0,0,0);
	}
	firstFrame++;

	c.a = c.a;
	grid->updateColour(verts[0],c);

	if (isGrowing) grow();
}

void Tri::startGrow(bool g)
{
	shouldGrow = true;
	if (!g)
	{
		colour = Dice::roll(0.0,0.7);
		colourDir = false;
	}
	else
	{
		colour = Dice::roll(0.0,0.4);
		colourDir = true;
		//firstGrow = 5;

	}

	isOn = g;
	shrink = !g;
	growCount = Dice::roll(1,1);
}

void Tri::update(int order)
{
	if (shouldGrow)
	{
		isGrowing = true;
		shouldGrow = false;
	}
}

void Tri::grow()
{
	growCount--;
	if (growCount==0)
	{
		growCount = Dice::roll(1,1);
		EntityList* growables = new EntityList();
		for (int i = 0; i < neighbours->size(); i++)
		{
			Tri* t = (Tri*) neighbours->get(i);
			if (t->getIsOn()==shrink) growables->add(t);
		}
		int s = growables->size();
		if (s>=1)
		{
			int i = Dice::roll(0,s-1);
			Tri* t = (Tri*) growables->get(i);
			t->startGrow(!shrink);
		}
		if (s<=1) isGrowing = false;
	}
}


