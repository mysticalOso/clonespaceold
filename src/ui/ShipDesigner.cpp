/*
 * ShipDesigner.cpp
 *
 *  Created on: 26 Feb 2015
 *      Author: Adam Nasralla
 */

#include <builders/ShipBuilder.h>
#include <builders/UIBuilder.h>
#include <core/CameraCS.h>
#include <glm/vec2.hpp>
#include <osogine/utils/Printer.h>
#include <ui/ShipDesigner.h>
#include <ui/TriGrid.h>
#include <core/Constants.h>
#include <hubs/RaceHub.h>
#include <hubs/UIHub.h>
#include <osogine/game/Game.h>
#include <osogine/game/TextEntity.h>
#include <osogine/input/Controller.h>
#include <osogine/utils/Polygon.h>
#include <races/Sentient.h>
#include <ship/Ship.h>
#include <ship/ShipGrower.h>
#include <ui/Button.h>
#include <ui/CostPanel.h>
#include <ui/ModuleEditor.h>
#include <ui/ModuleIcon.h>
#include <ui/OutlineEditor.h>

ShipDesigner::ShipDesigner(Entity* parent) : UIEntity(parent)
{
	hub = (UIHub*) parent;
	grid = 0;
	quitB = 0;
	initGfx();
	initGrid();
	setRenderOrder(7);
	initButtons();
	initEditors();
	startCount = 0;
	textBox = new TextEntity(this,400);
	textBox->setAlphaModifier(0.5);
	textBox->setText("Quantum Ship Designer");
	textBox->setPosition(-WIDTH2 + 27, HEIGHT2 - 97,0);
	//textBox->setRenderOrder(2);

	buildEnabled = true;

	messageBox = new TextEntity(this,400);
	messageBox->setAlphaModifier(0.5);
	messageBox->setText("");
	messageBox->setPosition(-WIDTH2 + 27, HEIGHT2 - 317,0);

	shipBuilder = (ShipBuilder*) game->getBuilder("ship");
}

ShipDesigner::~ShipDesigner()
{

}

void ShipDesigner::mouseDown()
{
	UIEntity::mouseDown();

}

void ShipDesigner::mouseUp()
{
	UIEntity::mouseUp();
}

void ShipDesigner::mouseMoved(float mx1, float my1)
{
	UIEntity::mouseMoved(mx1,my1);
	vec2 mp = vec2(mx,my);
	//Printer::print("MOUSE MOVED ",mp,"\n");
}

void ShipDesigner::initGrid()
{
	grid = new TriGrid(this);
	grid->init(4);
}

void ShipDesigner::initGfx()
{
	setTexture("ui", 2048);
	initQuad(0,0,WIDTH,HEIGHT);
}

void ShipDesigner::initButtons()
{
	quitB = builder->buildButton(this, "exit");
	buildB = builder->buildButton(this, "build");
//	buildB->setPosition(0,0,0);
//	buildB->setPrintOutline(true);
}

//TODO change to activate deactivate
void ShipDesigner::toggleActive(bool isActive)
{
	UIEntity::toggleActive(isActive);
	if (isActive)
	{
		game->event("shipDesignerActive");
		CameraCS* camera = (CameraCS*) game->getCamera();
		camera->setTargetPosition(-100,-100,2110, 1, 3);

		RaceHub* raceHub = (RaceHub*) game->getEntity("raceHub");
		Sentient* sentient = (Sentient*) raceHub->getRace("sentient");
		sentient->toggleCloneCtrl(false);

		EntityList* gems = hub->getShipGems();
		//costPanel->setStock(gems);

		startCount = 1;

	}
	else
	{
		//CameraCS* camera = (CameraCS*) game->getCamera();
		//camera->clearFixedTarget();
		RaceHub* raceHub = (RaceHub*) game->getEntity("raceHub");
		Sentient* sentient = (Sentient*) raceHub->getRace("sentient");
		sentient->toggleCloneCtrl(true);
		removeController("ui");
	}
}

void ShipDesigner::setText(string str)
{
	textBox->setText(str);
	costPanel->randomCost();
}

void ShipDesigner::setMessage(string s)
{
	messageBox->setText(s);
}

void ShipDesigner::initEditors()
{
	outlineEditor = new OutlineEditor(this);
	moduleEditor = new ModuleEditor(this);
	costPanel = new CostPanel(this);

}

void ShipDesigner::clicked(Button* button)
{
	if (button==buildB && buildEnabled && moduleEditor->hasNeeded())
	{

		toggleActive(false);
		grid->startGrow(0,0,false);

		fadeOut();
		textBox->fadeOut();
		Polygon* outline = outlineEditor->getOutline();
		Polygon* p = outline->clone();
		p->moveOutline(0,-20);
		p->scaleOutline(2.0 / 47.0f);

		EntityList* modules = moduleEditor->getAddedModules();

		Ship* ship = shipBuilder->buildShip(p, modules);

		ship->setMaxHealth(50);

		ShipGrower* grower = new ShipGrower(this);
		grower->grow(ship);


		for (int i=0; i<modules->size(); i++)
		{
			ModuleIcon* icon = (ModuleIcon*) modules->get(i);
			//Printer::print("Module code = ", icon->getCode(), "\n");
			//Printer::print("Module position = ", icon->getPosition(), "\n");
		}

	}
}

void ShipDesigner::update()
{
	UIEntity::update();
	if (startCount > 0)
	{
		startCount++;
		if (startCount==50)
		{
			grid->startGrow(0,0,true);
		}
		if (startCount==180)
		{
			setController("ui");
			fadeIn();
			textBox->fadeIn();
			costPanel->initCosts();
		}
	}
}

void ShipDesigner::saveLoad(int i, bool b)
{
	moduleEditor->saveLoad(i,b);
	outlineEditor->saveLoad(i,b);
}

void ShipDesigner::toggleAction()
{
	outlineEditor->toggleAction();
}
