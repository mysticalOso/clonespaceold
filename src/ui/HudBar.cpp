/*
 * HudBar.cpp
 *
 *  Created on: 15 Apr 2014
 *      Author: Adam Nasralla
 */

#include <osogine/game/TextEntity.h>
#include <ui/AmmoBar.h>
#include <ui/EnergyBar.h>
#include <ui/HudBar.h>
#include <ui/StorageBar.h>


HudBar::HudBar(Entity* parent, std::string label1, float x1, float y1, float width1, int type1) : Entity(parent)
{
	label = label1;
	x = x1;
	y = y1;
	width = width1;
	rightAligned = false;
	backGfx = 0;
	ratio = 1;
	type = type1;
	ammo = maxAmmo = 5;
	locked = false;
	setDisableCamera(true);
	init();

}

HudBar::~HudBar()
{
}

void HudBar::setRatio(float r)
{
	if (r!=ratio && !locked)
	{
		ratio = r;
		energyBar->setRatio(r);
	}
}

void HudBar::setAmmo(int a)
{
	if (a!=ammo && !locked)
	{
		ammo = a;
		ammoBar->setAmmo(a);
	}
}

void HudBar::updateGems(EntityList* gems)
{
	if (gems!=0 && !locked)
	{
		storageBar->updateGems(gems);
	}
}

void HudBar::init()
{
	int stringLength = label.length()*16;
	//box1 = new LineBox(x,y,stringLength + 22,40,5,false);
	//box2 = new LineBox(x+stringLength + 22, y, width, 40, 5,false);
	//text = new TextEntity(this, stringLength);
	//text->setText(label);
	if (type==0) energyBar = new EnergyBar(this, x+stringLength + 34, y+11, 120, 20, 6);

	else if (type==1) ammoBar = new AmmoBar(this, x+stringLength + 40, y+13, 5, width-22);
	else if (type==2) storageBar = new StorageBar(this, x+stringLength + 34, y+11, width-22, 18, 20);

//	backGfx = new GfxObject();
//	backGfx->hasShading = false;
//	backGfx->cameraDisabled = true;
//	backGfx->addQuad(0,0,stringLength + 22 + width,40,vec4(0,0,0,1));
//	backGfx->setPosition(x,y);
}


void HudBar::setText(std::string s)
{
	text->setText(s);
}

void HudBar::setPos(float x1, float y1)
{
	x = x1;
	y = y1;
}

void HudBar::setAlpha(float a)
{
	Entity::setAlpha(a);
	if (a!=1) locked = true;
	else locked = false;
	children->setAlpha(a*0.7);
}
