/*
 * Cursor.h
 *
 *  Created on: 12 Mar 2015
 *      Author: mysticalOso
 */

#ifndef SRC_UI_CURSOR_H_
#define SRC_UI_CURSOR_H_

#include <ui/UIEntity.h>

class Cursor: public UIEntity {
public:
	Cursor(Entity* parent);
	virtual ~Cursor();

};

#endif /* SRC_UI_CURSOR_H_ */
