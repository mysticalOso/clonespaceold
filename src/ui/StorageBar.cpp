/*
 * StorageBar.cpp
 *
 *  Created on: 16 Apr 2014
 *      Author: Adam Nasralla
 */

#include <osogine/game/EntityList.h>
#include <osogine/gfx/GfxObject.h>
#include <space/Gem.h>
#include <ui/StorageBar.h>

StorageBar::StorageBar(Entity* parent, float x1, float y1, float width1, float height1,
		int capacity1) : Entity(parent)
{
	setPosition(x1,y1,0);
	width = width1;
	height = height1;
	capacity = capacity1;
	gemSize = 5;
	spacing = 2;
	gems = new EntityList();
	noGems = 0;
	setDisableCamera(true);
	initGfx();
	updateGfx();
}

StorageBar::~StorageBar()
{
}

void StorageBar::updateGems(EntityList* gems1)
{
	if (gems1!=0)
	{
		if (gems != gems1 || noGems != gems1->size())
		{
			gems = gems1;
			updateGfx();
		}
	}
}


void StorageBar::updateGfx()
{
	gfx->clearAll();

	if (gems!=0)
	{
		noGems = gems->size();



		int xi = 0;
		int yi = 0;
		//float x1,y1;

		for (int i=0; i<noGems; i++)
		{
			Gem* gem = (Gem*) gems->get(i);
			vec3 c = gem->getBarRGB();

			int a = i%3;
			if( a == 2 || a == 0) xi++;
			if ( a == 0 ) yi = 0;
			if ( a == 1 ) yi = 2;
			if ( a == 2 ) yi = 1;

			float x1 = xi * (gemSize + spacing );
			float y1 = yi * (gemSize + spacing );

			gfx->addVertex(x1-gemSize,y1,0);
			gfx->addVertex(x1,y1+gemSize,0);
			gfx->addVertex(x1+gemSize,y1,0);
			gfx->addVertex(x1,y1-gemSize,0);

			gfx->addColour(c.r,c.g,c.b,0.7);
			gfx->addColour(c.r,c.g,c.b,0.7);
			gfx->addColour(c.r,c.g,c.b,0.7);
			gfx->addColour(c.r,c.g,c.b,0.7);
		}

		for (int i=0; i<noGems; i++)
		{
			int a = i*4;
			int b = a+1;
			int c = a+2;
			int d = a+3;
			gfx->addTriangle(a,b,c);
			gfx->addTriangle(a,c,d);
		}

	}

}

void StorageBar::initGfx()
{
	gfx = new GfxObject();
	setProgram("simple");
}
