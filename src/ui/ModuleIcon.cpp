/*
 * ModuleIcon.cpp
 *
 *  Created on: 27 Feb 2015
 *      Author: Adam Nasralla
 */

#include <osogine/utils/Dice.h>
#include <osogine/utils/Printer.h>
#include <osogine/utils/Utils.h>
#include <ui/ModuleEditor.h>
#include <ui/ModuleIcon.h>
#include <ui/ShipDesigner.h>



ModuleIcon::ModuleIcon(UIEntity* parent, vec2 code) : Button(parent, "module")
{
	moduleEditor = (ModuleEditor*) parent;
	outlineEditor = moduleEditor->getOutlineEditor();
	designer = moduleEditor->getShipDesigner();
	this->code = code;
	edge = 0;
	draggable = true;
	glowColour = vec3(0,0,0);
	glowUp = true;
	glowCount = 0;
}

ModuleIcon::~ModuleIcon()
{
	// TODO Auto-generated destructor stub
}

void ModuleIcon::update()
{
	Button::update();
	if (glowColour!=vec3(0,0,0))
	{
		updateGlow();
	}
}

void ModuleIcon::mouseDown()
{
	Button::mouseDown();
//	if (mouseIsOver)
//	{
//		Printer::print("Code = ",code,"\n");
//	}
}

void ModuleIcon::showEdge()
{
	edge->setTextureAlpha(1);
}

void ModuleIcon::hideEdge()
{
	edge->setTextureAlpha(0);
}

void ModuleIcon::mouseMoved(float mx, float my)
{
	Button::mouseMoved(mx,my);
	if (mouseIsOver)
	{
		designer->setMessage( getName() );
	}
}

void ModuleIcon::updateGlow()
{
	if (glowUp)
	{
		glowCount+=Dice::roll(0.01,0.03);
		if (glowCount>=1) {glowCount=1; glowUp=!glowUp;}
	}
	else
	{
		glowCount-=Dice::roll(0.01,0.03);
		if (glowCount<=0) {glowCount=0; glowUp=!glowUp;}
	}

	textureColour = Utils::sinInterpolate(startColour,glowColour,glowCount);
}
