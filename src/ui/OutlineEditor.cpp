/*
 * OutlineEditor.cpp
 *
 *  Created on: 27 Feb 2015
 *      Author: Adam Nasralla
 */

#include <builders/UIBuilder.h>
#include <osogine/gfx/GfxObject.h>
#include <ui/OutlineEditor.h>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <osogine/file/FileInterface.h>
#include <osogine/utils/Polygon.h>
#include <osogine/utils/Printer.h>
#include <osogine/utils/Transform.h>
#include <ui/ModuleEditor.h>
#include <ui/ModuleIcon.h>
#include <ui/OutlinePoint.h>
#include <ui/ShipDesigner.h>

OutlineEditor::OutlineEditor(Entity* parent) : UIEntity(parent)
{
	designer = (ShipDesigner*) parent;
	moduleEditor = designer->getModuleEditor();
	mouseHex = 0;
	mirrorHex = 0;
	boundary = new Polygon();
	boundary->add(-279,483);
	boundary->add(278,483);
	boundary->add(558,19);
	boundary->add(279,-442);
	boundary->add(-274,-439);
	boundary->add(-558,20);
	reset();
	//builder->buildStartingOutline(this);
	saveLoad(0,false);
	initGfx();
}

OutlineEditor::~OutlineEditor()
{
	// TODO Auto-generated destructor stub
}

void OutlineEditor::update()
{
	UIEntity::update();
	if (!symmetryOn) mirrorHex->setTextureAlpha(0);
}

void OutlineEditor::initGfx()
{
	gfx = new GfxObject();
	setProgram("lines");
	updateGfx();
	setAlpha(0);
}

void OutlineEditor::mouseMoved(float mx, float my)
{
	overPoint = false;
	UIEntity::mouseMoved(mx,my);
	if (symmetryOn) mirrorMouseMove();
	updateMouseHex();

	updateGfx();
}

void OutlineEditor::updateGfx()
{
	gfx->clearAll();
	outline->clear();

	for (int i=2; i<noPoints(); i++)
	{
		gfx->addVertex( getPoint(i)->getPosition() );
		gfx->addIndex(i-2);
		gfx->addIndex((i-1)%(noPoints()-2));
		gfx->addColour(vec4(1,1,1,0.5));
		outline->add( getPointPos(i) );
	}

}

vec2 OutlineEditor::getPointPos(int i)
{
	return vec2( getPoint(i)->getPosition() );
}

void OutlineEditor::updateMouseHex()
{
	mouseHex->setPosition(mx,my,0);
	if (mouseHex->intersectWith(outline) && !mouseHexCollideWithPoints())
	{
		mouseHex->setTextureAlpha(0.2);
		overLine = true;
	}
	else
	{
		mouseHex->setTextureAlpha(0);
		overLine = false;
	}

	if (symmetryOn)
	{
		mirrorHex->setPosition(-mx,my,0);
		if (mirrorHex->intersectWith(outline) && !mirrorHexCollideWithPoints())
		{
			mirrorHex->setTextureAlpha(0.2);
			mirrorOverLine = true;
		}
		else
		{
			mirrorHex->setTextureAlpha(0);
			mirrorOverLine = false;
		}
	}
}

void OutlineEditor::mouseDown()
{
	UIEntity::mouseDown();
	oldOutline = outline->clone();
	if (symmetryOn) mirrorMouseDown();
	if (overLine) addNewPoint();
	if (mirrorOverLine) addNewMirrorPoint();
}

void OutlineEditor::addNewPoint()
{
	mouseHex->setTextureAlpha(0);
	overLine = false;
	mouseHex->intersectWith(outline);
	int index = outline->getLastSegmentIntersected();

	//Printer::print("Index = ",index,"\n");

	OutlinePoint* point = builder->buildOutlinePoint(this,mx,my);


	setChildIndex(point,index+3);
	point->update();
	point->mouseMoved(mx,my);
	point->mouseDown();

	updateGfx();
	outline->print();
}

void OutlineEditor::addNewMirrorPoint()
{
	mirrorHex->setTextureAlpha(0);
	overLine = false;
	mirrorHex->intersectWith(outline);
	int index = outline->getLastSegmentIntersected();

	//Printer::print("Mirror Index = ",index,"\n");

	OutlinePoint* point = builder->buildOutlinePoint(this,-mx,my);


	setChildIndex(point,index+3);
	point->update();
	point->mouseMoved(-mx,my);
	point->setMirrorDrag(true);
	point->mouseDown();

	updateGfx();
	outline->print();
}

bool OutlineEditor::mouseHexCollideWithPoints()
{
	bool collide = false;
	for (int i=2; i<noPoints(); i++)
	{
		if (mouseHex->collideWith(getPoint(i))) collide = true;
	}
	return collide;
}

void OutlineEditor::mirrorMouseMove()
{
	for (int i=2; i<noPoints(); i++)
	{
		Polygon* p = getPoint(i)->getOutline();
		if (p->isInside(vec2(-mx,my)))
		{
			getPoint(i)->setTextureAlpha(0.75);
		}
		else if (!getPoint(i)->isMouseOver())
		{
			getPoint(i)->setTextureAlpha(0.5);
		}
	}
}

void OutlineEditor::mirrorMouseDown()
{
	for (int i=2; i<noPoints(); i++)
	{
		Polygon* p = getPoint(i)->getOutline();
		if (p->isInside(vec2(-mx,my)))
		{
			getPoint(i)->startMirrorDrag();
		}
	}
}

void OutlineEditor::setTextureAlpha(float a)
{
	TextureEntity::setTextureAlpha(a);
	setAlpha(a*0.5f);
}

void OutlineEditor::reset()
{
	children->clear();

	mouseHex = builder->buildOutlinePoint(this,0,0);

	mouseHex->setTextureAlpha(0);
	mouseHex->setIsCursor(true);

	mirrorHex = builder->buildOutlinePoint(this,0,0);
	mirrorHex->setTextureAlpha(0);
	mirrorHex->setIsCursor(true);



	symmetryOn = true;
	outline = new Polygon();
}

void OutlineEditor::restoreOldOutline()
{
	reset();
	for (int i=0; i<oldOutline->size(); i++)
	{
		vec2 point = oldOutline->get(i);
		builder->buildOutlinePoint(this, point.x, point.y);
	}
	updateGfx();
}

void OutlineEditor::mouseUp()
{
	UIEntity::mouseUp();
	vec2 p = vec2(mx,my);
	if (!boundary->isInside(p) || outlineCrossesIcons())
	{
		restoreOldOutline();
	}
	updateGfx();

}

bool OutlineEditor::mirrorHexCollideWithPoints()
{
	bool collide = false;
	for (int i=2; i<noPoints(); i++)
	{
		if (mirrorHex->collideWith(getPoint(i))) collide = true;
	}
	return collide;
}

void OutlineEditor::saveLoad(int index, bool isSaving)
{
	vector<vec2> points;
	if (isSaving)
	{
		for (int i=2; i<noPoints(); i++)
		{
			OutlinePoint* point = getPoint(i);
			points.push_back(vec2(point->getPosition()));
		}
		FileInterface::savePoints(index,points);
	}
	else
	{
		reset();
		points = FileInterface::loadPoints(index);
		for (uint i=0; i<points.size(); i++)
		{
			vec2 point = points[i];
			builder->buildOutlinePoint(this, point.x, point.y);
		}
	}
}

bool OutlineEditor::outlineCrossesIcons()
{
	bool result = false;
	moduleEditor = designer->getModuleEditor();
	EntityList* icons = moduleEditor->getAddedModules();
	if (icons!=0)
	{
		for (int i=0; i<icons->size(); i++)
		{
			ModuleIcon* icon = (ModuleIcon*) icons->get(i);
			if (icon!=0 && outline!=0 && outline->size()>0)
			{
				if (outline->intersectWith(icon->getOutline())) result = true;
			}

		}
	}
	return result;
}
