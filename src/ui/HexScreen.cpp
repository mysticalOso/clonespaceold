/*
 * HexScreen.cpp
 *
 *  Created on: 6 Mar 2015
 *      Author: Adam Nasralla
 */

#include <races/Xaccy.h>
#include <ui/HexScreen.h>
#include <ui/TriGrid.h>

HexScreen::HexScreen(Entity* parent) : UIEntity(parent)
{
	fadeDelay = 120;
	fadingIn = false;
	face = new Xaccy(this,2);
	face->setTextureAlpha(0);
	face->fadeOut();
	face->setAlphaModifier(0.8);
	face->setRenderOrder(8);
	back = new TextureEntity(this);
	back->setRenderOrder(7);
	back->setTexture("ui2", 2048);
	back->setDisableCamera(true);
	back->initQuad(0,0,WIDTH,HEIGHT);
	back->move(-4,-10,0);
	back->setScale(0.95);
	initGrid();
}

HexScreen::~HexScreen()
{
}

void HexScreen::show()
{
	fadingIn = true;
	grid->startGrow(10,10,true);
}

void HexScreen::hide()
{
	face->fadeOut();
	back->fadeOut();
	grid->startGrow(0,0,false);
}

void HexScreen::update()
{
	if (fadingIn)
	{
		fadeDelay--;
		if (fadeDelay==0)
		{
			back->fadeIn();
			face->fadeIn();
		}
	}
}

void HexScreen::initGrid()
{
	grid = new TriGrid(this);
	grid->init(5);
	grid->setPosition(-280,-450,0);
	grid->setRenderOrder(6);
}
