/*
 * TriGrid.h
 *
 *  Created on: 13 Jun 2014
 *      Author: mysticalOso
 */

#ifndef TRIGRID_H_
#define TRIGRID_H_

#include <glm/fwd.hpp>
#include <osogine/game/Entity.h>
#include <ui/Tri.h>
#include <ui/TriCoords.h>
#include <map>
#include <utility>


class Tri;
class EntityList;

class TriGrid : public Entity
{
public:
	TriGrid(Entity* parent);
	~TriGrid();

	float getTriHeight(){return triHeight;}
	float getTriWidth(){return triWidth;}

	void updateColour(int index, vec4 colour);
	int addVertex(int col, int row, float x1, float y1, vec4 colour, bool isLead);
	Tri* getTriangle(TriCoords coords) {return triLookup[coords];}
	void addTriangle(Tri* tri);
	void update();
	void startGrow(int col, int row, bool g);

	void updateMousePos(float mx, float my);
	void setMouseDown(bool isDown){mouseDown = isDown;}
	void setRightDown(bool isDown){rightDown = isDown;}
	vec2 findNearestCoord(float x1, float y1);

	vec2 getPosFromCoord(float row, float col);
	vec2 getCoordFromPos(float x1, float y1);

	void init(int type);


private:

	int addNewVertex(std::pair<int, int>, float x1, float y1, vec4 colour, bool isLead);

	void updateCursor();
	void overTri(int x, int y, bool isUp);
	void lightTri(int x, int y, bool isUp, bool isLit);

	float triHeight;
	float triWidth;


	bool mouseDown;
	bool rightDown;

	std::map<std::pair<int, int>, int> lookup;
	std::map<std::pair<int, int>, bool> leadLookup;
	std::map<TriCoords, Tri*> triLookup;

	EntityList* tris;
	EntityList* edges;

	TriCoords start;
	TriCoords end;

	int tsWidth;
	int tsHeight;

	float mx;
	float my;


	void initNeighbours();
	void initTris();
	void initGfx();
	void initEdges(int type);

	void findStart();
	bool findNextStart();
	bool findNextRowStart();
	void findNextEnd();

	void updateUI();
	int count;
	float alpha;


};

#endif /* TRIGRID_H_ */
