/*
 * AmmoBar.h
 *
 *  Created on: 15 Apr 2014
 *      Author: Adam Nasralla
 */

#ifndef CLONESPACEOLD_SRC_AMMOBAR_H_
#define CLONESPACEOLD_SRC_AMMOBAR_H_

#include <osogine/game/Entity.h>


class AmmoBar: public Entity
{
public:
	AmmoBar(Entity* parent, float x1, float y1, int maxAmmo, float width);
	~AmmoBar();
	void setAmmo(int a);

private:
	int ammo;
	int maxAmmo;
	float width;
	float spacing;
	vec3 colour;
	float iconHeight;
	float iconWidth;

	void updateGfx();
	void initGfx();

};

#endif /* CLONESPACEOLD_SRC_AMMOBAR_H_ */
