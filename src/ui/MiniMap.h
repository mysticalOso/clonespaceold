/*
 * MiniMap.h
 *
 *  Created on: 16 Apr 2014
 *      Author: Adam Nasralla
 */

#ifndef CLONESPACEOLD_SRC_MINIMAP_H_
#define CLONESPACEOLD_SRC_MINIMAP_H_

#include <osogine/game/Entity.h>


class Space;

class MiniMap: public Entity
{
public:
	MiniMap(Entity* parent, float x1, float y1, float size1, Space* space1);
	~MiniMap();
	void update();
	void setShowHome(bool b){showHome = b;}
	void setShowEnemy(bool b){showEnemy = b;}

	void addTarget(Entity* target, vec3 colour);
	void addTarget(vec2 pos, vec3 colour);
	void removeTarget(int index);

private:

	Space* space;
	float size;
	vec2 centre;
	bool showEnemy;
	bool showHome;

	List<Entity*> targetEntity;
	List<vec2> targets;
	List<vec3> targetColours;

	void init();
	void updateGfx();
	void updateCompass();


};

#endif /* CLONESPACEOLD_SRC_MINIMAP_H_ */
