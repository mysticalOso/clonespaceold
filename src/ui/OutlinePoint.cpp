/*
 * OutlinePoint.cpp
 *
 *  Created on: 27 Feb 2015
 *      Author: Adam Nasralla
 */

#include <ui/OutlineEditor.h>
#include <ui/OutlinePoint.h>



OutlinePoint::OutlinePoint(UIEntity* parent) : Button(parent, "point")
{
	editor = (OutlineEditor*) parent;
	draggable = true;
	snapToGrid = true;
	isCursor = false;
}

OutlinePoint::~OutlinePoint()
{
}

void OutlinePoint::mouseMoved(float mx, float my)
{
	Button::mouseMoved(mx,my);
	if (mouseIsOver && !isCursor) editor->setOverPoint(true);
}
