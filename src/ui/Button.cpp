/*
 * UITemplate.cpp
 *
 *  Created on: 26 Feb 2015
 *      Author: Adam Nasralla
 */

#include <glm/vec2.hpp>
#include <ui/Button.h>
#include <stdio.h>

Button::Button(UIEntity* parent, string name) : UIEntity(parent)
{
	ui = parent;
	this->name = name;
	alphaModifier = 0.5;
	setTexture("uiIcons",1024);
	renderOrder = parent->getRenderOrder();
}

Button::~Button()
{

}

void Button::mouseDown()
{
	UIEntity::mouseDown();
	if (mouseIsOver)
	{
		ui->clicked(this);
	}
	if (printOutline)
	{
		vec2 v;
		v.x = mx - getX();
		v.y = my - getY();
		printf("outline->add(%d,%d);\n",(int)v.x,(int)v.y);
		fflush(stdout);
	}
}

void Button::mouseUp()
{
	UIEntity::mouseUp();
}

void Button::mouseMoved(float mx1, float my1)
{
	UIEntity::mouseMoved(mx1,my1);
	if (mouseIsOver)
	{
		textureAlpha = 0.75;
	}
	else
	{
		textureAlpha = 0.5;
	}
}

