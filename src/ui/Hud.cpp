/*
 * Hud.cpp
 *
 *  Created on: 9 Mar 2015
 *      Author: Adam Nasralla
 */

#include <hubs/UIHub.h>
#include <osogine/game/Game.h>
#include <space/Space.h>
#include <ui/Hud.h>
#include <ui/HudBar.h>
#include <ui/MiniMap.h>
#include <ui/TriGrid.h>


Hud::Hud(Entity* parent) : UIEntity(parent)
{

	uiHub = (UIHub*) parent;

	setDisableCamera(true);
	grid = new TriGrid(this);
	grid->init(1);
	//grid->startGrow(-15,45,true);

	back = new TextureEntity(this);
	back->setDisableCamera(true);

	back->setTexture("ui",2048);
	back->setProgram("texture");
	back->initQuad(0,1191,1425,242);
	back->setTextureAlpha(1);
	back->setPosition(-254,415,0);

//	miniMap = new MiniMap(-WIDTH2+20,HEIGHT2-219,200,ui->getSpace());
	healthBar = new HudBar(this, "HEALTH",-WIDTH2+254,HEIGHT2-51,200,0);
	energyBar = new HudBar(this, "ENERGY",-WIDTH2+542,HEIGHT2-51,200,0);
	storageBar = new HudBar(this, "STORAGE",-WIDTH2+832,HEIGHT2-48,175,2);
	ammoBar = new HudBar(this, "AMMO",-WIDTH2+1147,HEIGHT2-51,150,1);

	Space* space = (Space*) game->getEntity("space");
	miniMap = new MiniMap(this, -WIDTH2+20 + 100 ,HEIGHT2-219 + 100,200,space);
	//miniMap = new MiniMap(this, 0,0,200,space);
	//miniMap = new MiniMap(-WIDTH2+20,HEIGHT2-219,200,ui->getSpace());

	setTextureAlpha(0);



//	energyBar->setPos(0,0);

	//healthBar = new HealthBar();

//	initGfx();
}

Hud::~Hud()
{
	// TODO Auto-generated destructor stub
}

void Hud::update()
{
	UIEntity::update();
	energyBar->setRatio( uiHub->getEnergy() );
	healthBar->setRatio( uiHub->getHealth() );
	ammoBar->setAmmo( uiHub->getAmmo() );
	storageBar->updateGems( uiHub->getGems() );
	miniMap->setAlpha(textureAlpha);
}

void Hud::setGems(EntityList* gems)
{
	storageBar->updateGems(gems);
}

void Hud::addTarget(vec2 pos, vec3 colour)
{
	miniMap->addTarget(pos,colour);
}

void Hud::addTarget(Entity* entity, vec3 colour)
{
	miniMap->addTarget(entity,colour);
}

void Hud::removeTarget(int index)
{
	miniMap->removeTarget(index);
}

void Hud::setTextureAlpha(float a)
{
	TextureEntity::setTextureAlpha(a);
	back->setTextureAlpha(a);


	healthBar->setAlpha(a);
	energyBar->setAlpha(a);
	storageBar->setAlpha(a);
	ammoBar->setAlpha(a);

}

void Hud::fadeOut()
{
	TextureEntity::fadeOut();
	grid->startGrow(0,0,false);
}

void Hud::fadeIn()
{
	TextureEntity::fadeIn();
	grid->startGrow(0,0,true);
}


