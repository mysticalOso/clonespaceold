/*
 * CommLink.cpp
 *
 *  Created on: 12 Feb 2015
 *      Author: Adam Nasralla
 */

#include <core/Constants.h>
#include <glm/vec2.hpp>
#include <osogine/game/Game.h>
#include <osogine/game/TextEntity.h>
#include <osogine/game/TextureEntity.h>
#include <osogine/gfx/GfxBuilder.h>
#include <osogine/gfx/GfxObject.h>
#include <osogine/utils/Printer.h>
#include <races/SentientFace.h>
#include <races/Xaccy.h>
#include <ui/CommLink.h>
#include <ui/TriGrid.h>


CommLink::CommLink(Entity* parent) : Entity(parent)
{
	textBox = 0;
	grid = 0;
	renderOrder = 5;
	fadeDelay = 50;
	fadingIn = false;
	displayTime = -1;
	back = 0;
	sentientFace = new SentientFace(this);
	xaccy = new Xaccy(this, 0.5);
	xaccy->setTextureAlpha(0);
	xaccy->setPos(vec3(-300,-400,0));
	face = sentientFace;

	init();
}

CommLink::~CommLink()
{
	// TODO Auto-generated destructor stub
}



void CommLink::showMessage(string msg, float displayTime)
{
	this->displayTime = displayTime;
	textBox->setText(msg);
	grid->startGrow(10,10,true);
	fadeDelay = 50;
	fadingIn = true;
}

void CommLink::changeMessage(string msg)
{
	textBox->setText(msg);
}

void CommLink::turnOff()
{
	textBox->fadeOut();
	back->fadeOut();
	face->fadeOut();
	grid->startGrow(10,10,false);
}

void CommLink::update()
{
	if (fadingIn)
	{
		fadeDelay--;
		if (fadeDelay==0)
		{
			fadingIn = false;
			textBox->fadeIn();
			back->fadeIn();
			face->fadeIn();
		}
	}
	else
	{
		if (displayTime > 0 ) displayTime--;
		if (displayTime == 0)
		{
			displayTime = -1;
			turnOff();
		}
	}
}


void CommLink::init()
{
	initGrid();
	initBack();
	initTextBox();
	setDisableCamera(true);
}

void CommLink::initGrid()
{
	grid = new TriGrid(this);
	grid->init(2);
	grid->setRenderOrder(6);
}

void CommLink::initTextBox()
{
	textBox = new TextEntity(this,500);
	textBox->setAlphaModifier(0.5);
	textBox->setPosition(-140,-331,0);
	textBox->setRenderOrder(8);
}

void CommLink::toggleFace()
{
	if (face==sentientFace) {face=xaccy; textBox->setInvert(true);}
	else {face=sentientFace; textBox->setInvert(false);}
}

void CommLink::initBack()
{
	back = new TextureEntity(this);
	back->setProgram("texture");
	back->setTexture("comms");
	//back->setAlphaModifier(0.5);

	back->setRenderOrder(7);

	GfxBuilder* builder = game->getGfxBuilder();
	GfxObject* gfx = new GfxObject();
	builder->addQuad(gfx, vec2(0,0), vec2(1024,1024), vec2(0,0), vec2(1,1) );
	back->setGfx(gfx);
	back->setPosition((WIDTH-1024)*0.5-WIDTH2,-HEIGHT2,0);
}


