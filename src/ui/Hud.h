/*
 * Hud.h
 *
 *  Created on: 9 Mar 2015
 *      Author: Adam Nasralla
 */

#ifndef SRC_UI_HUD_H_
#define SRC_UI_HUD_H_

#include <ui/UIEntity.h>

class MiniMap;
class TriGrid;
class HudBar;
class UIHub;

class Hud: public UIEntity
{
public:
	Hud(Entity* parent);
	~Hud();

	void update();

	void setGems(EntityList* gems);
	void addTarget(Entity* entity, vec3 colour);
	void addTarget(vec2 pos, vec3 colour);
	void removeTarget(int index);

	void setTextureAlpha(float a);

	void fadeOut();
	void fadeIn();

private:
	TriGrid* grid;
	HudBar* healthBar;
	HudBar* energyBar;
	HudBar* ammoBar;
	HudBar* storageBar;
	MiniMap* miniMap;
	TextureEntity* back;
	UIHub* uiHub;
};

#endif /* SRC_UI_HUD_H_ */
