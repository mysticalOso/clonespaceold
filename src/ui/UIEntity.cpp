/*
 * UIEntity.cpp
 *
 *  Created on: 26 Feb 2015
 *      Author: Adam Nasralla
 */

#include <builders/UIBuilder.h>
#include <osogine/utils/Polygon.h>
#include <ui/UIEntity.h>
#include <glm/vec2.hpp>
#include <osogine/game/Game.h>

UIEntity::UIEntity(Entity* parent) : TextureEntity(parent)
{
	outline = new Polygon();
	mx = -99999;
	my = -99999;
	oldMx = 0;
	oldMy = 0;
	mouseIsOver = false;
	mouseIsDown = false;
	printOutline = false;
	draggable = false;
	isDragging = false;
	mirrorDrag = false;
	snapToGrid = true;
	textureAlpha = 0;
	renderOrder = 8;
	gridX = 12;
	gridY = 10;
	isActive = true;
	builder = (UIBuilder*) game->getBuilder("ui");
	setDisableCamera(true);
}

UIEntity::~UIEntity()
{
}

void UIEntity::mouseMoved(float mx1, float my1)
{
	if (isActive)
	{
		mx = mx1;
		my = my1;
		updateMouseOver();
		children->mouseMoved(mx1,my1);

		if (isDragging && snapToGrid && !mirrorDrag)
		{
			float dmx = mx - oldMx;
			float dmy = my - oldMy;
			move(dmx,dmy,0);
			oldMx = mx;
			oldMy = my;
		}
		if (isDragging && snapToGrid && mirrorDrag)
		{
			float dmx = - mx - oldMx;
			float dmy = my - oldMy;
			move(dmx,dmy,0);
			oldMx = -mx;
			oldMy = my;
		}

	}

}

void UIEntity::mouseDown()
{
	if (isActive)
	{
		updateMouseOver();
		mouseIsDown = true;
		children->mouseDown();

		if (mouseIsOver && draggable)
		{
			isDragging = true;
			oldMx = mx;
			oldMy = my;
		}
	}
}

void UIEntity::mouseUp()
{
	if (isActive)
	{
		if (isDragging) snapPosToGrid();
		mouseIsDown = false;
		children->mouseUp();
		isDragging = false;
		mirrorDrag = false;
	}
}

void UIEntity::addOutline(float x1, float y1)
{
	outline->add(x1,y1);
}

void UIEntity::updateMouseOver()
{
	vec2 mPos = vec2(mx,my);
	mouseIsOver = (outline->isInside(mPos));
}

void UIEntity::setTopLeft(float x, float y)
{
	int halfWidth = round(width * 0.5);
	int halfHeight = round(height * 0.5);
	int x1 = x - WIDTH2 + halfWidth;
	int y1 = HEIGHT2 - y - halfHeight;
	setPosition(x1,y1,0);
	outline->setTransform(getTransform());
	outline->updateTransformedOutline();
}

void UIEntity::snapPosToGrid()
{
	vec3 pos = getPosition();
	float x = (round(pos.x / gridX)) * gridX;
	float y = (round(pos.y / gridY)) * gridY;
	setPosition(x,y,0);
}

void UIEntity::startMirrorDrag()
{
	isDragging = true;
	mirrorDrag = true;
	oldMx = -mx;
	oldMy = my;
}

void UIEntity::saveLoad(int i, bool b)
{
}
