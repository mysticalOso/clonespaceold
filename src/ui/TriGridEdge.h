/*
 * TriGridEdge.h
 *
 *  Created on: 18 Jun 2014
 *      Author: Adam Nasralla
 */

#ifndef TRIGRIDEDGE_H_
#define TRIGRIDEDGE_H_

#include <osogine/game/IEntity.h>

class TriCoords;


class TriGridEdge : public IEntity
{
public:
	TriGridEdge(int dir1, int colStart1, int rowStart1,
			int rowFinish1, bool opener1);
	~TriGridEdge();

	TriCoords getStartCoords();
	TriCoords getCoords(int row);
	bool inRange(int row){ return (row >= rowStart && row < rowFinish); }
	bool isOpener(){ return opener; }

private:

	int dir;
	bool opener;
	int rowStart;
	int rowFinish;
	int colStart;

};

#endif /* TRIGRIDEDGE_H_ */
