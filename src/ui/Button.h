/*
 * UITemplate.h
 *
 *  Created on: 26 Feb 2015
 *      Author: Adam Nasralla
 */

#ifndef SRC_UI_BUTTON_H_
#define SRC_UI_BUTTON_H_

#include <ui/UIEntity.h>

class Button: public UIEntity
{
public:
	Button(UIEntity* parent, string name);
	~Button();

	void mouseDown();
	void mouseUp();
	void mouseMoved(float mx, float my);

protected:
	UIEntity* ui;
	string name;
};

#endif /* SRC_UI_BUTTON_H_ */
