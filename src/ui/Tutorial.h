/*
 * Tutorial.h
 *
 *  Created on: 12 Feb 2015
 *      Author: Adam Nasralla
 */

#ifndef SRC_UI_TUTORIAL_H_
#define SRC_UI_TUTORIAL_H_

#include <osogine/game/IEntity.h>
#include <map>

class UIHub;
class CommLink;
class AlienRace;
class Sentient;
class HexScreen;

class Tutorial: public IEntity
{
public:
	Tutorial(Game* game);
	~Tutorial();
	void update();
	void init();
	void reset(int count);
	void event(string name);

	void turnOff();

	void completeAction(string name){ actions[name] = true; }
	bool isActionCompleted(string name){ return (actions[name]!=0 || name==""); }

private:

	bool sectionReady(int timePassed){ return sectionReady(timePassed,""); }
	bool sectionReady(int timePassed, string actionRequired);
	bool forceSection(string actionRequired);
	bool sectionReady(string actionRequired){ return sectionReady(150,actionRequired); }
	void resetAction(string name){ actions[name] = 0; }

	void commLinkMsg(string msg, float displayTime);
	void changeCommLinkMsg(string msg);
	void turnOffCommLink();

	void updateCount(){if(isActive){count++; sectionCount=0;}else{sectionCount=-1;}}
	void updateAttack();

	map<string,bool> actions;
	int attackMode;
	int attackCount;
	int maxAttackCount;
	int count;
	int section;
	int sectionCount;
	int resetCount;
	UIHub* uiHub;
	bool isActive;
	bool noReset;

	HexScreen* hex;
	CommLink* commLink;
	RaceHub* raceHub;
	AlienRace* xacadiens;
	Sentient* sentient;
	bool test;
	Game* game;
};

#endif /* SRC_UI_TUTORIAL_H_ */
