/*
 * ModuleEditor.h
 *
 *  Created on: 28 Feb 2015
 *      Author: Adam Nasralla
 */

#ifndef SRC_UI_MODULEEDITOR_H_
#define SRC_UI_MODULEEDITOR_H_

#include <ui/UIEntity.h>

class OutlineEditor;
class ShipDesigner;

class Button;
class ModuleIcon;

class ModuleEditor: public UIEntity
{
public:
	ModuleEditor(Entity* parent);
	~ModuleEditor();

	void clicked(Button* icon);

	EntityList* getAddedModules(){ return addedModules; }

	void mouseUp();

	void mouseDown();

	void saveLoad(int i, bool b);

	void reset();

	bool shouldAddModule();

	bool hasNeeded();

	OutlineEditor* getOutlineEditor(){ return outlineEditor; }
	ShipDesigner* getShipDesigner(){ return shipDesigner; }

private:
	bool checkTouchingShipOutline();
	bool isInsideShipOutline();
	bool touchingAnotherModule();
	bool illegalDouble();
	ShipDesigner* shipDesigner;
	OutlineEditor* outlineEditor;
	ModuleIcon* currentModule;
	EntityList* addedModules;

};

#endif /* SRC_UI_MODULEEDITOR_H_ */
