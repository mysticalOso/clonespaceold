/*
 * HudBar.h
 *
 *  Created on: 15 Apr 2014
 *      Author: Adam Nasralla
 */

#ifndef CLONESPACEOLD_SRC_HUDBAR_H_
#define CLONESPACEOLD_SRC_HUDBAR_H_

#include <osogine/game/Entity.h>
#include <string>


class TextEntity;
class EnergyBar;
class AmmoBar;
class EntityList;
class StorageBar;

class HudBar: public Entity
{
public:
	HudBar(Entity* parent, std::string label, float x, float y, float width, int type);
	~HudBar();

	void setRatio(float r);
	void setAmmo(int a);
	void updateGems(EntityList* gems);
	void setText(std::string s);
	void setPos(float x1, float y1);
	void setAlpha(float a);

private:

	void init();

	std::string label;
	float x;
	float y;
	float width;
	bool rightAligned;


	GfxObject* backGfx;

	//TODO optimise number of drawCalls e.g lineBox
	TextEntity* text;
	EnergyBar* energyBar;
	AmmoBar* ammoBar;
	StorageBar* storageBar;

	int type;

	float ratio;
	int ammo;
	int maxAmmo;

	bool locked;

};

#endif /* CLONESPACEOLD_SRC_HUDBAR_H_ */
