/*
 * EnergyBar.h
 *
 *  Created on: 14 Apr 2014
 *      Author: Adam Nasralla
 */

#ifndef CLONESPACEOLD_SRC_ENERGYBAR_H_
#define CLONESPACEOLD_SRC_ENERGYBAR_H_

#include <osogine/game/TextureEntity.h>


class EnergyBar: public TextureEntity
{
public:
	EnergyBar(Entity* parent, float x1, float y1, float width1, float height1, int noSegments1);
	~EnergyBar();
	void setRatio(float r);

private:
	float ratio;
	int noSegments;
	float segWidth;
	float width;
	float height;
	void updateGfx();
	void initGfx();

};

#endif /* CLONESPACEOLD_SRC_ENERGYBAR_H_ */
