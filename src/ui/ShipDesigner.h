/*
 * ShipDesigner.h
 *
 *  Created on: 26 Feb 2015
 *      Author: Adam Nasralla
 */

#ifndef SRC_UI_SHIPDESIGNER_H_
#define SRC_UI_SHIPDESIGNER_H_

#include <ui/UIEntity.h>

class TriGrid;
class Button;
class OutlineEditor;
class CostPanel;
class ModuleEditor;
class ShipBuilder;
class TextEntity;
class UIHub;

class ShipDesigner: public UIEntity
{
public:
	ShipDesigner(Entity* parent);
	~ShipDesigner();

	void clicked(Button* button);
	void mouseDown();
	void mouseUp();
	void mouseMoved(float mx, float my);

	void toggleActive(bool b);

	void setText(string str);
	void update();

	void saveLoad(int i, bool b);

	OutlineEditor* getOutlineEditor(){ return outlineEditor; }
	ModuleEditor* getModuleEditor(){ return moduleEditor; }
	void toggleBuildEnabled(bool b){ buildEnabled = b; }

	void setMessage(string s);

	void toggleAction();

private:
	void initGrid();
	void initGfx();
	void initButtons();
	void initEditors();

	int startCount;


	TriGrid* grid;

	Button* quitB;
	Button* buildB;

	bool buildEnabled;

	ModuleEditor* moduleEditor;
	CostPanel* costPanel;
	OutlineEditor* outlineEditor;

	ShipBuilder* shipBuilder;

	UIHub* hub;

	TextEntity* textBox;

	TextEntity* messageBox;
};

#endif /* SRC_UI_SHIPDESIGNER_H_ */
