/*
 * UIEntity.h
 *
 *  Created on: 26 Feb 2015
 *      Author: Adam Nasralla
 */

#ifndef SRC_UI_UIENTITY_H_
#define SRC_UI_UIENTITY_H_

#include <osogine/game/TextureEntity.h>
#include <glm/vec2.hpp>

class UIBuilder;
class Button;

class UIEntity: public TextureEntity
{
public:
	UIEntity(Entity* parent);
	virtual ~UIEntity();

	virtual void mouseMoved(float mx1, float my1);

	virtual void mouseDown();

	virtual void mouseUp();

	virtual void setTopLeft(float x, float y);

	virtual void toggleAction(){}

	virtual bool isMouseOver(){ return mouseIsOver; }

	virtual bool mouseDownOver(){ return mouseIsDown && mouseIsOver; }

	virtual void clicked(Button* button){}

	void setPrintOutline(bool b){ printOutline = b; }

	void addOutline(float x1, float y1);

	virtual void snapPosToGrid();

	void setMirrorDrag(bool b){ mirrorDrag = b; }

	void startMirrorDrag();

	void toggleActive(bool b){ isActive = b; }

	virtual void saveLoad(int i, bool b);




protected:
	virtual void updateMouseOver();



	bool mirrorDrag;
	bool printOutline;
	float mx;
	float my;
	bool mouseIsOver;
	bool mouseIsDown;

	bool draggable;
	bool isDragging;
	bool snapToGrid;

	float oldMx;
	float oldMy;

	float gridX;
	float gridY;

	bool isActive;


	UIBuilder* builder;


};

#endif /* SRC_UI_UIENTITY_H_ */
