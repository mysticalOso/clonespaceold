/*
 * CostBar.h
 *
 *  Created on: 27 Feb 2015
 *      Author: Adam Nasralla
 */

#ifndef SRC_UI_COSTBAR_H_
#define SRC_UI_COSTBAR_H_

#include <osogine/game/Entity.h>
#include <glm/vec2.hpp>

class CostBar: public Entity
{
public:
	CostBar(Entity* parent);
	~CostBar();

	void setValue(float value);
	void addValue();

	void update();

	void setGfx(GfxObject* gfx);

private:
	void initPoints();
	void updateGfx();

	float value;

	int count;
	int maxCount;

	vec2 topL;
	vec2 topR;
	vec2 bottomL;
	vec2 bottomR;
	vec2 r;
	vec2 l;
	vec2 oldL;
	vec2 oldR;
	vec2 newR;
	vec2 newL;

};

#endif /* SRC_UI_COSTBAR_H_ */
