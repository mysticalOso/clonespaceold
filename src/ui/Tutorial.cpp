/*
 * Tutorial.cpp
 *
 *  Created on: 12 Feb 2015
 *      Author: Adam Nasralla
 */

#include <clone/Clone.h>
#include <hubs/RaceHub.h>
#include <hubs/UIHub.h>
#include <osogine/game/Game.h>
#include <osogine/utils/Dice.h>
#include <races/AlienRace.h>
#include <ship/Ship.h>
#include <ui/CommLink.h>
#include <ui/Tutorial.h>
#include <core/Constants.h>
#include <glm/vec3.hpp>
#include <hubs/RoomHub.h>
#include <interior/Interior.h>
#include <interior/Pod.h>
#include <osogine/audio/SoundInterface.h>
#include <races/Sentient.h>
#include <ui/HexScreen.h>

Tutorial::Tutorial(Game* game) : IEntity()
{
	uiHub = 0;
	count = 0;
	section = 0;
	sectionCount = 0;
	actions[""] = 0;
	xacadiens = 0;
	attackMode = 0;
	commLink = 0;
	raceHub = 0;
	attackCount = 0;
	maxAttackCount = 50;
	resetCount = 0;
	noReset = false;
	sentient = 0;
	hex = 0;
	test = false;
	isActive = true;
	this->game = game;
}

Tutorial::~Tutorial()
{

}

void Tutorial::update()
{

	if (resetCount<=0)
	{
	updateCount();
	updateAttack();

	if (sectionReady(100))
	{
		uiHub->toggleHud();
		commLinkMsg("Greetings from the Quantum Web.", -1);
	}
	else if (sectionReady(180))
	{
		changeCommLinkMsg("Please take some time to get accustomed to your new clone host.");
	}
	else if (sectionReady(180))
	{
		changeCommLinkMsg("Move the mouse to direct your ship.");
	}
	else if (sectionReady("mouseMoved"))
	{
		changeCommLinkMsg("Use the mouse wheel to zoom in and out.");
	}
	else if (sectionReady("mouseWheel"))
	{
		changeCommLinkMsg("Press the 'W' key to accelerate forward.");
	}
	else if (sectionReady("shipForward"))
	{
		changeCommLinkMsg("Follow the blue arrow.");
		uiHub->addTarget( vec2(5000,5000), vec3( 50.f / 256.f, 50 / 256.f, 256.f / 256.f  ) );
	}
	else if (sectionReady("nearAsteroids"))
	{
		changeCommLinkMsg("Press the left mouse button to fire your lazers and destroy an asteroid.");
		uiHub->removeTarget(0);
	}
	else if (sectionReady(0, "asteroidDestroyed"))
	{
		changeCommLinkMsg("Good job!");
	}
	else if (sectionReady(150))
	{
		changeCommLinkMsg("Approach gems to beam them up.");
	}
	else if (sectionReady("gemCollected"))
	{
		changeCommLinkMsg("Incoming Xacadien scout!!");
		attackMode = 1;
	}
	else if (sectionReady(150))
	{
		changeCommLinkMsg("Kill the Xacadien scum with your lazers!");
	}

	else if (sectionReady(0, "enemyDestroyed"))
	{
		changeCommLinkMsg("Nice!!");
	}
	else if (sectionReady(150))
	{
		changeCommLinkMsg("Keep destroying asteroids and collecting gems until your storage is full.");
	}
	else if (sectionReady(180))
	{
		changeCommLinkMsg("But watch out for those damn Xacadiens!");
	}
	else if (sectionReady(180))
	{
		turnOffCommLink();
	}
	else if (sectionReady("scoutAttacking"))
	{
		commLinkMsg("Press the right mouse button to use your shield to block enemy lazers.",-1);
	}
	else if (sectionReady(200,"shieldsActive"))
	{
		changeCommLinkMsg("Keep an eye you your energy bar - drain it down and your lazers and shield will fail.");
	}
	else if (sectionReady(250))
	{
		turnOffCommLink();
	}
	else if (sectionReady("scoutAttacking"))
	{
		commLinkMsg("Press SPACE to fire a deadly missile!",-1);
	}
	else if (sectionReady("missilesFired"))
	{
		changeCommLinkMsg("You only have five of those killers - use sparingly.");
	}
	else if (sectionReady(200))
	{
		turnOffCommLink();
	}
	else if (sectionReady("storageFull"))
	{
		commLinkMsg("Now follow the arrow back to base.", -1);
		uiHub->addTarget(vec2(0,0), vec3( 50.f / 256.f, 50 / 256.f, 256.f / 256.f  ) );
		attackMode = 0;
	}


	else if (sectionReady("nearTarget"))
	{
		uiHub->removeTarget(0);
		changeCommLinkMsg("Approach the Sentient space station!");
	}
	else if (sectionReady("shipBeamOn"))
	{
		changeCommLinkMsg("The station beam will heal your ship and beam aboard all the gems you have collected.");
	}
	else if (sectionReady(0,"shipBeamOff"))
	{
		changeCommLinkMsg("You've collected enough gems to build a new ship!");
		sentient->addStationClone();
	}
	else if (sectionReady(180))
	{
		changeCommLinkMsg("Press TAB to switch to your next clone host.");
	}
	else if (sectionReady("tabPressed"))
	{
		changeCommLinkMsg("Use the W A S D keys to control your clone host directly");
	}
	else if (sectionReady("cloneMoved"))
	{
		changeCommLinkMsg("Press the right mouse button to draw your weapon");
	}
	else if (sectionReady("cloneDraw"))
	{
		changeCommLinkMsg("Press the left mouse button to fire the clone's lazer");
	}
	else if (sectionReady("lazerFired"))
	{
		changeCommLinkMsg("Stop shooting my space station you fool!");
	}
	else if (sectionReady(180))
	{
		changeCommLinkMsg("Follow the arrow to the ship designer pod.");
		Ship* ship = sentient->getPlayerShip();
		Interior* interior = ship->getInterior();
		RoomHub* roomHub = interior->getRoomHub();
		Pod* pod = roomHub->getPod(vec2(-1,-1));
		uiHub->addTarget(vec2(pod->getLocalPosition()), vec3( 50.f / 256.f, 50 / 256.f, 256.f / 256.f  ) );
	}
	else if (sectionReady("cloneNearTarget"))
	{
		changeCommLinkMsg("Press SPACE to activate pod.");
		uiHub->toggleHud();
	}
	else if (sectionReady(0,"shipDesignerActive"))
	{
		turnOffCommLink();
		uiHub->removeTarget(0);
		uiHub->setDesignerText("Welcome to the Quantum Ship Designer.");
		uiHub->toggleBuildEnabled(false);
	}
	else if (sectionReady(300))
	{
		uiHub->setDesignerText("Here you can design the ships of your dreams...");
	}
	else if (sectionReady(180))
	{
		uiHub->setDesignerText("Click and drag the points of the outline to change it's shape.");
	}
	else if (sectionReady(300))
	{
		uiHub->setDesignerText("Click on a line segment to add a new point.");
	}
	else if (sectionReady(300))
	{
		uiHub->setDesignerText("Drag modules from the panel and place them inside the outline to add them to your ship.");
	}
	else if (sectionReady(300))
	{
		uiHub->setDesignerText("You need at least a cockpit, an energy core and an engine to build a space worthy ship. But you'll probably be wanting some weapons too!");
	}
	else if (sectionReady(300))
	{
		uiHub->setDesignerText("Click build when you're ready!");
		uiHub->toggleBuildEnabled(true);
	}
	else if (sectionReady(0,"buildFinished"))
	{
		uiHub->toggleHud();
		commLinkMsg("Follow the arrow to the cockpit",-1);
		Ship* ship = sentient->getPlayerShip();
		Interior* interior = ship->getInterior();
		RoomHub* roomHub = interior->getRoomHub();
		Pod* pod = roomHub->getPod(vec2(2,0));
		if (pod==0) pod = roomHub->getPod(vec2(2,1));
		if (pod!=0)
		{
			uiHub->addTarget(vec2(pod->getLocalPosition()), vec3( 50.f / 256.f, 50 / 256.f, 256.f / 256.f  ) );
		}
	}
	else if (sectionReady("cloneNearTarget"))
	{
		changeCommLinkMsg("Press SPACE to launch!");
		uiHub->removeTarget(0);
	}
	else if (sectionReady("takeOff"))
	{
		changeCommLinkMsg("Wow - she is beautiful!");
	}
	else if (sectionReady(180))
	{
		changeCommLinkMsg("Press TAB to switch ships");
	}
	else if (sectionReady(0,"tabPressed"))
	{
		changeCommLinkMsg("Press TAB again to switch back to your awesome ship.");
	}
	else if (sectionReady(0,"tabPressed"))
	{
		changeCommLinkMsg("That's more like it!");
	}
	else if (sectionReady(180))
	{
		changeCommLinkMsg("Follow the arrow to an asteroid field in a sector to the east.");
		uiHub->addTarget(vec2(70000,0), vec3( 50.f / 256.f, 50 / 256.f, 256.f / 256.f  ) );
	}
	else if (sectionReady(180))
	{
		changeCommLinkMsg("Fill up your ships with gems and return to base");
		attackCount = 0;
		attackMode = 2;
	}
	else if (sectionReady(180))
	{
		turnOffCommLink();
	}

	else if (sectionReady(500))
		{
		uiHub->removeTarget(0);
		attackMode = 0;
			SoundInterface::playXaccy();
			//uiHub->toggleHud();
			//sentient->addStationClone();
			uiHub->toggleFace();
			commLinkMsg("Hi my name is Xaccy the Xacadien! Nice to meet you. I hope we can be friends...",-1);

		}
		else if (sectionReady(300))
		{
			turnOffCommLink();
		}










		else if (sectionReady(100))
		{
			uiHub->toggleFace();
			//uiHub->toggleHud();
			commLinkMsg("That was a message from the Xacadien mother ship! Those beasts are practically indestructable!",-1);
		}
		else if (sectionReady(250))
		{
			changeCommLinkMsg("We have to take them out from the inside! Sending a teleporter ship...");
			sentient->sendTeleporterShip();
		}
		else if (sectionReady(200))
		{
			changeCommLinkMsg("Press TAB to take control of the teleporter ship.");

		}
		else if (sectionReady("tabPressed"))
		{
			changeCommLinkMsg("Remember to follow my instructions when the mother ship arrives.");
		}
		else if (sectionReady(180))
		{
			changeCommLinkMsg("Enemy mother ship approaching!!!");
			xacadiens->addMother();
		}
		else if (sectionReady(180))
		{
			changeCommLinkMsg("Press TAB to switch to the teleporter passenger");
		}
		else if (sectionReady(80,"tabPressed"))
		{
			changeCommLinkMsg("Press SPACE to teleport!");
		}
		else if (sectionReady(0,"teleported"))
		{
			changeCommLinkMsg("Follow the red arrow and take out the crew");
			Ship* ship = sentient->getPlayerShip();

			Clone* clone = ship->getClone("Fredric");
			if (clone!=0)
			{
				uiHub->addTarget(clone, vec3( 256.f / 256.f, 0.f / 256.f, 0.f / 256.f ) );
			}
		}
		else if (sectionReady(180))
		{
			turnOffCommLink();
		}
		else if (sectionReady(0,"FredricKilled"))
		{
			uiHub->removeTarget(0);
			Ship* ship = sentient->getPlayerShip();

			Clone* clone = ship->getClone("Xaccy");
			if (clone!=0)
			{
				uiHub->addTarget(clone, vec3( 256.f / 256.f, 0.f / 256.f, 0.f / 256.f ) );
			}

		}
		else if (sectionReady("XaccyKilled"))
		{
			Printer::print("\nXACCYKILLED2\n");
			uiHub->removeTarget(0);
			Ship* ship = sentient->getPlayerShip();
			Interior* interior = ship->getInterior();
			RoomHub* roomHub = interior->getRoomHub();
			Pod* pod = roomHub->getPod(vec2(2,0));
			if (pod==0) pod = roomHub->getPod(vec2(2,1));
			if (pod!=0)
			{
				uiHub->addTarget(vec2(pod->getLocalPosition()), vec3( 50.f / 256.f, 50 / 256.f, 256.f / 256.f  ) );
				commLinkMsg("Follow the blue arrow to the cockpit.",-1);
			}
			else
			{
				Printer::print("\nNO POD!\n");
			}

		}
		else if (sectionReady("cloneNearTarget"))
		{

			changeCommLinkMsg("Press SPACE to steal the mother ship!");
		}
		else if (sectionReady("takeOff"))
		{
			uiHub->removeTarget(0);
			changeCommLinkMsg("CONGRATULATIONS!!! YOU JUST JACKED YOUR FIRST SHIP!!!");
		}
		else if (sectionReady(180))
		{
			changeCommLinkMsg("As a reward here are some reinforcements...");
			sentient->sendReinforcements();
		}
		else if (sectionReady(180))
		{
			changeCommLinkMsg("Press TAB to cycle through your fleet");
		}
		else if (sectionReady("tabPressed"))
		{
			turnOffCommLink();
		}
		else if (sectionReady(500))
		{
			uiHub->toggleFace();
			SoundInterface::playXaccy();
			commLinkMsg("Hi it's meeee Xaccy!! It's been a while dude how's things, I really just wanna be friends you know that.",-1);
		}
		else if (sectionReady(300))
		{
			turnOffCommLink();
			xacadiens->addWave(1);
		}
		else if (sectionReady("xacadiensKilled"))
		{
			uiHub->toggleFace();
			commLinkMsg("Great work!!",-1);
		}
		else if (sectionReady(180))
		{
			turnOffCommLink();
		}
		else if (sectionReady(200))
		{
			commLinkMsg("Oh no.",-1);
		}
		else if (sectionReady(180))
		{
			turnOffCommLink();
			xacadiens->addWave(2);
			noReset = true;
		}
		else if (sectionReady("allDead"))
		{


		}
		else if (sectionReady(80))
		{

			uiHub->toggleHud(false);
			hex = new HexScreen(uiHub);
			hex->show();
		}
		else if (sectionReady(50))
		{
			xacadiens->removeAllShips();
			SoundInterface::playXaccy();
		}
		else if (sectionReady(50))
		{
			xacadiens->removeAllShips();
		}
		else if (sectionReady(200))
		{
			hex->hide();
		}
		else if (sectionReady(150))
		{
			reset(5);
		}
//		else if (sectionReady(200))
//		{
//			sentient->birth();
//			sentient->toggleCloneCtrl(false);
//			noReset = false;
//			commLinkMsg("Incubation Complete.",-1);
//		}
//		else if (sectionReady(180))
//		{
//			turnOffCommLink();
//			Clone* clone = sentient->getCurrentClone();
//			if (clone!=0)
//			{
//				clone->use();
//			}
//		}
//		else if (sectionReady(350))
//		{
//			uiHub->toggleHud(true);
//			sentient->toggleCloneCtrl(true);
//		}
	}

	else
	{
		resetCount--;
		if (resetCount==0)
		{
			xacadiens->reset();
			sentient->reset();
			attackCount = 0;
			count = 0;
			section = 0;
			sectionCount = 0;
		}
	}

	//forceSection();


	//	else if (sectionReady("shieldBlock"))
//	{
//		changeCommLinkMsg("Press space to launch a missle.");
//	}
	//Press the right mouse button to use your shield to block enemy lazers
//	else if (sectionReady(0, "enemyHitByMissile"))
//	{
//		changeCommLinkMsg("Nice!");
//		attackMode = 0;
//	}
}

void Tutorial::init()
{
	uiHub = (UIHub*) game->getEntity("uiHub");
	commLink = uiHub->getCommLink();
	raceHub = (RaceHub*) game->getEntity("raceHub");
	xacadiens = (AlienRace*) raceHub->getRace("xacadiens");
	sentient = raceHub->getSentient();
}

void Tutorial::event(string name)
{
	if (name==name)
	{
		completeAction(name);
	}
	if (name=="enemyDestroyed")
	{
		attackCount = 0;
	}
	if (name=="enemyCrashed")
	{
		attackCount = maxAttackCount-2;
	}
	if (name=="nearTarget")
	{
		//Printer::print("NEARTARGET\n");
	}
	if (name=="allDead" && !noReset)
	{

		reset(100);
	}
	if (name=="XaccyKilled")
	{
		Printer::print("\nXACCYKILLED\n");
	}
}

bool Tutorial::sectionReady(int timePassed,
		string actionRequired)
{

	bool result = false;
	if (count<=1 && section==sectionCount)
	{
		resetAction(actionRequired);
	}

	if (section==sectionCount && count>timePassed && isActionCompleted(actionRequired))
	{
		result = true;
		section++;
		count = 0;
	}
	sectionCount++;

	return result;
}

void Tutorial::commLinkMsg(string msg, float displayTime)
{
	commLink->showMessage(msg,displayTime);
}

void Tutorial::changeCommLinkMsg(string msg)
{
	commLink->changeMessage(msg);
}

void Tutorial::turnOffCommLink()
{
	commLink->turnOff();
}

bool Tutorial::forceSection(string actionRequired)
{
	bool result = false;
	if (count<=1 && section==sectionCount)
	{
		resetAction(actionRequired);
	}

	if (isActionCompleted(actionRequired))
	{
		result = true;
		section++;
		count = 0;
	}
	sectionCount++;

	return result;
}

void Tutorial::updateAttack()
{
	if (attackMode==1)
	{
		attackCount++;
		if (xacadiens->getShipCount() < 1 && attackCount>maxAttackCount)
		{
			attackCount = 0;
			maxAttackCount = 500;
			Ship* player = raceHub->getPlayerShip();
			vec3 playerPos = player->getPosition();
			vec3 pos = playerPos + Dice::roll(vec3(-WIDTH2,HEIGHT2*2,0),vec3(WIDTH2,HEIGHT2*2,0));
			Ship* scout = xacadiens->addScout();
			scout->setPosition(pos);
			game->event("scoutAttacking");
		}
	}
	if (attackMode==2)
	{
		attackCount++;
		if (attackCount>maxAttackCount)
		{
			attackCount = 0;
			maxAttackCount = 400;
			Ship* player = raceHub->getPlayerShip();
			vec3 playerPos = player->getPosition();
			for (int i=0; i<5; i++)
			{
				vec3 pos = playerPos + Dice::roll(vec3(WIDTH,-HEIGHT2,0),vec3(WIDTH,HEIGHT2,0));
				Ship* scout = xacadiens->addScout();
				scout->setPosition(pos);
			}

		}
	}
}

void Tutorial::reset(int count)
{
	isActive = true;
	resetCount = count;
	uiHub->toggleHud(false);
	turnOffCommLink();
	uiHub->removeTarget(0);
	uiHub->removeTarget(1);
	sentient->enableDocking(false);
}



void Tutorial::turnOff()
{
	isActive = false;
	uiHub->toggleHud(true);
	turnOffCommLink();
	uiHub->removeTarget(0);
	uiHub->removeTarget(1);
	sentient->enableDocking(true);
}
