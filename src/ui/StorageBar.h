/*
 * StorageBar.h
 *
 *  Created on: 16 Apr 2014
 *      Author: Adam Nasralla
 */

#ifndef CLONESPACEOLD_SRC_STORAGEBAR_H_
#define CLONESPACEOLD_SRC_STORAGEBAR_H_

#include <osogine/game/Entity.h>

class StorageBar: public Entity
{
public:
	StorageBar(Entity* parent, float x1, float y1, float width1, float height1, int capacity1);
	~StorageBar();
	void updateGems(EntityList* gems1);

private:
	EntityList* gems;
	int capacity;
	float gemSize;
	float spacing;
	float width;
	float height;
	int noGems;
	void updateGfx();
	void initGfx();
};

#endif /* CLONESPACEOLD_SRC_STORAGEBAR_H_ */
