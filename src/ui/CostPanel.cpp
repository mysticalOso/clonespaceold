/*
 * CostPanel.cpp
 *
 *  Created on: 27 Feb 2015
 *      Author: Adam Nasralla
 */

#include <builders/UIBuilder.h>
#include <osogine/game/Game.h>
#include <osogine/utils/Dice.h>
#include <space/Gem.h>
#include <ui/CostBar.h>
#include <ui/CostPanel.h>

CostPanel::CostPanel(Entity* parent) : UIEntity(parent)
{

	UIBuilder* builder = (UIBuilder*) game->getBuilder("ui");
	costs = builder->buildCostBars(this);
	stocks = builder->buildCostBars(this);

	for (int i=0; i<stocks->size(); i++)
	{
		CostBar* bar = (CostBar*) stocks->get(i);
		bar->setValue(Dice::roll(0.0,1.0));
	}
	first = true;
	count = 0;
}

CostPanel::~CostPanel()
{
	// TODO Auto-generated destructor stub
}

void CostPanel::update()
{
	UIEntity::update();
//	count++;
//	if (count==150)
//	{
//
//
//
//	}
}




void CostPanel::setTextureAlpha(float a)
{
	TextureEntity::setTextureAlpha(a);
	stocks->setAlpha(a*0.25f);
	costs->setAlpha(a*0.5f);
}

void CostPanel::initCosts()
{
	for (int i=0; i<costs->size(); i++)
	{
		CostBar* bar = (CostBar*) costs->get(i);
		bar->setValue(Dice::roll(0.0,0.5));
	}
}

void CostPanel::randomCost()
{
//	if (first)
//	{
//		for (int i=0; i<costs->size(); i++)
//		{
//			CostBar* bar = (CostBar*) costs->get(i);
//			bar->setValue(Dice::roll(0.0,0.5));
//		}
//	}
//	first = false;
}

void CostPanel::setStock(EntityList* gems)
{

	for (int i=0; i<stocks->size(); i++)
	{
		CostBar* bar = (CostBar*) stocks->get(i);
		bar->setValue(0);
	}

	if (gems!=0)
	{
		for (int i=0; i<gems->size(); i++)
		{
			Gem* gem = (Gem*) gems->get(i);
			int colour = gem->getColour();
			CostBar* bar = (CostBar*) stocks->get(colour);
			bar->addValue();
		}
	}

}
