/*
 * AsteroidOutline.h
 *
 *  Created on: 21 Nov 2014
 *      Author: mysticalOso
 */

#ifndef ASTEROIDOUTLINE_H_
#define ASTEROIDOUTLINE_H_

#include <glm/vec4.hpp>
#include <osogine/game/Entity.h>

class Polygon;

class AsteroidOutline: public Entity {
public:
	AsteroidOutline(Game* game, bool isLoop);
	virtual ~AsteroidOutline();

	void setOutline(Polygon* outline);
	void render();

	void update();

	void setCollider(AsteroidOutline* c){collider = c;}


private:
	void setColour(vec4 colour);

	AsteroidOutline* collider;
};

#endif /* ASTEROIDOUTLINE_H_ */
