/*
 * AsteroidTest.cpp
 *
 *  Created on: 18 Dec 2014
 *      Author: mysticalOso
 */

#include <test/AsteroidOutline.h>
#include <test/AsteroidTest.h>
#include <builders/AsteroidBuilder.h>
#include <glm/vec2.hpp>
#include <osogine/game/Game.h>
#include <osogine/gfx/LinkedMesh.h>
#include <osogine/utils/Dice.h>
#include <osogine/utils/List.h>
#include <osogine/utils/Polygon.h>



AsteroidTest::AsteroidTest(Game* game) : Entity(game)
{
	AsteroidBuilder* builder = new AsteroidBuilder(game);
	gfx = builder->buildGfx();
	mesh = new LinkedMesh(gfx);
	Polygon* p = mesh->getOutline();
	count = 0;
	setOutline(p);

	aOutline = new AsteroidOutline(game,true);
	aOutline->setOutline(p);
	addChild(aOutline);

	setPosition(500,0,0);

	setProgram("flat");
}

AsteroidTest::~AsteroidTest() {
	// TODO Auto-generated destructor stub
}

void AsteroidTest::update()
{
	//Entity::update();
	count++;
	int noVerts = aOutline->getOutline()->size();
	//Printer::print("No verts = ",noVerts,"\n");

	if (count == 3 && noVerts>0 )
	{
		count = 0;
		//Printer::print("No verts = ",noVerts,"\n");
		int v = Dice::roll(0,noVerts-1);
		vec2 vert = aOutline->getOutline()->get(v);
		//Printer::print("Removing no = ",v,"\n");
		mesh->removeTrianglesNear( vert );
		mesh->updateGfx();

		Polygon* p = mesh->getOutline();
		aOutline->setOutline(p);
	}


}
