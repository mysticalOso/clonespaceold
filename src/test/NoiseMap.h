#ifndef NOISEMAP_H_
#define NOISEMAP_H_

#include "XYMap.h"

class NoiseMap : public XYMap {

public:
    virtual ~NoiseMap() {}
    NoiseMap(int sizeX, int sizeY);

    void generateNoiseMap(int octaves, float upperBound, float lowerBound, float persistence, float scale, bool ridged);
    void generateDistributedBlobs(int numberOfBlobs, float clampMaxHeight, float averageRadius, float variation, float additiveChance, bool bounded);
    void generateClumpedBlobs(int numberOfClumps, int numberOfBlobs, float clampMaxHeight, float averageRadius, float variation, float additiveChance);
    void scaleMinMax(float min, float max);

    void subtractMaps(NoiseMap* mapToSubtract);
    void addMaps(NoiseMap* mapToAdd);
    void multiplyMaps(NoiseMap* mapToMultiply);

    float getMaxHeight();
    float getMinHeight();
    void convertToContourMap(int numberOfContours);


private:

    float multiply(float bottomLayer, float topLayer);
    float add(float bottomLayer, float topLayer);
    void createBlob(int xCentre, int yCentre, float blobRadius, float additiveChance);
    float scaleValue(float valueToScale, float currentMin, float currentMax, float newMin, float newMax);
    void scaleToMaxHeight(float clampMaxHeight);
    int setBlobRadius(float averageRadius, float variation);
};

#endif /* NOISEMAP_H_ */
