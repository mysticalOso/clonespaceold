/*
 * AsteroidOutline.cpp
 *
 *  Created on: 21 Nov 2014
 *      Author: mysticalOso
 */

#include <test/AsteroidOutline.h>
#include <builders/AsteroidBuilder.h>
#include <glm/vec3.hpp>
#include <osogine/game/Game.h>
#include <osogine/gfx/GfxObject.h>
#include <osogine/utils/Dice.h>
#include <osogine/utils/List.h>
#include <osogine/utils/Polygon.h>
#include <osogine/utils/Printer.h>
#include <osogine/utils/Utils.h>

AsteroidOutline::AsteroidOutline(Game* game, bool isLoop) : Entity(game)
{
	AsteroidBuilder* builder = new AsteroidBuilder(game);

	List<vec2>* outline = builder->buildOutline();

	gfx = new GfxObject();

	for (int i = 0; i < outline->size(); i++)
	{
		gfx->addVertex(vec3(outline->get(i),0));
		gfx->addColour(vec4(1,1,1,1));
	}

	gfx->fillLines(isLoop);

	setOutline(new Polygon(outline));

	this->outline->setIsLine(!isLoop);



	//gfx->setColour(vec4(1,1,1,1));

	setProgram("lines");

	collider = 0;


}

AsteroidOutline::~AsteroidOutline()
{

}

void AsteroidOutline::render()
{
	Entity::render();
}

void AsteroidOutline::update()
{
	Entity::update();
	if (collider!=0)
	{
		if (collider->collideWith(this))
		{
			setColour(vec4(1,1,0,1));
		}
		else
		{
			setColour(vec4(0,0,1,1));
		}
	}
}

void AsteroidOutline::setOutline(Polygon* outline)
{
	//if (gfx!=0) //delete gfx;
	if (this->outline!=0) //delete this->outline;

	gfx = new GfxObject();

	for (int i = 0; i < outline->size(); i++)
	{
		gfx->addVertex(vec3(outline->get(i),0));
		gfx->addColour(vec4(1,1,1,1));
	}

	gfx->fillLines(true);

	Entity::setOutline(outline);
}

void AsteroidOutline::setColour(vec4 colour)
{
	gfx->setColour(colour);
}
