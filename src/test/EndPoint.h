/*
 * EndPoint.h
 *
 *  Created on: 21 Feb 2015
 *      Author: Adam Nasralla
 */

#ifndef SRC_TEST_ENDPOINT_H_
#define SRC_TEST_ENDPOINT_H_

#include <glm/vec2.hpp>

class Segment;

class EndPoint
{
public:
	EndPoint(){begin = false; segment=0; angle=0; x=0; y=0;}
	EndPoint(float x1, float y1){begin = false; segment=0; angle=0; x=x1; y=y1;}
	~EndPoint(){}


	float x;
	float y;
	bool begin;
	Segment* segment;
	float angle;

};

#endif /* SRC_TEST_ENDPOINT_H_ */
