#include <osogine/utils/Dice.h>
#include <osogine/utils/Printer.h>
#include "NoiseMap.h"
#include <osogine/utils/SimplexNoise.h>
#include <cmath>

using namespace std;

NoiseMap::NoiseMap(int sizeX, int sizeY) : XYMap(sizeX, sizeY)
{
}

float NoiseMap::multiply(float bottomLayer, float topLayer)
{
    float finalValue = topLayer * bottomLayer;
    return finalValue;
}

float NoiseMap::add(float bottomLayer, float topLayer)
{
    float finalValue = topLayer + bottomLayer;
    return finalValue;
}

void NoiseMap::createBlob(int xCentre, int yCentre, float blobRadius, float additiveChance)
{
    float diceRoll = Dice::roll(0.0, 1.0);
//    Printer::print("Roll = ",diceRoll,"\n");
//    Printer::print("xCentre",xCentre,"\n");
//    Printer::print("yCentre = ",yCentre,"\n");
//    Printer::print("blobRadius = ",blobRadius,"\n");
//    Printer::print("additiveChance = ",additiveChance,"\n");
    for (int x = 0; x < getXSize(); ++x) {
        for (int y = 0; y < getYSize(); ++y) {
            float distance = sqrtf((x - xCentre) * (x - xCentre) + (y - yCentre) * (y - yCentre));
            float height = (blobRadius-distance)/blobRadius;
            if (0 > height) {
                height = 0;
            }
            if (additiveChance > diceRoll) {
                height = height + getValueAtXY(x, y);
                setValueAtXY(height, x, y);
            } else {
                height = getValueAtXY(x, y) - height;
                if (height < 0) {
                    setValueAtXY(0, x, y);
                } else {
                    setValueAtXY(height, x, y);
                }
            }
        }
    }
}

void NoiseMap::scaleToMaxHeight(float clampMaxHeight)
{
    float maxHeight = getMaxHeight();
    for (int x = 0; x < getXSize(); ++x) {
        for (int y = 0; y < getYSize(); ++y) {
            setValueAtXY(scaleValue(getValueAtXY(x, y), 0, maxHeight, 0, clampMaxHeight), x, y);
        }
    }
}

int NoiseMap::setBlobRadius(float averageRadius, float variation)
{
    return (int) (averageRadius + Dice::roll((-averageRadius * variation), (averageRadius * variation)));;
}

void NoiseMap::convertToContourMap(int numberOfContours)
{
    float newHeight;
    for (int x = 0; x < getXSize(); ++x) {
        for (int y = 0; y < getYSize(); ++y) {
            newHeight = ceil(numberOfContours * getValueAtXY(x, y)) / numberOfContours;
            setValueAtXY(newHeight, x, y);
        }
    }
}

float NoiseMap::scaleValue(float valueToScale, float currentMin, float currentMax, float newMin, float newMax)
{
    float newValue = (((newMax - newMin)*(valueToScale - currentMin)) / (currentMax - currentMin)) + newMin;
    return newValue;
}

float NoiseMap::getMaxHeight()
{
    float maxHeight = 0;
    for (int x = 0; x < getXSize(); ++x) {
        for (int y = 0; y < getYSize(); ++y) {
            if (getValueAtXY(x, y) > maxHeight)
                maxHeight = getValueAtXY(x, y);
        }
    }
    return maxHeight;
}

float NoiseMap::getMinHeight()
{
    float minHeight = getValueAtXY(0, 0);
    for (int x = 0; x < getXSize(); ++x) {
        for (int y = 0; y < getYSize(); ++y) {
            if (getValueAtXY(x, y) < minHeight)
                minHeight = getValueAtXY(x, y);
        }
    }
    return minHeight;
}

void NoiseMap::generateNoiseMap(int octaves, float upperBound, float lowerBound, float persistence, float scale, bool ridged)
{
    float offset = Dice::roll(-1000.0, 1000.0);
    for (int x = 0; x < getXSize(); ++x) {
        for (int y = 0; y < getYSize(); ++y) {
            float noise = 0;
            if (!ridged) {
                noise = abs(SimplexNoise::octave(octaves, x + offset, y + offset, persistence, scale, lowerBound, upperBound));
            } else {
                noise = SimplexNoise::ridged(octaves, x + offset, y + offset, persistence, scale, lowerBound, upperBound);
            }
            setValueAtXY(noise, x, y);
        }
    }
}

void NoiseMap::generateDistributedBlobs(int numberOfBlobs, float clampMaxHeight, float averageRadius, float variation, float additiveChance, bool bounded)
{
    for (int blob = 0; blob < numberOfBlobs; ++blob) {
        int blobRadius = setBlobRadius(averageRadius, variation);

        int lowBound=0;
        int xHighBound= getXSize();
        int yHighBound= getYSize();

        if (bounded) {
            lowBound = blobRadius;
            xHighBound = getXSize() - blobRadius;
            yHighBound = getYSize() - blobRadius;
        }

        int xCentre = (int) Dice::roll(lowBound, xHighBound);
        int yCentre = (int) Dice::roll(lowBound, yHighBound);

        createBlob(xCentre, yCentre, blobRadius, additiveChance);
    }

    scaleToMaxHeight(clampMaxHeight);
}

void NoiseMap::generateClumpedBlobs(int numberOfClumps, int numberOfBlobs, float clampMaxHeight, float averageRadius, float variation, float additiveChance)
{
    for (int clump = 0; clump < numberOfClumps; ++clump) {
        int clumpRadius = (int) (averageRadius + (averageRadius * variation));
        int xyClumpLowBound = clumpRadius;
        int xClumpHighBound = getXSize() - clumpRadius;
        int yClumpHighBound = getYSize() - clumpRadius;

        int currentXCentre = (int) Dice::roll(xyClumpLowBound, xClumpHighBound);
        int currentYCentre = (int) Dice::roll(xyClumpLowBound, yClumpHighBound);

        for (int blob = 0; blob < numberOfBlobs; ++blob) {
            int blobRadius = setBlobRadius(averageRadius, variation);
            int xLowBound = currentXCentre - blobRadius;
            int yLowBound = currentYCentre - blobRadius;
            int xHighBound = currentXCentre + blobRadius;
            int yHighBound = currentYCentre + blobRadius;

            // this makes sure that the bounds will not create blobs that go off the map.
            xLowBound = (xLowBound < blobRadius) ? blobRadius : xLowBound;
            yLowBound = (yLowBound < blobRadius) ? blobRadius : yLowBound;
            xHighBound = (xHighBound > getXSize() - blobRadius) ? blobRadius : xHighBound;
            yHighBound = (yHighBound > getYSize() - blobRadius) ? blobRadius : yHighBound;

            int xCentre = (int) Dice::roll(xLowBound, xHighBound);
            int yCentre = (int) Dice::roll(yLowBound, yHighBound);

            createBlob(xCentre, yCentre, blobRadius, additiveChance);
        }
    }

    scaleToMaxHeight(clampMaxHeight);
}

void NoiseMap::addMaps(NoiseMap *mapToAdd)
{
    for (int x = 0; x < getXSize(); ++x) {
        for (int y = 0; y < getYSize(); ++y) {
            float newValue = add(mapToAdd->getValueAtXY(x, y), getValueAtXY(x, y));
            setValueAtXY(newValue, x, y);

        }
    }
}

void NoiseMap::multiplyMaps(NoiseMap *mapToMultiply)
{
    for (int x = 0; x < getXSize(); ++x) {
        for (int y = 0; y < getYSize(); ++y) {
            float newValue = multiply(mapToMultiply->getValueAtXY(x, y), getValueAtXY(x, y));
            setValueAtXY(newValue, x, y);
        }
    }
}

void NoiseMap::scaleMinMax(float min, float max)
{
    float currentMax = getMaxHeight();
    float currentMin = getMinHeight();
    for (int x = 0; x < getXSize(); ++x) {
        for (int y = 0; y < getYSize(); ++y) {
            float newValue = scaleValue(getValueAtXY(x, y), currentMin, currentMax, min, max);
        }
    }
}

void NoiseMap::subtractMaps(NoiseMap *mapToSubtract)
{
    for (int x = 0; x < getXSize(); ++x) {
        for (int y = 0; y < getYSize(); ++y) {
            float newValue =  getValueAtXY(x, y) - mapToSubtract->getValueAtXY(x,y);
            if (newValue<0) newValue = 0;
            setValueAtXY(newValue, x, y);
        }
    }
}
