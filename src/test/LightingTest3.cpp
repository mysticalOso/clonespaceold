/*
 * LightingTest3.cpp
 *
 *  Created on: 24 Feb 2015
 *      Author: Adam Nasralla
 */

#include <builders/InteriorBuilder.h>
#include <builders/LightBuilder.h>
#include <core/CameraCS.h>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <interior/Interior.h>
#include <osogine/game/Camera.h>
#include <osogine/game/Game.h>
#include <osogine/utils/Polygon.h>
#include <test/LightingTest3.h>


LightingTest3::LightingTest3(Entity* parent) : Entity(parent)
{
	interior = 0;
	InteriorBuilder* builder = new InteriorBuilder(game);

	outline = new Polygon();
	outline->add(glm::vec2(0,20));
	outline->add(glm::vec2(7,0));
	outline->add(glm::vec2(15,-10));
	outline->add(glm::vec2(0,-7));
	outline->add(glm::vec2(-15,-10));
	outline->add(glm::vec2(-7,0));



//
//	interior = builder->buildInterior(this);
//	interior->setScale(1);
//	interior->setPosition(10,0,0);
//	interior->rotateZ(10);
//
//	Camera* cam = game->getCamera();
//	cam->setPosition(0, 0, 2000);


//	LightBuilder* lightBuilder = new LightBuilder(game);
//	lightBuilder->setRoomHub( interior->getRoomHub() );
//	lightBuilder->refresh();
//
//	lightBuilder->buildLight(vec3(0,0,0),vec3(1,1,0));
}

LightingTest3::~LightingTest3()
{
	// TODO Auto-generated destructor stub
}

