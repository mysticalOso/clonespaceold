/*
 * Segment.h
 *
 *  Created on: 21 Feb 2015
 *      Author: Adam Nasralla
 */

#ifndef SRC_TEST_SEGMENT_H_
#define SRC_TEST_SEGMENT_H_

class EndPoint;

class Segment
{
public:
	Segment(){p1 = 0; p2 = 0; d = 0;}
	~Segment(){}

	EndPoint* p1;
	EndPoint* p2;
	float d;

};

#endif /* SRC_TEST_SEGMENT_H_ */
