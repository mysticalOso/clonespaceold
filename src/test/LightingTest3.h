/*
 * LightingTest3.h
 *
 *  Created on: 24 Feb 2015
 *      Author: Adam Nasralla
 */

#ifndef SRC_TEST_LIGHTINGTEST3_H_
#define SRC_TEST_LIGHTINGTEST3_H_

#include <osogine/game/Entity.h>

class Interior;

class LightingTest3 : public Entity
{
public:
	LightingTest3(Entity* parent);
	~LightingTest3();

private:
	Interior* interior;
};

#endif /* SRC_TEST_LIGHTINGTEST3_H_ */
