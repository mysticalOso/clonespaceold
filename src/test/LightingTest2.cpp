/*
 * LightingTest2.cpp
 *
 *  Created on: 23 Feb 2015
 *      Author: Adam Nasralla
 */

#include <builders/CloneBuilder.h>
#include <builders/LightBuilder.h>
#include <clone/Clone.h>
#include <core/CloneGfxP.h>
#include <interior/Walls.h>
#include <osogine/game/Game.h>
#include <osogine/game/IEntity.h>
#include <osogine/game/Light.h>
#include <test/LightingTest2.h>

LightingTest2::LightingTest2(Entity* parent) : Entity(parent)
{
	setController("simple");

	CloneBuilder* builder = new CloneBuilder(game);
	LightBuilder* lightBuilder = new LightBuilder(game);
	//lightBuilder->setRoomHub(this);
	lightBuilder->refresh();
	Light* light = lightBuilder->buildLight(vec3(100,100,100),vec3(0,0,0));
	light->setIsVisible(true);
	builder->init();
	clone = builder->buildSentientClone();
	clone->setScale(10);
	clone->setProgram("clone");
	program = (CloneGfxP*) clone->getProgram();

	program->addLight(light);

	light = lightBuilder->buildLight(vec3(-100,-100,100),vec3(1,0,1));
	program->addLight(light);

	clone->setRenderOrder(4);
	addChild(clone);

	initWalls();
}

LightingTest2::~LightingTest2()
{
	// TODO Auto-generated destructor stub
}

void LightingTest2::initWalls()
{

	walls = new Walls(this);
	float size = 10;
	std::vector<vec2> outline;
	outline.push_back(vec2(-24,0) * size);
	outline.push_back(vec2(-12,20) * size);
	outline.push_back(vec2(12,20) * size);
	outline.push_back(vec2(24,0) * size);
	outline.push_back(vec2(12,-20) * size);
	outline.push_back(vec2(-12,-20) * size);

	for (unsigned int i = 0; i < outline.size(); i++)
	{
		vec2 p = outline[i];
		walls->addExternal(p.x,p.y);
		walls->addGap(i,0.5,50);
	}

	walls->setWidth(10);
	walls->init();

	walls2 = new Walls(this);
	size = 10;
	outline.clear();
	outline.push_back(vec2(-24,0) * size);
	outline.push_back(vec2(-12,20) * size);
	outline.push_back(vec2(12,20) * size);
	outline.push_back(vec2(24,0) * size);
	outline.push_back(vec2(12,-20) * size);
	outline.push_back(vec2(-12,-20) * size);

	for (unsigned int i = 0; i < outline.size(); i++)
	{
		vec2 p = outline[i];
		walls2->addExternal(p.x,p.y);
		walls2->addGap(i,0.5,50);
	}

	walls2->setWidth(10);
	walls2->init();
	walls2->setPosition(550,0,0);

}

void LightingTest2::setTarget(float x, float y)
{
	clone->setPosition(vec3(x,y,0));
	clone->setTarget(x,y+10,0);
}
