/*
 * LightingTest.h
 *
 *  Created on: 21 Feb 2015
 *      Author: Adam Nasralla
 */

#ifndef SRC_TEST_LIGHTINGTEST_H_
#define SRC_TEST_LIGHTINGTEST_H_

#include <osogine/game/TextureEntity.h>
#include <glm/vec2.hpp>

class Segment;
class EndPoint;
class Walls;
class Clone;
class Light;
class CloneGfxP;

class LightingTest: public TextureEntity
{
public:
	LightingTest(Entity* parent);
	~LightingTest();

	void setTarget(float x, float y);
	//void render();

private:
	void initWalls();
	void initSegments();
	void updateAngles(float x, float y);

	void addSegment(float x1, float y1, float x2, float y2);
	void updateSegment(int index, float x1, float y1, float x2, float y2);

	bool segmentInFront(Segment a, Segment b, vec2 relativeTo);

	bool leftOf(Segment s, vec2 p);
	vec2 lineIntersection(vec2 p1, vec2 p2, vec2 p3, vec2 p4);

	void addTriangle(float angle1, float angle2, Segment* segment);

	void sweep();

	void updateEdgeSegments();

	void addUVs();

	vec2 clamp(vec2 p, vec2 topRight, vec2 bottomLeft);

	vec2 center;
	List<EndPoint*> endPoints;
	List<Segment*> segments;
	List<Segment*> open;
	Walls* walls;
	Walls* walls2;
	Clone* clone;
	CloneGfxP* program;
	Light* light;

};

#endif /* SRC_TEST_LIGHTINGTEST_H_ */
