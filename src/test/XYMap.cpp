#include "XYMap.h"

XYMap::XYMap(int xSize, int ySize)
{
    XYMap::xSize = xSize;
    XYMap::ySize = ySize;
    mapVector.resize((unsigned long)(xSize * ySize));
    mapVector.clear();
}

float XYMap::getValueAtXY(int xCoord, int yCoord)
{
    return mapVector[yCoord*xSize+xCoord];
}

void XYMap::setValueAtXY(float value, int xCoord, int yCoord)
{
    mapVector[yCoord*xSize+xCoord] = value;
}


