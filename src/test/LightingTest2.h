/*
 * LightingTest2.h
 *
 *  Created on: 23 Feb 2015
 *      Author: Adam Nasralla
 */

#ifndef SRC_TEST_LIGHTINGTEST2_H_
#define SRC_TEST_LIGHTINGTEST2_H_

#include <osogine/game/Entity.h>

class Clone;

class LightingTest2: public Entity
{
public:
	LightingTest2(Entity* parent);
	~LightingTest2();

	void initWalls();
	void setTarget(float x, float y);

private:
	Clone* clone;
	CloneGfxP* program;
	Walls* walls;
	Walls* walls2;
};

#endif /* SRC_TEST_LIGHTINGTEST2_H_ */
