/*
 * SpaceTest.cpp
 *
 *  Created on: 6 Jan 2015
 *      Author: mysticalOso
 */

#include <clone/CloneModel.h>
#include <hubs/GemHub.h>
#include <osogine/gfx/ObjLoader/ObjLoader.h>
#include <races/Xaccy.h>
#include <races/XaccyPart.h>
#include <space/Space.h>
#include <test/NoiseTest.h>
#include <test/OutlineTest.h>
#include <test/SpaceTest.h>
#include <ui/HexScreen.h>
#include <ui/Hud.h>

SpaceTest::SpaceTest(Space* space) : Entity(space)
{
	this->space = space;
//	CloneModel* model = new CloneModel(this);
//	model->setArmed(false);
//	NoiseTest* test = new NoiseTest(this);
//	OutlineTest* test = new OutlineTest(this);

	//Xaccy* xaccy = new Xaccy(this);
	//xaccy->setTextureAlpha(1);

///	HexScreen* hex = new HexScreen(this);
	//hex->show();



}

SpaceTest::~SpaceTest()
{

}

void SpaceTest::init()
{
	Hud* hud = new Hud(this);

	EntityList* gems = 0;

	GemHub* gemHub = space->getGemHub();

	gemHub->addGems(6);

	gems = gemHub->getGems();

	hud->setGems(gems);
}
