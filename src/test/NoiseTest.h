/*
 * NoiseTest.h
 *
 *  Created on: 28 Feb 2015
 *      Author: Adam Nasralla
 */

#ifndef SRC_TEST_NOISETEST_H_
#define SRC_TEST_NOISETEST_H_

#include <osogine/game/TextureEntity.h>

class NoiseMap;
class DynamicTexture;

class NoiseTest: public TextureEntity
{
public:
	NoiseTest(Entity* parent);
	~NoiseTest();

	void buildSimpleTexture();
	void drawOutline();

	void update();

	bool isEdgePixel(int i, int j);

private:
	NoiseMap* noiseMap ;
	NoiseMap* blobMap ;
	NoiseMap* blobMap2 ;
	NoiseMap* blobMap3 ;
	TextureEntity* e;

	DynamicTexture* texture;
};

#endif /* SRC_TEST_NOISETEST_H_ */
