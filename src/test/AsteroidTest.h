/*
 * AsteroidTest.h
 *
 *  Created on: 18 Dec 2014
 *      Author: mysticalOso
 */

#ifndef ASTEROIDTEST_H_
#define ASTEROIDTEST_H_

#include <osogine/game/Entity.h>

class AsteroidOutline;
class LinkedMesh;

class AsteroidTest : public Entity{
public:
	AsteroidTest(Game* game);
	virtual ~AsteroidTest();

	void update();


private:
	AsteroidOutline* aOutline;
	LinkedMesh* mesh;
	int count;

};

#endif /* ASTEROIDTEST_H_ */
