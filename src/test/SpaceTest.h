/*
 * SpaceTest.h
 *
 *  Created on: 6 Jan 2015
 *      Author: mysticalOso
 */

#ifndef SRC_TEST_SPACETEST_H_
#define SRC_TEST_SPACETEST_H_

#include <osogine/game/Entity.h>

class Space;

class SpaceTest: public Entity {
public:
	SpaceTest(Space* space);
	virtual ~SpaceTest();
	void init();

private:
	Space* space;

};

#endif /* SRC_TEST_SPACETEST_H_ */
