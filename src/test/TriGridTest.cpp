/*
 * TriGridTest.cpp
 *
 *  Created on: 25 Feb 2015
 *      Author: Adam Nasralla
 */

#include <test/TriGridTest.h>
#include <ui/TriGrid.h>

TriGridTest::TriGridTest(Entity* parent) : Entity(parent)
{
	grid = new TriGrid(this);
	grid->init(4);
	grid->startGrow(10,10,true);

}

TriGridTest::~TriGridTest()
{
	// TODO Auto-generated destructor stub
}

