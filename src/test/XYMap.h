#include <vector>

#ifndef XYMAP_H_
#define XYMAP_H_

class XYMap
{
public:
    XYMap(int xSize, int ySize);
    virtual ~XYMap(){}

    int getXSize() { return xSize; }
    int getYSize() { return ySize; }

    float getValueAtXY(int xCoord, int yCoord);
    void setValueAtXY(float value, int xCoord, int yCoord);


private:
    int xSize;
    int ySize;
    std::vector<float> mapVector;

};

#endif /* XYMAP_H_ */