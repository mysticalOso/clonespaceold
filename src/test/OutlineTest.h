/*
 * OutlineTest.h
 *
 *  Created on: 4 Mar 2015
 *      Author: Adam Nasralla
 */

#ifndef SRC_TEST_OUTLINETEST_H_
#define SRC_TEST_OUTLINETEST_H_

#include <osogine/game/Entity.h>
#include <glm/vec2.hpp>

class OutlineTest: public Entity
{
public:
	OutlineTest(Entity* parent);
	~OutlineTest();

	void update();

private:
	vec2 g1;
	vec2 g2;
	vec2 g3;
};

#endif /* SRC_TEST_OUTLINETEST_H_ */
