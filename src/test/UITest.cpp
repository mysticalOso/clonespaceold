/*
 * UITest.cpp
 *
 *  Created on: 26 Feb 2015
 *      Author: Adam Nasralla
 */

#include <test/UITest.h>
#include <ui/ShipDesigner.h>

UITest::UITest(Entity* parent) : UIEntity(parent)
{
	setController("ui");
	new ShipDesigner(this);
}

UITest::~UITest()
{
	// TODO Auto-generated destructor stub
}

