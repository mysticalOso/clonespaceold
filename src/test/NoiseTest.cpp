/*
 * NoiseTest.cpp
 *
 *  Created on: 28 Feb 2015
 *      Author: Adam Nasralla
 */

#include <core/Constants.h>

#include <glm/vec3.hpp>

#include <glm/vec4.hpp>

#include <GL/glew.h>
#include <osogine/game/Entity.h>
#include <osogine/game/Game.h>
#include <osogine/game/IEntity.h>
#include <osogine/gfx/DynamicTexture.h>
#include <osogine/gfx/GfxBuilder.h>
#include <osogine/gfx/GfxHub.h>
#include <osogine/gfx/GfxObject.h>
#include <osogine/gfx/LinkedMesh.h>
#include <osogine/utils/Polygon.h>
#include <osogine/utils/Transform.h>
#include <test/NoiseMap.h>
#include <test/NoiseTest.h>
#include <cmath>

NoiseTest::NoiseTest(Entity* parent) : TextureEntity(parent)
{

	e = new TextureEntity(parent);
	float size = 1024;
	GfxBuilder* gfxBuilder = game->getGfxBuilder();
	GfxObject* gfx = gfxBuilder->buildQuad(size,size,vec4(1,1,1,1));
	e->setGfx( gfx );
	e->setProgram("texture");
	e->setTextureAlpha(1);
	e->setPosition(0,300,0);

	noiseMap = new NoiseMap(32,32);
	noiseMap->generateNoiseMap(8,1,0,0.5,0.05,true);


	textureColour = vec3(0.5,0.3,0.4);
	blobMap = new NoiseMap(32,32);
	blobMap->generateDistributedBlobs(6,1,8,0.3,1,true);

	//blobMap2 = new NoiseMap(32,32);
	//blobMap2->generateClumpedBlobs(20,10,1,2.5,0.2,1);

	blobMap2 = new NoiseMap(32,32);
	blobMap2->generateDistributedBlobs(20,0.35,3,0.3,1,true);

	//blobMap2 = new NoiseMap(128,128);
	//blobMap2->generateDistributedBlobs(100,10,1,10,0.2,1);

	//blobMap3 = new NoiseMap(128,128);
	//blobMap3->generateDistributedBlobs(5,0.2,10,0.5,1,true);

	setProgram("flat");

	this->gfx = new GfxObject();

	outline = new Polygon();
	outline->setTransform(new Transform());
	//outline->add(0,0);
	outline->setIsLine(false);
	setRenderOrder(1);

	//blobMap->multiplyMaps(blobMap2);
	//blobMap2->multiplyMaps(blobMap);

	blobMap->multiplyMaps(noiseMap);

	blobMap->subtractMaps(blobMap2);

	texture = new DynamicTexture();
	buildSimpleTexture();


}

NoiseTest::~NoiseTest()
{
	// TODO Auto-generated destructor stub
}

void NoiseTest::buildSimpleTexture()
{

	for (int i = 0; i < 32; i++)
	{
		  for (int j = 0; j < 32; j++)
		  {
			  GLubyte b = (GLubyte) (blobMap->getValueAtXY(i,j) * 255);
			  //b = 0;
			  if (b<1) b = 0;
			  else b = 255;
			  texture->pixels[i][j][0] = (GLubyte) b;
			  texture->pixels[i][j][1] = (GLubyte) b;
			  texture->pixels[i][j][2] = (GLubyte) b;
			  texture->pixels[i][j][3] = (GLubyte) 255;
		  }
	}
	GfxHub* gfxHub = game->getGfxHub();
	gfxHub->addDynamicTexture(texture,"test");
	e->setTexture("test");
	drawOutline();
}

void NoiseTest::drawOutline()
{
//	int x,y;
//	for (int i = 0; i < 128; i++)
//	{
//		  for (int j = 0; j < 128; j++)
//		  {
//			  int p = texture->pixels[i][j][0];
//			  if (p!=0)
//			  {
//				  x=i; y=j;
//				  i=128; j=128;
//			  }
//		  }
//	}
//	outline->add(y*4-256,x*4-256);
//	bool hit =( texture->pixels[x-1][y][0]>0 );
//	int a,b;
//	for (int n=0; n<5000; n++)
//	{
//		for (int i=1;i<8;i++)
//		{
//			if (i==1) {a=-3;b=-3;}
//			if (i==2) {a=0;b=-3;}
//			if (i==3) {a=3;b=-3;}
//			if (i==4) {a=3;b=0;}
//			if (i==5) {a=3;b=3;}
//			if (i==6) {a=0;b=3;}
//			if (i==7) {a=-3;b=3;}
//
//			if (!hit)
//			{
//				if (texture->pixels[x+a][y+b][0]>0) {
//					hit=true; i=8;
//					outline->add((y+a)*4-256,(x+b)*4-256);
//					x = x+a;
//					y = y+b;
//				}
//			}
//			else
//			{
//				if (texture->pixels[x+a][y+b][0]==0) {hit=false;}
//			}
//		}
//
//	}
	bool found;
	for (int i=0; i<20; i++)
	{
		found = false;
		float k = 360.0f/20.0f;
		float a = (float)i*k/180.0f;
		float dx = cos(PI * a);
		float dy = sin(PI * a);
		float x = 0;
		float y = 0;
		for (int j=20; j>0; j--)
		{
			x = round(dx * j);
			y = round(dy * j);
			int a = x + 16;
			int b = y + 16;
			if (a>31) a = 31;
			if (a<0) a = 0;
			if (b>31) b = 31;
			if (b<0) b = 0;

			if (texture->pixels[b][a][0] > 0)
			{
				j = 0;
				found = true;
			}
		}
		if (found) outline->add(x*10,y*10);
	}

	vec3 colour1 = vec3(0.5,0.3,0.1);
	vec3 colour2 = vec3(0.6,0.4,0.1);

	GfxBuilder* gfxBuilder = game->getGfxBuilder();
	gfx = gfxBuilder->buildTriMesh( outline->getOutline(), 200, colour1, colour2, true, true);

	gfx->reColour(vec4(colour1,1),vec4(colour2,1));

	for (uint i=0; i<gfx->getNoVertices(); i++)
	{
		vec3 v = gfx->getVertex(i);
		int a = round(v.x*0.1 +16);
		int b = round(v.y*0.1 +16);
		if (a>31) a = 31;
		if (a<0) a = 0;
		if (b>31) b = 31;
		if (b<0) b = 0;
		v.z *= (blobMap->getValueAtXY(b,a)+0.5)*4;
		gfx->setVertex(i,v);
	}
	LinkedMesh* mesh = new LinkedMesh(gfx);
	mesh->updateGfx();
}

void NoiseTest::update()
{
	rotateZ(0.01);
}

bool NoiseTest::isEdgePixel(int i, int j)
{
	bool isEdge = false;
	int x = 0;
	int y = 0;
	for (int a = -1; a <= 1; a++)
	{
		for (int b = -1; b <=1; b++)
		{
			x = i + a;
			y = j + b;
			if (x<0 || x>=128) isEdge = true;
			if (y<0 || y>=128) isEdge = true;
			if (blobMap2->getValueAtXY(x,y) < 10.5f/255.0f) isEdge = true;
		}
	}
	return isEdge;
}
