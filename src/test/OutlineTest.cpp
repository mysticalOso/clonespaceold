/*
 * OutlineTest.cpp
 *
 *  Created on: 4 Mar 2015
 *      Author: Adam Nasralla
 */

#include <osogine/gfx/GfxObject.h>
#include <osogine/utils/Polygon.h>
#include <osogine/utils/Transform.h>
#include <test/OutlineTest.h>


OutlineTest::OutlineTest(Entity* parent) : Entity(parent)
{
	setProgram("outline");
	gfx = new GfxObject();
	outline = new Polygon();
	outline->setTransform(new Transform());
	outline->add(100,0);
	outline->add(-100,100);
	outline->add(100,-100);
}

OutlineTest::~OutlineTest()
{
}

void OutlineTest::update()
{
}
