/*
 * TriGridTest.h
 *
 *  Created on: 25 Feb 2015
 *      Author: Adam Nasralla
 */

#ifndef SRC_TEST_TRIGRIDTEST_H_
#define SRC_TEST_TRIGRIDTEST_H_

#include <osogine/game/Entity.h>

class TriGrid;

class TriGridTest : public Entity
{
public:
	TriGridTest(Entity* parent);
	~TriGridTest();

private:
	TriGrid* grid;
};

#endif /* SRC_TEST_TRIGRIDTEST_H_ */
