/*
 * LightingTest.cpp
 *
 *  Created on: 21 Feb 2015
 *      Author: Adam Nasralla
 */

#include <builders/CloneBuilder.h>
#include <clone/Clone.h>
#include <core/CloneGfxP.h>
#include <glm/vec3.hpp>
#include <glm/vec2.hpp>
#include <interior/Walls.h>
#include <interior/WallSection.h>
#include <osogine/gfx/GfxObject.h>
#include <osogine/utils/Polygon.h>
#include <osogine/utils/Transform.h>
#include <test/EndPoint.h>
#include <test/LightingTest.h>
#include <test/Segment.h>
#include <core/Constants.h>
#include <osogine/game/Light.h>
#include <osogine/gfx/GfxProgram.h>
#include <osogine/utils/Printer.h>
#include <osogine/utils/Utils.h>
#include <algorithm>

LightingTest::LightingTest(Entity* parent) : TextureEntity(parent)
{
	setProgram("texture");
	setController("simple");
	setTexture("light");
	renderOrder = 3;
	outline = new Polygon();
	gfx = new GfxObject();
	textureAlpha = 0.7;
	initWalls();
	initSegments();

	CloneBuilder* builder = new CloneBuilder(game);
	builder->init();
	clone = builder->buildSentientClone();
	clone->setScale(10);
	clone->setProgram("clone");
	program = (CloneGfxP*) clone->getProgram();
	light = new Light(this);
	light->setColour(vec3(1,1,1));
	program->addLight(light);

	clone->setRenderOrder(4);
	addChild(clone);

}

LightingTest::~LightingTest()
{

}

void LightingTest::setTarget(float x, float y)
{
//	//delete outline;
//	outline = new Polygon();
//	outline->setIsLine(true);
//
//	GfxObject* g = walls->getGfx();
//
//	for (uint i=0; i<endPoints.size(); i++)
//	{
//		EndPoint* p = endPoints.get(i);
//		outline->add(x,y);
//
//		outline->add(p->x,p->y);
//		//outline->add(x,y);
//	}
	center = vec2(x,y);
	light->setLightPosition(vec3(x,y,100));
//	center = vec2(0,0);
//	light->setPosition(0,0,100);
	updateEdgeSegments();
	updateAngles(center.x,center.y);

//	clone->setPosition(x,y,0);
//	clone->setTarget(x,y+10,0);
	//clone->setPosition(center.x,center.y,0);

	sweep();
}

void LightingTest::initWalls()
{

	walls = new Walls(this);
	float size = 10;
	std::vector<vec2> outline;
	outline.push_back(vec2(-24,0) * size);
	outline.push_back(vec2(-12,20) * size);
	outline.push_back(vec2(12,20) * size);
	outline.push_back(vec2(24,0) * size);
	outline.push_back(vec2(12,-20) * size);
	outline.push_back(vec2(-12,-20) * size);

	for (unsigned int i = 0; i < outline.size(); i++)
	{
		vec2 p = outline[i];
		walls->addExternal(p.x,p.y);
		walls->addGap(i,0.5,50);
	}

	walls->setWidth(10);
	walls->init();

	walls2 = new Walls(this);
	size = 10;
	outline.clear();
	outline.push_back(vec2(-24,0) * size);
	outline.push_back(vec2(-12,20) * size);
	outline.push_back(vec2(12,20) * size);
	outline.push_back(vec2(24,0) * size);
	outline.push_back(vec2(12,-20) * size);
	outline.push_back(vec2(-12,-20) * size);

	for (unsigned int i = 0; i < outline.size(); i++)
	{
		vec2 p = outline[i];
		walls2->addExternal(p.x,p.y);
		walls2->addGap(i,0.5,50);
	}

	walls2->setWidth(10);
	walls2->init();
	walls2->setPosition(550,0,0);

}

void LightingTest::initSegments()
{
	EntityList* sections = walls->getSections();

	for (int i=0; i<sections->size(); i++)
	{
		WallSection* section = (WallSection*) sections->get(i);
		Polygon* outline = section->getOutline();
		outline->setTransform(walls->getTransform());
		for (int j=0; j<outline->size(); j++)
		{
			vec2 p = outline->getTransformed(j);
			vec2 p2 = outline->get( (j+1)%outline->size() );
			addSegment(p.x,p.y,p2.x,p2.y);
		}
	}

	sections = walls2->getSections();

	for (int i=0; i<sections->size(); i++)
	{
		WallSection* section = (WallSection*) sections->get(i);
		Polygon* outline = section->getOutline();
		outline->setTransform(walls2->getTransform());
		for (int j=0; j<outline->size(); j++)
		{
			vec2 p = outline->getTransformed(j);
			vec2 p2 = outline->getTransformed( (j+1)%outline->size() );
			addSegment(p.x,p.y,p2.x,p2.y);
		}
	}

	addSegment(-500,-500,500,-500);
	addSegment(500,-500,500,500);
	addSegment(500,500,-500,500);
	addSegment(-500,500,-500,-500);


}

bool compByAngleOld(EndPoint* a, EndPoint* b)
{
	return ((a->angle < b->angle) || (a->angle == b->angle && a->begin && !b->begin));
}

void LightingTest::updateAngles(float x, float y)
{
	for (int i=0; i<segments.size(); i++)
	{
		Segment* segment = segments.get(i);
		float dx = 0.5f * (segment->p1->x + segment->p2->x) - x;
		float dy = 0.5f * (segment->p1->y + segment->p2->y) - y;

		segment->d = dx*dx + dy*dy;

		// NOTE: future optimization: we could record the quadrant
		// and the y/x or x/y ratio, and sort by (quadrant,
		// ratio), instead of calling atan2. See
		// <https://github.com/mikolalysenko/compare-slope> for a
		// library that does this. Alternatively, calculate the
		// angles and use bucket sort to get an O(N) sort.
		// also remove doubles!

		segment->p1->angle = atan2(segment->p1->y - y, segment->p1->x - x);
		segment->p2->angle = atan2(segment->p2->y - y, segment->p2->x - x);

		//Printer::print("P1Angle", segment->p1->angle, "\n");
		//Printer::print("P2Angle", segment->p2->angle, "\n");

		float dAngle = segment->p2->angle - segment->p1->angle;
		if (dAngle <= -PI) { dAngle += 2*PI; }
		if (dAngle > PI) { dAngle -= 2*PI; }
		segment->p1->begin = (dAngle > 0.0);
		segment->p2->begin = !segment->p1->begin;
	}

//	for (int i=0; i<endPoints.size(); i++)
//	{
//		EndPoint* p = endPoints.get(i);
//		vec2 p1 = vec2(p->x,p->y);
//		//Printer::print("EndPoint = ",p1,"\n");
//	}

	vector<EndPoint*> toSort;

	for (int i=0; i<endPoints.size(); i++)
	{
		toSort.push_back(endPoints.get(i));
	}
	sort(toSort.begin(), toSort.end(), compByAngleOld);

	endPoints.setVector(toSort);

//	for (int i=0; i<segments.size(); i++)
//	{
//		Segment* segment = segments.get(i);
//		EndPoint* ep1 = segment->p1;
//		EndPoint* ep2 = segment->p2;
//		vec2 p1 = vec2(ep1->x,ep1->y);
//		vec2 p2 = vec2(ep2->x,ep2->y);
//		//Printer::print("Segment No = ",i,"\n");
//		//Printer::print("P1 = ",p1,"\n");
//		//Printer::print("P2 = ",p2,"\n");
//	}

//	Printer::print("Endpoints\n");
//	for (int i=0; i<endPoints.size(); i++)
//	{
//		Printer::print("Angle = ",endPoints.get(i)->angle,"\n");
//	}

}

void LightingTest::addSegment(float x1, float y1, float x2, float y2)
{

    Segment* segment = new Segment();
    EndPoint* p1 = new EndPoint();
    EndPoint* p2 = new EndPoint();

    p1->x = x1; p1->y = y1;
    p2->x = x2; p2->y = y2;

    p1->segment = segment;
    p2->segment = segment;
    segment->p1 = p1;
    segment->p2 = p2;
    segment->d = 0.0;

    segments.add(segment);
    endPoints.add(p1);
    endPoints.add(p2);

//    vec2 vp1 = vec2(p1->x,p1->y);
//   vec2 vp2 = vec2(p2->x,p2->y);
}

bool LightingTest::segmentInFront(Segment a, Segment b, vec2 relativeTo)
{
	vec2 p1 = vec2(b.p1->x,b.p1->y);
	vec2 p2 = vec2(b.p2->x,b.p2->y);
	bool A1 = leftOf(a, Utils::interpolate(p1, p2, 0.01));
	bool A2 = leftOf(a, Utils::interpolate(p2, p1, 0.01));
	bool A3 = leftOf(a, relativeTo);
	p1 = vec2(a.p1->x,a.p1->y);
	p2 = vec2(a.p2->x,a.p2->y);
	bool B1 = leftOf(b, Utils::interpolate(p1, p2, 0.01));
	bool B2 = leftOf(b, Utils::interpolate(p2, p1, 0.01));
	bool B3 = leftOf(b, relativeTo);

	// NOTE: this algorithm is probably worthy of a short article
	// but for now, draw it on paper to see how it works. Consider
	// the line A1-A2. If both B1 and B2 are on one side and
	// relativeTo is on the other side, then A is in between the
	// viewer and B. We can do the same with B1-B2: if A1 and A2
	// are on one side, and relativeTo is on the other side, then
	// B is in between the viewer and A.
	if (B1 == B2 && B2 != B3) return true;
	if (A1 == A2 && A2 == A3) return true;
	if (A1 == A2 && A2 != A3) return false;
	if (B1 == B2 && B2 == B3) return false;

	// If A1 != A2 and B1 != B2 then we have an intersection.
	// Expose it for the GUI to show a message. A more robust
	// implementation would split segments at intersections so
	// that part of the segment is in front and part is behind.
	return false;

}

bool LightingTest::leftOf(Segment s, vec2 p)
{
	float cross = (s.p2->x - s.p1->x) * (p.y - s.p1->y)
                          - (s.p2->y - s.p1->y) * (p.x - s.p1->x);
	return cross < 0;

}

vec2 LightingTest::lineIntersection(vec2 p1, vec2 p2, vec2 p3, vec2 p4)
{
//	if ( ((p4.y - p3.y) * (p2.x - p1.x) - (p4.x - p3.x) * (p2.y - p1.y)) == 0 )
//	{
//		float i = 0;
//	}
	float s = ((p4.x - p3.x) * (p1.y - p3.y) - (p4.y - p3.y) * (p1.x - p3.x))
                    / ((p4.y - p3.y) * (p2.x - p1.x) - (p4.x - p3.x) * (p2.y - p1.y));
	return vec2(p1.x + s * (p2.x - p1.x), p1.y + s * (p2.y - p1.y));
}

void LightingTest::addTriangle(float angle1, float angle2, Segment* segment)
{
    vec2 p1 = center;
    vec2 p2 = vec2(center.x + cos(angle1), center.y + sin(angle1));
    vec2 p3 = vec2(0.0, 0.0);
    vec2 p4 = vec2(0.0, 0.0);

    if (segment != 0) {
        // Stop the triangle at the intersecting segment
        p3.x = segment->p1->x;
        p3.y = segment->p1->y;
        p4.x = segment->p2->x;
        p4.y = segment->p2->y;
    } else {
        // Stop the triangle at a fixed distance; this probably is
        // not what we want, but it never gets used in the demo
        p3.x = center.x + cos(angle1) * 500;
        p3.y = center.y + sin(angle1) * 500;
        p4.x = center.x + cos(angle2) * 500;
        p4.y = center.y + sin(angle2) * 500;
    }

    vec2 pBegin = lineIntersection(p3, p4, p1, p2);

    p2.x = center.x + cos(angle2);
    p2.y = center.y + sin(angle2);
    vec2 pEnd = lineIntersection(p3, p4, p1, p2);

    vec2 topRight = center + vec2(500,500);
    vec2 bottomLeft = center - vec2(500,500);

    pBegin = clamp(pBegin,topRight,bottomLeft);
    pEnd = clamp(pEnd,topRight,bottomLeft);
    	gfx->addTriangle(vec3(center,0),vec3(pBegin,0),vec3(pEnd,0),vec4(0.3,0.3,0.5,1));

    if (!outline->hasPoint(pBegin)) outline->add(pBegin);
    if (!outline->hasPoint(pEnd)) outline->add(pEnd);

    //    outline->add(center);
//    outline->add(pBegin);
//    outline->add(pEnd);
//    outline->add(center);
}

void LightingTest::sweep()
{
	open.clear();
	//delete gfx;
	gfx = new GfxObject();

	//delete outline;
	outline = new Polygon();
	//outline->setIsLine(true);
	float beginAngle = 0;


	for (int pass = 0; pass < 2; pass++)
	{
		for (int i = 0; i < endPoints.size(); i++)
		{
			EndPoint p = *(endPoints.get(i));
			Segment* currentOld = 0;
			if (open.size()>0) currentOld = open.get(0);

            if (p.begin) {
                // Insert into the right place in the list
                Segment* node = 0;
    			if (open.size()>0) node = open.get(0);
                int index = 0;
                while (node != 0 && segmentInFront(*p.segment, *node, center) && index<open.size()-1)
                {
                	index++;
                    node = open.get(index);

                }
                if (node == 0) {
                    open.add(p.segment);
                } else {
                    open.insert(index, p.segment);
                }
            }
            else {
                open.remove(p.segment);
            }


            Segment* currentNew = 0;
            if (open.size()>0) currentNew = open.get(0);
            if (currentOld != currentNew) {
                if (pass == 1) {
                    addTriangle(beginAngle, p.angle, currentOld);
                }
                beginAngle = p.angle;
            }
		}
	}
	addUVs();

	//Printer::print("Outline size = ",outline->size(),"\n");

	program->setLightMap(outline);
	//gfx->print();
	//Printer::print("Centre ",center,"\n");
}

void LightingTest::updateSegment(int index, float x1, float y1, float x2,
		float y2)
{
    Segment* segment = segments.get(index);
    EndPoint* p1 = segment->p1;
    EndPoint* p2 = segment->p2;

    p1->x = x1; p1->y = y1;
    p2->x = x2; p2->y = y2;
}

void LightingTest::updateEdgeSegments()
{
	int n = segments.size();
	float cx = center.x;
	float cy = center.y;

	updateSegment(n-4,cx-500,cy-500,cx+500,cy-500);
	updateSegment(n-3,cx+500,cy-500,cx+500,cy+500);
	updateSegment(n-2,cx+500,cy+500,cx-500,cy+500);
	updateSegment(n-1,cx-500,cy+500,cx-500,cy-500);

}

void LightingTest::addUVs()
{
	vec2 topLeft = center - vec2(500,500);
	vec2 bottomRight = center + vec2(500,500);
	for (uint i=0; i<gfx->getNoVertices(); i++)
	{
		vec2 v = vec2(gfx->getVertex(i));
		vec2 uv = (v-topLeft) / (bottomRight-topLeft);
		gfx->addUV(uv);
	}
}

vec2 LightingTest::clamp(vec2 p, vec2 topRight, vec2 bottomLeft)
{
	p -= center;
	topRight -= center;
	bottomLeft -= center;
	if (p.x > topRight.x)
	{
		p.y *= (topRight.x / p.x);
		p.x = topRight.x;
	}
	if (p.y > topRight.y)
	{
		p.x *= (topRight.y / p.y);
		p.y = topRight.y;
	}
	if (p.x < bottomLeft.x)
	{
		p.y *= (bottomLeft.x / p.x);
		p.x = bottomLeft.x;
	}
	if (p.y < bottomLeft.y)
	{
		p.x *= (bottomLeft.y / p.y);
		p.y = bottomLeft.y;
	}
	p += center;
	return p;
}
