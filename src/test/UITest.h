/*
 * UITest.h
 *
 *  Created on: 26 Feb 2015
 *      Author: Adam Nasralla
 */

#ifndef SRC_TEST_UITEST_H_
#define SRC_TEST_UITEST_H_

#include <ui/UIEntity.h>

class UITest: public UIEntity
{
public:
	UITest(Entity* parent);
	~UITest();
};

#endif /* SRC_TEST_UITEST_H_ */
