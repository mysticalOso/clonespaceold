/*
 * WindowInterface.h
 *
 *  Created on: 30 Sep 2014
 *      Author: Adam Nasralla
 */

#ifndef IWINDOW_H_
#define IWINDOW_H_

#include <string>

using namespace std;

class IWindow
{
public:
	IWindow(){}
	virtual ~IWindow(){}

	virtual void init(string name, int width, int height, bool fullscreen){}

	virtual void swapBuffers(){}

	virtual bool shouldCloseWindow(){ return false; }

	virtual void terminate(){}
};

#endif /* IWINDOW_H_ */
