/*
 * SoundTest.cpp
 *
 *  Created on: 14 Jan 2014
 *      Author: Adam Nasralla
 */

#include "SoundInterface.h"
#include <osogine/Utils/Dice.h>
#include <core/Constants.h>

Mix_Music *SoundInterface::music = NULL;

Mix_Chunk *SoundInterface::asteroidHit1 = NULL;
Mix_Chunk *SoundInterface::asteroidHit2 = NULL;
Mix_Chunk *SoundInterface::asteroidHit3 = NULL;

Mix_Chunk *SoundInterface::shipHit1 = NULL;
Mix_Chunk *SoundInterface::shipHit2 = NULL;
Mix_Chunk *SoundInterface::shipHit3 = NULL;

Mix_Chunk *SoundInterface::gem1 = NULL;
Mix_Chunk *SoundInterface::gem2 = NULL;
Mix_Chunk *SoundInterface::gem3 = NULL;

Mix_Chunk *SoundInterface::asteroidExplode1 = NULL;
Mix_Chunk *SoundInterface::asteroidExplode2 = NULL;

Mix_Chunk *SoundInterface::shipExplode1 = NULL;
Mix_Chunk *SoundInterface::shipExplode2 = NULL;

Mix_Chunk *SoundInterface::teleport1 = NULL;

Mix_Chunk *SoundInterface::missile = NULL;

Mix_Chunk *SoundInterface::xaccy = NULL;

Mix_Chunk *SoundInterface::lazer = NULL;

Mix_Chunk *SoundInterface::death = NULL;

SoundInterface::SoundInterface()
{

}


void SoundInterface::init()
{

	    int audio_rate = 44100;
	    Uint16 audio_format = AUDIO_S16;
	    int audio_channels = 4;
	    int audio_buffers = 2048;

	    if(Mix_OpenAudio(audio_rate, audio_format, audio_channels, audio_buffers)) {
	      printf("Unable to open audio!\n");
	      exit(1);
	    }

	    /* Pre-load sound effects */
	    asteroidHit1 = Mix_LoadWAV("AHit.wav");
	    asteroidHit2 = Mix_LoadWAV("AHit2.wav");
	    asteroidHit3 = Mix_LoadWAV("AHit3.wav");

	    shipHit1 = Mix_LoadWAV("SHit.wav");
	    shipHit2 = Mix_LoadWAV("SHit2.wav");
	    shipHit3 = Mix_LoadWAV("SHit3.wav");

	    gem1 = Mix_LoadWAV("little-bell-c1.wav");
	    gem2 = Mix_LoadWAV("little-bell-e1.wav");
	    gem3 = Mix_LoadWAV("little-bell-gs-1.wav");

	    asteroidExplode1 = Mix_LoadWAV("AsteroidExplosion.wav");
	    asteroidExplode2 = Mix_LoadWAV("AsteroidExplosion2.wav");
	    shipExplode1 = Mix_LoadWAV("ShipExplosion.wav");
	    shipExplode2 = Mix_LoadWAV("ShipExplosion2.wav");

	    lazer = Mix_LoadWAV("lazer2.wav");
	    missile = Mix_LoadWAV("missile.wav");
	    xaccy = Mix_LoadWAV("xaccy.wav");

	    teleport1 = Mix_LoadWAV("TeleportNew.wav");

	    death = Mix_LoadWAV("death.wav");

	    if(Mix_OpenAudio(audio_rate, audio_format, audio_channels, audio_buffers)) {
	      printf("Unable to open audio!\n");
	      exit(1);
	    }

}

void SoundInterface::close()
{
}

void SoundInterface::asteroidHit()
{
	if (SOUND_ON)
	{
		int r = Dice::roll(1,3);
		//Mix_Volume(-1, 20);
		if (r==1) Mix_PlayChannel( -1, asteroidHit1, 0 );
		if (r==2) Mix_PlayChannel( -1, asteroidHit2, 0 );
		if (r==3) Mix_PlayChannel( -1, asteroidHit3, 0 );
	}
}

void SoundInterface::asteroidExplode()
{
	if (SOUND_ON)
	{
		int r = Dice::roll(1,2);
		if (r==1) Mix_PlayChannel( -1, asteroidExplode1, 0 );
		if (r==2) Mix_PlayChannel( -1, asteroidExplode2, 0 );
	}
}

void SoundInterface::teleport()
{
	//Mix_Volume(-1, 80);
	if (SOUND_ON) Mix_PlayChannel( -1, teleport1, 0 );
}



void SoundInterface::shipHit()
{
	if (SOUND_ON)
	{
		int r = Dice::roll(1,3);
		//Mix_Volume(-1, 20);
		if (r==1) Mix_PlayChannel( -1, shipHit1, 0 );
		if (r==2) Mix_PlayChannel( -1, shipHit2, 0 );
		if (r==3) Mix_PlayChannel( -1, shipHit3, 0 );
	}
}

void SoundInterface::shipExplode()
{
	if (SOUND_ON)
	{
		int r = Dice::roll(1,2);
		//Mix_Volume(-1, 20);
		if (r==1) Mix_PlayChannel( -1, shipExplode1, 0 );
		if (r==2) Mix_PlayChannel( -1, shipExplode2, 0 );
	}
}

void SoundInterface::fireLazer()
{
	if (SOUND_ON)
	{
		Mix_PlayChannel( -1, lazer, 0 );
	}
}

void SoundInterface::playXaccy()
{
	if (SOUND_ON)
	{
		Mix_PlayChannel( -1, xaccy, 0 );
	}
}

void SoundInterface::fireMissile()
{
	if (SOUND_ON)
	{
		Mix_PlayChannel( -1, missile, 0 );
	}
}

void SoundInterface::playDeath()
{
	if (SOUND_ON)
	{
		Mix_PlayChannel( -1, death, 0 );
	}
}

void SoundInterface::musicDone() {
  Mix_HaltMusic();
  Mix_FreeMusic(music);
  music = NULL;
}


SoundInterface::~SoundInterface()
{
}

void SoundInterface::collectGem()
{
	if (SOUND_ON)
	{
		int r = Dice::roll(1,3);
		if (r==1) Mix_PlayChannel( -1, gem1, 0 );
		if (r==2) Mix_PlayChannel( -1, gem2, 0 );
		if (r==3) Mix_PlayChannel( -1, gem3, 0 );
	}
}

void SoundInterface::cutSounds()
{
	Mix_ExpireChannel(1,1);
//    int audio_rate = 44100;
//    Uint16 audio_format = AUDIO_S16;
//    int audio_channels = 4;
//    int audio_buffers = 2048;
//    if(Mix_OpenAudio(audio_rate, audio_format, audio_channels, audio_buffers)) {
//      printf("Unable to open audio!\n");
//      exit(1);
//    }
}

void SoundInterface::pause()
{
	Mix_Pause(1);
}

void SoundInterface::play()
{
	Mix_Resume(1);
}
