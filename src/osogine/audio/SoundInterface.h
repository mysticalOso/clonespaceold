/*
 * SoundTest.h
 *
 *  Created on: 14 Jan 2014
 *      Author: Adam Nasralla
 */

#ifndef SOUNDINTERFACE_H_
#define SOUNDINTERFACE_H_

#include <stdio.h>
#include <stdlib.h>
#include "SDL_mixer.h"
#include "SDL.h"


class SoundInterface
{
public:
	SoundInterface();
	~SoundInterface();
	static void init();
	static void close();

	static void asteroidHit();
	static void asteroidExplode();
	static void shipHit();
	static void shipExplode();
	static void collectGem();
	static void teleport();

	static void fireLazer();
	static void playXaccy();
	static void fireMissile();

	static void playDeath();

	static void pause();
	static void play();

	static void cutSounds();

private:
	static void musicDone();
	static Mix_Music *music;

	static Mix_Chunk *asteroidHit1;
	static Mix_Chunk *asteroidHit2;
	static Mix_Chunk *asteroidHit3;

	static Mix_Chunk *shipHit1;
	static Mix_Chunk *shipHit2;
	static Mix_Chunk *shipHit3;

	static Mix_Chunk *gem1;
	static Mix_Chunk *gem2;
	static Mix_Chunk *gem3;

	static Mix_Chunk *shipExplode1;
	static Mix_Chunk *shipExplode2;

	static Mix_Chunk *asteroidExplode1;
	static Mix_Chunk *asteroidExplode2;

	static Mix_Chunk *teleport1;

	static Mix_Chunk *xaccy;
	static Mix_Chunk *lazer;
	static Mix_Chunk *missile;

	static Mix_Chunk *death;

	static bool soundOn;

};

#endif /* SOUNDINTERFACE_H_ */
