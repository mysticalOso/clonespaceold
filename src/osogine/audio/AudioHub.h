/*
 * AudioHubBase.h
 *
 *  Created on: 8 Oct 2014
 *      Author: Adam Nasralla
 */

#ifndef AUDIOHUB_H_
#define AUDIOHUB_H_

class IAudio;

class AudioHub
{
public:
	AudioHub();
	virtual ~AudioHub();
	void init(IAudio* audioI);
	virtual void initSounds();
	virtual void initMusic();
};

#endif /* AUDIOHUB_H_ */
