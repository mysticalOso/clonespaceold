/*
 * AudioHubBase.cpp
 *
 *  Created on: 8 Oct 2014
 *      Author: Adam Nasralla
 */

#include <osogine/audio/AudioHub.h>
#include <osogine/audio/SoundInterface.h>

AudioHub::AudioHub()
{
}

AudioHub::~AudioHub()
{
}

void AudioHub::init(IAudio* audioI)
{
	initSounds();
	initMusic();
}

void AudioHub::initSounds()
{
	SoundInterface::init();
}

void AudioHub::initMusic()
{
}
