/*
 * InputHubBase.h
 *
 *  Created on: 8 Oct 2014
 *      Author: Adam Nasralla
 */

#ifndef INPUTHUBBASE_H_
#define INPUTHUBBASE_H_

#include <map>
#include <string>

class IInput;
class Controller;
class Game;

using namespace std;

class InputHub
{
public:
	InputHub();
	virtual ~InputHub();

	virtual void init(IInput* inputI, Game* game);

	virtual void initBindings();

	virtual void initControllers(Game* game);

	void setMouseVisible(bool isVisible);

	void setMouseRelative( bool isRelative );

	void addBinding(int keycode, string binding){ bindings[keycode] = binding; }

	void addController(Controller* controller, string name){ controllers[name] = controller; }

	Controller* getController(string name){ return controllers[name]; }

	void keyPressed(int keycode);

	void keyReleased(int keycode);

	void mouseButtonPressed(int button);

	void mouseButtonReleased(int button);

	void mouseMoved(float x, float y);

	void mouseScrolled(float x, float y);

	void setMouseCentre(float x, float y){ mouseCentreX = x; mouseCentreY = y; }

protected:
	map<int, string> bindings;
	map<string, Controller*> controllers;
	bool relativeMouse;
	float mouseCentreX;
	float mouseCentreY;
	bool invertMouseY;
	IInput* inputI;
};

#endif /* INPUTHUBBASE_H_ */
