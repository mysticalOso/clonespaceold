/*
 * TestController.h
 *
 *  Created on: 17 Oct 2014
 *      Author: Adam Nasralla
 */

#ifndef TESTCONTROLLER_H_
#define TESTCONTROLLER_H_

#include <osogine/input/Controller.h>

class TestController: public Controller
{
public:
	TestController(Game* game);
	TestController(Entity* entity);
	~TestController();

	void setEntity(Entity* e){ entity = e; }

	void keyPressed(string key);

	void keyReleased(string key);

	void mouseMoved(float x, float y);

	void mouseScrolled(float x, float y);

private:
	Entity* entity;

};

#endif /* TESTCONTROLLER_H_ */
