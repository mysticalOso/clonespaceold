/*
 * SimpleCameraController.h
 *
 *  Created on: 8 Oct 2014
 *      Author: Adam Nasralla
 */

#ifndef SIMPLECONTROLLER_H_
#define SIMPLECONTROLLER_H_

#include <osogine/input/Controller.h>
#include <string>

class Entity;

class SimpleController : public Controller
{
public:
	SimpleController(Game* game);
	SimpleController(Entity* entity);
	~SimpleController();

	void setEntity(Entity* e){ entity = e; }

	void keyPressed(string key);

	void keyReleased(string key);

	void mouseMoved(float x, float y);

private:
	Entity* entity;
};

#endif /* SIMPLECONTROLLER_H_ */
