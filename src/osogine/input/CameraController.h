/*
 * CameraController.h
 *
 *  Created on: 17 Oct 2014
 *      Author: Adam Nasralla
 */

#ifndef CAMERACONTROLLER_H_
#define CAMERACONTROLLER_H_

#include <glm/fwd.hpp>
#include <glm/vec3.hpp>
#include <osogine/input/Controller.h>

class Transform;

class CameraController: public Controller
{
public:
	CameraController(Game* game);
	CameraController(Entity* entity);
	~CameraController();

	void setEntity(Entity* e){ entity = e; }

	void keyPressed(string key);

	void keyReleased(string key);

	void mouseMoved(float x, float y);

	void mouseScrolled(float x, float y);

private:
	Entity* entity;
	Transform* transform;
	glm::vec3 translation;
	glm::vec3 rotation;
};

#endif /* CAMERACONTROLLER_H_ */
