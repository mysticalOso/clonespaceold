/*
 * TestController.cpp
 *
 *  Created on: 17 Oct 2014
 *      Author: Adam Nasralla
 */

#include <osogine/game/Entity.h>
#include <osogine/input/TestController.h>
#include <stdio.h>

using namespace std;

TestController::TestController(Game* game) : Controller(game)
{
	entity = 0;

}

TestController::TestController(Entity* entity)
{
	this->entity = entity;
	game = entity->getGame();
}

TestController::~TestController()
{
}

void TestController::keyPressed(string key)
{
	printf("Key Pressed - %s\n", key.c_str());
	fflush(stdout);
}

void TestController::keyReleased(string key)
{
	printf("Key Released - %s\n", key.c_str());
	fflush(stdout);
}

void TestController::mouseMoved(float x, float y)
{
	printf("Mouse moved X = %f Y = %f\n", x, y);
	fflush(stdout);
}

void TestController::mouseScrolled(float x, float y)
{
	printf("Mouse scrolled X = %f Y = %f\n", x, y);
	fflush(stdout);
}
