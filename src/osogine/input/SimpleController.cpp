/*
 * SimpleCameraController.cpp
 *
 *  Created on: 8 Oct 2014
 *      Author: Adam Nasralla
 */

#include <osogine/game/Entity.h>
#include <osogine/input/SimpleController.h>
#include <osogine/utils/Transform.h>

using namespace glm;

SimpleController::SimpleController(Entity* entity)
{
	game = entity->getGame();
	this->entity = entity;
}

SimpleController::~SimpleController()
{
}

void SimpleController::keyPressed(string key)
{
	if (entity != 0)
	{
		vec3 v;
		if (key == "forward")
		{
			v.z = 1;
		}
		if (key == "back")
		{
			v.z = -1;
		}
		if (key == "left")
		{
			v.x = -1;
		}
		if (key == "right")
		{
			v.x = 1;
		}
		if (key == "up")
		{
			v.y = 1;
		}
		if (key == "down")
		{
			v.y = -1;
		}
		entity->setVelocity(v);
	}
}

SimpleController::SimpleController(Game* game) : Controller(game)
{
	entity = 0;
}

void SimpleController::keyReleased(string key)
{
	if (entity != 0)
	{
		vec3 v;
		if (key == "forward")
		{
			v.z = 0;
		}
		if (key == "back")
		{
			v.z = 0;
		}
		if (key == "left")
		{
			v.x = 0;
		}
		if (key == "right")
		{
			v.x = 0;
		}
		if (key == "up")
		{
			v.y = 0;
		}
		if (key == "down")
		{
			v.y = 0;
		}
		entity->setVelocity(v);
	}
}

void SimpleController::mouseMoved(float x, float y)
{
	if (entity!=0) entity->setTarget(x,y);
}
