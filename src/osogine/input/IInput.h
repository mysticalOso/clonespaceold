/*
 * InputInterface.h
 *
 *  Created on: 9 Sep 2014
 *      Author: mysticalOso
 */

#ifndef IINPUT_H_
#define IINPUT_H_

#include <string>
#include <map>

using namespace std;

class InputHub;

class IInput {
public:
	IInput(){}
	virtual ~IInput(){}

	virtual bool escapeIsPressed(){ return false; }

	virtual void pollEvents(){}

	virtual void setHub( InputHub* hub ){}

	virtual void setMousePos( float x, float y ){}

	virtual void setMouseVisible( bool isVisible ){}


};

#endif /* IINPUT_H_ */
