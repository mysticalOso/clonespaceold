/*
 * CameraController.cpp
 *
 *  Created on: 17 Oct 2014
 *      Author: Adam Nasralla
 */

#include <osogine/game/Entity.h>
#include <osogine/input/CameraController.h>
#include <osogine/utils/Transform.h>

CameraController::CameraController(Game* game) : Controller(game)
{
	entity = 0;
	transform = new Transform();
}

CameraController::CameraController(Entity* entity)
{
	this->entity = entity;
	transform = new Transform();
}

CameraController::~CameraController()
{
}

void CameraController::keyPressed(string key)
{
	if (entity != 0)
	{
		if (key == "forward")
		{
			translation.z = -0.3;
		}
		if (key == "back")
		{
			translation.z = 0.3;
		}
		if (key == "left")
		{
			translation.x = -3;
		}
		if (key == "right")
		{
			translation.x = 3;
		}
		if (key == "up")
		{
			translation.y = -3;
		}
		if (key == "down")
		{
			translation.y = 3;
		}
		if (key == "rollCW")
		{
			rotation.z = -0.05;
		}
		if (key == "rollCCW")
		{
			rotation.z = 0.05;
		}
		entity->setVelocity(translation);
		entity->setSpin(rotation);
	}
}


void CameraController::keyReleased(string key)
{
	if (entity != 0)
	{
		if (key == "forward")
		{
			translation.z = 0;
		}
		if (key == "back")
		{
			translation.z = 0;
		}
		if (key == "left")
		{
			translation.x = 0;
		}
		if (key == "right")
		{
			translation.x = 0;
		}
		if (key == "up")
		{
			translation.y = 0;
		}
		if (key == "down")
		{
			translation.y = 0;
		}
		if (key == "rollCW")
		{
			rotation.z = 0;
		}
		if (key == "rollCCW")
		{
			rotation.z = 0;
		}
		entity->setVelocity(translation);
		entity->setSpin(rotation);
	}
}

void CameraController::mouseMoved(float x, float y)
{
	//entity->rotateY(-x*0.1f);
	//entity->rotateXRelative(y*0.1f);

}

void CameraController::mouseScrolled(float x, float y)
{

}
