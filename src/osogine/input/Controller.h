/*
 * Controller.h
 *
 *  Created on: 8 Oct 2014
 *      Author: Adam Nasralla
 */

#ifndef CONTROLLER_H_
#define CONTROLLER_H_

#include <string>

class Entity;
class Game;
using namespace std;

class Controller
{
public:
	Controller(){game = 0;}
	Controller(Game* game){this->game = game;}
	virtual ~Controller(){}

	virtual void setEntity(Entity* e){}

	virtual void keyPressed(string key){}

	virtual void keyReleased(string key){}

	virtual void mouseMoved(float x, float y){}

	virtual void mouseScrolled(float x, float y){}


protected:
	Game* game;
};

#endif /* CONTROLLER_H_ */
