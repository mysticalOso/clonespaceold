/*
 * InputHubBase.cpp
 *
 *  Created on: 8 Oct 2014
 *      Author: Adam Nasralla
 */

#include <osogine/input/CameraController.h>
#include <osogine/input/Controller.h>
#include <osogine/input/IInput.h>
#include <osogine/input/InputHub.h>
#include <osogine/input/SimpleController.h>
#include <osogine/input/TestController.h>
#include <utility>
#include <GLFW/glfw3.h>
#include <core/Constants.h>

InputHub::InputHub()
{
	relativeMouse = false;
	invertMouseY = true;
	mouseCentreX = 0;
	mouseCentreY = 0;
	inputI = 0;
}

InputHub::~InputHub()
{
}



void InputHub::init(IInput* inputI, Game* game)
{
	this->inputI = inputI;

	inputI->setHub(this);
	initBindings();
	initControllers(game);

	inputI->setMouseVisible(true);
	setMouseCentre(800, 450);
	relativeMouse = false;
}

void InputHub::initBindings()
{
	bindings[GLFW_KEY_G] = "g";
	bindings[GLFW_KEY_E] = "use";
	bindings[GLFW_KEY_TAB] = "tab";
	bindings[32] = "secondaryFire";
	bindings[65] = "left";
	bindings[68] = "right";
	bindings[87] = "forward";
	bindings[83] = "back";
	//bindings[81] = "rollCW";
	//bindings[69] = "rollCCW";
//	bindings[70] = "down";
//	bindings[82] = "up";
	bindings[1000] = "lmb";
	bindings[1001] = "rmb";
	bindings[1002] = "mouseButton3";
	bindings[2000] = "scrollUp";
	bindings[2001] = "scrollDown";

	bindings[GLFW_KEY_LEFT_SHIFT] = "shift";
	bindings[GLFW_KEY_1] = "1";
	bindings[GLFW_KEY_2] = "2";
	bindings[GLFW_KEY_3] = "3";
	bindings[GLFW_KEY_4] = "4";
	bindings[GLFW_KEY_5] = "5";
	bindings[GLFW_KEY_6] = "6";
	bindings[GLFW_KEY_7] = "7";
	bindings[GLFW_KEY_8] = "8";
	bindings[GLFW_KEY_9] = "9";
	bindings[GLFW_KEY_0] = "0";

	bindings[GLFW_KEY_F] = "f";
	bindings[GLFW_KEY_H] = "h";
	bindings[GLFW_KEY_J] = "j";
	bindings[GLFW_KEY_C] = "c";
	bindings[GLFW_KEY_M] = "m";
	bindings[GLFW_KEY_N] = "n";
	bindings[GLFW_KEY_O] = "o";
	bindings[GLFW_KEY_P] = "p";
	bindings[GLFW_KEY_B] = "b";
	bindings[GLFW_KEY_Y] = "y";
	bindings[GLFW_KEY_U] = "u";
	bindings[GLFW_KEY_I] = "i";
	bindings[GLFW_KEY_O] = "o";
	bindings[GLFW_KEY_K] = "k";
	bindings[GLFW_KEY_L] = "l";
	bindings[GLFW_KEY_ESCAPE] = "esc";
}

void InputHub::initControllers(Game* game)
{
	//controllers["simple"] = new SimpleController();
	//controllers["test"] = new TestController();
	controllers["camera"] = new CameraController(game);
}

void InputHub::keyPressed(int keycode)
{
	typedef map<string, Controller*>::iterator it_type;
	for(it_type it = controllers.begin(); it != controllers.end(); it++)
	{
		Controller* controller = it->second;
		controller->keyPressed( bindings[keycode] );
	}
}

void InputHub::keyReleased(int keycode)
{
	typedef map<string, Controller*>::iterator it_type;
	for(it_type it = controllers.begin(); it != controllers.end(); it++)
	{
		Controller* controller = it->second;
		controller->keyReleased( bindings[keycode] );
	}
}

void InputHub::mouseButtonPressed(int button)
{
	typedef map<string, Controller*>::iterator it_type;
	for(it_type it = controllers.begin(); it != controllers.end(); it++)
	{
		Controller* controller = it->second;
		controller->keyPressed( bindings[button + 1000] );
	}
}

void InputHub::mouseButtonReleased(int button)
{
	typedef map<string, Controller*>::iterator it_type;
	for(it_type it = controllers.begin(); it != controllers.end(); it++)
	{
		Controller* controller = it->second;
		controller->keyReleased( bindings[button + 1000] );
	}
}

void InputHub::mouseMoved(float x, float y)
{
	if (!relativeMouse)
	{
		x = x - mouseCentreX;
		y = y - mouseCentreY;
	}

	if (invertMouseY)
	{
		y = -y;
	}

	float x2=0;
	float y2=0;
	if (relativeMouse)
	{
		x2 = x;
		y2 = y;
		x = x - mouseCentreX;
		y = y - mouseCentreY;
	}

	typedef map<string, Controller*>::iterator it_type;
	for(it_type it = controllers.begin(); it != controllers.end(); it++)
	{
		Controller* controller = it->second;
		float ratio = (float)WIDTH / (float)SCREEN_WIDTH;
		controller->mouseMoved( x * ratio , y * ratio );
	}

	if (relativeMouse)
	{
		mouseCentreX = x2;
		mouseCentreY = y2;
	}
}

void InputHub::mouseScrolled(float x, float y)
{
	typedef map<string, Controller*>::iterator it_type;
	for(it_type it = controllers.begin(); it != controllers.end(); it++)
	{
		Controller* controller = it->second;
		controller->mouseScrolled( x, y );
		if (y>0) controller->keyPressed( bindings[2000] );
		else if (y<0) controller->keyPressed( bindings[2001] );
	}
}

void InputHub::setMouseVisible(bool isVisible)
{
	inputI->setMouseVisible(isVisible);
}

void InputHub::setMouseRelative(bool isRelative)
{
	relativeMouse = isRelative;
}
