/*
 * FunctionTimer.cpp
 *
 *  Created on: 2 Mar 2015
 *      Author: Adam Nasralla
 */

#include <osogine/utils/FunctionTimer.h>

FunctionTimer::FunctionTimer(IEntity* parent, string function, int time, bool repeat)
{
	this->repeat = repeat;
	parent->addChild(this);
	this->parent = parent;
	functionName = function;
	maxCount = time;
	count = 0;
}

FunctionTimer::~FunctionTimer()
{
	parent->removeChild(this);
}

void FunctionTimer::update()
{
	count++;
	if (count==maxCount)
	{
		parent->callBack(functionName);
		if (repeat)
		{
			count = 0;
		}
		else
		{
			//delete this;
		}
	}
}
