/*
 * FunctionTimer.h
 *
 *  Created on: 2 Mar 2015
 *      Author: Adam Nasralla
 */

#ifndef SRC_OSOGINE_UTILS_FUNCTIONTIMER_H_
#define SRC_OSOGINE_UTILS_FUNCTIONTIMER_H_

#include <osogine/game/IEntity.h>

class FunctionTimer: public IEntity
{
public:
	FunctionTimer(IEntity* parent, string function, int time, bool repeat);
	~FunctionTimer();
	void update();

private:
	int count;
	int maxCount;
	IEntity* parent;
	string functionName;
	bool repeat;
};

#endif /* SRC_OSOGINE_UTILS_FUNCTIONTIMER_H_ */
