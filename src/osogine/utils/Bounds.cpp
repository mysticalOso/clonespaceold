/*
 * Bounds.cpp
 *
 *  Created on: 7 Feb 2014
 *      Author: Adam Nasralla
 */

#include <osogine/utils/Bounds.h>
#include <cfloat>

using namespace glm;

Bounds::Bounds()
{
	bxmax = bymax = bxmin = bymin = xmin = xmax = ymin = ymax = 0;
}

Bounds::~Bounds()
{

}

Bounds::Bounds(float xmin1, float ymin1, float xmax1, float ymax1)
{
	bxmin = xmin = xmin1;
	bymin = ymin = ymin1;
	bxmax = xmax = xmax1;
	bymax = ymax = ymax1;
}

bool Bounds::isInside(float x1, float y1)
{
	return (x1 <= xmax && x1 >= xmin && y1 <= ymax && y1 >= ymin);
}

void Bounds::set(float xmin1, float ymin1, float xmax1, float ymax1)
{
	bxmin = xmin = xmin1;
	bymin = ymin = ymin1;
	bxmax = xmax = xmax1;
	bymax = ymax = ymax1;
}

void Bounds::transform(glm::mat4 trans)
{
	std::vector<vec4> points;

	points.push_back( trans * vec4(bxmin,bymin,0,1) );
	points.push_back( trans * vec4(bxmin,bymax,0,1) );
	points.push_back( trans * vec4(bxmax,bymax,0,1) );
	points.push_back( trans * vec4(bxmax,bymin,0,1) );

	xmin = FLT_MAX;
	xmax = -FLT_MAX;
	ymin = FLT_MAX;
	ymax = -FLT_MAX;

	for (unsigned int i=0; i<points.size(); i++)
	{
		vec4 p = points[i];
		if (p.x < xmin) xmin = p.x;
		if (p.x > xmax) xmax = p.x;
		if (p.y < ymin) ymin = p.y;
		if (p.y > ymax) ymax = p.y;
	}
}

void Bounds::updateOutline(std::vector<glm::vec2> outline)
{
	xmin = FLT_MAX;
	xmax = -FLT_MAX;
	ymin = FLT_MAX;
	ymax = -FLT_MAX;

	for (unsigned int i=0; i<outline.size(); i++)
	{
		vec2 p = outline[i];
		if (p.x < xmin) xmin = p.x;
		if (p.x > xmax) xmax = p.x;
		if (p.y < ymin) ymin = p.y;
		if (p.y > ymax) ymax = p.y;
	}

	bxmin = xmin;
	bymin = ymin;
	bxmax = xmax;
	bymax = ymax;
}

bool Bounds::doesIntersect(Bounds b)
{
	return (xmin <= b.xmax && xmax >= b.xmin && ymin <= b.ymax && ymax >= b.ymin);
}

bool Bounds::doesIntersect(Bounds* b)
{
	return (xmin <= b->xmax && xmax >= b->xmin && ymin <= b->ymax && ymax >= b->ymin);
}

void Bounds::update(std::vector<glm::vec2> outline, glm::mat4 trans)
{
	updateOutline(outline);
	transform(trans);
}
