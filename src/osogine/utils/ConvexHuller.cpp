/*
 * ConvexHuller.cpp

 *
 *  Created on: 8 Feb 2015
 *      Author: mysticalOso
 */

#include <osogine/gfx/GfxObject.h>
#include <algorithm>
#include <osogine/utils/ConvexHuller.h>


float ConvexHuller::cross(const Point &O, const Point &A, const Point &B)
{
	return (A.x - O.x) * (B.y - O.y) - (A.y - O.y) * (B.x - O.x);
}


vector<Point> ConvexHuller::convex_hull(vector<Point> P)
{
	int n = P.size(), k = 0;
	vector<Point> H(2*n);

	// Sort points lexicographically
	sort(P.begin(), P.end());

	// Build lower hull
	for (int i = 0; i < n; ++i) {
		while (k >= 2 && cross(H[k-2], H[k-1], P[i]) <= 0) k--;
		H[k++] = P[i];
	}

	// Build upper hull
	for (int i = n-2, t = k+1; i >= 0; i--) {
		while (k >= t && cross(H[k-2], H[k-1], P[i]) <= 0) k--;
		H[k++] = P[i];
	}

	H.resize(k);
	return H;
}

List<vec2>* ConvexHuller::getConvexHull(GfxObject* gfx)
{
	List<vec2>* list = new List<vec2>();
	vector<vec3> points = gfx->getVertexBuffer()->getVector();
	vector<Point> points2d;
	for (unsigned int i = 0; i < points.size(); i++)
	{
		Point p;
		p.x = points[i].x;
		p.y = points[i].y;
		points2d.push_back(p);
	}

	points2d = convex_hull(points2d);

	for (unsigned int i = 0; i < points2d.size(); i++)
	{
		Point p = points2d[i];
		vec2 v = vec2(p.x,p.y);
		list->add(v);
	}
	return list;
}

