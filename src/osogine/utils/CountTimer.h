/*
 * CountTimer.h
 *
 *  Created on: 30 Jan 2014
 *      Author: Adam Nasralla
 */

#ifndef COUNTTIMER_H_
#define COUNTTIMER_H_

class CountTimer
{
public:
	CountTimer(int time);
	~CountTimer(){}

	void set(int time);
	void reset();
	void update();
	bool isReady();

private:
	int count;
	int maxCount;
};

#endif /* COUNTTIMER_H_ */
