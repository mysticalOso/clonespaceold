/*
 * Targeter.cpp
 *
 *  Created on: 12 Jan 2015
 *      Author: mysticalOso
 */

#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/geometric.hpp>
#include <osogine/game/Entity.h>
#include <osogine/utils/Dice.h>
#include <osogine/utils/Targeter.h>
#include <osogine/utils/Utils.h>

Targeter::Targeter() {
}

Targeter::~Targeter() {
}

//TODO investigate + improve targeter

vec2 Targeter::getTarget(Entity* subject, Entity* object,
		Entity* target,float spread, float modifier)
{
	vec2 velDiff = vec2( target->getVelocity() - subject->getVelocity() );
	vec2 posDiff = vec2( target->getPosition() - subject->getPosition() );

	float objectSpeed;
	if (object!=0) objectSpeed = object->getSpeed() + subject->getSpeed();
	else objectSpeed = subject->getSpeed();

	float squareSpeed = ( objectSpeed ) * ( objectSpeed );


	float a,b,c,d,time;


	c = posDiff.x*posDiff.x + posDiff.y*posDiff.y;
	b = 2 * ( posDiff.x*velDiff.x + posDiff.y*velDiff.y );
	a = velDiff.x*velDiff.x + velDiff.y*velDiff.y - squareSpeed;


	vec2* quadratic = Utils::solveQuadratic(a,b,c);

	if (quadratic==0)
	{
		time = 50;
	}
	else
	{
		//time = smallest positive
		time = glm::min(quadratic->x,quadratic->y);
		if (time<0) time = glm::max(quadratic->x,quadratic->y);
	}

	if (time<0) time = 50; //if negative assume 1 sec;

	time *= modifier;

	vec2 targetPos = posDiff + time*velDiff + vec2(subject->getPosition());

	if (spread != 0)
	{
		float speedModifier = glm::sqrt(velDiff.x*velDiff.x + velDiff.y*velDiff.y)*0.01 + 0.1;
		targetPos += Dice::roll(vec2(-spread,-spread), vec2(spread,spread))*speedModifier;
	}

	return targetPos;
}
