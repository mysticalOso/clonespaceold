/*
 * ForceMover.h
 *
 *  Created on: 2 Feb 2015
 *      Author: mysticalOso
 */

#ifndef SRC_OSOGINE_UTILS_FORCEMOVER_H_
#define SRC_OSOGINE_UTILS_FORCEMOVER_H_

#include <glm/vec3.hpp>
#include <glm/vec4.hpp>

using namespace glm;

class Entity;

class ForceMover : public Entity
{
public:
	ForceMover(Game* game);
	virtual ~ForceMover();

	void update();
	void setSubjectEntity(Entity* e);
	void setTargetEntity(Entity* e);
	void setTargetParent(Entity* e){ targetParent = e ; }
	void setTarget(vec4 t);

	void setXYSpeed(float s){ xySpeed = s;}
	void moveTarget(vec4 move){ setTarget(target + move); }

	void setSpeed(float speed){ this->speed = speed; }

	void setLockEntityX(bool b){ lockEntityX = b; }
	void setLockEntityY(bool b){ lockEntityY = b; }
	void setLockEntityZ(bool b){ lockEntityZ = b; }
	void setLockEntityR(bool b){ lockEntityR = b; }

	void setTargetX(float t){ lockEntityX = false; target.x = t; }
	void setTargetY(float t){ lockEntityY = false; target.y = t; }
	void setTargetZ(float t){ lockEntityZ = false; target.z = t; }
	void setTargetR(float t){ lockEntityR = false; target.a = t; }

private:

	vec4 target;

	Entity* targetEntity;
	Entity* subjectEntity;
	Entity* targetParent;

	int count;
	int maxCount;

	bool lockEntityX;
	bool lockEntityY;
	bool lockEntityZ;
	bool lockEntityR;

	float xySpeed;

	bool adjustForVelocity;

	float angDiff;

	float speed;

	void updateVelocityAdjustment();

	void updateSelf();
	void updateSubject();


};

#endif /* SRC_OSOGINE_UTILS_FORCEMOVER_H_ */
