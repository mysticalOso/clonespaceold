#ifndef DICE_H_
#define DICE_H_

#include <glm/fwd.hpp>

using namespace glm;

class Dice
{
public:
	static int roll(int, int);
	static int roll(int);
	static vec4 roll(vec4, vec4);
	static vec3 roll(vec3, vec3);
	static vec2 roll(vec2, vec2);
	static float roll(double, double);
	static float roll(double);
	static void seed();
	static bool chance(int);
	static bool chance(int, int);
};

#endif /* DICE_H_ */
