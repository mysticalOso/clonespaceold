/*
 * Targeter.h
 *
 *  Created on: 12 Jan 2015
 *      Author: mysticalOso
 */

#ifndef SRC_OSOGINE_UTILS_TARGETER_H_
#define SRC_OSOGINE_UTILS_TARGETER_H_

#include <vector>
#include <glm/fwd.hpp>
#define _USE_MATH_DEFINES
#include <math.h>
#include <string>

class Entity;

using namespace glm;

extern const float PI;

class Targeter {
public:
	Targeter();
	virtual ~Targeter();

	static vec2 getTarget(Entity* subject, Entity* object, Entity* target,
			float spread, float modifier);
};

#endif /* SRC_OSOGINE_UTILS_TARGETER_H_ */
