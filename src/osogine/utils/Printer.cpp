/*
 * Printer.cpp
 *
 *  Created on: 21 Oct 2014
 *      Author: Adam Nasralla
 */

#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <glm/vec2.hpp>
#include <osogine/utils/Printer.h>

#include <stdio.h>

Printer::Printer()
{
	// TODO Auto-generated constructor stub

}

Printer::~Printer()
{
	// TODO Auto-generated destructor stub
}

void Printer::print(string prefix, vec2 v, string postfix)
{
	printf("%sx = %f  y = %f%s",prefix.c_str(),v.x,v.y,postfix.c_str());
	flush();
}


void Printer::print(string prefix, vec3 v, string postfix)
{
	printf("%sx = %f  y = %f  z = %f%s",prefix.c_str(),v.x,v.y,v.z,postfix.c_str());
	flush();
}

void Printer::print(string prefix, vec4 v, string postfix)
{
	printf("%sr = %f  g = %f  b = %f  a = %f%s",prefix.c_str(),v.r,v.g,v.b,v.a,postfix.c_str());
	flush();
}

void Printer::print(string prefix, int i, string postfix)
{
	printf("%s = %d%s",prefix.c_str(), i ,postfix.c_str());
	flush();
}

void Printer::print(string prefix, float f, string postfix)
{
	printf("%s = %f%s",prefix.c_str(), f ,postfix.c_str());
	flush();
}

void Printer::flush()
{
	fflush(stdout);
}

void Printer::print(string prefix, List<vec2>* list, string postfix)
{
	for (int i=0; i < list->size(); i++)
	{
		print(prefix,list->get(i),postfix);
	}
}

void Printer::print(string s)
{
	printf("%s",s.c_str());
	fflush(stdout);
}
