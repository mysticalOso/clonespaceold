/*
 * GfxTransform.cpp
 *
 *  Created on: 21 Jul 2014
 *      Author: Adam Nasralla
 */
#include <clone/KeyFrame.h>
#include <osogine/utils/Utils.h>

#define GLM_FORCE_RADIANS
#define degreesToRadians(x) x*(3.141592f/180.0f)

#include <osogine/utils/Transform.h>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>

Transform::Transform()
{
	rotation = mat4(1.0f);
	translation = vec3(0,0,0);
	scaleV = vec3(1,1,1);
	parent = 0;
	shouldInvert = false;
	hasChanged = false;
	matrix = mat4(1.0f);
}

Transform::~Transform()
{
	// TODO Auto-generated destructor stub
}

mat4 Transform::getLocalMatrix()
{
	if (hasChanged)
	{
		matrix = translate(translation) * rotation *  scale(scaleV);
	}
	hasChanged = false;

	return matrix;
}

mat4 Transform::getMatrix()
{
	if (parent!=0) return parent->getMatrix()* getLocalMatrix();
	else return getLocalMatrix();
}

void Transform::rotateX( float degrees )
{
	vec3 axis = vec3(1,0,0);
	rotation = rotate(degrees,axis) * rotation;
	hasChanged = true;
}

void Transform::rotateY( float degrees )
{
	vec3 axis = vec3(0,1,0);
	rotation = rotate(degrees,axis) * rotation;
	hasChanged = true;
}

void Transform::rotateZ( float degrees )
{
	vec3 axis = vec3(0,0,1);
	rotation = rotate(degrees,axis) * rotation;
	hasChanged = true;
}

void Transform::setRotation(vec3 r)
{
	resetRotation();
	rotateX(r.x);
	rotateY(r.y);
	rotateZ(r.z);
	hasChanged = true;
}

void Transform::rotateXRelative( float degrees )
{
	vec3 axis = vec3(1,0,0);
	rotation = rotation * rotate(degrees,axis);
	hasChanged = true;
}

void Transform::rotateYRelative( float degrees )
{
	vec3 axis = vec3(0,1,0);
	rotation = rotation * rotate(degrees,axis);
	hasChanged = true;
}

void Transform::rotateZRelative( float degrees )
{
	vec3 axis = vec3(0,0,1);
	rotation = rotation * rotate(degrees,axis);
	hasChanged = true;
}

void Transform::resetRotation()
{
	rotation = mat4();
	hasChanged = true;
}

void Transform::moveRelative( vec3 direction )
{
	vec4 d =  rotation * vec4(direction,0);
	move(vec3(d));
	hasChanged = true;
}

vec3 Transform::getLocalTranslation( )
{
	return translation;
}

vec3 Transform::getTranslation( )
{
	if (parent!=0) return vec3(parent->getMatrix() * vec4(translation,1));
	else return translation;
}

mat4 Transform::getRotation( )
{
	if (parent!=0) return parent->getRotation() * rotation;
	else return rotation;
}

void Transform::move( vec3 direction )
{
	translation += direction;
	hasChanged = true;
}

void Transform::lookAt(vec3 target, vec3 up)
{
	rotation = inverse( glm::lookAt(vec3(0,0,0),target-translation,up) ) ;
	hasChanged = true;
}

vec3 Transform::transformDirection(vec3 direction)
{
	vec4 d =  getRotation() * vec4(direction,0);
	return vec3(d);
}

vec3 Transform::getDirection()
{
	return transformDirection(vec3(0,1,0));
}

vec3 Transform::getLocalDirection()
{
	vec4 d = rotation * vec4(0,1,0,0);
	return vec3(d);
}



vec2 Transform::applyTo(vec2 p)
{
	vec4 p4 = vec4(p,0,1);
	mat4 m = getMatrix();
	p4 = m * p4;
	return vec2(p4);
}

vec3 Transform::applyTo(vec3 p)
{
	vec4 p4 = vec4(p,1);
	mat4 m = getMatrix();
	p4 = m * p4;
	return vec3(p4);
}

vec2 Transform::applyInverseTo(vec2 p)
{
	vec4 p4 = vec4(p,0,1);
	mat4 m = getInverseMatrix();
	p4 = m * p4;
	return vec2(p4);
}

vec3 Transform::applyInverseTo(vec3 p)
{
	vec4 p4 = vec4(p,1);
	mat4 m = getInverseMatrix();
	p4 = m * p4;
	return vec3(p4);
}

float Transform::getAngle()
{
	vec3 dir = getDirection();
	return atan2(-dir.x,dir.y);
}

mat4 Transform::getInverseMatrix()
{
	return inverse(getMatrix());
}


void Transform::zeroRotation()
{
	resetRotation();
	mat4 rot = getRotation();
	setRotation(inverse(rot));
}

void Transform::setRotationD(float x, float y, float z)
{
	setRotation(degreesToRadians(x),degreesToRadians(y),degreesToRadians(z));
}

vec2 Transform::applyInverseRotation(vec2 p)
{
	vec4 p4 = vec4(p,0,1);
	mat4 m = getInverseRotation();
	p4 = m * p4;
	return vec2(p4);
}

vec3 Transform::applyInverseRotation(vec3 p)
{
	vec4 p4 = vec4(p,1);
	mat4 m = getInverseRotation();
	p4 = m * p4;
	return vec3(p4);
}

mat4 Transform::getInverseRotation()
{
	return inverse(getRotation());
}

vec2 Transform::applyRotation(vec2 p)
{
	vec4 p4 = vec4(p,0,1);
	mat4 m = getRotation();
	p4 = m * p4;
	return vec2(p4);
}

vec3 Transform::applyRotation(vec3 p)
{
	vec4 p4 = vec4(p,1);
	mat4 m = getRotation();
	p4 = m * p4;
	return vec3(p4);
}

void Transform::applyMatrixOf(Transform* trans)
{
	mat4 matrix = trans->getRotation();
	rotation = matrix * rotation;
	matrix = trans->getMatrix();
	translation = vec3( matrix * vec4(translation,1) );
}

void Transform::applyInverseOf(Transform* trans)
{
	mat4 matrix = trans->getInverseRotation();
	rotation = matrix * rotation;
	matrix = trans->getInverseMatrix();
	translation = vec3( matrix * vec4(translation,1) );
}

void Transform::applyLocalMatrixOf(Transform* trans)
{
	mat4 matrix = trans->getLocalRotation();
	rotation = matrix * rotation;
	matrix = trans->getLocalMatrix();
	translation = vec3( matrix * vec4(translation,1) );
}

void Transform::tween(KeyFrame* key, int frame)
{
	float ratio = key->calcRatio( frame);
	setTranslation( Utils::interpolate(key->trans, key->next->trans, ratio) );
	quat qRot = slerp(key->qRot, key->next->qRot, ratio);
	setRotation( mat4_cast(qRot) );
}
