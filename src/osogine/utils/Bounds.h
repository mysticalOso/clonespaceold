/*
 * Bounds.h
 *
 *  Created on: 7 Feb 2014
 *      Author: Adam Nasralla
 */

#ifndef BOUNDS_H_
#define BOUNDS_H_

#include <glm/vec2.hpp>
#include <glm/mat4x4.hpp>

#include <vector>

using namespace glm;

class Bounds
{
public:
	Bounds();
	Bounds(float xmin1, float ymin1, float xmax1, float ymax1);
	~Bounds();
	void set(float xmin1, float ymin1, float xmax1, float ymax1);
	bool isInside(float x1, float y1);
	bool doesIntersect(Bounds b);
	bool doesIntersect(Bounds* b);
	void transform(glm::mat4 trans);
	void updateOutline(std::vector<glm::vec2> outline);
	void update(std::vector<glm::vec2> outline, glm::mat4 trans);

	vec2 getMin(){ return vec2(xmin,ymin); }
	vec2 getMax(){ return vec2(xmax,ymax); }

	float xmin;
	float xmax;
	float ymin;
	float ymax;

private:
	float bxmin; //base values
	float bymin;
	float bxmax;
	float bymax;


};

#endif /* BOUNDS_H_ */
