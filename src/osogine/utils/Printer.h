/*
 * Printer.h
 *
 *  Created on: 21 Oct 2014
 *      Author: Adam Nasralla
 */

#ifndef PRINTER_H_
#define PRINTER_H_

#include <glm/fwd.hpp>
#include <osogine/utils/List.h>
#include <string>

using namespace glm;
using namespace std;

class Printer
{
public:
	Printer();
	~Printer();

	static void print(string s);

	static void print(string prefix, vec2 v, string postfix);
	static void print(string prefix, vec3 v, string postfix);
	static void print(string prefix, vec4 v, string postfix);

	static void print(string prefix, int i, string postfix);
	static void print(string prefix, float i, string postfix);

	static void print(string prefix, List<vec2>* l, string postfix);

	static void flush();
};

#endif /* PRINTER_H_ */
