/*
 * List.h
 *
 *  Created on: 2 Oct 2014
 *      Author: Adam Nasralla
 */

#ifndef LIST_H_
#define LIST_H_

#include <vector>
#include <algorithm>
#include <glm/fwd.hpp>

using namespace glm;
using namespace std;

template <class T>
class List
{
public:
	List(){}
	virtual ~List(){storage.clear();}
	virtual void add( T value ){ storage.push_back(value); }
	virtual int size(){ return storage.size(); }
	virtual T get( uint n ){ return storage[n]; }
	virtual vector<T> getVector(){ return storage; }
	virtual void setVector(vector<T> v){ storage = v; }
	virtual void set( int index, T value ){ storage[index] = value; }
	virtual void insert( int index, T value){ storage.insert(storage.begin()+index,value); }
	virtual void clear(){ storage.clear(); }
	virtual void append(List<T>* newList){ vector<T> v = newList->getVector(); storage.insert(storage.end(), v.begin(), v.end());}
	virtual void remove( T value ){ storage.erase(std::remove(storage.begin(), storage.end(), value), storage.end()); }
	virtual void erase( uint n ){ if (n<storage.size()) storage.erase( storage.begin() + n ) ;}
	virtual List<T>* clone(){ List<T>* l = new List<T>(); l->setVector(getVector()); return l;}
	virtual bool contains( T value ){ return std::find(storage.begin(), storage.end(), value) != storage.end(); }
	vector<T> storage;

};



#endif /* LIST_H_ */
