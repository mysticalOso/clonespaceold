/*
 * Fader.cpp
 *
 *  Created on: 2 Mar 2015
 *      Author: Adam Nasralla
 */

#include <osogine/utils/Fader.h>
#include <osogine/utils/Utils.h>


Fader::Fader(IEntity* parent, float* target, float min, float max, float fadeTime)
{
	parent->addChild(this);
	this->min = min;
	this->max = max;
	this->fadeTime = fadeTime;

	fadingIn = false;
	count = 0;


	this->target = target;
	isFlashing = false;

}

Fader::~Fader()
{
	// TODO Auto-generated destructor stub
}

void Fader::update()
{
	if (fadingIn && count<fadeTime)
	{
		count++;
		if (count>=fadeTime)
		{
			count = fadeTime;
			if (isFlashing) fadingIn = false;
		}
		(*target) = Utils::sinInterpolate(min,max, count/fadeTime);
	}
	else if (!fadingIn && count>0)
	{
		count--;
		if (count<=0)
		{
			count = 0;
			if (isFlashing) fadingIn = true;
		}
		(*target) = Utils::sinInterpolate(min,max, count/fadeTime);
	}
}

void Fader::fadeIn()
{
	fadingIn = true;
	count = 0;
}

void Fader::fadeOut()
{
	fadingIn = false;
	count = fadeTime;
}

void Fader::flash()
{
	isFlashing = true;
	fadeIn();
}
