/*
 * ConvexHuller.h
 *
 *  Created on: 8 Feb 2015
 *      Author: mysticalOso
 */

#ifndef SRC_OSOGINE_UTILS_CONVEXHULLER_H_
#define SRC_OSOGINE_UTILS_CONVEXHULLER_H_

#include <glm/fwd.hpp>
#include <osogine/utils/List.h>
#include <vector>

class GfxObject;

using namespace glm;

struct Point {
	float x, y;

	bool operator <(const Point &p) const {
		return x < p.x || (x == p.x && y < p.y);
	}
};

class ConvexHuller {
public:

	ConvexHuller(){}
	virtual ~ConvexHuller(){}

	static List<vec2>* getConvexHull(GfxObject* gfx);

private:


	static float cross(const Point &O, const Point &A, const Point &B);
	static vector<Point> convex_hull(vector<Point> P);


};

#endif /* SRC_OSOGINE_UTILS_CONVEXHULLER_H_ */
