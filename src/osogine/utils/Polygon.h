/*
 * Polygon.h
 *
 *  Created on: 21 Nov 2014
 *      Author: mysticalOso
 */

#ifndef SRC_OSOGINE_UTILS_POLYGON_H_
#define SRC_OSOGINE_UTILS_POLYGON_H_

#include <glm/vec2.hpp>
#include <osogine/utils/List.h>

class Transform;
class Bounds;
class GfxObject;

class Polygon {
public:
	Polygon();
	Polygon(List<vec2>* outline);
	Polygon(GfxObject* gfx);
	void setTransform(Transform* transform1);
	Transform* getTransform(){return transform;}
	virtual ~Polygon();
	void add(vec2 point){outline->add(point); updateBounds();}
	void add(float x, float y){ add(vec2(x,y)); }
	void clear(){outline->clear(); updateBounds();}
	bool isInside(vec2 point);
	float getArea();

	void set(int i, vec2 point){ outline->set(i,point), updateBounds();}

	void setPosition(float x, float y);

	bool hasPoint(vec2 point);

	bool hasSimilar(vec2 point, float e);

	List<vec2>* getOutline(){return outline;}
	List<vec2>* getTransformedOutline();

	vec2 getMinBounds();
	vec2 getMaxBounds();

	bool collideWith(Polygon* p);
	bool intersectWith(Polygon* p);

	int getLastSegmentIntersected(){ return lastSegmentIntersected; }
	void setLastSegmentIntersected(int i){lastSegmentIntersected = i;}

	int getNoSegments(){ if (isLine) return size()-1; else return size(); }

	bool inBounds(Polygon* p);

	Bounds* getBounds(){ return bounds; }

	void setIsLine(bool b){ isLine = b; }
	bool getIsLine(){ return isLine; }

	vec2 getLastCollision(){ return lastCollision; }

	void update();

	vec2 get(int i){ i = i % size(); return outline->get(i); }

	vec2 getTransformed(int i){
		i = i % size();
		if (transformedOutline->size()>i) return transformedOutline->get(i);
		else return outline->get(i);
	}

	int size(){ return outline->size(); }

	vec2 getClosestVertex( vec2 point );

	Polygon* clone(){ return new Polygon(outline->clone()); }

	void updateTransformedOutline();

	void setLastCollision(vec2 point){ lastCollision = point; }

	void print();

	void scaleOutline(float s);
	void moveOutline(float x, float y);
private:
	Transform* transform;
	List<vec2>* outline;
	List<vec2>* transformedOutline;
	Bounds* bounds;

	vec2 lastCollision;
	int lastSegmentIntersected;

	bool shouldUpdateBounds;
	bool shouldUpdateOutline;



	void transformBounds();
	void updateBounds();

	bool intersect(vec2 p1, vec2 p2, vec2 p3, vec2 p4);

	void init();

	bool isLine;
};

#endif /* SRC_OSOGINE_UTILS_POLYGON_H_ */
