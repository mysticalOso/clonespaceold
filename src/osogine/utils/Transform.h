/*
 * GfxTransform.h
 *
 *  Created on: 21 Jul 2014
 *      Author: Adam Nasralla
 */

#ifndef TRANSFORM_H_
#define TRANSFORM_H_

#define GLM_FORCE_RADIANS

#include <glm/fwd.hpp>
#include <glm/mat4x4.hpp>
#include <glm/vec3.hpp>

using namespace glm;

class KeyFrame;

class Transform
{
public:
	Transform();
	~Transform();

	mat4 getLocalMatrix();
	mat4 getMatrix();

	void setParent(Transform* parent1){ parent = parent1; }

	//Export -Z up -Y forward view Z up Y left X up (model facing up)
	void rotateX( float degrees );
	void rotateY( float degrees );
	void rotateZ( float degrees );

	void rotateXRelative( float degrees );
	void rotateYRelative( float degrees );
	void rotateZRelative( float degrees );

	void resetRotation();

	void setRotation(float x, float y, float z){ setRotation(vec3(x,y,z)); }
	void setRotationD(float x, float y, float z);
	void setRotation(vec3 r);
	void setRotation(mat4 r){ rotation = r; hasChanged = true;}

	void setTranslation(float x, float y, float z){ translation = vec3(x,y,z); hasChanged = true; }
	void setTranslation(vec3 translation1){ translation = translation1; hasChanged = true; }

	void setScale(vec3 scale){ scaleV = scale; hasChanged = true;}
	void setScale(float x, float y, float z){ scaleV = vec3(x,y,z); hasChanged = true;}
	vec3 getScale(){ return scaleV; }
	vec3 getLocalTranslation();
	vec3 getTranslation();

	void move(vec3 direction);
	void move(float x, float y, float z){ move(vec3(x,y,z)); }

	void moveRelative(vec3 direction);
	void moveRelative(float x, float y, float z){ moveRelative(vec3(x,y,z)); }

	void lookAt(vec3 target, vec3 up);
	vec3 transformDirection(vec3 direction);
	vec3 getDirection();
	vec3 getLocalDirection();
	float getAngle();

	mat4 getLocalRotation(){ return rotation; }
	mat4 getRotation();

	mat4 getInverseRotation();

	Transform* getParent(){ return parent; }

	bool getHasChanged(){ return hasChanged; }

	vec2 applyTo(vec2 p);
	vec3 applyTo(vec3 p);

	vec2 applyInverseTo(vec2 p);
	vec3 applyInverseTo(vec3 p);

	vec2 applyInverseRotation(vec2 p);
	vec3 applyInverseRotation(vec3 p);

	vec2 applyRotation(vec2 p);
	vec3 applyRotation(vec3 p);

	mat4 getInverseMatrix();

	void applyLocalMatrixOf(Transform* trans);
	void applyMatrixOf(Transform* trans);
	void applyInverseOf(Transform* trans);

	void zeroRotation();

	void tween(KeyFrame* key, int frame);

private:

	mat4 rotation;
	vec3 translation;
	vec3 scaleV;
	Transform* parent;
	bool hasChanged;
	bool shouldInvert;

	mat4 matrix;


};

#endif /* TRANSFORM_H_ */
