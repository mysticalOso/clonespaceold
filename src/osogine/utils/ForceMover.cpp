/*
 * ForceMover.cpp
 *
 *  Created on: 2 Feb 2015
 *      Author: mysticalOso
 */

#include <glm/geometric.hpp>
#include <osogine/game/Entity.h>
#include <osogine/utils/ForceMover.h>
#include <osogine/utils/Printer.h>
#include <osogine/utils/Utils.h>



ForceMover::ForceMover(Game* game) : Entity(game)
{
	angDiff = 0;
	subjectEntity = 0;
	targetParent = 0;
	targetEntity = 0;

	lockEntityX = true;
	lockEntityY = true;
	lockEntityZ = false;
	lockEntityR = false;

	xySpeed = 1;
	maxCount = 50;
	count = 0;

	speed = 0;

	adjustForVelocity = true;

	setFriction(0.75);
}

ForceMover::~ForceMover()
{
}

void ForceMover::update()
{
	if (targetEntity!=0)
	{
	updateSelf();
	updateSubject();

	/*
	Printer::print("Mover pos = ",getPosition(),"\n");
	Printer::print("Target pos = ",vec3(target),"\n");
	Printer::print("Subject pos = ",subjectEntity->getPosition(),"\n");
	Printer::print("Target Entity pos = ",targetEntity->getPosition(),"\n");*/

	Entity::update();
	}
}


void ForceMover::setTargetEntity(Entity* e)
{

	if (targetParent!=0) setVelocity(targetParent->getVelocity());
	if (targetEntity!=0)
	{
		if (lockEntityX) setX( targetEntity->getX() );
		if (lockEntityY) setY( targetEntity->getY() );
		if (lockEntityZ) setZ( targetEntity->getZ() );
		if (lockEntityR) setRotation(0,0, targetParent->getAngle());
	}

	targetEntity = e;

	if (targetEntity == 0)
	{
		lockEntityX = false;
		lockEntityY = false;
		lockEntityZ = false;
		lockEntityR = false;
	}
	else
	{
		count = 0;
	}
}

void ForceMover::setTarget(vec4 t)
{
	target = t;
	lockEntityX = false;
	lockEntityY = false;
	lockEntityZ = false;
	lockEntityR = false;
}

void ForceMover::updateVelocityAdjustment()
{
	applyForce(targetParent->getVelocity()*0.325f);
}

void ForceMover::setSubjectEntity(Entity* e)
{
	subjectEntity = e;
}

void ForceMover::updateSelf()
{

	if (lockEntityX) target.x = targetEntity->getX();
	if (lockEntityY) target.y = targetEntity->getY();
	if (lockEntityZ) target.z = targetEntity->getZ();
	if (lockEntityR) target.a = targetParent->getAngle();


	vec3 targetPos = vec3(target);
	//Printer::print("Target",targetPos,"\n");
	vec3 dTarget = targetPos - getPosition();
	//Printer::print("dTarget",dTarget,"\n");


	dTarget.z *= speed;

	dTarget.x *= xySpeed;
	dTarget.y *= xySpeed;


	dTarget = (dTarget)*0.01f*speed;
	applyForce(dTarget);

	angDiff = Utils::angDiff(getAngle(),target.a);

	applyTorque(0,0, angDiff*0.01f);

	if (adjustForVelocity) updateVelocityAdjustment();
}

void ForceMover::updateSubject()
{
	if ( count < maxCount )
	{
		count++;
		float ratio = (float)count / (float)maxCount;
		if (lockEntityX)
		{
			subjectEntity->setX( Utils::sinInterpolate(getX(), targetEntity->getX(), ratio) );
		}
		if (lockEntityY)
		{
			subjectEntity->setY( Utils::sinInterpolate(getY(), targetEntity->getY(), ratio) );
		}
		if (lockEntityZ)
		{
			subjectEntity->setZ( Utils::sinInterpolate(getZ(), targetEntity->getZ(), ratio) );
		}
		if (lockEntityR)
		{
			float a = Utils::sinInterpolate(getAngle(), getAngle()+angDiff, ratio );
			subjectEntity->setRotation(0,0,a);

		}
	}
	else
	{
		if (lockEntityX)
		{
			subjectEntity->setX( targetEntity->getX() );
		}
		if (lockEntityY)
		{
			subjectEntity->setY( targetEntity->getY() );
		}
		if (lockEntityZ)
		{
			subjectEntity->setZ( targetEntity->getZ() );
		}
		if (lockEntityR)
		{
			subjectEntity->setRotation(0,0, targetParent->getAngle() );
		}
	}

	if (!lockEntityX) subjectEntity->setX(getX());
	if (!lockEntityY) subjectEntity->setY(getY());
	if (!lockEntityZ) subjectEntity->setZ(getZ());
	if (!lockEntityR) subjectEntity->setRotation(0,0,getAngle());
}










/*OLD CODE*/

//		vec2 dTarget = vec2(target->getPosition()) - vec2(getPosition());
//		vec2 brake = vec2(-getVelocity());
//		setFriction(0.75f);
//		if (brake!=vec2(0,0))
//		{
//			//brake = normalize(brake);
//		}
//
//		if (dTarget!=vec2(0,0))
//		{
//			dTarget = dTarget*0.03f;
//			//Printer::print("DTarget = ",dTarget,"\n");
//			applyForce(vec3(dTarget,0));
//		}


//		vec2 dTarget = vec2(target->getPosition()) - vec2(getPosition());
//		vec2 brake = vec2(-getVelocity());
//
//
//		if (brake!=vec2(0,0))
//		{
//			setFriction(1.0f/(length(getVelocity())+2));
//			//brake = normalize(brake);
//		}
//
//		if (dTarget!=vec2(0,0))
//		{
//			dTarget = dTarget*3.0f;
//			applyForce(vec3(dTarget,0));
//		}
