/*
 * Fader.h
 *
 *  Created on: 2 Mar 2015
 *      Author: Adam Nasralla
 */

#ifndef SRC_OSOGINE_UTILS_FADER_H_
#define SRC_OSOGINE_UTILS_FADER_H_

#include <osogine/game/IEntity.h>

class Fader: public IEntity
{
public:
	Fader(IEntity* parent, float* target,
			float min, float max, float fadeTime);
	~Fader();

	void update();

	void fadeIn();
	void fadeOut();
	void flash();

private:
	bool fadingIn;
	bool isFlashing;
	float count;
	float fadeTime;
	float min;
	float max;
	float* target;
};

#endif /* SRC_OSOGINE_UTILS_FADER_H_ */
