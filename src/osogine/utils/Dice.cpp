#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <glm/vec3.hpp>
#include <glm/vec2.hpp>
#include <glm/vec4.hpp>
#include <osogine/utils/Dice.h>
//TODO test this class

int Dice::roll(int min, int max)
{
	return (rand() % ((max-min)+1) + min);
}

int Dice::roll(int max)
{
	return roll(1,max);
}

float Dice::roll(double min, double max)
{
	return (min + (float)rand()/((float)RAND_MAX/(max-min)));
}

float Dice::roll(double max)
{
	return roll(0.0,max);
}

void Dice::seed()
{
	srand(time(NULL));
}

bool Dice::chance(int denominator)
{
	return (roll(denominator)==1);
}

bool Dice::chance(int numerator, int denominator)
{
	return (roll(denominator)<=numerator);
}

vec3 Dice::roll(vec3 min, vec3 max)
{
	vec3 r;
	r.x = roll(min.x,max.x);
	r.y = roll(min.y,max.y);
	r.z = roll(min.z,max.z);
	return r;
}

vec2 Dice::roll(vec2 min, vec2 max)
{
	vec2 r;
	r.x = roll(min.x,max.x);
	r.y = roll(min.y,max.y);
	return r;
}

vec4 Dice::roll(vec4 min, vec4 max)
{
	vec4 r;
	r.x = roll(min.x,max.x);
	r.y = roll(min.y,max.y);
	r.z = roll(min.z,max.z);
	r.a = roll(min.a,max.a);
	return r;
}
