/*
 * Utils.h
 *
 *  Created on: 27 Nov 2013
 *      Author: Adam Nasralla
 */

#ifndef UTILS_H_
#define UTILS_H_

#include <vector>
#include <glm/fwd.hpp>
#include <osogine/gfx/Triangle.h>
#include <osogine/utils/List.h>
#define _USE_MATH_DEFINES
#include <math.h>
#include <string>

using namespace glm;

extern const float PI;

class Polygon;

class Utils
{
public:
	Utils();
	~Utils();

	static float distFromPointToPoly(vec2 point, std::vector<vec2> poly);
	static vec2 closestPointOnPoly(vec2 point, std::vector<vec2> poly);
	static int findIndexOfNewPoint(vec2 point, std::vector<vec2> poly);
	static std::vector<vec2> sortClockwise(std::vector<vec2> points);
	static bool comparePoints(vec2 p1, vec2 p2);
	static float dist2(vec2 p1, vec2 p2);
	static float dist(vec2 p1, vec2 p2);
	static float dist(float x1, float y1, float x2, float y2);
	static float ang2(vec2 p1, vec2 p2);
	static float bisect(vec2 p, vec2 p1, vec2 p2);
	static vec2 getPointAwayFrom(vec2 p1, float ang, float dist);
	static float getArea(std::vector<vec2> points);
	static bool polyCollide(std::vector<vec2> poly1, std::vector<vec2> poly2);
	static bool polyOverlap(std::vector<vec2> poly1, std::vector<vec2> poly2);
	static bool pointInside(vec2 p1, std::vector<vec2> poly);
	static std::vector<vec2> getCircle(int noPoints, float radius);
	static vec2* getIntersection(vec2 p1, vec2 p2, vec2 p3, vec2 p4);
	static bool checkVertical(vec2 p1, vec2 p2);
	static float getGradient(vec2 p1, vec2 p2);
	static float getYIntercept(float m1, vec2 p1);
	static vec2 getBisector(vec2 prev, vec2 current, vec2 next, std::vector<vec2> outline);
	static vec2 getAdjustedBisector(vec2 prev, vec2 current, vec2 next, std::vector<vec2> outline);
	static vec2 getAdjustedBisector(int prev, int current, int next, Polygon* outline);
	static float interpolate(float start, float finish, float ratio);
	static vec2 interpolate(vec2 start, vec2 finish, float ratio);
	static vec3 interpolate(vec3 start, vec3 finish, float ratio);
	static vec4 interpolate(vec4 start, vec4 finish, float ratio);
	static vec2 getUnitDirection(vec2 start, vec2 finish);
	static vec2 getPerpendicular(vec2 v);
	static vec2 getUnitPerpendicular(vec2 v);
	static float angBetween(vec2 prev, vec2 current, vec2 next);
	static float angBetween(vec2 l, vec2 l2);
	static float angDiff(float a, float a2);
	static float sinInterpolate(float start, float finish, float ratio);
	static float easeInInterpolate(float start, float finish, float ratio);
	static float easeOutInterpolate(float start, float finish, float ratio);
	static vec2 easeOutInterpolate(vec2 start, vec2 finish, float ratio);
	static vec3 easeOutInterpolate(vec3 start, vec3 finish, float ratio);
	static vec2 sinInterpolate(vec2 start, vec2 finish, float ratio);
	static vec3 sinInterpolate(vec3 start, vec3 finish, float ratio);

	static vec2 transformVert(vec2 vert, mat4 trans);
	static std::vector<vec2> transformVerts(std::vector<vec2>, mat4 trans);
	static float anticlockwiseDiff(float start, float finish);
	static float mirrorAng(float ang);

	static vec3 getCentre(std::vector<vec3> points);

	static vec3 getCentre(List<Triangle> tris);

	static vec3 calculateNormal(vec3 p1, vec3 p2, vec3 p3);

	static bool isClockwise(std::vector<vec2> );

	static bool isClockwiseTo(vec2 start, vec2 finish);

	static std::string toString(int value);
	static std::string toString(float value);

	static List<vec2>* getPointsFromTo(vec2 start, vec2 finish, int noPoints);

	static vec3 rotatePointAboutAxis(vec3 p, float theta, vec3 p1, vec3 p2);

	static bool stringIsEqual(string a, string b);

	static float squareLength(vec2 a);

	static vec2* solveQuadratic(float a, float b, float c);

private:
	static bool less(vec2 a, vec2 b);


};

#endif /* UTILS_H_ */
