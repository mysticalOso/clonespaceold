/*
 * Utils.cpp
 *
 *  Created on: 27 Nov 2013
 *      Author: Adam Nasralla
 */

#include <algorithm>
#include <math.h>
#include <sstream>

#include <glm/vec3.hpp>
#include <glm/vec2.hpp>
#include <glm/mat4x4.hpp>
#include <glm/geometric.hpp>
#include <float.h>

#include <osogine/utils/Printer.h>
#include <osogine/utils/Utils.h>
#include <osogine/utils/Polygon.h>

const float PI = 3.14159265;

using namespace std;

Utils::Utils()
{
}

Utils::~Utils()
{
}

//TODO combine with distFromPointToPoly
vec2 Utils::closestPointOnPoly(vec2 point, std::vector<vec2> poly)
{

	vec2 p1,p2;
	vec2 closestP;
	vec2 p;
	float dist,minDist;
	unsigned int i,j;
	minDist = FLT_MAX;

	for (i=0; i < poly.size(); i++)
	{
		j = i+1;
		if (j == poly.size()) j = 0;

		p1 = poly[i];
		p2 = poly[j];

		float r = dot( p2-p1, point-p1);

		if (length(point - p1) > 0)
		{
			r /= length(p1 - p2)*length(p1 - p2);

			if (r<0) dist = length(point-p1);
			else if (r>1) dist = length(p2 - point);
			else
			{
				p = vec2(p1.x + r * (p2.x-p1.x), p1.y + r * (p2.y-p1.y));
				dist = length(point - p);
			}
		}
		else
		{
			dist = 0;
		}
		if (dist < minDist)
		{
			minDist = dist;
			closestP = p;
		}
	}

	return closestP;

}



float Utils::distFromPointToPoly(vec2 point, std::vector<vec2> poly)
{

	vec2 p1,p2;
	float dist,minDist;
	unsigned int i,j;
	minDist = FLT_MAX;

	for (i=0; i < poly.size(); i++)
	{
		j = i+1;
		if (j == poly.size()) j = 0;

		p1 = poly[i];
		p2 = poly[j];

		//printf("X = %f Y = %f\n",p1.x,p1.y);


		float r = dot( p2-p1, point-p1);

		if (length(point - p1) > 0)
		{
			r /= length(p1 - p2)*length(p1 - p2);

			if (r<0) dist = length(point-p1);
			else if (r>1) dist = length(p2 - point);
			else
			{
				//l1 = length(point - p1);
				//l2 = length(p2-p1);
				//dist = glm::sqrt( l1*l1 - r*l2*l2);
				vec2 p = vec2(p1.x + r * (p2.x-p1.x), p1.y + r * (p2.y-p1.y));
				dist = length(point - p);
			}
		}
		else
		{
			dist = 0;
		}
		minDist = std::min(dist,minDist);
	}
	//printf("\n\n");

	return minDist;

	/*
	for p1, p2 in vertices:

	  var r = dotProduct(vector(p2 - p1), vector(x - p1))
	  //x is the point you're looking for

	  r /= magnitude(vector(x - p1))

	  if r < 0:
	    var dist = magnitude(vector(x - p1))
	  else if r > 0:
	    dist = magnitude(vector(p2 - x))
	  else:
	    dist = glm::sqrt(magnitude(vector(x - p1)) ^ 2 - r * magnitude(vector(p2-p1)) ^ 2)

	  minDist = min(dist,minDist)
	  */
}

bool Utils::comparePoints(vec2 p1, vec2 p2)
{
	return (p1.x==p2.x && p1.y==p2.y);
}

float Utils::angBetween(vec2 prev, vec2 current, vec2 next)
{
	vec2 l = (prev-current);
	vec2 l2 = (next-current);
	return angBetween(l,l2);
}

float Utils::angBetween(vec2 l, vec2 l2)
{

	float length2 = (length(l)*length(l2));
	if (length2!=0)
	{
		float cosa = dot(l,l2) / length2 ;
		float e = 0.000001;
		if (cosa > 1-e && cosa < 1+e) return 0;
		else return acos(cosa);
	}
	else return 0;

}

float Utils::angDiff(float a1, float a2)
{
	a1 += PI;
	a2 += PI;

	float diff = a2 - a1;

	if (diff > PI) diff = -(2*PI - diff);
	if (diff < -PI) diff = -(-2*PI - diff);

	return diff;
}


bool Utils::polyCollide(std::vector<vec2> poly1, std::vector<vec2> poly2)
{
	for (unsigned int i = 0; i < poly1.size(); i++)
	{
		if (pointInside(poly1[i],poly2)) return true;
	}
	return false;
}
//TODO investidate polyOverlap and remove hack
bool Utils::polyOverlap(std::vector<vec2> poly1, std::vector<vec2> poly2)
{
	for (unsigned int i = 0; i < poly1.size(); i++)
	{
		vec2 a = poly1[i];
		vec2 b = poly1[ (i+1) % poly1.size() ];
		if (a.x == b.x) a.x+=0.1;
		if (a.y == b.y) a.y+=0.1;
		for (unsigned int j = 0; j < poly2.size(); j++)
		{
			vec2 c = poly2[j];
			vec2 d = poly2[ (j+1) % poly2.size() ];
			if (c.x == d.x) c.x+=0.1;
			if (c.y == d.y) c.y+=0.1;
			vec2* inter = getIntersection(a,b,c,d);
			if (inter !=0)
			{
				//delete inter;
				return true;
			}
		}
	}
	return false;
}


vec2 Utils::sinInterpolate(vec2 start, vec2 finish, float ratio)
{
	float i = sinInterpolate(0,1,ratio);
	return (start + (finish-start)*i);
}

vec3 Utils::sinInterpolate(vec3 start, vec3 finish, float ratio)
{
	float i = sinInterpolate(0,1,ratio);
	return (start + (finish-start)*i);
}


bool Utils::less(vec2 a, vec2 b)
{
	vec2 center = vec2(1,1);

	float angB = atan2((center.y-b.y),(center.x-b.x));
	float angA = atan2((center.y-a.y),(center.x-a.x));

	return (angA<angB);

/*
    if (a.x-center.x >= 0 && b.x-center.x < 0)
        return true;
    if (a.x-center.x == 0 && b.x-center.x == 0) {
        if (a.y-center.y >= 0 || b.y-center.y >= 0)
            return a.y > b.y;
        return b.y > a.y;
    }*/

    // compute the cross product of vectors (center -> a) x (center -> b)
    int det = (a.x-center.x) * (b.y-center.y) - (b.x - center.x) * (a.y - center.y);
    if (det < 0)
        return true;
    if (det > 0)
        return false;

    // points a and b are on the same line from the center
    // check which point is closer to the center
    int d1 = (a.x-center.x) * (a.x-center.x) + (a.y-center.y) * (a.y-center.y);
    int d2 = (b.x-center.x) * (b.x-center.x) + (b.y-center.y) * (b.y-center.y);
    return d1 > d2;
}

std::vector<vec2> Utils::sortClockwise(std::vector<vec2> points)
{
	std::sort(points.begin(), points.end(), &Utils::less);

	for (unsigned int i=0; i < points.size(); i++)
	{
		//printf("x=%f y=%f\n",points[i].x, points[i].y);
	}
	std::reverse(points.begin(), points.end());
	return points;
}

float Utils::dist2(vec2 p1, vec2 p2)
{
	float dx = p1.x - p2.x;
	float dy = p1.y - p2.y;
	return (dx*dx+dy*dy);
}

float Utils::dist(float x1, float y1, float x2, float y2)
{
	return glm::sqrt(dist2(vec2(x1,y1),vec2(x2,y2)));
}

float Utils::ang2(vec2 p1, vec2 p2)
{
	return atan2(p2.y-p1.y, p2.x-p1.x);
}

vec2 Utils::getPointAwayFrom(vec2 p1, float ang, float dist)
{
	float dx = cos(ang);
	float dy = sin(ang);
	return vec2(p1.x + dx*dist, p1.y + dy*dist);
}

float Utils::dist(vec2 p1, vec2 p2)
{
	return glm::sqrt(dist2(p1,p2));
}

vector<vec2> Utils::getCircle(int noPoints, float r)
{
	vector<vec2> circle = vector<vec2>();
	float x1,y1,a;
	for (int i=0; i<noPoints; i++)
	{
		a = -(2*i*PI)/(noPoints) + PI/2.0;
		x1 = r*cos(a);
		y1 = r*sin(a);
		circle.push_back(vec2(x1,y1));
	}
	return circle;
}

float Utils::getArea(std::vector<vec2> points)
{
	float area = 0;

	for (unsigned int i=0; i < points.size(); i++)
	{
		//printf("x = %f, y = %f",points[i].x, points[i].y);
		int j = (i+1) % points.size();
		area += points[i].x * points[j].y;
		area -= points[i].y * points[j].x;
	}
	area = abs( area / 2.0 );
	return area;

}

float Utils::bisect(vec2 p, vec2 p1, vec2 p2)
{
	return 0;
//	float a1 = ang2(p,p1);
//	float a2 = ang2(p,p2);
//	float da = a1 - a2;
//	if (da > PI) da = 2*PI - da;
//	float result = a1 - (da * 0.5);
//	return result;
}

//TODO investigate negative numbers with pointInside function
bool Utils::pointInside(vec2 p1, std::vector<vec2> poly)
{
	bool success = false;
	int nvert = poly.size();
	int i,j;
	for (i = 0, j = nvert-1; i < nvert; j = i++)
	{
		if ( ((poly[i].y>p1.y) != (poly[j].y>p1.y)) &&
			 (p1.x < (poly[j].x-poly[i].x) * (p1.y-poly[i].y) / (poly[j].y-poly[i].y) + poly[i].x) )
				success= !success;
	}
	return success;
}

vec2* Utils::getIntersection(vec2 p1, vec2 p2, vec2 p3, vec2 p4)
{
	vec2* result = new vec2();
	float m1,m2,c1,c2;
	c1 = 0;
	c2 = 0;
	m1 = 0;
	m2 = 0;
	bool l1Vert = checkVertical(p1,p2);
	bool l2Vert = checkVertical(p3,p4);

	if (!l1Vert)
	{
		m1 = getGradient(p1,p2);
		c1 = getYIntercept(m1,p1);
	}
	if (!l2Vert)
	{
		m2 = getGradient(p3,p4);
		c2 = getYIntercept(m2,p3);
	}

	if (!l1Vert && !l2Vert)
	{
		if (m1 != m2)
		{
			result->x = (c2-c1) / (m1-m2);
			result->y = m1 * result->x + c1;
		}
		else
		{
			//delete result;
			result = 0;
		}
	}
	else if (l1Vert && l2Vert)
	{
		//delete result;
		result = 0;
	}
	else if (l1Vert)
	{
		result->x = p1.x;
		result->y = m2*result->x + c2;
	}
	else if (l2Vert)
	{
		result->x = p3.x;
		result->y = m1*result->x + c1;
	}

	if (result!=0)
	{
		float minX = glm::min (p1.x,p2.x) + 0.001;
		float maxX = glm::max (p1.x,p2.x) - 0.001;
		if (result->x <= minX || result->x >= maxX)
		{
			//delete result;
			result = 0;
		}
		if (result!=0)
		{
			minX = glm::min (p3.x,p4.x) + 0.001;
			maxX = glm::max (p3.x,p4.x) - 0.001;
			if (result->x <= minX || result->x >= maxX)
			{
				//delete result;
				result = 0;
			}
		}
	}

	return result;
}

bool Utils::checkVertical(vec2 p1, vec2 p2)
{
	return (p1.x == p2.x);
}

float Utils::getGradient(vec2 p1, vec2 p2)
{
	return ((p2.y - p1.y) / (p2.x - p1.x));
}

float Utils::getYIntercept(float m1, vec2 p1)
{
	return (p1.y - m1 * p1.x);
}

int Utils::findIndexOfNewPoint(vec2 point, std::vector<vec2> poly)
{
	float minDist = FLT_MAX;
	int index;
	index = 0;
	for (unsigned int i=0; i<poly.size(); i++)
	{

		unsigned int j = i+1;
		if (j == poly.size()) j = 0;

		std::vector<vec2> line;
		line.push_back(poly[i]);
		line.push_back(poly[j]);
		float dist = distFromPointToPoly(point,line);
		if (dist<minDist)
		{
			minDist = dist;
			if (j!=0) index = j;
			else index = poly.size();
		}

	}
	return index;
}

vec2 Utils::getBisector(vec2 prev, vec2 current, vec2 next, std::vector<vec2> outline)
{
	vec2 l1 = (next - current);
	float length = glm::length(l1);
	l1 = (l1) / length;
	vec2 l2 = (prev - current);
	length = glm::length(l2);
	l2 = (l2) / length;
	vec2 bi = (l1 + l2) / 2.0f;
	bi = bi / glm::length(bi);
	vec2 p = current + bi;
	if (!Utils::pointInside(p,outline)) bi = -bi;
	return bi;
}

vec2 Utils::getAdjustedBisector(vec2 prev, vec2 current, vec2 next, std::vector<vec2> outline)
{
	vec2 l1 = (next - current);
	float length = glm::length(l1);
	l1 = (l1) / length;
	vec2 l2 = (prev - current);
	length = glm::length(l2);
	l2 = (l2) / length;
	vec2 bi = (l1 + l2) / 2.0f;
	bi = bi / glm::length(bi);
	vec2 p = current + bi;
	float cosa = dot(l1,bi);
	float sina = glm::sqrt(1-cosa*cosa);
	if (!Utils::pointInside(p,outline)) bi = -bi;
	return bi / sina;
}

vec2 Utils::getAdjustedBisector(int prev, int current, int next, Polygon* outline)
{
	return getAdjustedBisector(outline->get(prev), outline->get(current),
			outline->get(next), outline->getOutline()->getVector());
}

vec2 Utils::interpolate(vec2 start, vec2 finish, float ratio)
{
	return start + (finish-start)*ratio;

}

float Utils::interpolate(float start, float finish, float ratio)
{
	return start + (finish-start)*ratio;

}

vec2 Utils::getUnitDirection(vec2 start, vec2 finish)
{
	return (finish-start) / dist(start,finish);
}

vec2 Utils::getPerpendicular(vec2 v)
{
	return vec2(-v.y,v.x);
}

//only currently works for -PI <= x <= PI
float Utils::anticlockwiseDiff(float a1, float a2)
{
	//a1 = fmod(a1,PI);
	//a2 = fmod(a2,PI);
	if (a1 < 0) a1 = (2*PI) + a1;
	if (a2 < 0) a2 = (2*PI) + a2;
	float b =  (a2 - a1);
	if (b < 0) b = (2*PI) + b;
	return b;
}

//only currently works for -PI <= x <= PI
float Utils::mirrorAng(float ang)
{
	if (ang > 0) ang -= PI;
	else if (ang <= 0) ang += PI;
	return ang;

}

float Utils::sinInterpolate(float start, float finish, float ratio)
{
	float a1 = ratio * PI;
	float i =  (1 - cos(a1))/2.0f;

	return (start + (finish-start)*i);
}


float Utils::easeInInterpolate(float start, float finish, float ratio)
{
	float a1 = (ratio-1) * PI*0.5;
	float i = sin(a1) + 1;
	return (start + (finish-start)*i);
}

float Utils::easeOutInterpolate(float start, float finish, float ratio)
{
	float a1 = ratio * PI*0.5;
	float i = sin(a1);
	return (start + (finish-start)*i);
}

vec2 Utils::easeOutInterpolate(vec2 start, vec2 finish, float ratio)
{
	float a1 = ratio * PI*0.5;
	float i = sin(a1);
	return (start + (finish-start)*i);
}

vec3 Utils::easeOutInterpolate(vec3 start, vec3 finish, float ratio)
{
	float a1 = ratio * PI*0.5;
	float i = sin(a1);
	return (start + (finish-start)*i);
}

vec3 Utils::interpolate(vec3 start, vec3 finish, float ratio)
{
	return start + (finish-start)*ratio;
}

vec2 Utils::transformVert(vec2 vert, mat4 trans) {
	vec4 p2 = trans* vec4(vert.x,vert.y,0,1);
	vec2 p1 = vec2(p2.x,p2.y);
	return p1;
}

std::vector<vec2> Utils::transformVerts(std::vector<vec2> verts, mat4 trans)
{
	std::vector<vec2> newVerts;
	for (uint i=0; i<verts.size(); i++)
	{
		newVerts.push_back(transformVert(verts[i],trans));
	}
	return newVerts;
}

vec3 Utils::getCentre(std::vector<vec3> points)
{
	vec3 centre;
	for (uint i=0; i < points.size(); i++)
	{
		centre += points[i];
	}
	centre /= (float)points.size();
	return centre;
}

vec3 Utils::getCentre(List<Triangle> tris)
{
	vec3 centre;
	for (uint i=0; i < tris.size(); i++)
	{
		Triangle tri = tris.get(i);
		centre += tri.getVertex(0);
		centre += tri.getVertex(1);
		centre += tri.getVertex(2);
	}
	centre /= (float)tris.size()*3.0f;
	return centre;
}

vec3 Utils::calculateNormal(vec3 p1, vec3 p2, vec3 p3)
{
	vec3 u = p2 - p1;
	vec3 v = p3 - p1;

	vec3 n;
	n.x = u.y * v.z - u.z * v.y;
	n.y = u.z * v.x - u.x * v.z;
	n.z = u.x * v.y - u.y * v.x;

	return normalize( n );
}

bool Utils::isClockwise(std::vector<vec2> verts)
{
	vec2 a,b;
	float total = 0;

	for (uint i=0; i<verts.size(); i++)
	{
		vec2 a = verts[i];
		vec2 b = verts[ (i+1) % verts.size() ];
		total += (b.x-a.x)*(b.y+a.y);
	}

	return (total>0);
}

std::string Utils::toString(int value)
{
	std::ostringstream oss;
	oss << value;
	return oss.str();
}

std::string Utils::toString(float value)
{
	std::ostringstream oss;
	oss << value;
	return oss.str();
}

bool Utils::isClockwiseTo(vec2 start, vec2 finish)
{
	return (start.y*finish.x < start.x*finish.y);
}

vec4 Utils::interpolate(vec4 start, vec4 finish, float ratio)
{
	return start + (finish-start)*ratio;
}

vec2 Utils::getUnitPerpendicular(vec2 v)
{
	vec2 p = getPerpendicular(v);
	return p / glm::length(p);
}

List<vec2>* Utils::getPointsFromTo(vec2 start, vec2 finish, int noPoints)
{
	List<vec2>* list = new List<vec2>();
	for (int i=0; i<noPoints; i++)
	{
		float ratio = i / (noPoints-1);
		list->add( interpolate(start,finish,ratio ));
	}
	return list;
}

vec3 Utils::rotatePointAboutAxis(vec3 p, float theta, vec3 p1, vec3 p2)
{

	   vec3 u,q1,q2;
	   	   double d;

	   	   /* Step 1 */
	   	   q1.x = p.x - p1.x;
	   	   q1.y = p.y - p1.y;
	   	   q1.z = p.z - p1.z;

	   	   u.x = p2.x - p1.x;
	   	   u.y = p2.y - p1.y;
	   	   u.z = p2.z - p1.z;

	   	   u = normalize(u);
	   	   d = glm::sqrt(u.y*u.y + u.z*u.z);

	   	   /* Step 2 */
	   	   if (d != 0) {
	   	      q2.x = q1.x;
	   	      q2.y = q1.y * u.z / d - q1.z * u.y / d;
	   	      q2.z = q1.y * u.y / d + q1.z * u.z / d;
	   	   } else {
	   	      q2 = q1;
	   	   }

	   	   /* Step 3 */
	   	   q1.x = q2.x * d - q2.z * u.x;
	   	   q1.y = q2.y;
	   	   q1.z = q2.x * u.x + q2.z * d;


	   	   /* Step 4 */
	   	   float ct = cos(theta);
	   	   float st = sin(theta);
	   	   q2.x = q1.x * ct - q1.y * st;
	   	   q2.y = q1.x * st + q1.y * ct;
	   	   q2.z = q1.z;

	   	   /* Inverse of step 3 */
	   	   q1.x =   q2.x * d + q2.z * u.x;
	   	   q1.y =   q2.y;
	   	   q1.z = - q2.x * u.x + q2.z * d;

	   	   /* Inverse of step 2 */
	   	   if (d != 0) {
	   	      q2.x =   q1.x;
	   	      q2.y =   q1.y * u.z / d + q1.z * u.y / d;
	   	      q2.z = - q1.y * u.y / d + q1.z * u.z / d;
	   	   } else {
	   	      q2 = q1;
	   	   }

	   	   /* Inverse of step 1 */
	   	   q1.x = q2.x + p1.x;
	   	   q1.y = q2.y + p1.y;
	   	   q1.z = q2.z + p1.z;
	   	   return(q1);
	   /*

	    */
}


bool Utils::stringIsEqual(string a, string b)
{
	return (a==b);
}

float Utils::squareLength(vec2 a)
{
	return a.x*a.x + a.y*a.y;
}

vec2* Utils::solveQuadratic(float a, float b, float c)
{
  vec2* result = new vec2();

  if(glm::abs(a)<0.000001)    // ==0
  {
    if(glm::abs(b)>0.000001)
    {
      result->x=result->y=-c/b;
    }
    else if(glm::abs(c)>0.00001)
    {
    	  //delete result;
    	  result = 0;
    }
    else
    {
    	result->x = result->y = 0;
    }
    return result;
  }

  float delta=b*b-4*a*c;
  if(delta>=0)
  {
    result->x=(-b-glm::sqrt(delta))/2/a;
    result->y=(-b+glm::sqrt(delta))/2/a;
  }
  else
  {
	//delete result;
    result = 0;
  }

  return result;
}
