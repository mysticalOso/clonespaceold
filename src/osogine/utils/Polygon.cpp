/*
 * Polygon.cpp
 *
 *  Created on: 21 Nov 2014
 *      Author: mysticalOso
 */

#include <osogine/utils/Bounds.h>
#include <osogine/utils/ConvexHuller.h>
#include <osogine/utils/Polygon.h>
#include <osogine/utils/Printer.h>
#include <osogine/utils/Transform.h>
#include <osogine/utils/Utils.h>
#include <glm/mat4x4.hpp>

Polygon::Polygon()
{
	outline = new List<vec2>();
	init();
}

Polygon::Polygon(List<vec2>* outline)
{
	this->outline = outline;
	init();
}

Polygon::Polygon(GfxObject* gfx)
{
	outline = ConvexHuller::getConvexHull(gfx);
	init();
}


Polygon::~Polygon()
{
	//delete outline;
	//delete transformedOutline;
	//delete bounds;
}


bool Polygon::isInside(vec2 point)
{
	if (transform!=0)
	{

		transformBounds();


		if (bounds->isInside(point.x,point.y))
		{
			updateTransformedOutline();
			return Utils::pointInside(point,transformedOutline->getVector());
		}
		else
		{
			return false;
		}
	}
	else
	{
		if (bounds->isInside(point.x,point.y))
		{
			return Utils::pointInside(point,outline->getVector());
		}
		else
		{
			return false;
		}
	}
}

bool Polygon::collideWith(Polygon* p)
{


	if (inBounds(p))
	{
		if (!isLine)
		{
			List<vec2>* outline2 = p->getTransformedOutline();

			for (int i = 0; i < outline2->size(); i++)
			{
				if (isInside(outline2->get(i)))
				{
					lastCollision = outline2->get(i);
					return true;
				}
			}
		}
		if (!p->isLine)
		{
			updateTransformedOutline();
			for (int i = 0; i <transformedOutline->size(); i++)
			{
				if (p->isInside(transformedOutline->get(i)))
				{
					lastCollision = transformedOutline->get(i);
					return true;
				}
			}
		}
	}
	return false;
}

List<vec2>* Polygon::getTransformedOutline()
{
	if (transform==0) return outline;

	updateTransformedOutline();
	return transformedOutline;
}

void Polygon::updateTransformedOutline()
{
	if (shouldUpdateOutline && transform!=0)
	{
		transformedOutline->clear();
		mat4 matrix = transform->getMatrix();
		for (int i = 0; i < outline->size(); i++)
		{
			vec4 p = vec4(outline->get(i),0,1);
			if (transform!=0)
			{
				vec2 tPoint = vec2(  matrix * p ) ;
				transformedOutline->add(tPoint);
			}
		}
	}
	shouldUpdateOutline = false;
}


//TODO improve collision detection to only transform if inside bounds!!
void Polygon::transformBounds()
{
	if (transform!=0 && shouldUpdateBounds)
	{
		bounds->transform(transform->getMatrix());
	}
	shouldUpdateBounds = false;
}

void Polygon::updateBounds()
{
	bounds->updateOutline(outline->getVector());
}

bool Polygon::inBounds(Polygon* p)
{
	p->transformBounds();
	transformBounds();
	return (bounds->doesIntersect(p->getBounds()));
}

void Polygon::update()
{
	shouldUpdateOutline = true;
	shouldUpdateBounds = true;
}

vec2 Polygon::getMinBounds()
{
	return bounds->getMin();
}

vec2 Polygon::getMaxBounds()
{
	return bounds->getMax();
}

float Polygon::getArea()
{
	return Utils::getArea(outline->getVector());
}

vec2 Polygon::getClosestVertex(vec2 point)
{
	float minDist = 99999999;
	vec2 best;
	float dist = 0;
	for (int i = 0; i < outline->size(); i++)
	{
		vec2 p = outline->get(i);
		dist = Utils::dist2(point,p);
		if (dist < minDist)
		{
			minDist = dist;
			best = p;
		}
	}
	return best;
}

void Polygon::print()
{
	for (int i = 0; i < outline->size(); i++)
	{
		vec2 p = outline->get(i);
		//Printer::print("Point = ",p,"\n");
	}
}

bool Polygon::hasSimilar(vec2 p1, float e)
{
	bool similar = false;
	for (int i = 0; i < outline->size(); i++)
	{
		vec2 p = outline->get(i);
		if (p.x < p1.x+e && p.x > p1.x-e && p.y < p1.y+e && p.y > p1.y-e)
		{
			similar = true;
		}
	}
	return similar;
}



void Polygon::init()
{
	transformedOutline = new List<vec2>();
	isLine = false;
	transform = 0;
	bounds = new Bounds();
	shouldUpdateBounds = true;
	shouldUpdateOutline = true;
	lastSegmentIntersected = -1;
	updateBounds();
}

bool Polygon::hasPoint(vec2 point)
{
	return outline->contains(point);
}

void Polygon::setTransform(Transform* transform1)
{
	transform = transform1;
	shouldUpdateOutline=true;
	shouldUpdateBounds=true;
	//updateTransformedOutline();
	//updateBounds();
}

void Polygon::setPosition(float x, float y)
{
	if (transform == 0) transform = new Transform();
	transform->setTranslation(x,y,0);
}

bool Polygon::intersectWith(Polygon* p)
{
	if (inBounds(p))
	{
		p->updateTransformedOutline();
		updateTransformedOutline();
		for (int i=0; i<getNoSegments(); i++)
		{
			vec2 p1 = getTransformed(i);
			vec2 p2 = getTransformed(i+1);
			for (int j=0; j<p->getNoSegments(); j++)
			{
				vec2 p3 = p->getTransformed(j);
				vec2 p4 = p->getTransformed(j+1);
				if (intersect(p1,p2,p3,p4))
				{
					lastSegmentIntersected = i;
					p->setLastSegmentIntersected(j);
					return true;
				}
			}
		}
	}
	return false;
}

bool Polygon::intersect(vec2 p1, vec2 p2, vec2 p3, vec2 p4)
{
	float denom = ((p4.y - p3.y) * (p2.x - p1.x) - (p4.x - p3.x) * (p2.y - p1.y));
	if (denom==0) return false;

	float ua = ((p4.x - p3.x) * (p1.y - p3.y) - (p4.y - p3.y) * (p1.x - p3.x)) / denom;

	float ub = ((p2.x - p1.x) * (p1.y - p3.y) - (p2.y - p1.y) * (p1.x - p3.x)) / denom;

	if (ua>0.0f && ua<1.0f && ub>0.0f && ub<1.0f)
	{
		lastCollision = vec2(p1.x + ua * (p2.x - p1.x), p1.y + ua * (p2.y - p1.y));
		return true;
	}

	return false;
}

void Polygon::scaleOutline(float s)
{
	for (int i = 0; i < outline->size(); i++)
	{
		vec2 p = outline->get(i);
		p *= s;
		outline->set(i,p);
	}
}

void Polygon::moveOutline(float x, float y)
{
	for (int i = 0; i < outline->size(); i++)
	{
		vec2 p = outline->get(i);
		p += vec2(x,y);
		outline->set(i,p);
	}
}
