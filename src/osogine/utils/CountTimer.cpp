/*
 * CountTimer.cpp
 *
 *  Created on: 30 Jan 2014
 *      Author: Adam Nasralla
 */

#include <osogine/utils/CountTimer.h>

CountTimer::CountTimer(int time)
{
	count = 0;
	maxCount = time;
}

void CountTimer::set(int time)
{
	count = 0;
	maxCount = time;
}

void CountTimer::update()
{
	if (count<maxCount) count++;
}

void CountTimer::reset()
{
	count = 0;
}

bool CountTimer::isReady()
{
	return count==maxCount;
}
