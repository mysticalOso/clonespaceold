/*
 * FileInterface.h
 *
 *  Created on: 21 Jan 2014
 *      Author: Adam Nasralla
 */

#ifndef FILEINTERFACE_H_
#define FILEINTERFACE_H_

#include <vector>
#include <glm/fwd.hpp>
#include <string>

class FileInterface
{
public:
	//FileInterface();
	//~FileInterface();;
	static void savePoints(int i, std::vector<glm::vec2> points);
	static std::vector<glm::vec2> loadPoints(int i);
	static void saveModules(int i, std::vector<glm::vec4> modules);
	static std::vector<glm::vec4> loadModules(int i);


};

#endif /* FILEINTERFACE_H_ */
