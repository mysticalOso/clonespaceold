/*
 * FileInterface.cpp
 *
 *  Created on: 21 Jan 2014
 *      Author: Adam Nasralla
 */


#include <osogine/file/FileInterface.h>
#include <fstream>
#include <glm/vec2.hpp>
#include <glm/vec4.hpp>

using namespace std;



void FileInterface::savePoints(int i, std::vector<glm::vec2> points)
{

	// Write out a list to a disk file
	char filename [15];
	sprintf(filename,"design%d.dat",i);
	ofstream os (filename, ios::binary);

	int size1 = points.size();
	os.write((const char*)&size1, 4);
	os.write((const char*)&points[0], size1 * sizeof(glm::vec2));
	os.close();
}

std::vector<glm::vec2> FileInterface::loadPoints(int i)
{
	vector<glm::vec2> points;
	char filename [15];
	//printf("a\n");
		fflush(stdout);
	sprintf(filename,"design%d.dat",i);
	//printf("%s\n",filename);
		fflush(stdout);
	ifstream is(filename, ios::binary);
	//printf("c\n");
		//fflush(stdout);
	int size2;
	is.read((char*)&size2, 4);
	//printf("size2 %d\n i = %d",size2,i);
		fflush(stdout);
	points.resize(size2);
	//printf("e\n");
		//fflush(stdout);

	 // Is it safe to read a whole array of structures directly into the vector?
	is.read((char*)&points[0], size2 * sizeof(glm::vec2));

	return points;
}

void FileInterface::saveModules(int i, std::vector<glm::vec4> modules)
{
	// Write out a list to a disk file
	char filename [15];
	sprintf(filename,"module%d.dat",i);
	ofstream os (filename, ios::binary);

	int size1 = modules.size();
	os.write((const char*)&size1, 4);
	os.write((const char*)&modules[0], size1 * sizeof(glm::vec4));
	os.close();
}

std::vector<glm::vec4> FileInterface::loadModules(int i)
{
	vector<glm::vec4> modules;
	char filename [15];
	sprintf(filename,"module%d.dat",i);
	ifstream is(filename, ios::binary);


		int size2;
		is.read((char*)&size2, 4);
		modules.resize(size2);

		 // Is it safe to read a whole array of structures directly into the vector?
		is.read((char*)&modules[0], size2 * sizeof(glm::vec4));


		return modules;


}
