/*
 * DesktopPlatform.h
 *
 *  Created on: 30 Sep 2014
 *      Author: Adam Nasralla
 */

#ifndef DESKTOPPLATFORM_H_
#define DESKTOPPLATFORM_H_

#include <osogine/platform/IPlatform.h>
#include <string>


class IWindow;
class IGfx;
class IInput;
class IAudio;

class DesktopPlatform : public IPlatform
{
public:
	DesktopPlatform(string name, int width, int height, bool fullscreen);
	~DesktopPlatform();

	IWindow* getWindowInterface(){ return windowInterface; }
	IGfx* getGfxInterface(){ return gfxInterface; }
	IInput* getInputInterface(){ return inputInterface; }
	IAudio* getAudioInterface(){ return audioInterface; }

private:
	IWindow* windowInterface;
	IGfx* gfxInterface;
	IInput* inputInterface;
	IAudio* audioInterface;


};

#endif /* DESKTOPPLATFORM_H_ */
