/*
 * OpenGLGfx.cpp
 *
 *  Created on: 30 Sep 2014
 *      Author: Adam Nasralla
 */

#include <GL/glew.h>
#include <osogine/platform/OpenGL/OpenGLGfx.h>
#include <stdio.h>


OpenGLGfx::OpenGLGfx()
{
}

OpenGLGfx::~OpenGLGfx()
{
	// TODO Auto-generated destructor stub
}

void OpenGLGfx::init()
{

	// Initialize GLEW
	glewExperimental = true; // Needed for core profile
	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialize GLEW\n");
	}

	GLuint VertexArrayID;
	glGenVertexArrays(1, &VertexArrayID);
	glBindVertexArray(VertexArrayID);


	//for flat shading use first vertex of triangle for colour and normal
	glProvokingVertex(GL_FIRST_VERTEX_CONVENTION);

	clearColour(0,0,0);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDepthFunc(GL_LEQUAL);
	//glEnable(GL_CULL_FACE);
}

void OpenGLGfx::toggleDepthTest(bool isOn)
{
	if (isOn)
	{
		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LEQUAL);
	}
	else
	{
		glDisable(GL_DEPTH_TEST);
	}
}

void OpenGLGfx::clearBufferBits()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void OpenGLGfx::clearDepthBit()
{
	glClear(GL_DEPTH_BUFFER_BIT);
}

void OpenGLGfx::clearColour(float r, float g, float b)
{
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	//glClearColor(r,g,b,1.0f);
}



uint OpenGLGfx::declareUniform(uint programId, string name)
{
	return glGetUniformLocation(programId, name.c_str());
}

void OpenGLGfx::setUniformValue(uint uniformId, int value)
{
	glUniform1i(uniformId,value);
}

void OpenGLGfx::setUniformValue(uint uniformId, float value)
{
	glUniform1f(uniformId,value);
}

void OpenGLGfx::setUniformValue(uint uniformId, vec2 value)
{
	glUniform2f(uniformId, value.x, value.y);
}

void OpenGLGfx::setUniformValue(uint uniformId, vec3 value)
{
	glUniform3f(uniformId, value.x, value.y, value.z);
}

void OpenGLGfx::setUniformValue(uint uniformId, vec4 value)
{
	glUniform4f(uniformId, value.r, value.g, value.b, value.a);
}

void OpenGLGfx::setUniformValue(uint uniformId, mat4 value)
{
	glUniformMatrix4fv(uniformId, 1, GL_FALSE, &value[0][0]);
}

void OpenGLGfx::setUniformValue(uint uniformId, vector<int> value, uint count)
{
	glUniform1iv(uniformId, count, &value[0]);
}

void OpenGLGfx::setUniformValue(uint uniformId, vector<float> value, uint count)
{
	glUniform1fv(uniformId, count, &value[0]);
}

void OpenGLGfx::setUniformValue(uint uniformId, vector<vec3> value, uint count)
{
	glUniform3fv(uniformId, count, &value[0][0]);
}

void OpenGLGfx::setUniformValue(uint uniformId, vector<vec2> value, uint count)
{
	glUniform2fv(uniformId, count, &value[0][0]);
}

void OpenGLGfx::uploadBuffer(uint* bufferId, uint size, void* buffer,
		bool isDynamic, bool isElementArray)
{
	glGenBuffers(1,bufferId);
	updateBuffer(bufferId, size, buffer, isDynamic, isElementArray);
}

void OpenGLGfx::updateBuffer(uint* bufferId, uint size, void* buffer,
		bool isDynamic, bool isElementArray)
{
	if (!isElementArray)
	{
		glBindBuffer(GL_ARRAY_BUFFER,*bufferId);
	}
	else
	{
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,*bufferId);
	}
	if (!isElementArray)
	{
		if (isDynamic) glBufferData(GL_ARRAY_BUFFER,size,buffer,GL_DYNAMIC_DRAW);
		else glBufferData(GL_ARRAY_BUFFER,size,buffer,GL_STATIC_DRAW);
	}
	else
	{
		if (isDynamic) glBufferData(GL_ELEMENT_ARRAY_BUFFER,size,buffer,GL_DYNAMIC_DRAW);
		else glBufferData(GL_ELEMENT_ARRAY_BUFFER,size,buffer,GL_STATIC_DRAW);
	}
}

void OpenGLGfx::activateBuffer(uint id, uint location, uint noDimensions,
		bool isElementArray)
{
	uint bufferType;
	if (isElementArray) bufferType = GL_ELEMENT_ARRAY_BUFFER;
	else bufferType = GL_ARRAY_BUFFER;
	if (!isElementArray)
	{
	glEnableVertexAttribArray(location);
	}
	glBindBuffer(bufferType, id);
	if (!isElementArray)
	{
		glVertexAttribPointer(
			location,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
			noDimensions,                  // size
			GL_FLOAT,           // type
			GL_FALSE,           // normalized?verte
			0,                  // stride
			(void*)0            // array buffer offset
		);
	}
	else
	{
	}
}


void OpenGLGfx::useProgram(uint id)
{
	glUseProgram(id);
}

void OpenGLGfx::disableAttribArray(uint location)
{
	glDisableVertexAttribArray(location);
}

void OpenGLGfx::activateTexture( uint texture, uint location)
{
	uint tLocation;
	if ( location==0 ) tLocation = GL_TEXTURE0;
	if ( location==1 ) tLocation = GL_TEXTURE1;
	if ( location==2 ) tLocation = GL_TEXTURE2;
	if ( location==3 ) tLocation = GL_TEXTURE3;
	if ( location==4 ) tLocation = GL_TEXTURE4;
	if ( location==5 ) tLocation = GL_TEXTURE5;

	glActiveTexture(tLocation);
	glBindTexture(GL_TEXTURE_2D, texture);

}


uint OpenGLGfx::loadFloatTexture(const void *floats, int size)
{
	GLuint texture;			// This is a handle to our texture object
	//GLenum texture_format;


	// Have OpenGL generate a texture object handle for us
	glGenTextures( 1, &texture );

	// Bind the texture object
	glBindTexture( GL_TEXTURE_2D, texture );

	// Set the texture's stretching properties
	//
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );

	// Edit the texture object's image data using the information SDL_Surface gives us
	glTexImage2D( GL_TEXTURE_2D, 0, GL_RGB32F, size, size, 0,
			GL_RGB, GL_UNSIGNED_BYTE, floats );

	return texture;
}

uint OpenGLGfx::loadDynamicTexture(const void *pixels, int size)
{
	GLuint texture;			// This is a handle to our texture object
	//GLenum texture_format;


	// Have OpenGL generate a texture object handle for us
	glGenTextures( 1, &texture );

	// Bind the texture object
	glBindTexture( GL_TEXTURE_2D, texture );

	// Set the texture's stretching properties
	//
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );

	// Edit the texture object's image data using the information SDL_Surface gives us
	glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, size, size, 0,
			GL_RGBA, GL_UNSIGNED_BYTE, pixels );

	return texture;
}

void OpenGLGfx::updateTexture(uint texture, const void* pixels, int size)
{
	glBindTexture( GL_TEXTURE_2D, texture );
	glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, size, size, 0,
				GL_RGBA, GL_UNSIGNED_BYTE, pixels );
}

void OpenGLGfx::updateFloatTexture(uint texture, const void* pixels, int size)
{
	glBindTexture( GL_TEXTURE_2D, texture );
	glTexImage2D( GL_TEXTURE_2D, 0, GL_RGB32F, size, size, 0,
			GL_RGB, GL_UNSIGNED_BYTE, pixels );
}


void OpenGLGfx::drawTriangles(uint n, bool isIndexed)
{
	if (!isIndexed)
	{
		glDrawArrays(GL_TRIANGLES, 0, n);
	}
	else
	{
		glDrawElements(GL_TRIANGLES, n, GL_UNSIGNED_SHORT, (void*)0);
	}
}

void OpenGLGfx::enableAdditiveBlend()
{
	glEnable(GL_BLEND);
	glBlendFunc(GL_ONE, GL_ONE);
}

void OpenGLGfx::enableAlphaBlend()
{
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

void OpenGLGfx::toggleBlend(bool b)
{
	if (b) glEnable(GL_BLEND);
	else glDisable(GL_BLEND);
}



void OpenGLGfx::drawLines(uint n)
{
	glDrawElements(GL_LINES, n, GL_UNSIGNED_SHORT, (void*)0);
}

void OpenGLGfx::deleteBuffer(uint* id)
{
	glDeleteBuffers(1,id);
}

void OpenGLGfx::toggleDepthMask(bool b)
{
	glDepthMask(b);
}
