/*
 * GlfwGfx.h
 *
 *  Created on: 30 Sep 2014
 *      Author: Adam Nasralla
 */

#ifndef GLFWGFX_H_
#define GLFWGFX_H_

#include <glm/fwd.hpp>
#include <osogine/gfx/IGfx.h>
#include <osogine/platform/OpenGL/shader.hpp>
#include <osogine/platform/OpenGL/texture.hpp>
#include <string>
#include <vector>


class OpenGLGfx : public IGfx
{
public:
	OpenGLGfx();
	~OpenGLGfx();

	void init();

	void toggleDepthTest(bool isOn);

	void clearBufferBits();
	void clearDepthBit();

	void clearColour(float r, float g, float b);

	uint loadShaders(string vsFile, string fsFile)
	{return LoadShaders(vsFile.c_str(), fsFile.c_str());}

	uint declareUniform(uint programId, string name);

	void setUniformValue(uint uniformId, int value);
	void setUniformValue(uint uniformId, float value);
	void setUniformValue(uint uniformId, vec2 value);
	void setUniformValue(uint uniformId, vec3 value);
	void setUniformValue(uint uniformId, vec4 value);
	void setUniformValue(uint uniformId, mat4 value);
	void setUniformValue(uint uniformId, vector<int> value, uint count);
	void setUniformValue(uint uniformId, vector<float> value, uint count);
	void setUniformValue(uint uniformId, vector<vec2> value, uint count);
	void setUniformValue(uint uniformId, vector<vec3> value, uint count);

	void uploadBuffer(uint* bufferId, uint size, void* buffer, bool isDynamic, bool isElementArray);

	void updateBuffer(uint* bufferId, uint size, void* buffer, bool isDynamic, bool isElementArray);

	void activateBuffer(uint id, uint location, uint noDimensions, bool isElementArray);

	void deleteBuffer(uint* id);

	void useProgram(uint id);

	void disableAttribArray(uint location);

	uint loadTexture( string filename, int size ) { return loadPNG( filename.c_str(), size ); }

	uint loadDynamicTexture(const void *pixels, int size);
	uint loadFloatTexture(const void *floats, int size);

	void updateTexture( uint texture, const void *pixels, int size);
	void updateFloatTexture(uint texture, const void *floats, int size);
	void activateTexture( uint texture, uint location ) ;

	void drawTriangles(uint n, bool isIndexed);

	void enableAdditiveBlend();
	void enableAlphaBlend();
	void toggleBlend(bool b);

	void drawLines(uint n);

	void toggleDepthMask(bool b);



};

#endif /* GLFWGFX_H_ */
