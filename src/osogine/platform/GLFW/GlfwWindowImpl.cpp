/*
 * GlfwWindowImpl.cpp
 *
 *  Created on: 30 Sep 2014
 *      Author: Adam Nasralla
 */

#include <stdio.h>
#include <stdlib.h>
#include <GLFW/glfw3.h>
#include <osogine/platform/GLFW/GlfwWindowImpl.h>

GlfwWindowImpl::GlfwWindowImpl()
{
	window = 0;

}

GlfwWindowImpl::~GlfwWindowImpl()
{
	// TODO Auto-generated destructor stub
}

void GlfwWindowImpl::init(string name, int width, int height, bool fullscreen)
{
	if( !glfwInit() )
	{
		fprintf( stderr, "Failed to initialize GLFW\n" );
	}

	glfwWindowHint(GLFW_SAMPLES, 8);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, true);    // To make MacOS happy; should not be needed


	// Open a window and create its OpenGL context
	if (!fullscreen)
	{
		window = glfwCreateWindow( width, height, name.c_str(), NULL, NULL);
	}
	else
	{
		window = glfwCreateWindow( width, height, name.c_str(),  glfwGetPrimaryMonitor(), NULL);
	}
	if( window == NULL ){
		fprintf( stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n" );
		glfwTerminate();
	}
	glfwMakeContextCurrent(window);

}

void GlfwWindowImpl::swapBuffers()
{
	glfwSwapBuffers(window);
}

bool GlfwWindowImpl::shouldCloseWindow()
{
	return (glfwWindowShouldClose(window) != 0);
}

void GlfwWindowImpl::terminate()
{
	glfwTerminate();
}
