/*
 * GlfwInput.cpp
 *
 *  Created on: 30 Sep 2014
 *      Author: Adam Nasralla
 */

#include <osogine/input/InputHub.h>
#include <osogine/platform/GLFW/GlfwInput.h>

InputHub* GlfwInput::inputHub = 0;

GlfwInput::GlfwInput()
{
	window = 0;
}

GlfwInput::~GlfwInput()
{
	// TODO Auto-generated destructor stub
}

void GlfwInput::init(GLFWwindow* window1)
{
	window = window1;
	glfwSetInputMode(window, GLFW_STICKY_KEYS, true);
	glfwSetKeyCallback(window, key_callback);
	glfwSetMouseButtonCallback(window, mouseButtonCallback);
	glfwSetCursorPosCallback(window, mouseMoveCallback);
	glfwSetScrollCallback(window, mouseScrollCallback);
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);

}

void GlfwInput::pollEvents()
{
	glfwPollEvents();
}

bool GlfwInput::escapeIsPressed()
{
	return false;
	//glfwGetKey(window, GLFW_KEY_ESCAPE ) == GLFW_PRESS;
}

void GlfwInput::setHub(InputHub* hub)
{
	inputHub = hub;
}

void GlfwInput::key_callback(GLFWwindow* window, int key, int scancode,
		int action, int mods)
{
	if (action == GLFW_PRESS) inputHub->keyPressed(key);
	if (action == GLFW_RELEASE) inputHub->keyReleased(key);
}

void GlfwInput::mouseButtonCallback(GLFWwindow* window, int button, int action,
		int mods)
{
	if (action == GLFW_PRESS) inputHub->mouseButtonPressed(button);
	if (action == GLFW_RELEASE) inputHub->mouseButtonReleased(button);
}

void GlfwInput::mouseMoveCallback(GLFWwindow* window, double x, double y)
{
	inputHub->mouseMoved(x,y);
}

void GlfwInput::mouseScrollCallback(GLFWwindow* window, double x, double y)
{
	inputHub->mouseScrolled(x,y);
}

void GlfwInput::setMousePos(float x, float y)
{
	glfwSetCursorPos(window, x, y);
}

void GlfwInput::setMouseVisible(bool isVisible)
{
	if (isVisible)
	{
		//glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
	}
	else
	{
		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	}
}
