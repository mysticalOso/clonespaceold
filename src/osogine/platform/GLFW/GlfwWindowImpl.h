/*
 * GlfwWindowImpl.h
 *
 *  Created on: 30 Sep 2014
 *      Author: Adam Nasralla
 */

#ifndef GLFWWINDOWIMPL_H_
#define GLFWWINDOWIMPL_H_

#include <osogine/window/IWindow.h>
#include <string>

using namespace std;

class GLFWwindow;

class GlfwWindowImpl : public IWindow
{
public:
	GlfwWindowImpl();
	~GlfwWindowImpl();


	void init(string name, int width, int height, bool fullscreen);


	void swapBuffers();

	bool shouldCloseWindow();

	void terminate();


	GLFWwindow* getWindow(){ return window; }

private:

	GLFWwindow* window;

};

#endif /* GLFWWINDOWIMPL_H_ */
