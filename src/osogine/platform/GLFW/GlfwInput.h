/*
 * GlfwInput.h
 *
 *  Created on: 30 Sep 2014
 *      Author: Adam Nasralla
 */

#ifndef GLFWINPUT_H_
#define GLFWINPUT_H_

#include <GLFW/glfw3.h>
#include <osogine/input/IInput.h>

class InputHub;

class GlfwInput : public IInput
{
public:
	GlfwInput();
	~GlfwInput();

	void init(GLFWwindow* window1);

	void pollEvents();

	bool escapeIsPressed();

	void setHub( InputHub* hub );

	void setMousePos( float x, float y );

	void setMouseVisible( bool isVisible );

	static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);

	static void mouseButtonCallback(GLFWwindow* window, int button, int action, int mods);

	static void mouseMoveCallback(GLFWwindow* window, double x, double y);

	static void mouseScrollCallback(GLFWwindow* window, double x, double y);


private:
	GLFWwindow* window;
	static InputHub* inputHub;
};

#endif /* GLFWINPUT_H_ */
