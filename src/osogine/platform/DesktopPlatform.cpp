/*
 * DesktopPlatform.cpp
 *
 *  Created on: 30 Sep 2014
 *      Author: Adam Nasralla
 */


#define GLM_FORCE_RADIANS


#include <osogine/audio/IAudio.h>
#include <osogine/gfx/IGfx.h>
#include <osogine/platform/DesktopPlatform.h>
#include <osogine/platform/GLFW/GlfwInput.h>
#include <osogine/platform/GLFW/GlfwWindowImpl.h>
#include <osogine/platform/OpenGL/OpenGLGfx.h>

#include <osogine/utils/Dice.h>

DesktopPlatform::DesktopPlatform(string name, int width, int height, bool fullscreen) : IPlatform()
{
	Dice::seed();

	GlfwWindowImpl* glfwWindow = new GlfwWindowImpl();
	glfwWindow->init(name, width, height, fullscreen);
	windowInterface = glfwWindow;

	gfxInterface = new OpenGLGfx();
	gfxInterface->init();

	GlfwInput* glfwInput = new GlfwInput();
	glfwInput->init(glfwWindow->getWindow());
	inputInterface = glfwInput;

	audioInterface = 0;
}

DesktopPlatform::~DesktopPlatform()
{
	//delete windowInterface;
	//delete gfxInterface;
	//delete inputInterface;
	//delete audioInterface;
}

