/*
 * PlatformInterface.h
 *
 *  Created on: 30 Sep 2014
 *      Author: Adam Nasralla
 */

#ifndef IPLATFORM_H_
#define IPLATFORM_H_

#include <string>

using namespace std;

class IWindow;
class IGfx;
class IInput;
class IAudio;
class GfxBuilder;
class GfxStorage;

class IPlatform
{
public:
	IPlatform(){}
	virtual ~IPlatform(){}

	virtual IWindow* getWindowInterface(){ return 0; }
	virtual IGfx* getGfxInterface(){ return 0; }
	virtual IInput* getInputInterface(){ return 0; }
	virtual IAudio* getAudioInterface(){ return 0; }


};

#endif /* IPLATFORM_H_ */
