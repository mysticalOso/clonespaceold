/*
 * GameBase.h
 *
 *  Created on: 8 Oct 2014
 *      Author: Adam Nasralla
 */

#ifndef GAME_H_
#define GAME_H_

#include <string>
#include <glm/fwd.hpp>

using namespace std;
using namespace glm;

class IPlatform;
class IInput;
class IGfx;
class IWindow;
class IAudio;

class GfxHub;
class AudioHub;
class InputHub;
class EntityHub;

class GfxObject;
class GfxProgram;
class Camera;
class Controller;
class Entity;
class IEntity;
class EntityBuilder;

class GfxBuilder;
class GfxIndexer;

class Game
{
public:
	Game();
	virtual ~Game();

	Camera* getCamera();

	GfxObject* getGfxObject( string name );
	GfxProgram* getGfxProgram( string name );
	uint getTexture( string name );

	Controller* getController( string name );

	IEntity* getEntity( string name );
	EntityBuilder* getBuilder( string name );

	void initPlatform( IPlatform* platform );

	void initGfxHub(GfxHub* gfxHub);
	void initAudioHub(AudioHub* audioHub);
	void initInputHub(InputHub* inputHub);
	void initEntityHub(EntityHub* entityHub);

	virtual GfxHub* getGfxHub(){ return gfxHub; }
	InputHub* getInputHub(){ return inputHub; }
	AudioHub* getAudioHub(){ return audioHub; }
	EntityHub* getEntityHub(){ return entityHub; }

	GfxBuilder* getGfxBuilder();
	GfxIndexer* getGfxIndexer();

	virtual void event(string name);
	void run(){ gameLoop(); }

protected:

	virtual void gameLoop();
	virtual bool shouldQuit();

	IPlatform* platform;
	IWindow* window;
	IInput* input;
	IGfx* gfxI;
	IAudio* audio;

	GfxHub* gfxHub;
	InputHub* inputHub;
	AudioHub* audioHub;
	EntityHub* entityHub;

	Camera* camera;


};

#endif /* GAME_H_ */
