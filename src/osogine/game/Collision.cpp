/*
 * Collision.cpp
 *
 *  Created on: 18 Dec 2014
 *      Author: mysticalOso
 */

#include <osogine/game/Collision.h>
#include <osogine/game/Entity.h>
#include <osogine/utils/Polygon.h>
#include <osogine/utils/Utils.h>



Collision::Collision(Entity* a, Entity* b, string name)
{
	this->a = a;
	this->b = b;
	point = a->getOutline()->getLastCollision();
	this->name = name;
}

Collision::~Collision()
{
}

bool Collision::nameIs(string s)
{
	return Utils::stringIsEqual(s,name);
}
