/*
 * LightBase.h
 *
 *  Created on: 8 Oct 2014
 *      Author: Adam Nasralla
 */

#ifndef LIGHT_H_
#define LIGHT_H_

#include <osogine/game/Light.h>
#include <osogine/game/TextureEntity.h>

class LightHub;

//TODO remove lightPosition and use centered gfx
//TODO take out explodingLight sub class
class Light: public TextureEntity
{
public:
	Light(Entity* parent);
	~Light(){}

	void update();
	vec3 getColour(){ return colour; }
	void setColour(vec3 c){ colour = c; }

	void setLightPosition(vec3 p){ lightPosition = p; }
	vec3 getLightPosition();
	vec3 getLocalLightPos(){ return lightPosition; }

	void explode(float peakTime, float deathTime, vec3 targetColour);

	LightHub* getOwner(){ return ownerHub; }

private:
	void updateExplosion();
	vec3 colour;
	vec3 lightPosition;
	int count;
	int peakTime;
	int deathTime;
	vec3 startColour;
	vec3 targetColour;
	LightHub* ownerHub;
};

#endif /* LIGHT_H_ */
