/*
 * Camera.cpp
 *
 *  Created on: 8 Oct 2014
 *      Author: Adam Nasralla
 */

#define GLM_FORCE_RADIANS
#define degreesToRadians(x) x*(3.141592f/180.0f)

#include <osogine/game/Camera.h>
#include <glm/gtc/matrix_transform.hpp>

#include <osogine/utils/Transform.h>

Camera::Camera(Game* game) : Entity(game)
{
	setPosition(0, 0, 50000);
	setVelocityIsRelative(true);
	setSpinIsRelative(true);
	zoom = 1.238f;

	projectionMatrix = glm::perspective( degreesToRadians(zoom) , 16.0f / 9.0f, 1000.0f, 100000.0f);


	glm::mat4 View =  glm::inverse( getTransform()->getMatrix() );

	uiMatrix = projectionMatrix * View;
}

Camera::~Camera()
{

}


mat4 Camera::getViewProjectionMatrix()
{
	// Camera matrix
//	glm::mat4 View       = glm::lookAt(
//								pos, // Camera is at (4,3,-3), in World Space
//								glm::vec3(0,0,0), // and looks at the origin
//								glm::vec3(0,1,0)  // Head is up (set to 0,-1,0 to look upside-down)
//						   );

	glm::mat4 View =  glm::inverse( getTransform()->getMatrix() );

	return projectionMatrix * View;
}

vec2 Camera::getClipPos(vec3 pos)
{
	vec4 homo =  getViewProjectionMatrix() * vec4(pos,1);
	vec2 clipPos = vec2(homo / homo.w);
	return clipPos;
}

mat4 Camera::getProjectionMatrix()
{
	return uiMatrix;
}
