/*
 * Entity.h
 *
 *  Created on: 30 Sep 2014
 *      Author: Adam Nasralla
 */

#ifndef ENTITY_H_
#define ENTITY_H_

#include <glm/fwd.hpp>
#include <osogine/game/EntityList.h>
#include <osogine/game/IEntity.h>
#include <osogine/utils/List.h>
#include <string>


class Game;
class GfxObject;
class Camera;
class GfxProgram;
class Transform;
class Physics;
class Polygon;

class Entity : public IEntity
{
public:
	Entity();
	Entity(Game* game);
	Entity(IEntity* parent);
	virtual ~Entity();

	virtual void addChild(IEntity* e);
	virtual void removeChild(IEntity* e);
	void setChildIndex(IEntity* child, int index);

	virtual void update();
	virtual void update(int order);

	virtual void render();
	virtual void render(int order);

	virtual Game* getGame(){ return game; }

	virtual GfxObject* getGfx() { return gfx; }
	virtual void setGfx( GfxObject* gfx1 ) { gfx = gfx1; }
	virtual void setGfx( string name );

	virtual GfxProgram* getProgram() { return program; }
	virtual void setProgram( GfxProgram* program1 ) { program = program1; }
	virtual void setProgram( string name );

	virtual void setAlpha( float alpha );
	virtual float getAlpha();

	virtual Camera* getCamera();

	virtual void setController(string name);
	virtual void removeController(string name);

	virtual void setParent(IEntity* parent);
	virtual IEntity* getParent(){return parent;}

	virtual void matchRotation(Entity* entity);

	virtual void setOutline(Polygon* p);
	virtual void setOutline(List<vec2>* o);
	virtual Polygon* getOutline(){ return outline; }

	virtual bool collideWith(IEntity* e);
	virtual bool collideWith(Entity* e);
	virtual bool collideWith(Polygon* p);
	virtual bool intersectWith(Polygon* p);
	virtual bool intersectWith(Entity* p);

	float getArea();

	//TODO move getSpeed to physics
	virtual float getSpeed();

	Entity* getClosest(EntityList* entities);

	float distTo2(Entity* entity);
	float distTo(Entity* entity);
	float angleTo(Entity* entity);

	void globaliseTransform();

	void applyInverseOf(Entity* e);



	//**TRANSFORM INTERFACE**//

	virtual mat4 getLocalMatrix();
	virtual mat4 getMatrix();

	virtual void rotateX( float degrees );
	virtual void rotateY( float degrees );
	virtual void rotateZ( float degrees );

	virtual void rotateXRelative( float degrees );
	virtual void rotateYRelative( float degrees );
	virtual void rotateZRelative( float degrees );

	virtual void resetRotation();

	virtual mat4 getLocalRotation();
	virtual void setRotation(float x, float y, float z);
	virtual void setRotationD(float x, float y, float z);
	virtual void setRotation(vec3 r);
	virtual void setRotation(mat4 r);

	virtual void setPosition(vec3 translation);
	virtual void setPosition(float x, float y, float z);
	virtual void setX(float x);
	virtual void setY(float y);
	virtual void setZ(float z);

	virtual void setScale(vec3 scale);
	virtual void setScale(float s);
	virtual void setScale(float x, float y, float z);
	virtual vec3 getScale();

	virtual vec3 getLocalPosition();
	virtual vec3 getPosition();
	virtual float getX();
	virtual float getY();
	virtual float getZ();

	virtual void move(vec3 direction);
	virtual void move(float x, float y, float z);

	virtual void moveRelative(vec3 direction);

	virtual void lookAt(vec3 target, vec3 up);
	virtual vec3 transformDirection(vec3 direction);
	virtual vec3 getDirection();
	virtual vec3 getLocalDirection();
	virtual mat4 getRotation();
	virtual float getAngle();
	virtual Transform* getTransform(){ return transform; }



	//**PHYSICS INTERFACE**//

	virtual void setVelocity(float x, float y, float z);
	virtual void setVelocity(vec3 v);
	virtual vec3 getVelocity();

	virtual void setSpin(float x, float y, float z);
	virtual void setSpin(vec3 s);
	virtual vec3 getSpin();

	virtual void setForce(float x, float y, float z);
	virtual void setForce(vec3 f);
	virtual vec3 getForce();

	virtual void applyTorque(float x, float y, float z);
	virtual void applyTorque(vec3 t);

	virtual void applyForce(float x, float y, float z);
	virtual void applyForce(vec3 f);

	virtual void setTorque(float x, float y, float z);
	virtual void setTorque(vec3 t);
	virtual vec3 getTorque();

	virtual void setFriction(float f);
	virtual float getFriction();

	virtual void setMass(float m);
	virtual float getMass();

	virtual void setVelocityIsRelative(bool isRelative);
	virtual bool getVelocityIsRelative();

	virtual void setSpinIsRelative(bool isRelative);
	virtual bool getSpinIsRelative();



	//**UTIL FUNCTIONS**//
	virtual float angBetween(vec2 direction);
	virtual float isClockwiseTo(vec2 direction);

	virtual void setDisableCamera(bool b){ disableCamera = b; children->setDisableCamera(b); }
	virtual bool getDisableCamera(){ return disableCamera; }

	virtual void setIsVisible(bool b){ isVisible = b; children->setIsVisible(b); }
	virtual bool getIsVisible(){ return isVisible; }

protected:

	Game* game;
	EntityList* children;
	GfxObject* gfx;
	Transform* transform;
	GfxProgram* program;
	Physics* physics;
	IEntity* parent;
	Polygon* outline;
	bool disableCamera;
	bool isVisible;


	void initPointers();
};

#endif /* ENTITY_H_ */
