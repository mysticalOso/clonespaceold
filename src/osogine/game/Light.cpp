

/*
 * LightBase.cpp
 *
 *  Created on: 8 Oct 2014
 *      Author: Adam Nasralla
 */

#include <hubs/LightHub.h>
#include <osogine/game/IEntity.h>
#include <osogine/game/Light.h>
#include <osogine/utils/Polygon.h>
#include <osogine/utils/Transform.h>
#include <osogine/utils/Utils.h>





Light::Light(Entity* parent) : TextureEntity(parent)
{
	ownerHub = (LightHub*) parent;
	outline = new Polygon();
	colour = vec3(1,1,1);
	count = 0;
	peakTime = 0;
	deathTime = 0;
}

void Light::update()
{
	TextureEntity::update();
	if (count<deathTime)
	{
		updateExplosion();
	}
}

void Light::explode(float peakTime, float deathTime, vec3 targetColour)
{
	count = 0;
	this->peakTime = peakTime;
	this->deathTime = deathTime;
	this->targetColour = targetColour;
	startColour = colour;
}

vec3 Light::getLightPosition()
{
	return transform->applyTo(lightPosition);
}

void Light::updateExplosion()
{
	count++;
	if (count<peakTime)
	{
		float ratio = (float)count / (float)peakTime;
		colour = Utils::easeOutInterpolate(startColour,targetColour,ratio);

	}
	else if (count<deathTime)
	{
		float ratio = (float)(count-peakTime) / (float)(deathTime-peakTime);
		colour = Utils::sinInterpolate(targetColour,vec3(0,0,0),ratio);
	}
	else if (count==deathTime)
	{
		parent->removeChild(this);
		delete this;
	}
}
