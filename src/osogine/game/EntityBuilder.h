/*
 * EntityBuilder.h
 *
 *  Created on: 24 Oct 2014
 *      Author: Adam Nasralla
 */

#ifndef ENTITYBUILDER_H_
#define ENTITYBUILDER_H_

class Game;
class GfxBuilder;
class IEntity;
class Hub;

#include <string>

class EntityBuilder
{
public:
	EntityBuilder(Game* game);
	virtual ~EntityBuilder(){}
    virtual IEntity* build(){ return 0; }
	virtual IEntity* build(std::string s){ return 0; }
	virtual void setHub(Hub* hub){}

protected:
	GfxBuilder* gfxBuilder;
	Game* game;
};

#endif /* ENTITYBUILDER_H_ */
