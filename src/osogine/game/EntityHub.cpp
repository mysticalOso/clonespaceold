/*
 * EntityHub.cpp
 *
 *  Created on: 8 Oct 2014
 *      Author: Adam Nasralla
 */

#include <osogine/game/Camera.h>
#include <osogine/game/EntityHub.h>
#include <osogine/game/Game.h>
#include <osogine/gfx/GfxHub.h>
#include <string>

EntityHub::EntityHub() : Entity()
{
	gfxBuilder = 0;
	camera = 0;
}

void EntityHub::init(Game* game)
{
	this->game = game;

	gfxBuilder = game->getGfxHub()->getBuilder();

	initBuilders();
	initCamera();
	initEntities();


	addChild(camera);
}

void EntityHub::initCamera()
{
	camera = new Camera(game);
}

void EntityHub::addBuilder(string name, EntityBuilder* builder)
{
	builders[name] = builder;
}

IEntity* EntityHub::getEntity(string name)
{
	return entities[name];
}

EntityBuilder* EntityHub::getBuilder(string name)
{
	return builders[name];
}

EntityHub::~EntityHub()
{
}

void EntityHub::addEntity(string name, IEntity* entity)
{
	entities[name] = entity;
	addChild(entity);
}

Camera* EntityHub::getCamera()
{
	return camera;
}

void EntityHub::initEntities()
{
	//TestEntity* t = new TestEntity(game);
	//addEntity("test",t);
	//t->setController("simple");
}
