/*
 * EntityList.cpp
 *
 *  Created on: 31 Oct 2014
 *      Author: Adam Nasralla
 */

#include <osogine/game/EntityList.h>
#include <osogine/game/IEntity.h>

void EntityList::update()
{
	for (int i = 0; i < entities.size(); i++)
	{
		entities.get(i)->update();
	}
}

void EntityList::update(int order)
{
	for (int i = 0; i < entities.size(); i++)
	{
		IEntity* e = entities.get(i);
		e->update(order);
	}
}

void EntityList::render()
{
	for (int i = 0; i < entities.size(); i++)
	{
		entities.get(i)->render();
	}
}

void EntityList::render(int order)
{
	for (int i = 0; i < entities.size(); i++)
	{
		entities.get(i)->render(order);
	}
}

void EntityList::empty()
{
	for (int i = 0; i < entities.size(); i++)
	{
		//delete entities.get(i);
	}
	entities.clear();
}

void EntityList::init()
{
	for (int i = 0; i < entities.size(); i++)
	{
		entities.get(i)->init();
	}
}

bool EntityList::collideWith(IEntity* entity)
{
	bool result = false;
	for (int i = 0; i < entities.size(); i++)
	{
		if (entities.get(i)->collideWith(entity)) result = true;
	}
	return result;
}

bool EntityList::intersectWith(IEntity* entity)
{
	bool result = false;
	for (int i = 0; i < entities.size(); i++)
	{
		if (entities.get(i)->intersectWith(entity)) result = true;
	}
	return result;
}

void EntityList::setDisableCamera(bool b)
{
	for (int i = 0; i < entities.size(); i++)
	{
		entities.get(i)->setDisableCamera(b);
	}
}

void EntityList::setIsVisible(bool b)
{
	for (int i = 0; i < entities.size(); i++)
	{
		entities.get(i)->setIsVisible(b);
	}
}

void EntityList::event(string name)
{
	for (int i = 0; i < entities.size(); i++)
	{
		entities.get(i)->event(name);
	}
}

void EntityList::mouseDown()
{
	for (int i = 0; i < entities.size(); i++)
	{
		entities.get(i)->mouseDown();
	}
}

void EntityList::mouseUp()
{
	for (int i = 0; i < entities.size(); i++)
	{
		entities.get(i)->mouseUp();
	}
}

void EntityList::mouseMoved(float x, float y)
{
	for (int i = 0; i < entities.size(); i++)
	{
		entities.get(i)->mouseMoved(x,y);
	}
}

void EntityList::setAlpha(float a)
{
	for (int i = 0; i < entities.size(); i++)
	{
		entities.get(i)->setAlpha(a);
	}
}

void EntityList::fadeOut()
{
	for (int i = 0; i < entities.size(); i++)
	{
		entities.get(i)->fadeOut();
	}
}

void EntityList::fadeIn()
{
	for (int i = 0; i < entities.size(); i++)
	{
		entities.get(i)->fadeIn();
	}
}

void EntityList::toggleActive(bool b)
{
	for (int i = 0; i < entities.size(); i++)
	{
		entities.get(i)->toggleActive(b);
	}
}
