/*
 * TextEntity.h
 *
 *  Created on: 11 Feb 2015
 *      Author: Adam Nasralla
 */

#ifndef SRC_OSOGINE_GAME_TEXTENTITY_H_
#define SRC_OSOGINE_GAME_TEXTENTITY_H_

#include <osogine/game/TextureEntity.h>
#include <glm/fwd.hpp>

class TextEntity: public TextureEntity
{
public:
	TextEntity(Entity* parent, float width);
	~TextEntity();

	void setInvert(bool b){ invert = b; }
	void setText(string str);

private:

	void buildGfx(string str);
	void addLineToGfx(string str, float yOffset);
	List<string> spiltLines(string str);

	float charWidth;
	float charHeight;
	float charWidthUV;
	float charHeightUV;

	bool invert;

	int charsPerLine;
	float lineHeight;
};

#endif /* SRC_OSOGINE_GAME_TEXTENTITY_H_ */
