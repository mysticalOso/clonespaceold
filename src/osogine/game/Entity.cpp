/*
 * Entity.cpp
 *
 *  Created on: 30 Sep 2014
 *      Author: Adam Nasralla
 */

#include <glm/vec2.hpp>
#include <glm/geometric.hpp>
#include <osogine/game/Camera.h>
#include <osogine/game/Entity.h>
#include <osogine/game/Game.h>
#include <osogine/game/Physics.h>
#include <osogine/gfx/GfxObject.h>
#include <osogine/gfx/GfxProgram.h>
#include <osogine/input/Controller.h>
#include <osogine/utils/Polygon.h>
#include <osogine/utils/Transform.h>
#include <osogine/utils/Utils.h>

Entity::Entity( ) : IEntity()
{
	initPointers();
}

Entity::Entity( Game* game ) : IEntity()
{
	initPointers();

	this->game = game;
}

Entity::Entity( IEntity* parent ) : IEntity()
{
	initPointers();
	renderOrder = parent->getRenderOrder();
	game = parent->getGame();
	parent->addChild(this);
	this->parent = parent;
}


Entity::~Entity()
{
	//delete transform;
	//delete physics;
	//delete children;
}

Camera* Entity::getCamera()
{
	return game->getCamera();
}

void Entity::render()
{
	if (program!=0 && gfx!=0) program->render( this );
	//children->render();
}

void Entity::render(int order)
{
	if (renderOrder==order) render();
	children->render(order);
}

void Entity::setController(string name)
{
	game->getController(name)->setEntity(this);
}

void Entity::removeController(string name)
{
	game->getController(name)->setEntity(0);
}

void Entity::setGfx(string name)
{
	setGfx(game->getGfxObject(name));
}

void Entity::setProgram(string name)
{
	setProgram(game->getGfxProgram(name));
}

void Entity::matchRotation(Entity* entity)
{
	setRotation(entity->getRotation());
}


void Entity::initPointers()
{
	children = new EntityList();
	game = 0;
	transform = new Transform();
	gfx = 0;
	program = 0;
	outline = 0;
	updateOrder = 0;
	renderOrder = 0;
	parent = 0;
	disableCamera = false;
	isVisible = true;
	physics = new Physics();
}

void Entity::update()
{
	//if (transform!=0 && velocity!=0 ) move(velocity);
	if (outline!=0) outline->update();
	physics->update(this);
	//children->update();
}

void Entity::update(int order)
{
	children->update(order);
	if (updateOrder == order) update();

}

void Entity::addChild(IEntity* e)
{
	children->add(e);
	Transform* childTransform = e->getTransform();
	if (childTransform != 0 && transform != 0)
	{
		childTransform->setParent(transform);
	}
	//IEntity* oldParent = e->getParent();
	//if (oldParent!=0) oldParent->removeChild(e);
	e->setParent(this);
}

void Entity::removeChild(IEntity* e)
{
	children->remove(e);
}

void Entity::setChildIndex(IEntity* child, int index)
{
	children->remove(child);
	children->insert(index,child);
}



//**TRANSFORM INTERFACE**//

mat4 Entity::getLocalMatrix(){ return transform->getLocalMatrix(); }
mat4 Entity::getMatrix(){ return transform->getMatrix(); }

void Entity::rotateX( float degrees ){ transform->rotateX(degrees); }
void Entity::rotateY( float degrees ){ transform->rotateY(degrees); }
void Entity::rotateZ( float degrees ){ transform->rotateZ(degrees); }

void Entity::rotateXRelative( float degrees ){ transform->rotateXRelative(degrees); }
void Entity::rotateYRelative( float degrees ){ transform->rotateYRelative(degrees); }
void Entity::rotateZRelative( float degrees ){ transform->rotateZRelative(degrees); }

void Entity::resetRotation(){ transform->resetRotation(); }

mat4 Entity::getLocalRotation(){ return transform->getLocalRotation(); }
void Entity::setRotation(float x, float y, float z){ transform->setRotation(vec3(x,y,z)); }
void Entity::setRotation(vec3 r){ transform->setRotation(r); }
void Entity::setRotation(mat4 r){ transform->setRotation(r); }
void Entity::setRotationD(float x, float y, float z){transform->setRotationD(x,y,z);}
void Entity::setPosition(float x, float y, float z){ transform->setTranslation(x,y,z); }

void Entity::setPosition(vec3 translation){ transform->setTranslation(translation); }

void Entity::setScale(vec3 scale){ transform->setScale(scale); }
void Entity::setScale(float s){ transform->setScale(s,s,s); }
void Entity::setScale(float x, float y, float z){ transform->setScale(x,y,z); }
vec3 Entity::getScale(){ return transform->getScale(); }

vec3 Entity::getLocalPosition(){ return transform->getLocalTranslation(); }
vec3 Entity::getPosition(){ return transform->getTranslation(); }
float Entity::getX(){ return transform->getTranslation().x; }
float Entity::getY(){ return transform->getTranslation().y; }
float Entity::getZ(){ return transform->getTranslation().z; }

void Entity::setX(float x){ transform->setTranslation(x,transform->getTranslation().y,transform->getTranslation().z); }
void Entity::setY(float y){ transform->setTranslation(transform->getTranslation().x,y,transform->getTranslation().z); }
void Entity::setZ(float z){ transform->setTranslation(transform->getTranslation().x,transform->getTranslation().y,z); }

void Entity::move(vec3 direction){ transform->move(direction); }
void Entity::move(float x, float y, float z){ transform->move(x,y,z); }

void Entity::moveRelative(vec3 direction){ transform->moveRelative(direction); }

vec3 Entity::transformDirection(vec3 direction) { return transform->transformDirection(direction); }
vec3 Entity::getDirection() { return transform->getDirection(); }
vec3 Entity::getLocalDirection() { return transform->getLocalDirection(); }
float Entity::getAngle() { return transform->getAngle(); }



//**PHYSICS INTERFACE**//

void Entity::setVelocity(float x, float y, float z){ setVelocity(vec3(x,y,z)); }
void Entity::setVelocity(vec3 v){ physics->setVelocity(v); }
vec3 Entity::getVelocity(){ return physics->getVelocity(); }
float Entity::getSpeed(){ return physics->getSpeed(); }

void Entity::setSpin(float x, float y, float z){ setSpin(vec3(x,y,z)); }
void Entity::setSpin(vec3 s){ physics->setSpin(s); }
vec3 Entity::getSpin(){ return physics->getSpin(); }

void Entity::setForce(float x, float y, float z){ setForce(vec3(x,y,z)); }
void Entity::setForce(vec3 f){ physics->setForce(f); }
vec3 Entity::getForce(){ return physics->getForce(); }

void Entity::applyTorque(float x, float y, float z){ physics->applyTorque(x,y,z); }
void Entity::applyTorque(vec3 t){ physics->applyTorque(t); }

void Entity::applyForce(float x, float y, float z){ physics->applyForce(x,y,z); }
void Entity::applyForce(vec3 f){ physics->applyForce(f); }

void Entity::setTorque(float x, float y, float z){ setVelocity(vec3(x,y,z)); }
void Entity::setTorque(vec3 t){ physics->setTorque(t); }
vec3 Entity::getTorque(){ return physics->getTorque(); }

void Entity::setFriction(float f){ physics->setFriction(f); }
float Entity::getFriction(){ return physics->getFriction(); }

void Entity::setMass(float m){ physics->setMass(m); }
float Entity::getMass(){ return physics->getMass(); }

void Entity::setVelocityIsRelative(bool isRelative){ physics->setVelocityIsRelative(isRelative); }
bool Entity::getVelocityIsRelative(){ return physics->getVelocityIsRelative(); }

void Entity::setSpinIsRelative(bool isRelative){ physics->setVelocityIsRelative(isRelative); }
bool Entity::getSpinIsRelative() {
	return physics->getVelocityIsRelative();
}

void Entity::lookAt(vec3 target, vec3 up) { transform->lookAt(target,up); }



mat4 Entity::getRotation() {
	return transform->getRotation();
}


float Entity::angBetween(vec2 direction) {
	return Utils::angBetween(vec2(getDirection()), direction);
}

float Entity::isClockwiseTo(vec2 direction)
{
	return Utils::isClockwiseTo(vec2(getDirection()), direction);
}

bool Entity::collideWith(Entity* e)
{
	getOutline()->setTransform(getTransform());
	return getOutline()->collideWith(e->getOutline());
}

bool Entity::collideWith(Polygon* p)
{
	getOutline()->setTransform(getTransform());
	return getOutline()->collideWith(p);
}

bool Entity::intersectWith(Polygon* p)
{
	getOutline()->setTransform(getTransform());
	return getOutline()->intersectWith(p);
}

bool Entity::intersectWith(Entity* e)
{
	getOutline()->setTransform(getTransform());
	return getOutline()->intersectWith(e->getOutline());
}

void Entity::setOutline(Polygon* p)
{
	 outline = p;
	 outline->setTransform(getTransform());
}

void Entity::setOutline(List<vec2>* o)
{
	outline = new Polygon(o);
	outline->setTransform(getTransform());
}

float Entity::getArea()
{
	return outline->getArea();
}



Entity* Entity::getClosest(EntityList* entities)
{
	float minDist = 99999999999;
	Entity* closest = 0;
	for (int i = 0; i < entities->size(); i++)
	{
		float dist = distTo2((Entity*)entities->get(i));
		if (dist < minDist)
		{
			closest = (Entity*) entities->get(i);
			minDist = dist;
		}
	}
	return closest;
}

float Entity::distTo2(Entity* object)
{
	vec2 subjectPos = vec2(getPosition());
	vec2 objectPos = vec2(object->getPosition());
	return Utils::dist2(subjectPos,objectPos);
}

float Entity::distTo(Entity* entity)
{
	return glm::sqrt(distTo2(entity));
}

float Entity::angleTo(Entity* entity)
{
	vec3 subjectPos = getPosition();
	vec3 objectPos = entity->getPosition();
	return atan2(objectPos.y - subjectPos.y, objectPos.x - subjectPos.y);
}

void Entity::setAlpha(float alpha)
{
	if (gfx!=0) gfx->setAlpha(alpha);
}

float Entity::getAlpha()
{
	return gfx->getAlpha();
}

bool Entity::collideWith(IEntity* e)
{
	return collideWith((Entity*)e);
}

void Entity::globaliseTransform()
{
	setPosition(getPosition());
	setRotation(getRotation());
	transform->setParent(0);
}

void Entity::applyInverseOf(Entity* e)
{
	Transform* trans = getTransform();
	Transform* trans2 = e->getTransform();
	trans->applyInverseOf(trans2);
}

void Entity::setParent(IEntity* parent)
{
	this->parent = parent;
	transform->setParent(parent->getTransform());
}
