/*
 * EntityList.h
 *
 *  Created on: 8 Oct 2014
 *      Author: Adam Nasralla
 */

#ifndef ENTITYLIST_H_
#define ENTITYLIST_H_


#include <osogine/utils/List.h>
#include <string>

using namespace std;

class IEntity;


class EntityList
{
public:
	EntityList(){}
	virtual ~EntityList(){}

	virtual void update();
	virtual void render();
	virtual void update(int order);
	virtual void render(int order);
	virtual void init();
	virtual bool intersectWith(IEntity* entity);
	virtual bool collideWith(IEntity* entity);
	virtual void empty();

	virtual bool contains( IEntity* value ){ return entities.contains(value); }
	virtual void add( IEntity* value ){ if (!contains(value)) entities.add(value);  }
	virtual int size(){ return entities.size(); }
	virtual IEntity* get( uint n ){ return entities.get(n); }
	virtual void set( int index, IEntity* value ){ entities.set(index,value); }
	virtual void clear(){ entities.clear(); }
	virtual void insert(int index, IEntity* value){ entities.insert(index,value); }
	virtual void remove( IEntity* value ){ entities.remove(value); }

	virtual void setDisableCamera( bool b );
	virtual void setIsVisible( bool b );

	virtual void setAlpha( float a );

	virtual void event( string name );

	virtual void mouseDown();
	virtual void mouseUp();
	virtual void mouseMoved(float x, float y);

	virtual void fadeOut();
	virtual void fadeIn();

	virtual void toggleActive(bool b);
	//TODO add append if needed


private:

	List<IEntity*> entities;
};


#endif /* ENTITYLIST_H_ */
