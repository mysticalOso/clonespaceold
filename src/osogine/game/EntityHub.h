/*
 * EntityHub.h
 *
 *  Created on: 8 Oct 2014
 *      Author: Adam Nasralla
 */

#ifndef ENTITYHUB_H_
#define ENTITYHUB_H_

#include <osogine/game/Entity.h>
#include <map>

class EntityBuilder;
class Game;
class GfxBuilder;

class EntityHub : public Entity
{
public:
	EntityHub();
	void init(Game* game);
	void addEntity(string name, IEntity* entity);
	void addBuilder(string name, EntityBuilder* builder);
	IEntity* getEntity(string name);
	EntityBuilder* getBuilder(string name);
	virtual Camera* getCamera();
	virtual void initEntities();
	virtual void initCamera();
	virtual void initBuilders(){}
	virtual ~EntityHub();


protected:
	map<string, IEntity*> entities;
	map<string, EntityBuilder*> builders;
	Camera* camera;

	GfxBuilder* gfxBuilder;
};

#endif /* ENTITYHUB_H_ */
