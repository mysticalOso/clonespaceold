/*
 * GameBase.cpp
 *
 *  Created on: 8 Oct 2014
 *      Author: Adam Nasralla
 */

#include <osogine/audio/AudioHub.h>
#include <osogine/game/EntityHub.h>
#include <osogine/game/Game.h>
#include <osogine/gfx/GfxHub.h>
#include <osogine/gfx/IGfx.h>
#include <osogine/input/IInput.h>
#include <osogine/input/InputHub.h>
#include <osogine/platform/IPlatform.h>
#include <osogine/window/IWindow.h>

Game::Game()
{
	audio = 0;
	gfxI = 0;
	platform = 0;
	window = 0;
	input = 0;
	gfxHub = 0;
	inputHub = 0;
	audioHub = 0;
	entityHub = 0;
	camera = 0;
}

Game::~Game()
{
}


GfxObject* Game::getGfxObject(string name)
{
	return gfxHub->getObject( name );
}

GfxProgram* Game::getGfxProgram(string name)
{
	return gfxHub->getProgram(name);
}


uint Game::getTexture(string name)
{
	return gfxHub->getTexture(name);
}

void Game::initGfxHub(GfxHub* gfxHub)
{
	this->gfxHub = gfxHub;
	gfxHub->init(gfxI);

}

void Game::initAudioHub(AudioHub* audioHub)
{
	this->audioHub = audioHub;
	audioHub->init(audio);

}

void Game::initInputHub(InputHub* inputHub)
{
	this->inputHub = inputHub;
	inputHub->init(input, this);

}

void Game::initEntityHub(EntityHub* entityHub)
{
	this->entityHub = entityHub;
	entityHub->init(this);

}



void Game::gameLoop()
{
	while ( !shouldQuit() )
	{
		gfxI->clearBufferBits();
		entityHub->update();
		entityHub->render();
		window->swapBuffers();
		input->pollEvents();
	}
}

bool Game::shouldQuit()
{
	return ( window->shouldCloseWindow() || input->escapeIsPressed() );
}

void Game::initPlatform(IPlatform* platform1)
{
	platform = platform1;
	window = platform->getWindowInterface();
	input = platform->getInputInterface();
	gfxI = platform->getGfxInterface();
	audio = platform->getAudioInterface();
}

Controller* Game::getController(string name)
{
	return inputHub->getController( name );
}

Camera* Game::getCamera()
{
	return entityHub->getCamera();
}

IEntity* Game::getEntity(string name)
{
	return entityHub->getEntity(name);
}

EntityBuilder* Game::getBuilder(string name)
{
	return entityHub->getBuilder(name);
}

GfxBuilder* Game::getGfxBuilder()
{
	return gfxHub->getBuilder();
}

GfxIndexer* Game::getGfxIndexer()
{
	return gfxHub->getIndexer();
}

void Game::event(string name)
{
	entityHub->event(name);
}
