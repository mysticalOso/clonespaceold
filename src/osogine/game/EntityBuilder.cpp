/*
 * EntityBuilder.cpp
 *
 *  Created on: 28 Oct 2014
 *      Author: Adam Nasralla
 */

#include <osogine/game/EntityBuilder.h>
#include <osogine/game/Game.h>
#include <osogine/gfx/GfxHub.h>


EntityBuilder::EntityBuilder(Game* game)
{
	gfxBuilder = game->getGfxHub()->getBuilder();
	this->game = game;
}


