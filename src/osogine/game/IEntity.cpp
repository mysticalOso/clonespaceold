/*
 * IEntity.cpp
 *
 *  Created on: 25 Jan 2015
 *      Author: mysticalOso
 */
#include <osogine/game/IEntity.h>

IEntity::IEntity()
{
	renderOrder = 0;
	updateOrder = 0;
}

void IEntity::render(int order)
{
	if (renderOrder==order) render();
}

void IEntity::update(int order)
{
	if (updateOrder==order) update();
}
