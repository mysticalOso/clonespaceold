/*
 * TextureEntity.h
 *
 *  Created on: 12 Feb 2015
 *      Author: Adam Nasralla
 */

#ifndef SRC_OSOGINE_GAME_TEXTUREENTITY_H_
#define SRC_OSOGINE_GAME_TEXTUREENTITY_H_

#include <osogine/game/Entity.h>
#include <glm/vec3.hpp>

class TextureEntity: public Entity
{
public:
	TextureEntity(Entity* parent);
	TextureEntity(Game* game);
	virtual ~TextureEntity();

	virtual float getTextureAlpha(){ return textureAlpha; }
	virtual void setTextureAlpha(float a){ textureAlpha = a; }

	virtual uint getTexture() { return texture; }
	virtual void setTexture( uint texture1 ) { texture = texture1; }
	virtual void setTexture( string name );
	virtual void setTexture( string name, int size ){ textureSize = size; setTexture(name); }
	void setFadeTime(float t){ fadeTime = t; }

	void fadeIn(){ fadingIn = true; fadeCount = 0; children->fadeIn();}
	void fadeOut(){ startAlpha = textureAlpha; fadingIn = false; fadeCount = fadeTime; children->fadeOut(); }

	void setAlphaModifier(float m){ alphaModifier = m; }
	float getAlphaModifier(){ return alphaModifier; }

	vec3 getTextureColour(){ return textureColour; }
	void setTextureColour(vec3 c){ textureColour = c; }

	void update();

	void initQuad(float uvx, float uvy, float width, float height);



protected:
	uint texture;
	float textureAlpha;
	vec3 textureColour;
	float alphaModifier;
	bool fadingIn;
	int textureSize;
	float fadeTime;
	float fadeCount;
	float width;
	float height;
	float startAlpha;

	void init();

};

#endif /* SRC_OSOGINE_GAME_TEXTUREENTITY_H_ */
