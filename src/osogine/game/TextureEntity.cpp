/*
 * TextureEntity.cpp
 *
 *  Created on: 12 Feb 2015
 *      Author: Adam Nasralla
 */

#include <glm/vec2.hpp>
#include <osogine/game/Game.h>
#include <osogine/game/TextureEntity.h>
#include <osogine/gfx/GfxBuilder.h>
#include <osogine/gfx/GfxObject.h>
#include <osogine/utils/Utils.h>
#include <string>


TextureEntity::TextureEntity(Entity* parent) : Entity(parent)
{
	init();
}

TextureEntity::TextureEntity(Game* game) : Entity(game)
{
	init();
}

TextureEntity::~TextureEntity()
{
}

void TextureEntity::setTexture(string name)
{
	setTexture(game->getTexture(name));
}

void TextureEntity::update()
{
	Entity::update();
	if (fadingIn && fadeCount < fadeTime)
	{
		fadeCount++;
		setTextureAlpha(  Utils::sinInterpolate(0,1, fadeCount / fadeTime) * alphaModifier );
	}
	if (!fadingIn && fadeCount > 0)
	{
		fadeCount--;
		setTextureAlpha( Utils::sinInterpolate(0,startAlpha, fadeCount / fadeTime) * alphaModifier );
	}
}

void TextureEntity::init()
{
	width = 0;
	height = 0;
	texture = 0;
	startAlpha = 1;
	textureAlpha = 0;
	fadeTime = 50;
	fadeCount = 0;
	fadingIn = false;
	alphaModifier = 1;
	textureColour = vec3(1,1,1);

}


void TextureEntity::initQuad(float uvx, float uvy, float width, float height)
{
	this->width = width;
	this->height = height;

	setProgram("texture");
	GfxBuilder* builder = game->getGfxBuilder();
	gfx = new GfxObject();

	vec2 topLeftUV, bottomRightUV;
	vec2 bottomLeft, topRight;

	float halfWidth = width*0.5;
	float halfHeight = height*0.5;

	bottomLeft = vec2(-halfWidth, -halfHeight);
	topRight = vec2(halfWidth, halfHeight);

	float tSize = (float)textureSize;

	vec2 uvSize = vec2(width / tSize, -height / tSize);

	topLeftUV = vec2(uvx / textureSize, 1 - (uvy / textureSize));
	bottomRightUV = topLeftUV + uvSize;

	float temp = topLeftUV.y;
	topLeftUV.y = bottomRightUV.y;
	bottomRightUV.y = temp;


	builder->addQuad(gfx, bottomLeft, topRight, topLeftUV, bottomRightUV);

}
