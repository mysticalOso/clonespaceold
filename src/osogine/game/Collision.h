/*
 * Collision.h
 *
 *  Created on: 18 Dec 2014
 *      Author: mysticalOso
 */

#ifndef SRC_HUBS_COLLISION_H_
#define SRC_HUBS_COLLISION_H_

#include <glm/vec2.hpp>
#include <string>

class Entity;

using namespace glm;
using namespace std;

class Collision {
public:
	Collision(Entity* a, Entity* b, string name);
	virtual ~Collision();

	bool nameIs(string s);

	Entity* getA(){ return a; }
	Entity* getB(){ return b; }
	vec2 getPoint(){ return point; }

private:

	Entity* a;
	Entity* b;
	vec2 point;
	string name;



};

#endif /* SRC_HUBS_COLLISION_H_ */
