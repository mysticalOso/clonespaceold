/*
 * TextEntity.cpp
 *
 *  Created on: 11 Feb 2015
 *      Author: Adam Nasralla
 */


#include <glm/vec4.hpp>
#include <osogine/game/Game.h>
#include <osogine/game/TextEntity.h>
#include <osogine/gfx/GfxBuilder.h>
#include <osogine/gfx/GfxObject.h>
#include <osogine/utils/Printer.h>

TextEntity::TextEntity(Entity* parent, float width) : TextureEntity(parent)
{
	setTexture("font");
	setProgram("text");
	setDisableCamera(true);

	invert = false;

	charWidth = (512.f / 16.f) * 0.46875;
	charHeight = 512.f / 16.f;

	lineHeight = charHeight - 1;
	charsPerLine = (int)width / (int)charWidth;

	charWidthUV = charWidth / 512.f;
	charHeightUV = charHeight / 512.f;

	renderOrder = 8;

	setText("Greetings for the Quantum Web.");
}

TextEntity::~TextEntity()
{
}

void TextEntity::setText(string str)
{
	buildGfx(str);
}





void TextEntity::buildGfx(string str)
{
	//if (gfx!=0) //delete gfx;
	gfx = new GfxObject();


	List<string> lines = spiltLines(str);

	for (uint i=0; i<lines.size(); i++)
	{
		string line = lines.get(i);
		float yOffset = i*lineHeight;
		addLineToGfx(line, yOffset);
	}

}


void TextEntity::addLineToGfx(string str, float yOffset)
{
	vec2 bottomLeft,topRight,topLeftUV,bottomRightUV;
	GfxBuilder* gfxBuilder = game->getGfxBuilder();

	for (uint i=0; i<str.length(); i++)
	{
		char character = str[i];
		vec2 uv;
		uv.x = ((character-32)%16)/16.0f;
		uv.y = ((character-32)/16)/16.0f;

		bottomLeft.x = i * charWidth;
		bottomLeft.y = -yOffset;
		topRight.x = (i+1) * charWidth;
		topRight.y = charHeight - yOffset;
		topLeftUV.x = uv.x;
		if (!invert) topLeftUV.y = 1 - (uv.y + charHeightUV);
		else topLeftUV.y = (uv.y + charHeightUV);
		bottomRightUV.x = uv.x + charWidthUV;
		if (!invert) bottomRightUV.y = 1 - uv.y;
		else bottomRightUV.y = uv.y;


		//Printer::print("UV = ",uv,"\n");

		gfxBuilder->addQuad(gfx, bottomLeft,topRight,topLeftUV,bottomRightUV);
		//gfxBuilder->addQuad(gfx, bottomLeft,topRight,vec2(0,0),vec2(1,1));
	}
}

List<string> TextEntity::spiltLines(string s)
{

	bool spaceFound = false;
	unsigned int cursor=charsPerLine;
	while (s.length() > cursor)
	{
		if (s[cursor]==' ') s[cursor] = '\n';
		else
		{
			spaceFound = false;
			for (int i = cursor-1; i >= 0; i--)
			{
				if (s[i] == ' ')
				{
					spaceFound = true;
					s[i] = '\n';
					cursor = i;
					i=0;
				}
			}
		}
		if (!spaceFound)
		{
			s.insert(cursor,1,' ');
		}
		cursor += charsPerLine;
	}

	vector<string> lines;
	string delimiter = "\n";
	size_t pos = 0;
	std::string token;
	while ((pos = s.find(delimiter)) != std::string::npos) {
	    token = s.substr(0, pos);
	    lines.push_back(token);
	    s.erase(0, pos + delimiter.length());
	}
	lines.push_back(s);

	List<string> strings;
	strings.setVector(lines);

	return strings;

}
