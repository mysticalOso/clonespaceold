/*
 * Physics.h
 *
 *  Created on: 19 Oct 2014
 *      Author: Adam Nasralla
 */

#ifndef PHYSICS_H_
#define PHYSICS_H_

#include <glm/fwd.hpp>
#include <glm/vec3.hpp>
#include <osogine/game/EntityComponent.h>

using namespace glm;
using namespace std;

class Entity;

class Physics: public EntityComponent
{
public:
	Physics();
	virtual ~Physics();

	virtual void setVelocity(float x, float y, float z){ setVelocity(vec3(x,y,z)); }
	virtual void setVelocity(vec3 v){ velocity = v; }
	virtual vec3 getVelocity(){ return velocity; }
	virtual float getSpeed();

	virtual void setSpin(float x, float y, float z){ setSpin(vec3(x,y,z)); }
	virtual void setSpin(vec3 s){ spin = s; }
	virtual vec3 getSpin(){ return spin; }

	virtual void applyForce(float x, float y, float z){ applyForce(vec3(x,y,z)); }
	virtual void applyForce(vec3 f){ velocity += f/mass; }

	virtual void applyTorque(float x, float y, float z){ applyTorque(vec3(x,y,z)); }
	virtual void applyTorque(vec3 t){ spin += t/mass; }

	virtual void setForce(float x, float y, float z){ setForce(vec3(x,y,z)); }
	virtual void setForce(vec3 f){ force = f; }
	virtual vec3 getForce(){ return force; }

	virtual void setTorque(float x, float y, float z){ setTorque(vec3(x,y,z)); }
	virtual void setTorque(vec3 t){ torque = t; }
	virtual vec3 getTorque(){ return torque; }

	virtual void setFriction(float f){ friction = f; }
	virtual float getFriction(){ return friction; }

	virtual void setMass(float m){ mass = m; }
	virtual float getMass(){ return mass; }

	virtual void update(Entity* entity);

	virtual void setVelocityIsRelative(bool isRelative){ velocityIsRelative = isRelative;  }
	virtual bool getVelocityIsRelative(){ return velocityIsRelative; }

	virtual void setSpinIsRelative(bool isRelative){ spinIsRelative = isRelative; }
	virtual bool getSpinIsRelative(){ return spinIsRelative; }

protected:

	vec3 velocity;
	vec3 spin;
	vec3 force;
	vec3 torque;

	float friction;
	float mass;

	bool spinIsRelative;
	bool velocityIsRelative;


};

#endif /* PHYSICS_H_ */
