/*
 * Hub.cpp
 *
 *  Created on: 31 Oct 2014
 *      Author: Adam Nasralla
 */

#include <osogine/game/EntityBuilder.h>
#include <osogine/game/EntityList.h>
#include <osogine/game/Game.h>
#include <osogine/game/Hub.h>



Hub::Hub(IEntity* parent) : Entity(parent)
{
	builder = 0;
	collisions = new List<Collision*>();
}

Hub::Hub(Game* game) : Entity(game)
{
	builder = 0;
	collisions = new List<Collision*>();
}

Hub::~Hub()
{
	//TODO should Hub destroy children?
	//delete children;
}

void Hub::setBuilder(string name)
{
	builder = game->getBuilder(name);
	builder->setHub(this);
}



void Hub::remove(IEntity* entity)
{
	removeChild(entity);
}



void Hub::collideWith(Hub* hub, string collisionName)
{

	if (hub!=0)
	{
		EntityList* children2 = hub->getChildren();
		for (int i = 0; i < children->size(); i++)
		{
			Entity* a = (Entity*) children->get(i);
			for (int j = 0; j < children2->size(); j++)
			{
				Entity* b = (Entity*) children2->get(j);
				if (a->collideWith(b))
				{
					collisions->add(new Collision(a,b,collisionName));
				}
			}
		}

		for (int i = 0; i < collisions->size(); i++)
		{
			handleCollision(collisions->get(i));
		}

		for (int i = 0; i < collisions->size(); i++)
		{
			//delete collisions->get(i);
		}
	}
	collisions->clear();


}


void Hub::handleCollision(Collision* collision) {
}

void Hub::removeAndDelete(IEntity* entity)
{
	remove(entity);
	removeCollisionsWith(entity);
	//delete entity;
}

void Hub::removeCollisionsWith(IEntity* e)
{
	for (int i = 0; i < collisions->size(); i++)
	{
		Collision* c = collisions->get(i);
		Entity* a = c->getA();
		Entity* b = c->getB();
		if (a==e || b==e)
		{
			collisions->remove(c);
			//delete c;
			i--;
		}
	}
}

void Hub::init()
{
	children->init();
}

bool Hub::contains(IEntity* e)
{
	return children->contains(e);
}

void Hub::addWithMove(Entity* e)
{
	e->globaliseTransform();
	e->applyInverseOf(this);
	add(e);
}

void Hub::transferFrom(Hub* transferHub)
{
	while(transferHub->size()>0)
	{
		Entity* child = (Entity*)transferHub->get(0);
		addWithMove(child);
		transferHub->remove(child);
	}
}

Entity* Hub::getClosest(Entity* subject, float range)
{
	Entity* closest = 0;
	float range2 = range*range;
	for (int i = 0; i < children->size(); i++)
	{
		Entity* entity = (Entity*) children->get(i);
		float dist2 = entity->distTo2(subject);
		if (dist2 <= range2)
		{
			closest = entity;
		}
	}
	return closest;
}

void Hub::transferFrom(Hub* transferHub, Entity* child)
{
	addWithMove(child);
	transferHub->remove(child);
}
