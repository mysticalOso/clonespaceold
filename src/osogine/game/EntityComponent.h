/*
 * EntityComponent.h
 *
 *  Created on: 19 Oct 2014
 *      Author: Adam Nasralla
 */

#ifndef ENTITYCOMPONENT_H_
#define ENTITYCOMPONENT_H_

class Entity;

class EntityComponent
{
public:
	EntityComponent(){}
	virtual ~EntityComponent(){}

	virtual void update(Entity* entity){}
};

#endif /* ENTITYCOMPONENT_H_ */
