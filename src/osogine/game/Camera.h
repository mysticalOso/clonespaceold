/*
 * Camera.h
 *
 *  Created on: 8 Oct 2014
 *      Author: Adam Nasralla
 */

#ifndef CAMERA_H_
#define CAMERA_H_

#define GLM_FORCE_RADIANS

#include <osogine/game/Entity.h>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>

class Game;

class Camera: public Entity
{
public:
	Camera(Game* game);
	virtual ~Camera();
	virtual void setZoom(float z){ zoom = z; }
	virtual float getZoom(){ return zoom; }
	virtual void setTarget(Entity* e){}
	virtual mat4 getViewProjectionMatrix();
	virtual mat4 getProjectionMatrix();
	virtual vec2 getClipPos(vec3 pos);
	virtual vec3 getWorldPos(vec3 clipPosZ){return vec3(0,0,0);}

protected:
	float zoom;
	mat4 uiMatrix;
	mat4 projectionMatrix;
};

#endif /* CAMERA_H_ */
