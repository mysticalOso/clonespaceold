/*
 * Hub.h
 *
 *  Created on: 31 Oct 2014
 *      Author: Adam Nasralla
 */

#ifndef HUB_H_
#define HUB_H_

#include <osogine/game/Collision.h>
#include <osogine/game/Entity.h>
#include <osogine/utils/List.h>
#include <string>

class EntityBuilder;


class Hub: public Entity {
public:
	Hub(IEntity* parent);
	Hub(Game* game);
	virtual ~Hub();
	virtual void add(IEntity* entity){ addChild(entity); }
	void remove(IEntity* entity);
	void removeAndDelete(IEntity* entity);
	void setBuilder(std::string name);

	EntityList* getChildren() { return children; }

	void init();

	virtual void collideWith(Hub* hub, std::string collisionName);

	void removeCollisionsWith(IEntity* e);

	void addWithMove(Entity* e);
	bool contains(IEntity* e);

	void transferFrom(Hub* hub);

	void transferFrom(Hub* hub, Entity* subject);

	int size(){ return children->size(); }

	virtual void print(){}
	IEntity* get(int i){ return children->get(i); }

	virtual void handleCollision(Collision* collision);

	Entity* getClosest(Entity* subject, float range);

protected:

	EntityBuilder* builder;
	List<Collision*>* collisions;
};

#endif /* HUB_H_ */
