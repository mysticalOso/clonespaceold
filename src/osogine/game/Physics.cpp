/*
 * Physics.cpp
 *
 *  Created on: 19 Oct 2014
 *      Author: Adam Nasralla
 */

#include <osogine/game/Entity.h>
#include <osogine/game/Physics.h>
#include <glm/geometric.hpp>

Physics::Physics()
{
	friction = 1;
	mass = 1;
	velocityIsRelative = false;
	spinIsRelative = false;
}

Physics::~Physics()
{

}

void Physics::update(Entity* entity)
{
	if (force!=vec3(0,0,0)) velocity += force/mass;
	if (torque!=vec3(0,0,0)) spin += torque/mass;
	if ( ! ( velocity.x==0 && velocity.y==0 && velocity.z==0 ))
	{
		velocity *= friction;
		if (velocityIsRelative)
		{
			entity->moveRelative(velocity);
		}
		else
		{
			entity->move(velocity);
		}
	}
	if ( ! ( spin.x==0 && spin.y==0 && spin.z==0 ))
	{
		spin *= friction;
		if (spinIsRelative)
		{
			entity->rotateXRelative(spin.x);
			entity->rotateYRelative(spin.y);
			entity->rotateZRelative(spin.z);
		}
		else
		{
			entity->rotateX(spin.x);
			entity->rotateY(spin.y);
			entity->rotateZ(spin.z);
		}
	}

}

//TODO add getSpeed2 to physics and bullet
float Physics::getSpeed()
{
	return glm::length(velocity);
}
