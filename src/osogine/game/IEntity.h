/*
 * IEntity.h
 *
 *  Created on: 9 Oct 2014
 *      Author: Adam Nasralla
 */

#ifndef IENTITY_H_
#define IENTITY_H_

#include <string>
#include <core/Constants.h>
#include <osogine/utils/Printer.h>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <glm/mat4x4.hpp>

using namespace std;

class Transform;
class Game;

class IEntity
{
public:
	IEntity();
	virtual ~IEntity(){}

	virtual void render(){}
	virtual void update(){}
	virtual void render(int order);
	virtual void update(int order);
	virtual void init(){}
	virtual Transform* getTransform(){ return 0; }
	virtual void setTransform(Transform*){}
	virtual Game* getGame(){return 0;}
	virtual void addChild(IEntity* child){}
	virtual void removeChild(IEntity* child){}
	virtual void setParent(IEntity* parent){}
	virtual IEntity* getParent(){return 0;}
	virtual IEntity* clone(){ return 0; }
	virtual void setTarget(float x, float y){}
	virtual bool collideWith(IEntity* entity){return false;}
	virtual bool intersectWith(IEntity* entity){return false;}
	virtual int getRenderOrder(){ return renderOrder; }
	virtual void setRenderOrder( int r ){ renderOrder = r; }
	virtual void setDisableCamera( bool b ){}
	virtual void setIsVisible( bool b ){}
	virtual void event( string name ){}
	virtual void fadeOut(){}
	virtual void fadeIn(){}
	virtual void setAlpha( float a ){}

	virtual string getName(){return "";}

	virtual void mouseUp(){}
	virtual void mouseDown(){}
	virtual void mouseMoved(float x, float y){}

	virtual void toggleActive(bool b){}

	virtual void callBack(string functionName){}

protected:

	int updateOrder;
	int renderOrder;

};

#endif /* IENTITY_H_ */
