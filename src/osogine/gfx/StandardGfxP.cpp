/*
 * StandardGfxProgram.cpp
 *
 *  Created on: 4 Oct 2014
 *      Author: Adam Nasralla
 */

#include <vector>
#include <glm/mat4x4.hpp>
#include <osogine/game/Entity.h>
#include <osogine/gfx/GfxObject.h>
#include <osogine/gfx/StandardGfxP.h>

StandardGfxP::StandardGfxP() : GfxProgram()
{
}

StandardGfxP::~StandardGfxP()
{
	// TODO Auto-generated destructor stub
}

void StandardGfxP::init()
{
	addUniform( "MVP" );
	addUniform( "M" );
	addUniform( "TextureSampler" );
	addUniform( "lightColour" );
	addUniform( "lightDirection" );
	addUniform( "numLights" );
}

void StandardGfxP::execute()
{

	toggleDepthTest(false);

	setUniformValue( "MVP", getMVP() );
	setUniformValue( "M", entity->getMatrix() );
	setUniformValue( "textureSampler", 0 );

	vector<vec3> colours;
	colours.push_back(vec3(1,0,0));
	colours.push_back(vec3(0,1,0));
	colours.push_back(vec3(0,0,1));

	vector<vec3> directions;
	directions.push_back(vec3(1,0,0));
	directions.push_back(vec3(0,1,0));
	directions.push_back(vec3(0,0,-1));


	setUniformValue( "lightColour", colours, 8 );
	setUniformValue( "lightDirection", directions, 8 );
	setUniformValue( "numLights", 3 );


	activateBuffer( gfx->getVertexBuffer(), 0 );
	activateBuffer( gfx->getUVBuffer(), 1 );
	activateBuffer( gfx->getNormalBuffer(), 2 );

	activateBuffer( gfx->getIndexBuffer(), 0 );

	//activateTexture( entity->getTexture(), 0 );

	draw();

	//TODO auto cleanup buffers after use
	//TODO cleanup textures after use (auto)
	deactivateBuffer( 0 );
	deactivateBuffer( 1 );
	deactivateBuffer( 2 );
}
