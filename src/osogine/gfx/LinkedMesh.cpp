/*
 * LinkedMesh.cpp
 *
 *  Created on: 20 Oct 2014
 *      Author: mysticalOso
 */

#include <osogine/gfx/GfxObject.h>
#include <osogine/gfx/LinkedMesh.h>
#include <osogine/gfx/LinkedTriangle.h>
#include <osogine/gfx/LinkedVertex.h>
#include <osogine/gfx/Triangle.h>
#include <osogine/utils/Polygon.h>
#include <osogine/utils/Printer.h>

LinkedMesh::LinkedMesh( GfxObject* gfx )
{
	triangles = new List<LinkedTriangle*>();
	verts = new List<LinkedVertex*>();
	build( gfx );
}

LinkedMesh::~LinkedMesh( )
{


}

void LinkedMesh::build(GfxObject* gfx)
{

	this->gfx = gfx;

	buildVertices();
	buildTriangles();

}

void LinkedMesh::updateGfx()
{
	gfx->clearAll();

	//updateGfxVertices();
	updateGfxIndices();
	gfx->calculateNormals(false);
}





void LinkedMesh::buildVertices()
{
	for (uint i = 0; i < gfx->getNoVertices(); i++)
	{
		vec3 v = gfx->getVertex(i);
		if (!isVertexAdded(v))
		{
			verts->add( new LinkedVertex(gfx,i) );
		}
	}
}

void LinkedMesh::buildTriangles()
{
	for (uint i = 0; i < gfx->getNoIndices(); i+=3)
	{
		LinkedVertex* v1 = getMatchingVertexForIndex(i);
		LinkedVertex* v2 = getMatchingVertexForIndex(i+1);
		LinkedVertex* v3 = getMatchingVertexForIndex(i+2);
		if (!isTriangleAdded(v1,v2,v3))
		{
			triangles->add( new LinkedTriangle(v1,v2,v3) );
		}
	}
}

bool LinkedMesh::isVertexAdded(vec3 v)
{
	for (int i = 0; i < verts->size(); i++ )
	{
		if (verts->get(i)->isMatch(v)) return true;
	}
	return false;
}

LinkedVertex* LinkedMesh::getMatchingVertex(vec3 v)
{
	for (int i = 0; i < verts->size(); i++ )
	{
		LinkedVertex* v1 = verts->get(i);
		if (v1->isMatch(v)) return v1;
	}
	return 0;
}

bool LinkedMesh::isTriangleAdded(LinkedVertex* v1, LinkedVertex* v2, LinkedVertex* v3)
{
	for (int i = 0; i < triangles->size(); i++ )
	{
		LinkedTriangle* t = triangles->get(i);
		if (t->isMatch(v1,v2,v3)) return true;
	}
	return false;
}

LinkedVertex* LinkedMesh::getMatchingVertexForIndex(int i)
{
	return getMatchingVertex( gfx->getVertex( gfx->getIndex(i) ) );
}

void LinkedMesh::updateGfxVertices()
{
//	for (uint i = 0; i < verts->size(); i++)
//	{
//		Vertex* v = verts->get(i);
//		v->setIndex(i);
//		v->addToGfx(gfx);
//	}
}

void LinkedMesh::removeVertex(int i)
{
	LinkedVertex* v = verts->get(i);
	verts->remove(v);

	List<LinkedTriangle*>* tris = v->getTriangles();

	while(tris->size() > 0)
	{
		LinkedTriangle* t = tris->get(0);
		triangles->remove(t);
		delete t;
	}

	delete v;

	clearLooseVertices();


}

void LinkedMesh::updateGfxIndices()
{
	for (int i = 0; i < triangles->size(); i++ )
	{
		LinkedTriangle* t = triangles->get(i);
		t->addToGfx(gfx);
		gfx->addIndex(i*3);
		gfx->addIndex(i*3+1);
		gfx->addIndex(i*3+2);
	}
}

void LinkedMesh::clearLooseVertices()
{
	for (int i = 0; i < verts->size(); i++)
	{
		LinkedVertex* v = verts->get(i);
		if (v->getTriangles()->size()==0)
		{
			verts->remove(v);
			delete v;
			i--;
		}
	}
}

Polygon* LinkedMesh::getOutline()
{
	Polygon* outline = new Polygon();
	int noVerts = verts->size();
	int maxX = -99999999;
	int best = 0;

	for (int i = 0; i < noVerts; i++)
	{
		if (verts->get(i)->getX() > maxX)
		{
			best = i;
			maxX = verts->get(i)->getX();
		}
	}

	LinkedVertex* first = verts->get(best);
	outline->add(vec2(first->getPosition()));

	LinkedVertex* next = first->getFirstAntiClockwise();
	int count = 0;

	while (!next->isNear2d(first))
	{
		count++;
		outline->add(vec2(next->getPosition()));
		next = next->getFirstAntiClockwise();
	}

	return outline;
}

List<Triangle> LinkedMesh::removeTrianglesNear(vec2 pos)
{
	int index = -1;
	for (int i = 0; i < verts->size(); i++)
	{
		if (verts->get(i)->isNear2d(pos)) index = i;
	}

	List<LinkedTriangle*>* linkedTriangles = verts->get(index)->getTriangles();
	List<Triangle> triangles;

	for (int i = 0; i < linkedTriangles->size(); i++)
	{
		triangles.add( Triangle( linkedTriangles->get(i) ) );
	}

	if (index!=-1) removeVertex(index);

	return triangles;
}
