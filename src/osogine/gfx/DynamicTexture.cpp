/*
 * DynamicTexture.cpp
 *
 *  Created on: 1 Nov 2014
 *      Author: mysticalOso
 */

#include <osogine/gfx/DynamicTexture.h>
#include <osogine/utils/Dice.h>
#include <stdio.h>
DynamicTexture::DynamicTexture()
{

}

DynamicTexture::~DynamicTexture()
{

}

void DynamicTexture::makeRandom()
{
	for (int i = 0; i < getSize(); i++)
	{
		for (int j = 0; j < getSize(); j++)
		{
			for (int k = 0; k < 3; k++)
			{
				pixels[i][j][k] = (GLubyte) Dice::roll(255);
			}
			pixels[i][j][3] = (GLubyte) 255;
		}
	}

}
