/*
 * Vertex.h
 *
 *  Created on: 20 Oct 2014
 *      Author: mysticalOso
 */

#ifndef VERTEX_H_
#define VERTEX_H_

#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <osogine/utils/List.h>

class LinkedTriangle;
class LinkedVertex;
class GfxObject;

using namespace glm;

class LinkedVertex {
public:
	LinkedVertex( GfxObject* g, int i );
	virtual ~LinkedVertex();

	void setPosition(vec3 p){ position = p; }
	vec3 getPosition(){ return position; }

	void setColour(vec4 c){ colour = c; }
	vec4 getColour(){ return colour; }

	void addTriangle(LinkedTriangle* t);
	void removeTriangle(LinkedTriangle* t);

	void addLink(LinkedVertex* v);
	void removeLink(LinkedVertex* v);

	void addToGfx( GfxObject* g );

	bool isMatch( LinkedVertex* v){ return v->position == position; }
	bool isMatch( vec3 pos ){ return position == pos; }

	bool isLinkAdded( LinkedVertex* v );

	void setIndex(int i){ index = i; }
	int getIndex(){ return index; }

	List< LinkedTriangle* >* getTriangles(){ return triangles; }

	bool isNear2d(LinkedVertex* v);

	bool isNear2d(vec2 pos);

	LinkedVertex* getFirstAntiClockwise();

	void setAngTo(float a){ angTo = a; }

	float getX(){ return position.x; }
	float getY(){ return position.y; }

	List< LinkedVertex* >* getLinks(){ return links; }

private:
	vec3 position;
	vec4 colour;
	List< LinkedTriangle* >* triangles;
	List< LinkedVertex* >* links;
	int index;
	float angTo;

	//TODO multiple UVs could be stored as a list?

};

#endif /* VERTEX_H_ */
