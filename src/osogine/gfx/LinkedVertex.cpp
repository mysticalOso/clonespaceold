/*
 * Vertex.cpp
 *
 *  Created on: 20 Oct 2014
 *      Author: mysticalOso
 */

#include <osogine/gfx/GfxObject.h>
#include <osogine/gfx/LinkedTriangle.h>
#include <osogine/gfx/LinkedVertex.h>
#include <osogine/utils/Printer.h>
#include <osogine/utils/Utils.h>

LinkedVertex::LinkedVertex(GfxObject* g, int i)
{
	index = 0;

	angTo = 0;

	triangles = new List<LinkedTriangle*>();
	links = new List<LinkedVertex*>();

	position = g->getVertex(i);
	colour = g->getColour(i);
}

LinkedVertex::~LinkedVertex()
{
	for (int i = 0; i < links->size(); i++)
	{
		links->get(i)->removeLink(this);
	}

	//delete triangles;
	//delete links;
}


void LinkedVertex::addToGfx(GfxObject* g)
{
	g->addColour(colour);
	g->addVertex(position);
}

void LinkedVertex::addTriangle(LinkedTriangle* t)
{
	triangles->add(t);

}

void LinkedVertex::removeTriangle(LinkedTriangle* t)
{
	triangles->remove(t);
}

void LinkedVertex::addLink(LinkedVertex* v)
{
	if (!isLinkAdded(v))
	{
		links->add(v);
		v->addLink(this);
	}

}

void LinkedVertex::removeLink(LinkedVertex* v)
{
	if (isLinkAdded(v))
	{
		links->remove(v);
		v->removeLink(v);
	}
}

bool LinkedVertex::isLinkAdded(LinkedVertex* v)
{
	for (int i = 0; i < links->size(); i++ )
	{
		if (links->get(i)->isMatch(v)) return true;
	}
	return false;
}


LinkedVertex* LinkedVertex::getFirstAntiClockwise()
{
	int noLinks = links->size();
	vec2 vertex2d = vec2(position);
	float minAng = 99999999;
	LinkedVertex* best = 0;
	vec2 v; float a;
	float a2;
	float ang2;

	for (int i = 0; i < noLinks; i++)
	{
		if (!isNear2d(links->get(i)))
		{
			v = vec2(links->get(i)->getPosition());

			a2 = Utils::ang2(vertex2d, v);
			a = Utils::anticlockwiseDiff(angTo,a2);

			if (a < minAng && a > 0.000001)
			{
				best = links->get(i);
				minAng = a;
				ang2 = a2;
			}
		}
	}
	if (best==0)
	{
		best = 0;
	}
	else
	{
		ang2 = Utils::mirrorAng(ang2);
		best->setAngTo(ang2);
	}
	return best;
}


bool LinkedVertex::isNear2d(LinkedVertex* v)
{
	vec2 pos = vec2(v->getPosition());
	return isNear2d(pos);
}

bool LinkedVertex::isNear2d(vec2 pos)
{
	return  ( floor(position.x+0.5f) == floor(pos.x+0.5) && floor(position.y+0.5f) == floor(pos.y+0.5));
}
