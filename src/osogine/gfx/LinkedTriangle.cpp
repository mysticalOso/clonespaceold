/*
 * Triangle.cpp
 *
 *  Created on: 20 Oct 2014
 *      Author: mysticalOso
 */

#include <osogine/gfx/GfxObject.h>
#include <osogine/gfx/LinkedTriangle.h>
#include <osogine/gfx/LinkedVertex.h>
#include <osogine/utils/Printer.h>

LinkedTriangle::LinkedTriangle(LinkedVertex* v1, LinkedVertex* v2, LinkedVertex* v3)
{
	verts = new List<LinkedVertex*>();

	verts->add(v1);
	verts->add(v2);
	verts->add(v3);

	//Printer::print("V1 ",v1->getPosition(),"\n");
	//Printer::print("V2 ",v2->getPosition(),"\n");
	//Printer::print("V3 ",v3->getPosition(),"\n");

	v1->addTriangle(this);
	v2->addTriangle(this);
	v3->addTriangle(this);

	v1->addLink(v2);
	v2->addLink(v3);
	v3->addLink(v1);
}

LinkedTriangle::~LinkedTriangle()
{
	for (int i=0; i<verts->size(); i++)
	{
		verts->get(i)->removeTriangle(this);
	}
	//delete verts;
}

bool LinkedTriangle::isMatch(LinkedTriangle* t)
{
	return (t->getVertex(0)==getVertex(0) &&
			t->getVertex(1)==getVertex(1) &&
			t->getVertex(2)==getVertex(2));
}

bool LinkedTriangle::isMatch(LinkedVertex* v1, LinkedVertex* v2, LinkedVertex* v3)
{
	return (v1==getVertex(0) &&
			v2==getVertex(1) &&
			v3==getVertex(2));
}

void LinkedTriangle::addToGfx(GfxObject* g)
{
//	g->addIndex(getVertex(0)->getIndex());
//	g->addIndex(getVertex(1)->getIndex());
//	g->addIndex(getVertex(2)->getIndex());
	getVertex(0)->addToGfx(g);
	getVertex(1)->addToGfx(g);
	getVertex(2)->addToGfx(g);
}
