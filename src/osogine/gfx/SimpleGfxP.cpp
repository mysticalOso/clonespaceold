/*
 * SimpleGfxProgram.cpp
 *
 *  Created on: 3 Oct 2014
 *      Author: Adam Nasralla
 */


#include <glm/mat4x4.hpp>
#include <osogine/game/Camera.h>
#include <osogine/game/Entity.h>
#include <osogine/gfx/GfxObject.h>
#include <osogine/gfx/SimpleGfxP.h>
#include <osogine/utils/Printer.h>

SimpleGfxP::SimpleGfxP() : GfxProgram()
{
}

SimpleGfxP::~SimpleGfxP() {}


void SimpleGfxP::init()
{
	addUniform( "MVP" );
}

void SimpleGfxP::execute()
{

	toggleDepthTest(false);

	setUniformValue( "MVP", getMVP() );


	activateBuffer( gfx->getVertexBuffer(), 0 );
	activateBuffer( gfx->getColourBuffer(), 1 );

	activateBuffer( gfx->getIndexBuffer(), 0 );
	//int n = gfx->getNoVertices();
	draw( );

	//TODO auto cleanup buffers after use
	deactivateBuffer( 0 );
	deactivateBuffer( 1 );

}
