/*
 * OutlineGfxP.cpp
 *
 *  Created on: 7 Feb 2015
 *      Author: mysticalOso
 */

#include <osogine/game/Entity.h>
#include <glm/mat4x4.hpp>
#include <osogine/game/Camera.h>
#include <osogine/gfx/GfxBuilder.h>
#include <osogine/utils/Polygon.h>
#include <osogine/gfx/GfxObject.h>
#include <osogine/gfx/OutlineGfxP.h>

OutlineGfxP::OutlineGfxP() {
	builder = new GfxBuilder();

}

OutlineGfxP::~OutlineGfxP() {
	// TODO Auto-generated destructor stub
}

void OutlineGfxP::init()
{
	addUniform( "MVP" );

}

void OutlineGfxP::execute()
{
	toggleDepthTest(false);

	setUniformValue( "MVP", camera->getViewProjectionMatrix() );

	gfx = getOutlineGfx();

	activateBuffer( gfx->getIndexBuffer(), 0 );
	activateBuffer( gfx->getVertexBuffer(), 0 );
	activateBuffer( gfx->getColourBuffer(), 1 );

	drawLines();

	//TODO auto cleanup buffers after use
	deactivateBuffer( 0 );
	deactivateBuffer( 1 );

	//delete gfx;
}

GfxObject* OutlineGfxP::getOutlineGfx()
{
	return builder->buildLinePolygon(entity->getOutline(),vec4(0,1,0,1));
}
