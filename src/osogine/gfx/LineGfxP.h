/*
 * LineGfxP.h
 *
 *  Created on: 17 Oct 2014
 *      Author: Adam Nasralla
 */

#ifndef LINEGFXP_H_
#define LINEGFXP_H_

#include <osogine/gfx/GfxProgram.h>

class LineGfxP : public GfxProgram
{
public:
	LineGfxP();
	~LineGfxP();

	void init();
	void execute();

};

#endif /* LINEGFXP_H_ */
