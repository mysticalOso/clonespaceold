#pragma once

#include <osogine/gfx/ObjLoader/Vertex.h>
#include <vector>

class GfxObject;

class Mesh
{
public:
	Mesh(void);
	~Mesh(void);

	void addVertex(const Vertex& vertex) {
		vertices.push_back(vertex);
	}
	void addIndex(unsigned int index) {
		indices.push_back(index);
	}

	GfxObject* getGfxObject();


	std::vector<Vertex> vertices;
	std::vector<unsigned int> indices;




};
