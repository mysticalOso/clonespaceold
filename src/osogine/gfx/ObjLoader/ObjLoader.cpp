/*
 * ObjLoader.cpp
 *
 *  Created on: 30 Jun 2014
 *      Author: Adam Nasralla
 */

#include <osogine/gfx/GfxBuffer.h>
#include <osogine/gfx/GfxObject.h>
#include <osogine/gfx/ObjLoader/ObjLoader.h>
#include <osogine/gfx/ObjLoader/ObjParser.h>



std::map<std::string,GfxObject*> ObjLoader::gfxLookup;

GfxObject* ObjLoader::getObject(std::string name)
{
	Mesh* mesh;

	GfxObject* gfx;

	if (gfxLookup[name]==0)
	{
		if (name=="driver")
		{
			mesh = ObjParser::parse("cloneDriver.obj");
		}
		if (name=="passenger")
		{
			mesh = ObjParser::parse("cloneGun.obj");
		}
		if (name=="desk")
		{
			mesh = ObjParser::parse("desk.obj");
		}
		if (name=="teleport")
		{
			mesh = ObjParser::parse("teleport.obj");
		}

		if (name=="body")
		{
			mesh = ObjParser::parse("body.obj");
		}
		if (name=="foreArm")
		{
			mesh = ObjParser::parse("foreArm.obj");
		}
		if (name=="upperArm")
		{
			mesh = ObjParser::parse("upperArm.obj");
		}
		if (name=="head")
		{
			mesh = ObjParser::parse("head.obj");
		}
		if (name=="xacHead")
		{
			mesh = ObjParser::parse("xacHead.obj");
		}
		if (name=="xacForeArm")
		{
			mesh = ObjParser::parse("foreArm.obj");
		}
		if (name=="xacUpperArm")
		{
			mesh = ObjParser::parse("upperArm.obj");
		}
		if (name=="gun")
		{
			mesh = ObjParser::parse("gun.obj");
		}
		if (name=="cloneGun")
		{
			mesh = ObjParser::parse("cloneGun.obj");
		}
		if (name=="sentient")
		{
			mesh = ObjParser::parse("sentient.obj");
		}
		if (name=="pod")
		{
			mesh = ObjParser::parse("pod.obj");
		}
		if (name=="xaccy")
		{
			mesh = ObjParser::parse("xaccyHead.obj");
		}
		if (name=="xaccyJaw")
		{
			mesh = ObjParser::parse("xaccyJaw.obj");
		}
		if (name=="xaccyTeeth")
		{
			mesh = ObjParser::parse("xaccyTeeth.obj");
		}
		if (name=="test")
		{
			mesh = ObjParser::parse("test.obj");
		}

		gfx = mesh->getGfxObject();
		gfxLookup[name] = gfx;
	}
	else
	{
		gfx  = gfxLookup[name];
	}

	return gfx;

}



//TODO figure out how to colour vertices in Blender and remove colourObject function
//TODO fix Z coordinates (BIG ONE!)
void ObjLoader::colourObject(GfxObject* gfx, vec3 c1, vec3 c2, int mode)
{


	for (unsigned int i=0; i<gfx->getNoVertices(); i++)
	{
		vec3 v = gfx->getVertex(i);

		//PASSENGER
		if (mode == 0)
		{

			gfx->setVertex(i, vec3(v.x, v.y, v.z*0.1 - 0.5));
			if (v.z<-0.39 && v.y > 1.4 &&  v.x>-0.6 && v.x < 0.6 )
			{
				gfx->addColour(0.9,0.9,0.9,1);
			}
			else
			{
				if (v.z>-1.7)
				{
					gfx->addColour(c1.r,c1.g,c1.b,1.0f);
				}
				else
				{
					gfx->addColour(c2.r,c2.g,c2.b,1);
				}
			}
			//gfx->setVertex(i, v.x, v.y, v.z + 0.5);
		}

		//DRIVER
		if (mode == 1)
		{
			gfx->setVertex(i, vec3(v.x, v.y, v.z*0.1 - 0.5));
			if (v.z>-1.7)
			{
				gfx->addColour(c1.r,c1.g,c1.b,1.0f);
			}
			else
			{
				gfx->addColour(c2.r,c2.g,c2.b,1);
			}
		}

		//DESK
		if (mode == 2)
		{
			gfx->setVertex(i, vec3(v.x, v.y, v.z*0.1));
			gfx->addColour(c1.r,c1.g,c1.b,1.0f);
		}

		//TELEPORT
		if (mode == 3)
		{
			gfx->setVertex(i, vec3(v.x, v.y, v.z*0.01));
			if (v.z > -0.2 && v.z < -0.03)
			{
				gfx->addColour(0.0,0.0,1,1);
			}
			else
			{
				gfx->addColour(0.9,0.9,0.9,1);
			}
		}

		if (mode == 4)
		{
			gfx->setVertex(i, vec3(v.x, v.y, v.z));
			gfx->addColour(c1.r,c1.g,c1.b,1.0f);
		}
	}

}
