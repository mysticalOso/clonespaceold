/*
 * ObjLoader.h
 *
 *  Created on: 30 Jun 2014
 *      Author: Adam Nasralla
 */

#ifndef OBJLOADER_H_
#define OBJLOADER_H_

#include <glm/vec3.hpp>
#include <osogine/gfx/ObjLoader/Mesh.h>
#include <map>
#include <string>




using namespace glm;

class Mesh;
class GfxObject;

class ObjLoader
{
public:
	ObjLoader(){}
	~ObjLoader(){}

	static GfxObject* getObject(std::string name);
	static void colourObject(GfxObject* gfx, vec3 c1, vec3 c2, int mode);


private:
	static std::map<std::string,GfxObject*> gfxLookup;




};

#endif /* OBJLOADER_H_ */
