#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <osogine/gfx/GfxObject.h>
#include <osogine/gfx/ObjLoader/Mesh.h>


using namespace glm;

Mesh::Mesh(void)
{
}

Mesh::~Mesh(void)
{
}

GfxObject* Mesh::getGfxObject()
{
	GfxObject* gfx = new GfxObject();

	for (unsigned int i=0; i<vertices.size(); i++)
	{
		Vertex v = vertices[i];
		gfx->addVertex(v.position.x, v.position.y, v.position.z);
		gfx->addNormal(-v.normal.x, -v.normal.y, v.normal.z );
		gfx->addUV(v.texcoord.x, v.texcoord.y);
		gfx->addColour(1,1,1,1);
	}

	for (unsigned int i=0; i<indices.size(); i++)
	{
		unsigned int j =  indices[i];
		gfx->addIndex(j);
	}

	gfx->setDepthEnabled(true);

	return gfx;
}


/*
void Mesh::initBuffers()
{
	glGenBuffers(1, &vertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex)*vertices.size(), (void*)&vertices[0], GL_STREAM_DRAW);

	glGenBuffers(1, &indexBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int)*indices.size(), (void*)&indices[0], GL_STREAM_DRAW);
}

void Mesh::draw(CGtechnique cgTechnique)
{
	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);

	glVertexPointer(3, GL_FLOAT, sizeof(Vertex), 0);
	glEnableClientState(GL_VERTEX_ARRAY);

	glNormalPointer(GL_FLOAT, sizeof(Vertex), (void*)(sizeof(glm::vec3)));
	glEnableClientState(GL_NORMAL_ARRAY);

	glClientActiveTexture(GL_TEXTURE0);
	glTexCoordPointer(2, GL_FLOAT, sizeof(Vertex), (void*)(sizeof(glm::vec3)+sizeof(glm::vec3)));
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);


	CGpass pass = cgGetFirstPass(cgTechnique);
	while (pass)
	{
		cgSetPassState(pass);
		
		glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0);

		cgResetPassState(pass);
		pass = cgGetNextPass(pass);
	}
	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_NORMAL_ARRAY);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
}
*/
