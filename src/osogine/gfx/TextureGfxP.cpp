/*
 * StandardGfxProgram.cpp
 *
 *  Created on: 4 Oct 2014
 *      Author: Adam Nasralla
 */

#include <glm/mat4x4.hpp>
#include <osogine/game/Entity.h>
#include <osogine/game/TextureEntity.h>
#include <osogine/gfx/GfxObject.h>
#include <osogine/gfx/TextureGfxP.h>

TextureGfxP::TextureGfxP() : GfxProgram()
{
}

TextureGfxP::~TextureGfxP()
{
	// TODO Auto-generated destructor stub
}

void TextureGfxP::init()
{
	addUniform( "MVP" );
	addUniform( "TextureSampler" );
	addUniform( "alpha" );
	addUniform( "textureColour" );
}

void TextureGfxP::execute()
{
	TextureEntity* tEntity = (TextureEntity*) entity;

	toggleDepthTest(false);

	setUniformValue( "MVP", getMVP() );
	setUniformValue( "TextureSampler", 0 );
	setUniformValue( "alpha", tEntity->getTextureAlpha());
	setUniformValue( "textureColour", tEntity->getTextureColour());
	activateBuffer( gfx->getVertexBuffer(), 0 );
	activateBuffer( gfx->getUVBuffer(), 1 );

	activateTexture( tEntity->getTexture(), 0 );

	activateBuffer( gfx->getIndexBuffer(), 0 );
	draw( );

	//TODO auto cleanup buffers after use
	deactivateBuffer( 0 );
	deactivateBuffer( 1 );
}
