/*
 * LinkedMesh.h
 *
 *  Created on: 20 Oct 2014
 *      Author: mysticalOso
 */

#ifndef LINKEDMESH_H_
#define LINKEDMESH_H_

#include <osogine/utils/List.h>
#include <osogine/gfx/Triangle.h>

class GfxObject;
class LinkedVertex;
class LinkedTriangle;
class Polygon;


using namespace glm;

class LinkedMesh {
public:
	LinkedMesh( GfxObject* gfx );
	virtual ~LinkedMesh();

	void build( GfxObject* gfx );

	void updateGfx();

	GfxObject* getGfx(){ return gfx; }

	void removeVertex(int i);

	int getNoVertices(){ return verts->size(); }

	LinkedVertex* getVertex(int i){ return verts->get(i); }

	Polygon* getOutline();

	List<Triangle> removeTrianglesNear(vec2 pos);


private:

	void buildVertices();
	void buildTriangles();

	void clearLooseVertices();
	void updateGfxVertices();
	void updateGfxIndices();

	bool isVertexAdded(vec3 v);
	bool isTriangleAdded(LinkedVertex* v1, LinkedVertex* v2, LinkedVertex* v3);
	LinkedVertex* getMatchingVertex(vec3 v);
	LinkedVertex* getMatchingVertexForIndex(int i);
	GfxObject* gfx;
	List< LinkedVertex* >* verts;
	List< LinkedTriangle* >* triangles;
};

#endif /* LINKEDMESH_H_ */
