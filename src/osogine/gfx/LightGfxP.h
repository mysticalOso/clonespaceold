/*
 * LightGfxP.h
 *
 *  Created on: 24 Feb 2015
 *      Author: Adam Nasralla
 */

#ifndef SRC_OSOGINE_GFX_LIGHTGFXP_H_
#define SRC_OSOGINE_GFX_LIGHTGFXP_H_

#include <osogine/gfx/GfxProgram.h>

class LightGfxP: public GfxProgram
{
public:
	LightGfxP();
	~LightGfxP();

	void init();
	void execute();
};

#endif /* SRC_OSOGINE_GFX_LIGHTGFXP_H_ */
