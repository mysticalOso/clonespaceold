/*
 * DynamicTexture.h
 *
 *  Created on: 1 Nov 2014
 *      Author: mysticalOso
 */

#ifndef SRC_OSOGINE_GFX_DYNAMICTEXTURE_H_
#define SRC_OSOGINE_GFX_DYNAMICTEXTURE_H_

#include <GL/glew.h>

class DynamicTexture {
public:
	DynamicTexture();
	virtual ~DynamicTexture();
	int getSize(){return 128;}
	void makeRandom();

	//TODO change DynamicTexture to variable array and make private
	GLubyte pixels[128][128][4];

};

#endif /* SRC_OSOGINE_GFX_DYNAMICTEXTURE_H_ */
