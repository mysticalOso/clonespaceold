/*
 * LineGfxP.cpp
 *
 *  Created on: 17 Oct 2014
 *      Author: Adam Nasralla
 */

#include <glm/mat4x4.hpp>
#include <osogine/gfx/GfxObject.h>
#include <osogine/gfx/LineGfxP.h>

LineGfxP::LineGfxP()
{
	// TODO Auto-generated constructor stub

}

LineGfxP::~LineGfxP()
{
	// TODO Auto-generated destructor stub
}

void LineGfxP::init()
{
	addUniform( "MVP" );
}

void LineGfxP::execute()
{
	toggleDepthTest(false);

	setUniformValue( "MVP", getMVP() );

	activateBuffer( gfx->getIndexBuffer(), 0 );
	activateBuffer( gfx->getVertexBuffer(), 0 );
	activateBuffer( gfx->getColourBuffer(), 1 );

	drawLines();

	//TODO auto cleanup buffers after use
	deactivateBuffer( 0 );
	deactivateBuffer( 1 );
}
