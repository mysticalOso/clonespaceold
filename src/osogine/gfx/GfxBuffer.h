/*
 * GfxBuffer.h
 *
 *  Created on: 2 Oct 2014
 *      Author: Adam Nasralla
 */

#ifndef GFXBUFFER_H_
#define GFXBUFFER_H_

#include <glm/fwd.hpp>
#include <osogine/gfx/IGfx.h>
#include <osogine/utils/List.h>

using namespace glm;

template <class T>
class GfxBuffer : public List<T>
{
public:
	GfxBuffer();
	~GfxBuffer();

	bool getIsUploaded(){ return isUploaded; }
	void setIsUploaded( bool b ) { isUploaded = b; }

	bool getHasChanged(){ return hasChanged; }
	void setHasChanged( bool b ) { hasChanged = b; }

	uint getId() { return id; }

	uint* getIdPointer() { return &id; }


	uint getByteSize();

	void setIGfx(IGfx* g){ iGfx = g;}

	void add( T value ){ hasChanged = true; List<T>::add(value); }
	void remove( T value ){ hasChanged = true; List<T>::remove(value); }
	void set( int index, T value ){ hasChanged = true; List<T>::set(index, value); }
	void clear(){ hasChanged = true; List<T>::clear(); }

	GfxBuffer* clone();


private:
	uint id;
	bool hasChanged;
	bool isUploaded;
	IGfx* iGfx;

};

template<class T>
inline GfxBuffer<T>::GfxBuffer()
{
	id = 0;
	iGfx = 0;
	hasChanged = false;
	isUploaded = false;
}

template<class T>
inline GfxBuffer<T>::~GfxBuffer()
{
	if (iGfx!=0) iGfx->deleteBuffer(getIdPointer());
}

template<class T>
inline uint GfxBuffer<T>::getByteSize()
{
	return this->size()*sizeof(T);
}

template<class T>
inline GfxBuffer<T>* GfxBuffer<T>::clone()
{
	GfxBuffer<T>* g = new GfxBuffer<T>();
	g->setVector(this->getVector());
	return g;
}

#endif /* GFXBUFFER_H_ */
