/*
 * Triangle.cpp
 *
 *  Created on: 8 Jan 2015
 *      Author: mysticalOso
 */

#include <osogine/gfx/LinkedTriangle.h>
#include <osogine/gfx/LinkedVertex.h>
#include <osogine/gfx/Triangle.h>
#include <osogine/utils/Utils.h>


Triangle::Triangle(vec3 p1, vec3 p2, vec3 p3, vec4 c)
{
	init(p1,p2,p3,c);
}

Triangle::Triangle(LinkedTriangle* linkedTri)
{
	init(linkedTri->getVertex(0)->getPosition(),
			linkedTri->getVertex(1)->getPosition(),
			linkedTri->getVertex(2)->getPosition(),
			linkedTri->getVertex(0)->getColour());
}

Triangle::~Triangle() {}

void Triangle::updateNormal()
{
	normal = Utils::calculateNormal(verts.get(0),verts.get(1),verts.get(2));
}

void Triangle::init(vec3 p1, vec3 p2, vec3 p3, vec4 c)
{
	verts.clear();
	verts.add(p1);
	verts.add(p2);
	verts.add(p3);
	colour = c;
	updateNormal();
}
