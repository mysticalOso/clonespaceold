/*
 * SimpleGfxProgram.h
 *
 *  Created on: 3 Oct 2014
 *      Author: Adam Nasralla
 */

#ifndef SIMPLEGFXPROGRAM_H_
#define SIMPLEGFXPROGRAM_H_

#include <osogine/gfx/GfxProgram.h>

class SimpleGfxP : public GfxProgram
{
public:
	SimpleGfxP();
	~SimpleGfxP();

	void init();
	void execute();

};

#endif /* SIMPLEGFXPROGRAM_H_ */
