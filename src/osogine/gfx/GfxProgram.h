/*
frd * GfxProgram.h
 *
 *  Created on: 9 Sep 2014
 *      Author: mysticalOso
 */

#ifndef GFXPROGRAM_H_
#define GFXPROGRAM_H_

#include <glm/fwd.hpp>
#include <osogine/game/EntityList.h>
#include <osogine/gfx/GfxBuffer.h>
#include <osogine/utils/List.h>
#include <map>
#include <string>
#include <vector>

using namespace std;
using namespace glm;

class IGfx;
class GfxObject;
class Entity;
class Camera;
class Light;
class FloatTexture;

class GfxProgram {
public:
	GfxProgram();
	virtual ~GfxProgram();

	void load(IGfx* g1, string vertexShader, string fragmentShader);

	void render(Entity* entity);

	virtual void init(){}

	virtual void execute(){}

	virtual void addLight(Light* l);

	virtual Light* getLight(int i){ return (Light*) lights->get(i); }

protected:

	void addUniform(string name);

//	template <class T>
//  setUniformValue(string name, T value);
	void setUniformValue(string name, int value);
	void setUniformValue(string name, float value);
	void setUniformValue(string name, vec2 value);
	void setUniformValue(string name, vec3 value);
	void setUniformValue(string name, vec4 value);
	void setUniformValue(string name, mat4 value);
	void setUniformValue(string name, vector<int> value, uint count);
	void setUniformValue(string name, vector<float> value, uint count);
	void setUniformValue(string name, vector<vec3> value, uint count);
	void setUniformValue(string name, vector<vec2> value, uint count);
//	template <class T>
//	void activateBuffer(GfxBuffer<T>* buffer, uint location);

	void activateBuffer(GfxBuffer<short>* buffer, uint location);
	void activateBuffer(GfxBuffer<float>* buffer, uint location);
	void activateBuffer(GfxBuffer<vec2>* buffer, uint location);
	void activateBuffer(GfxBuffer<vec3>* buffer, uint location);
	void activateBuffer(GfxBuffer<vec4>* buffer, uint location);

	void deactivateBuffer(uint location);

	void activateTexture( uint texture, uint location );

	void drawTriangles(uint noVertices);

	void drawLines();

	void draw();

	void addFloatTexture(FloatTexture* texture);
	void updateFloatTexture(FloatTexture* texture);

	mat4 getMVP();

	mat4 getVP();

	void useProgram();
	void toggleDepthTest(bool b);

	void clearScreen();

	void clearDepthBit();

	Entity* entity;
	GfxObject* gfx;
	Camera* camera;

	uint id;
	IGfx* g;
	map<string, uint> uniforms;

	EntityList* lights;
};



#endif /* GFXPROGRAM_H_ */
