/*
 * GfxHub.h
 *
 *  Created on: 8 Oct 2014
 *      Author: Adam Nasralla
 */

#ifndef GFXHUB_H_
#define GFXHUB_H_

#include <glm/fwd.hpp>
#include <map>
#include <string>

using namespace std;
using namespace glm;

class IGfx;
class GfxBuilder;
class GfxObject;
class GfxProgram;
class GfxBuilder;
class GfxProgram;
class GfxIndexer;
class DynamicTexture;
class FloatTexture;

class GfxHub
{
public:
	GfxHub();
	virtual ~GfxHub();

	void init(IGfx* gfxInterface1);

	GfxProgram* getProgram( string name ){ return programs[name]; }
	uint getTexture( string name ){ return textures[name]; }
	GfxObject* getObject( string name ){ return objects[name]; }

	void addDynamicTexture(DynamicTexture* texture, string name);

	void addFloatTexture(FloatTexture* texture);
	void updateFloatTexture(FloatTexture* texture);
	GfxBuilder* getBuilder(){ return builder; }

	GfxIndexer* getIndexer();

	void clearDepth();

protected:

	virtual void initPrograms();
	virtual void initTextures();
	virtual void initObjects();


	map<string, GfxProgram*> programs;
	map<string, uint> textures;
	map<string, GfxObject*> objects;

	IGfx* gfxI;
	GfxBuilder* builder;

};

#endif /* GFXHUB_H_ */
