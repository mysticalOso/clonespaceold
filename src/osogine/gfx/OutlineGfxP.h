/*
 * OutlineGfxP.h
 *
 *  Created on: 7 Feb 2015
 *      Author: mysticalOso
 */

#ifndef SRC_OSOGINE_GFX_OUTLINEGFXP_H_
#define SRC_OSOGINE_GFX_OUTLINEGFXP_H_

#include <osogine/gfx/GfxProgram.h>

class GfxObject;
class GfxBuilder;

class OutlineGfxP: public GfxProgram {
public:
	OutlineGfxP();
	virtual ~OutlineGfxP();

	void init();
	void execute();

private:

	GfxObject* getOutlineGfx();
	GfxBuilder* builder;
};

#endif /* SRC_OSOGINE_GFX_OUTLINEGFXP_H_ */
