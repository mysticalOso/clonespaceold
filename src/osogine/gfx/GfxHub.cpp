/*
 * GfxStorage.cpp
 *
 *  Created on: 4 Oct 2014
 *      Author: Adam Nasralla
 */

#include <osogine/gfx/DynamicTexture.h>
#include <osogine/gfx/FlatGfxP.h>
#include <osogine/gfx/FloatTexture.h>
#include <osogine/gfx/GfxBuilder.h>
#include <osogine/gfx/GfxHub.h>
#include <osogine/gfx/IGfx.h>
#include <osogine/gfx/LightGfxP.h>
#include <osogine/gfx/LineGfxP.h>
#include <osogine/gfx/OutlineGfxP.h>
#include <osogine/gfx/SimpleGfxP.h>
#include <osogine/gfx/StandardGfxP.h>
#include <osogine/gfx/TextureGfxP.h>
#include <stdio.h>

GfxHub::GfxHub()
{
	gfxI = 0;
	builder = 0;
}

GfxHub::~GfxHub()
{
}

void GfxHub::init(IGfx* gfxInterface1)
{
	gfxI = gfxInterface1;
	builder = new GfxBuilder();
	initPrograms();
	initTextures();
	initObjects();
}

void GfxHub::initPrograms()
{
	programs[ "simple" ] = new SimpleGfxP();
	programs[ "simple" ]->load( gfxI, "simple.vs" , "simple.fs" );

	programs[ "simpleFlat" ] = new SimpleGfxP();
	programs[ "simpleFlat" ]->load( gfxI, "simpleFlat.vs" , "simpleFlat.fs" );

	programs[ "lines" ] = new LineGfxP();
	programs[ "lines" ]->load( gfxI, "simple.vs" , "simple.fs" );

	programs[ "outline" ] = new OutlineGfxP();
	programs[ "outline" ]->load( gfxI, "simple.vs" , "simple.fs" );

	programs[ "texture" ] = new TextureGfxP();
	programs[ "texture" ]->load( gfxI, "texture.vs" , "texture.fs" );

	programs[ "text" ] = new TextureGfxP();
	programs[ "text" ]->load( gfxI, "texture.vs" , "text.fs" );

	programs[ "standard" ] = new StandardGfxP();
	programs[ "standard" ]->load( gfxI, "standard.vs", "standard.fs" );

	programs[ "flat" ] = new FlatGfxP();
	programs[ "flat" ]->load( gfxI, "flatshading.vs", "flatshading.fs" );

	programs[ "pod" ] = new FlatGfxP();
	programs[ "pod" ]->load( gfxI, "flatshading.vs", "pod.fs" );

	programs[ "light" ] = new LightGfxP();
	programs[ "light" ]->load( gfxI, "texture.vs", "light.fs" );


}

void GfxHub::initTextures()
{
	//textures[ "rock" ] = gfxI->loadTexture( "rock.bmp" );
	textures[ "ui" ] = gfxI->loadTexture("UIPanels.png",2048);
	textures[ "ui2" ] = gfxI->loadTexture("UIPanels2.png",2048);
	textures[ "uiIcons" ] = gfxI->loadTexture("UIIcons.png", 1024);
	textures[ "light" ] = gfxI->loadTexture( "light.png", 512 );
	textures[ "font" ] = gfxI->loadTexture( "font2.png", 512 );
	textures[ "comms" ] = gfxI->loadTexture( "comms.png", 1024 );
	textures[ "roomIcons" ] = gfxI->loadTexture( "RoomSprites.png", 1024);
	textures[ "teleportRoom" ] = gfxI->loadTexture( "TeleportRoom.png", 1024 );
	textures[ "engineRoom" ] = gfxI->loadTexture( "EngineRoom.png", 1024 );
	textures[ "gemRoom" ] = gfxI->loadTexture( "GemRoom.png", 1024 );
	textures[ "energyRoom" ] = gfxI->loadTexture( "EnergyRoom.png", 1024 );
	textures[ "steeringRoom" ] = gfxI->loadTexture( "SteeringRoom.png", 1024 );
	textures[ "missileRoom" ] = gfxI->loadTexture( "MissileRoom.png", 1024 );
	textures[ "lazerRoom" ] = gfxI->loadTexture( "LazerRoom.png", 1024 );
	textures[ "shieldRoom" ] = gfxI->loadTexture( "ShieldRoom.png", 1024 );
}

void GfxHub::addDynamicTexture(DynamicTexture* texture, string name)
{
//	for (int i = 0; i < 512; i++)
//	{
//		for (int j = 0; j < 512; j++)
//		{
//			for (int k = 0; k < 4; k++)
//			{
//				printf("%d\n",texture->pixels[i][j][k]);
//			}
//		}
//	}

	textures[name] = gfxI->loadDynamicTexture(texture->pixels, texture->getSize());

}



void GfxHub::initObjects()
{
	objects[ "cube" ] = builder->buildCube( 2 );
}

GfxIndexer* GfxHub::getIndexer()
{
	return builder->getIndexer();
}

void GfxHub::clearDepth()
{
	gfxI->clearDepthBit();
}

void GfxHub::addFloatTexture(FloatTexture* texture)
{
	texture->id = gfxI->loadFloatTexture(texture->floats, texture->getSize());
}

void GfxHub::updateFloatTexture(FloatTexture* texture)
{
	gfxI->updateFloatTexture(texture->id, texture->floats, texture->getSize());
}
