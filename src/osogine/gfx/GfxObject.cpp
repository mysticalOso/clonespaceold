/*
 * GfxObject.cpp
 *
 *  Created on: 9 Sep 2014
 *      Author: mysticalOso
 */

#include <osogine/gfx/GfxObject.h>
#include <osogine/utils/Dice.h>
#include <osogine/utils/Printer.h>
#include <osogine/utils/Transform.h>
#include <osogine/utils/Utils.h>
#include <glm/mat4x4.hpp>

GfxObject::GfxObject()
{
	vertices = new GfxBuffer<vec3>();
	normals = new GfxBuffer<vec3>();
	colours = new GfxBuffer<vec4>();
	uvs = new GfxBuffer<vec2>();
	indices = new GfxBuffer<short>();
	outline = new List<vec2>();
	depthEnabled = true;
	depthMask = true;
}

GfxObject::~GfxObject()
{
	//delete vertices;
	//delete normals;
	//delete colours;
	//delete uvs;
	//delete indices;
	//delete outline;
}


void GfxObject::addTriangle(vec3 a, vec3 b, vec3 c)
{

	addVertex(a);
	addVertex(b);
	addVertex(c);


}

void GfxObject::calculateNormals( bool isIndexed )
{
	if (!isIndexed)
	{
		for ( uint i = 0; i < getNoVertices(); i+=3 )
		{
			vec3 n = Utils::calculateNormal(  getVertex(i), getVertex(i+1), getVertex(i+2) );
			addNormal(n);
			addNormal(n);
			addNormal(n);
			fflush(stdout);
		}
	}
}

void GfxObject::add(GfxObject* newGfx)
{
	newGfx->incrementIndices( getNoVertices() );

	indices->append( newGfx->getIndexBuffer() );
	vertices->append( newGfx->getVertexBuffer() );
	normals->append( newGfx->getNormalBuffer() );
	colours->append( newGfx->getColourBuffer() );
	uvs->append( newGfx->getUVBuffer() );
}

void GfxObject::incrementIndices(int increment)
{
	for ( uint i = 0; i < getNoIndices(); i++ )
	{
		indices->set(i, indices->get(i) + increment);
	}
}

void GfxObject::setColour(vec4 colour)
{
	colours->clear();
	for ( uint i = 0; i < getNoVertices(); i++ )
	{
		colours->add(colour);
	}
	colours->setHasChanged(true);
}

void GfxObject::setAlpha(float alpha)
{
	for ( uint i = 0; i < colours->size(); i++ )
	{
		vec4 c = colours->get(i);
		c.a = alpha;
		colours->set(i,c);
	}
}

void GfxObject::clearAll()
{
	vertices->clear();
	colours->clear();
	normals->clear();
	uvs->clear();
	indices->clear();
	outline->clear();
}

void GfxObject::print()
{
	for ( uint i = 0; i < getNoIndices(); i++ )
	{
		printf("%d  \n",i);
		Printer::print("  Index",(int)getIndex(i),"\n");
//		printf("%d  \n",i);
		Printer::print("  Vertex ",getVertex(getIndex(i)),"   \n");
		Printer::print("  Colour ",getColour(getIndex(i)),"   \n");
		//Printer::print("  Normal ",getNormal(getIndex(i)),"   \n");
//		printf("\n \n");
//		Printer::flush();
	}

//	for (uint i = 0; i < getNoVertices(); i++)
//	{
//		printf("%d  \n",i);
//		Printer::print("  Vertex ",getVertex(i),"   \n");
//	}


}

void GfxObject::applyTransform(Transform* transform)
{
	mat4 matrix = transform->getMatrix();
	for ( uint i = 0; i < getNoVertices(); i++ )
	{
		vec4 v = matrix * vec4(getVertex(i),1);
		setVertex(i, vec3(v) );
	}
}

void GfxObject::applyLocalTransform(Transform* transform)
{
	mat4 matrix = transform->getLocalMatrix();
	for ( uint i = 0; i < getNoVertices(); i++ )
	{
		vec4 v = matrix * vec4(getVertex(i),1);
		setVertex(i, vec3(v) );
	}
}

void GfxObject::applyInverseTransform(Transform* transform)
{
	mat4 matrix = transform->getInverseMatrix();
	for ( uint i = 0; i < getNoVertices(); i++ )
	{
		vec4 v = matrix * vec4(getVertex(i),1);
		setVertex(i, vec3(v) );
	}
}

void GfxObject::addTriangle(int i, int j, int k)
{
	addIndex(i);
	addIndex(j);
	addIndex(k);
}

void GfxObject::addTriangle(float x, float y, float size, float ratio, float angle,
		glm::vec4 colour)
{
	int v = vertices->size();
	vec2 p1 = Utils::getPointAwayFrom(vec2(x,y),angle,size*ratio);
	vec2 p2 = Utils::getPointAwayFrom(vec2(x,y),angle+(PI*0.66666),size*(1.f/ratio));
	vec2 p3 = Utils::getPointAwayFrom(vec2(x,y),angle-(PI*0.66666),size*(1.f/ratio));

	addVertex( p1.x, p1.y, 0 );
	addVertex( p2.x, p2.y, 0 );
	addVertex( p3.x, p3.y, 0 );

	addColour(colour.r, colour.g, colour.b, colour.a);
	addColour(colour.r, colour.g, colour.b, colour.a);
	addColour(colour.r, colour.g, colour.b, colour.a);

	addTriangle(v,v+1,v+2);
}

void GfxObject::addQuad(float x, float y, float width, float height,
		glm::vec4 c)
{
	int v = getNoVertices();
	addVertex(x,y,0);
	addVertex(x+width,y,0);
	addVertex(x+width,y+height,0);
	addVertex(x,y+height,0);
	addColour(c.r,c.g,c.b,c.a);
	addColour(c.r,c.g,c.b,c.a);
	addColour(c.r,c.g,c.b,c.a);
	addColour(c.r,c.g,c.b,c.a);

	addIndex(v); addIndex(v+1); addIndex(v+2);
	addIndex(v+2); addIndex(v+3); addIndex(v);
}

void GfxObject::addQuad(vec2 a, vec2 b, vec2 c, vec2 d, vec4 colour)
{
	int offset = getNoVertices();
	addVertex(a.x,a.y,0);
	addVertex(b.x,b.y,0);
	addVertex(c.x,c.y,0);
	addVertex(d.x,d.y,0);
	addColour(colour.r,colour.g,colour.b,colour.a);
	addColour(colour.r,colour.g,colour.b,colour.a);
	addColour(colour.r,colour.g,colour.b,colour.a);
	addColour(colour.r,colour.g,colour.b,colour.a);
	addTriangle(offset,offset+1,offset+2);
	addTriangle(offset,offset+3,offset+2);
}

void GfxObject::fillTriangles()
{
	int noSegments = getNoVertices()/2;
	for (int i=0; i<noSegments-1; i++)
	{
		int a = i*2;
		int b = a+1;
		int c = a+2;
		int d = a+3;
		addTriangle(a,c,b);
		addTriangle(b,c,d);
	}
}

void GfxObject::fillLines(bool isLoop)
{
	for (int i=0; i<getNoVertices()-1; i++)
	{
		addIndex(i);
		addIndex(i+1);
	}
	if (isLoop)
	{
		addIndex(getNoVertices()-1);
		addIndex(0);
	}
}



void GfxObject::reColour(vec4 colour1, vec4 colour2)
{
	for (int i=0; i<colours->size(); i++)
	{
		colours->set(i, Dice::roll(colour1,colour2));
	}
}

void GfxObject::addTriangle(vec3 a, vec3 b, vec3 c, vec4 colour)
{
	addIndex(getNoVertices());
	addIndex(getNoVertices()+1);
	addIndex(getNoVertices()+2);
	addTriangle(a,b,c);
	addColour(colour);
	addColour(colour);
	addColour(colour);
}

void GfxObject::setTriangle(int i, vec3 a, vec3 b, vec3 c, vec4 colour)
{
	setVertex(i*3,a);
	setVertex(i*3 +1, b);
	setVertex(i*3 +2, c);
	setColour(i*3, colour);
	setColour(i*3 +1, colour);
	setColour(i*3 +2, colour);
}

GfxObject* GfxObject::clone()
{
	GfxObject* g = new GfxObject();
	g->getVertexBuffer()->setVector(vertices->getVector());
	g->getNormalBuffer()->setVector(normals->getVector());
	g->getUVBuffer()->setVector(uvs->getVector());
	g->getIndexBuffer()->setVector(indices->getVector());
	g->getColourBuffer()->setVector(colours->getVector());
	g->getOutline()->setVector(outline->getVector());
	return g;
}

void GfxObject::addLine(vec3 a, vec3 b, vec4 colour)
{
	addIndex(getNoVertices());
	addIndex(getNoVertices()+1);
	addVertex(a);
	addVertex(b);
	addColour(colour);
	addColour(colour);
}

float GfxObject::getAlpha()
{
	vec4 c = getColour(0);
	return c.a;
}
