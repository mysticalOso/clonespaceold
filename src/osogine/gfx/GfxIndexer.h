/*
 * GfxUtils.h
 *
 *  Created on: 16 Oct 2014
 *      Author: Adam Nasralla
 */

#ifndef GFXINDEXER_H_
#define GFXINDEXER_H_

#include <glm/fwd.hpp>
#include <glm/vec3.hpp>
#include <osogine/utils/List.h>

using namespace glm;
using namespace std;

class GfxObject;

class GfxIndexer
{
public:
	GfxIndexer();
	~GfxIndexer();

	void setupForFlatShading(GfxObject* gfx);

	void setupForWireframe(GfxObject* gfx);

	void setupForStandardShading(GfxObject* gfx);

	void setupForDrawingNormals(GfxObject* gfx, float normalLength);

private:

	void fillNormals();
	void initBuffers(GfxObject* gfx);
	void initProvokingList();
	void getTri(int i);

	bool isProvoking(int i);
	void setIsProvoking(int i);
	void calculateCurrentNormal();
	void addNewProvokingVertex();
	void useExistingAsProvoking();
	void addRemainingIndices();

	vec3 getTriCentre();

	List<bool>* provokingList;
	List<short>* indices;
	List<short>* oldIndices;
	List<short>* tri;
	List<vec3>* normals;
	List<vec3>* vertices;
	List<vec4>* colours;
	vec3 currentNormal;
	int currentIndex;
	int normalAddedAt;
};


#endif /* GFXINDEXER_H_ */
