/*
 * TriangleInterface.cpp
 *
 *  Created on: 23 Oct 2013
 *      Author: Adam Nasralla
 */

#define REAL double

#include <osogine/gfx/MeshGenerator2d.h>

#include <glm/vec3.hpp>

#include <stdio.h>
#include <stdlib.h>
#include <queue>

#include <osogine/utils/Dice.h>
#include <osogine/gfx/GfxObject.h>
#include <cstring>

#include <osogine/utils/Utils.h>
#include <osogine/utils/Dice.h>

using namespace glm;
using namespace std;

MeshGenerator2d::MeshGenerator2d()
{
	init();
}

MeshGenerator2d::~MeshGenerator2d()
{
}

//TODO remove colour from TriangleInterface

void MeshGenerator2d::buildMesh(vector<vec2> points, string area, vec3 colour1, vec3 colour2, bool is3d, bool useRows1)
{
	init();

	useRows = useRows1;
	is3D = is3d;
	outline = points;

	callTriangulate(points, area);
	initArrays();

	calculateHeight();

	setGfxVertices();

	for (int i = 0; i < tri.numberoftriangles; i++)
	{
		addTriangle(i,colour1,colour2);
	}



	setGfxOutline();

	freeMemory();

}




void MeshGenerator2d::addTriangleFlat(int triangleIndex, glm::vec3 colour1, glm::vec3 colour2)
{
	if (added[triangleIndex]!=1)
	{
		totalAdded++;
		//float b = totalRows;
		//float a = (row[triangleIndex]+1)/2;
		//printf("a=%f b=%f\n",a,b);
		//fflush(stdout);
		//float color = a/b;
		int index = 0;
		int i = triangleIndex;
		//printf("Adding %d\n",i);
		int misses = 0;
		int index1 = tri.trianglelist[i * 3];
		int index2 = tri.trianglelist[i * 3 + 1];
		int index3 = tri.trianglelist[i * 3 + 2];
		glm::vec3 a1 = glm::vec3(tri.pointlist[index1*2], tri.pointlist[index1*2 + 1], height[index1]);
		glm::vec3 b1 = glm::vec3(tri.pointlist[index2*2], tri.pointlist[index2*2 + 1], height[index2]);
		glm::vec3 c1 = glm::vec3(tri.pointlist[index3*2], tri.pointlist[index3*2 + 1], height[index3]);

		glm::vec3 normal;
		if (is3D)
		{
			normal = Utils::calculateNormal( a1, b1, c1 ); //-glm::cross(b1-c1,a1-c1);
		}
		else
		{
			normal = glm::vec3(0,0,1);
		}

		for (int j = 0; j < 3; j++)
		{

			index = tri.trianglelist[i * 3 + j];
			if (j==misses)
			{
				if (tri.pointattributelist[index]!=1)
				{
					gfx->addIndex(tri.trianglelist[i * 3 + j]);
					gfx->setColour(tri.trianglelist[i * 3 + j], vec4(Dice::roll(colour1.r,colour2.r),Dice::roll(colour1.g,colour2.g),Dice::roll(colour1.b,colour2.b),1.0f) );
					gfx->setNormal(tri.trianglelist[i * 3 + j], vec3(normal.x,normal.y,normal.z) );
					tri.pointattributelist[index] = 1;
				}
				else
				{
					misses++;
				}
			}
			else
			{
				gfx->addIndex(tri.trianglelist[i * 3 + j]);
			}

		}
		int start = 0;
		if (misses==3)
		{
			index = tri.trianglelist[i * 3];
			float z1 = 0;
			if (is3D) z1 = height[index];
			gfx->addVertex(tri.pointlist[index*2],tri.pointlist[index*2 +1],z1);
			gfx->addColour(Dice::roll(colour1.r,colour2.r),Dice::roll(colour1.g,colour2.g),Dice::roll(colour1.b,colour2.b),1.0f);
			gfx->addNormal(normal.x,normal.y,normal.z);
			gfx->addIndex(indexCount);
			indexCount++;
			start = 1;
		}
		for (int j = start; j < misses; j++)
		{
			index = tri.trianglelist[i * 3 + j];
			gfx->addIndex(tri.trianglelist[i * 3 + j]);
		}
		added[triangleIndex] = 1;
	}
}



//TODO optimize calculateRows if necessary
void MeshGenerator2d::calculateHeightByRows()
{
	int neighbour = 0;
	for (int i = 0; i < tri.numberoftriangles; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			neighbour = tri.neighborlist[i * 3 + j];
			if (neighbour==-1)
			{
				row[i]=1;
			}

		}
	}
	bool allRowsFound = false;
	int currentRow = 1;
	totalRows = 0;
	while (!allRowsFound)
	{
		allRowsFound = true;
		currentRow++;
		for (int i = 0; i < tri.numberoftriangles; i++)
		{
			if (row[i]==0)
			{
				for (int j = 0; j < 3; j++)
				{
					neighbour = tri.neighborlist[i * 3 + j];
					if (row[neighbour]==currentRow-1)
					{
						row[i]=currentRow;
					}

				}
				allRowsFound = false;
			}

		}
	}
	totalRows = currentRow-1;
	int pointIndex = 0;
	float adjustedRow;
	float adjustedTotal = (totalRows-1)/2;
	//TODO improve height algorithm


	float maxDist = 0;
	for (int i = 0; i < tri.numberofpoints; i++)
	{
		vec2 p1 = vec2(tri.pointlist[i*2],tri.pointlist[i*2+1]);
		float dist = Utils::distFromPointToPoly(p1,outline);
		if (dist>maxDist)
		{
			maxDist = dist;
		}
	}

	for (currentRow=3; currentRow<=totalRows; currentRow+=2)
	{
		for (int i = 0; i < tri.numberoftriangles; i++)
		{
			if (row[i]==currentRow)
			{
				adjustedRow = (row[i]-1) / 2;
				for (int j = 0; j < 3; j++)
				{
					pointIndex = tri.trianglelist[i*3+j];
					height[pointIndex] = maxDist * sin( acos ( 1 - adjustedRow / adjustedTotal) );
					//printf("Row = %d  Height = %f\n",currentRow, height[pointIndex]);
				}
			}
		}
	}
}

void MeshGenerator2d::calculateHeightByDistance()
{
	float maxDist = 0;

	for (int i = 0; i < tri.numberofpoints; i++)
	{
		vec2 p1 = vec2(tri.pointlist[i*2],tri.pointlist[i*2+1]);
		float dist = Utils::distFromPointToPoly(p1,outline);
		if (dist>maxDist)
		{
			maxDist = dist;
		}
	}
	for (int i = 0; i < tri.numberofpoints; i++)
	{
		vec2 p1 = vec2(tri.pointlist[i*2],tri.pointlist[i*2+1]);
		float dist = Utils::distFromPointToPoly(p1,outline);
		height[i] = maxDist * sin( acos ( 1 - (dist/ maxDist)) ) +1;
	}
}

void MeshGenerator2d::addTriangle(int triangleIndex, glm::vec3 colour1,
		glm::vec3 colour2)
{
	//colour2 = colour1;
	if (added[triangleIndex]!=1)
	{
		totalAdded++;
		//float b = totalRows;
		//float a = (row[triangleIndex]+1)/2;
		//printf("a=%f b=%f\n",a,b);
		//fflush(stdout);
		//float color = a/b;
		//int index = 0;
		int i = triangleIndex;
		//printf("Adding %d\n",i);
	    //int misses = 0;
		int index1 = tri.trianglelist[i * 3];
		int index2 = tri.trianglelist[i * 3 + 1];
		int index3 = tri.trianglelist[i * 3 + 2];
		glm::vec3 a1 = glm::vec3(tri.pointlist[index1*2], tri.pointlist[index1*2 + 1], height[index1]);
		glm::vec3 b1 = glm::vec3(tri.pointlist[index2*2], tri.pointlist[index2*2 + 1], height[index2]);
		glm::vec3 c1 = glm::vec3(tri.pointlist[index3*2], tri.pointlist[index3*2 + 1], height[index3]);

		glm::vec3 normal;
		if (is3D)
		{
			normal = -glm::cross(b1-c1,a1-c1);
		}
		else
		{
			normal = glm::vec3(0,0,1);
		}

		for (int j = 0; j < 3; j++)
		{

					gfx->addIndex(tri.trianglelist[i * 3 + j]);
					gfx->setColour(tri.trianglelist[i * 3 + j], vec4( Dice::roll(colour1.r,colour2.r),Dice::roll(colour1.g,colour2.g),Dice::roll(colour1.b,colour2.b),1.0f ) );
					gfx->setNormal(tri.trianglelist[i * 3 + j], vec3(normal.x,normal.y,normal.z) );


		}

		added[triangleIndex] = 1;
	}
}

void MeshGenerator2d::callTriangulate(std::vector<glm::vec2> points, std::string area)
{
	int noPoints = points.size();

	in.numberofpointattributes = 1;
	in.numberofpoints = noPoints;
	in.pointlist = (REAL *) malloc(in.numberofpoints * 2 * sizeof(REAL));
	in.numberofsegments = noPoints;
	in.segmentlist = (int *) malloc(in.numberofsegments * 2 * sizeof(int));
	in.segmentmarkerlist = (int *) malloc(in.numberofsegments * 2 * sizeof(int));

	for (int i=0; i<noPoints; i++)
	{
		in.pointlist[i*2] = points[i].x;
		in.pointlist[i*2+1] = points[i].y;
		in.segmentlist[i*2] = i;
		int j = i+1;
		if (j==noPoints) j=0;
		in.segmentlist[i*2+1] = j;
	}

  in.pointattributelist = (REAL *) malloc(in.numberofpoints *
										  in.numberofpointattributes *
										  sizeof(REAL));

  in.pointmarkerlist = (int *) malloc(in.numberofpoints * sizeof(int));
  in.numberoftriangleattributes = 1;
  in.numberofholes = 0;
  in.numberofregions = 0;

  tri.pointlist = (REAL *) NULL;            /* Not needed if -N switch used. */
  tri.pointattributelist = (REAL *) NULL;
  tri.pointmarkerlist = (int *) NULL; /* Not needed if -N or -B switch used. */
  tri.trianglelist = (int *) NULL;          /* Not needed if -E switch used. */
  tri.triangleattributelist = (REAL *) NULL;
  tri.neighborlist = (int *) NULL;         /* Needed only if -n switch used. */
  tri.segmentlist = (int *) NULL;
  tri.segmentmarkerlist = (int *) NULL;

  tri.edgelist = (int *) NULL;             /* Needed only if -e switch used. */
  tri.edgemarkerlist = (int *) NULL;   /* Needed if -e used and -B not used. */



  std::string s = "pa" + area + "q28znABQ";

  char *flags=new char[s.size()+1];
  std::strcpy(flags, s.c_str());

  triangulate(flags, &in, &tri, NULL);

  free(in.pointlist);
  free(in.segmentlist);
  free(in.segmentmarkerlist);
  free(in.pointattributelist);
  free(in.pointmarkerlist);

}

void MeshGenerator2d::init()
{
	added = 0;
	totalAdded = 0;
	totalRows = 0;
	is3D = false;
	row = 0;
	visited = 0;
	height = 0;
	explored = 0;
	indexCount = 0;
	gfx = new GfxObject();
	useRows = false;
	outline.clear();
}


void MeshGenerator2d::initArrays()
{
	visited = (int *) malloc(tri.numberoftriangles * sizeof(int));

	for (int i = 0; i < tri.numberoftriangles; i++)
	{
		visited[i] = 0;
	}

	added = (int *) malloc(tri.numberoftriangles * sizeof(int));

	for (int i = 0; i < tri.numberoftriangles; i++)
	{
		added[i] = 0;
	}

	explored = (int *) malloc(tri.numberoftriangles * sizeof(int));

	for (int i = 0; i < tri.numberoftriangles; i++)
	{
		explored[i] = 0;
	}

	row = (int *) malloc(tri.numberoftriangles * sizeof(int));

	for (int i = 0; i < tri.numberoftriangles; i++)
	{
		row[i] = 0;
	}

	height = (float *) malloc(tri.numberofpoints * sizeof(float));

	for (int i = 0; i < tri.numberofpoints; i++)
	{
		height[i] = 0;
	}
}

void MeshGenerator2d::calculateHeight()
{
	if (useRows)
	{
		calculateHeightByRows();
	}
	else
	{
		calculateHeightByDistance();
	}
}

void MeshGenerator2d::setGfxVertices()
{
	//TODO remove colour and normals
	for (int i = 0; i < tri.numberofpoints; i++)
	{
		float z1 = 0;
		if (is3D) z1 = height[i];
		gfx->addVertex(tri.pointlist[i*2],tri.pointlist[i*2 +1],z1);
		gfx->addColour(Dice::roll(0.0,0.0),Dice::roll(0.0,1.0),Dice::roll(0.5,1.0), 1);
		gfx->addNormal(0,0,1);
		indexCount++;
	}
}

void MeshGenerator2d::setGfxOutline()
{
	for (uint i = 0; i < outline.size(); i++)
	{
		gfx->addOutline(outline[i]);
	}
}

void MeshGenerator2d::freeMemory()
{
	free(visited);
	free(added);
	free(explored);
	free(height);
	free(row);

	free(tri.pointlist);
	free(tri.pointattributelist);
	free(tri.trianglelist);
	free(tri.triangleattributelist);
	free(tri.pointmarkerlist);
	free(tri.edgelist);
	free(tri.edgemarkerlist);
	free(tri.neighborlist);
	free(tri.segmentlist);
	free(tri.segmentmarkerlist);
}

