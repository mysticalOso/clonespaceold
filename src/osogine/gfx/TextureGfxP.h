/*
 * StandardGfxProgram.h
 *
 *  Created on: 4 Oct 2014
 *      Author: Adam Nasralla
 */

#ifndef TEXTUREGFXP_H_
#define TEXTUREGFXP_H_

#include <osogine/gfx/GfxProgram.h>

class TextureGfxP: public GfxProgram
{
public:
	TextureGfxP();
	~TextureGfxP();

	void init();
	void execute();
};

#endif /* TEXTUREGFXP_H_ */
