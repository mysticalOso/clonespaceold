/*
 * LightGfxP.cpp
 *
 *  Created on: 24 Feb 2015
 *      Author: Adam Nasralla
 */

#include <osogine/game/Light.h>
#include <osogine/gfx/GfxObject.h>
#include <osogine/gfx/LightGfxP.h>

LightGfxP::LightGfxP()
{
	// TODO Auto-generated constructor stub

}

LightGfxP::~LightGfxP()
{
	// TODO Auto-generated destructor stub
}

void LightGfxP::init()
{
	addUniform( "MVP" );
	addUniform( "textureSampler" );
	addUniform( "lightColour" );
}

void LightGfxP::execute()
{
	Light* light = (Light*) entity;

	toggleDepthTest(false);

	g->enableAdditiveBlend();

	setUniformValue( "MVP", getVP() );
	setUniformValue( "textureSampler", 0 );
	setUniformValue( "lightColour", light->getColour());
	activateBuffer( gfx->getVertexBuffer(), 0 );
	activateBuffer( gfx->getUVBuffer(), 1 );

	activateTexture( light->getTexture(), 0 );

	activateBuffer( gfx->getIndexBuffer(), 0 );
	draw( );

	//TODO auto cleanup buffers after use
	deactivateBuffer( 0 );
	deactivateBuffer( 1 );

	g->enableAlphaBlend();

}
