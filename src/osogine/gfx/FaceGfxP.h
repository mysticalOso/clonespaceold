/*
 * FlatGfxP.h
 *
 *  Created on: 15 Oct 2014
 *      Author: Adam Nasralla
 */

#ifndef FACEGFXP_H_
#define FACEGFXP_H_

#include <osogine/gfx/GfxProgram.h>

class FaceGfxP: public GfxProgram
{
public:
	FaceGfxP();
	~FaceGfxP();

	void init();
	void execute();
};

#endif /* FACEGFXP_H_ */
