/*
 * GfxUtils.cpp
 *
 *  Created on: 16 Oct 2014
 *      Author: Adam Nasralla
 */

#include <glm/vec3.hpp>
#include <osogine/gfx/GfxIndexer.h>
#include <osogine/gfx/GfxObject.h>
#include <osogine/utils/List.h>
#include <osogine/utils/Utils.h>

using namespace std;

GfxIndexer::GfxIndexer()
{
	tri = new List<short>();

	colours = 0;
	normals = 0;
	oldIndices = 0;
	vertices = 0;

	tri = new List<short>();
	indices = new List<short>();

	currentIndex = 0;
	normalAddedAt = 0;

	provokingList = new List<bool>();
}

GfxIndexer::~GfxIndexer()
{
	// TODO Auto-generated destructor stub
}

void GfxIndexer::setupForStandardShading(GfxObject* gfx)
{
	vertices = new List<vec3>();
	for (int i = 0; i < gfx->getNoIndices(); i++)
	{
		vertices->add( gfx->getVertex(gfx->getIndex(i)));
	}
	gfx->clearAll();
	for (int i = 0; i < vertices->size(); i++)
	{
		vec3 vertex = vertices->get(i);
		gfx->addVertex(vertex);
		gfx->addIndex(i);
		gfx->addColour(vec4(1,1,1,1));
	}
	gfx->calculateNormals(false);
}


void GfxIndexer::setupForFlatShading(GfxObject* gfx)
{

	initBuffers(gfx);
	initProvokingList();
	fillNormals();
//	printf("JUST FILLED\n\n");
//
//
//	printf("oldIndicesSize %d \n", vertices->size());
	fflush(stdout);

	for (int i = 0; i < oldIndices->size(); i+=3)
	{

		getTri(i);
		calculateCurrentNormal();

		useExistingAsProvoking();

		if (normalAddedAt == -1)
		{
			addNewProvokingVertex();
			gfx->addColour(vec4(1,1,1,1));
		}

		addRemainingIndices();


	}

//	printf("IndicesSize %d \n", vertices->size());
	fflush(stdout);

	fflush(stdout);

	gfx->setIndexBuffer( indices );


}




void GfxIndexer::setupForWireframe(GfxObject* gfx)
{
	initBuffers(gfx);

	normals->clear();

	colours->clear();

	for (int i = 0; i < oldIndices->size(); i+=3)
	{
		getTri(i);
		calculateCurrentNormal();
		indices->add(tri->get(0));
		indices->add(tri->get(1));

		indices->add(tri->get(1));
		indices->add(tri->get(2));

		indices->add(tri->get(2));
		indices->add(tri->get(0));
	}

	gfx->setColour(vec4(0.5,0.5,0.5,1));

	gfx->setIndexBuffer( indices );
}



void GfxIndexer::setupForDrawingNormals(GfxObject* gfx, float normalLength)
{
	initBuffers(gfx);

	normals->clear();

	colours->clear();

	List<vec3>* newVerts = new List<vec3>();

	for (int i = 0; i < oldIndices->size(); i+=3)
	{
		getTri(i);
		calculateCurrentNormal();
		vec3 centre = getTriCentre();

		indices->add(newVerts->size());
		indices->add(newVerts->size()+1);
		newVerts->add(centre);
		newVerts->add( centre + currentNormal * normalLength );

		colours->add(vec4(abs(currentNormal),1));
		colours->add(vec4(abs(currentNormal),1));
	}

	gfx->setIndexBuffer( indices );
	gfx->setVertexBuffer( newVerts );



	//delete newVerts;
}




/**PRIVATE METHODS**/






void GfxIndexer::fillNormals()
{
	normals->clear();
	for (int i = 0; i < vertices->size(); i++)
	{
		normals->add(vec3(0,0,0));
	}
}

void GfxIndexer::initBuffers(GfxObject* gfx)
{
	colours = gfx->getColourBuffer();
	normals = gfx->getNormalBuffer();
	oldIndices = gfx->getIndexBuffer();
	vertices = gfx->getVertexBuffer();
	indices->clear();

}

void GfxIndexer::getTri(int i)
{
	tri->clear();
	tri->add( oldIndices->get(i) );
	tri->add( oldIndices->get(i+1) );
	tri->add( oldIndices->get(i+2) );
}

void GfxIndexer::initProvokingList()
{
	provokingList->clear();
	for(int i=0; i<vertices->size(); i++)
	{
		provokingList->add(false);
	}
}

bool GfxIndexer::isProvoking(int i)
{
	return provokingList->get(i);
}

void GfxIndexer::setIsProvoking(int i)
{
	provokingList->set(i,true);
}

void GfxIndexer::calculateCurrentNormal()
{
	vec3 a = vertices->get( tri->get(0) );
	vec3 b = vertices->get( tri->get(1) );
	vec3 c = vertices->get( tri->get(2) );
	currentNormal = Utils::calculateNormal( a, b, c );
}

void GfxIndexer::addNewProvokingVertex()
{
	indices->add( vertices->size() );

	vec3 vert = vertices->get( tri->get(0) );
	vertices->add( vert );

	normals->add( currentNormal );
}

void GfxIndexer::useExistingAsProvoking()
{
	normalAddedAt = -1;

	for (int j = 0; j < 3; j++)
	{
		currentIndex = tri->get(j);

		if ( ! isProvoking( currentIndex ) )
		{
			normals->set(currentIndex, currentNormal);
			normalAddedAt = j;
			setIsProvoking( currentIndex );
			j=3; //break
		}
	}
}



void GfxIndexer::addRemainingIndices()
{
	if (normalAddedAt == -1)
	{
		indices->add( tri->get(1) );
		indices->add( tri->get(2) );
	}
	else
	{
		for ( int i = normalAddedAt+1; i < normalAddedAt+4; i++ )
		{
			int j = i % 3;
			indices->add( tri->get(j) );
		}
	}
}

vec3 GfxIndexer::getTriCentre()
{
	List<vec3> verts;
	verts.add(vertices->get(tri->get(0)));
	verts.add(vertices->get(tri->get(1)));
	verts.add(vertices->get(tri->get(2)));

	return Utils::getCentre(verts.getVector());

}
