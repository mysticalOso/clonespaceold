/*
 * DynamicTexture.h
 *
 *  Created on: 1 Nov 2014
 *      Author: mysticalOso
 */

#ifndef SRC_OSOGINE_GFX_FLOATTEXTURE_H_
#define SRC_OSOGINE_GFX_FLOATTEXTURE_H_

#include <GL/glew.h>

class FloatTexture {
public:
	FloatTexture();
	virtual ~FloatTexture();
	int getSize(){return 128;}
	void makeRandom();
	void clear();

	float floats[128][128][3];
	int id;

};

#endif /* SRC_OSOGINE_GFX_FLOATTEXTURE_H_ */
