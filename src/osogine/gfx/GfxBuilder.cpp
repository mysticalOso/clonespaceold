/*
 * GfxBuilder.cpp
 *
 *  Created on: 29 Sep 2014
 *      Author: Adam Nasralla
 */

#include <osogine/gfx/GfxBuilder.h>
#include <osogine/gfx/GfxIndexer.h>
#include <osogine/gfx/GfxObject.h>
#include <osogine/gfx/MeshGenerator2d.h>
#include <osogine/utils/Dice.h> // TODO remove dice
#include <osogine/utils/Polygon.h>
#include <osogine/utils/Printer.h>
#include <osogine/utils/Utils.h>

using namespace std;

GfxBuilder::GfxBuilder()
{
	meshGen = new MeshGenerator2d();
	indexer = new GfxIndexer();
}

GfxBuilder::~GfxBuilder()
{
}

GfxObject* GfxBuilder::buildCube(float size)
{
	return buildCuboid(size, size, size);
}

GfxObject* GfxBuilder::buildTriMesh(List<vec2>* outline, float segmentArea,
		vec3 colour1, vec3 colour2, bool isRaised, bool isRidged )
{
	meshGen->buildMesh(outline->getVector(), Utils::toString(segmentArea), colour1,colour2, isRaised, isRidged);
	////delete outline;
	indexer->setupForStandardShading(meshGen->gfx);
	return meshGen->gfx;
}

GfxObject* GfxBuilder::buildTriMesh(List<vec2>* outline, float segmentArea,
	 bool isRaised, bool isRidged )
{
	meshGen->buildMesh(outline->getVector(), Utils::toString(segmentArea),
			vec3(1,1,1) , vec3(1,1,1), isRaised, isRidged);
	////delete outline;
	indexer->setupForStandardShading(meshGen->gfx);
	return meshGen->gfx;
}

GfxObject* GfxBuilder::buildCuboid(float width, float height, float depth)
{
//	vector<vec3> p;
//
//	float halfWidth = width * 0.5;
//	float halfHeight = height * -0.5;
//	float halfDepth = depth * -0.5;
//
//	p.push_back( vec3(  -halfWidth, -halfHeight, halfDepth ) );
//	p.push_back( vec3(  -halfWidth, halfHeight, halfDepth ) );
//	p.push_back( vec3(  halfWidth, halfHeight, halfDepth ) );
//	p.push_back( vec3(  halfWidth, -halfHeight, halfDepth ) );
//	p.push_back( vec3(  -halfWidth, -halfHeight, -halfDepth ) );
//	p.push_back( vec3(  -halfWidth, halfHeight, -halfDepth ) );
//	p.push_back( vec3(  halfWidth, halfHeight, -halfDepth ) );
//	p.push_back( vec3(  halfWidth, -halfHeight, -halfDepth ) );
//
	GfxObject* gfx = new GfxObject();
//
//	gfx->addTriangle(p[0],p[1],p[2]);
//	gfx->addTriangle(p[0],p[2],p[3]);
//	gfx->addTriangle(p[0],p[4],p[5]);
//	gfx->addTriangle(p[0],p[5],p[1]);
//	gfx->addTriangle(p[0],p[4],p[7]);
//	gfx->addTriangle(p[0],p[7],p[3]);
//	gfx->addTriangle(p[6],p[5],p[4]);
//	gfx->addTriangle(p[6],p[4],p[7]);
//	gfx->addTriangle(p[6],p[5],p[1]);
//	gfx->addTriangle(p[6],p[1],p[2]);
//	gfx->addTriangle(p[6],p[7],p[3]);
//	gfx->addTriangle(p[6],p[3],p[2]);

	float g_vertex_buffer_data[] = {
			-1.0f,-1.0f,-1.0f,
			-1.0f,-1.0f, 1.0f,
			-1.0f, 1.0f, 1.0f,
			 1.0f, 1.0f,-1.0f,
			-1.0f,-1.0f,-1.0f,
			-1.0f, 1.0f,-1.0f,
			 1.0f,-1.0f, 1.0f,
			-1.0f,-1.0f,-1.0f,
			 1.0f,-1.0f,-1.0f,
			 1.0f, 1.0f,-1.0f,
			 1.0f,-1.0f,-1.0f,
			-1.0f,-1.0f,-1.0f,
			-1.0f,-1.0f,-1.0f,
			-1.0f, 1.0f, 1.0f,
			-1.0f, 1.0f,-1.0f,
			 1.0f,-1.0f, 1.0f,
			-1.0f,-1.0f, 1.0f,
			-1.0f,-1.0f,-1.0f,
			-1.0f, 1.0f, 1.0f,
			-1.0f,-1.0f, 1.0f,
			 1.0f,-1.0f, 1.0f,
			 1.0f, 1.0f, 1.0f,
			 1.0f,-1.0f,-1.0f,
			 1.0f, 1.0f,-1.0f,
			 1.0f,-1.0f,-1.0f,
			 1.0f, 1.0f, 1.0f,
			 1.0f,-1.0f, 1.0f,
			 1.0f, 1.0f, 1.0f,
			 1.0f, 1.0f,-1.0f,
			-1.0f, 1.0f,-1.0f,
			 1.0f, 1.0f, 1.0f,
			-1.0f, 1.0f,-1.0f,
			-1.0f, 1.0f, 1.0f,
			 1.0f, 1.0f, 1.0f,
			-1.0f, 1.0f, 1.0f,
			 1.0f,-1.0f, 1.0f
		};

	float g_uv_buffer_data[] = {
	    0.000059f, 1.0f-0.000004f,
	    0.000103f, 1.0f-0.336048f,
	    0.335973f, 1.0f-0.335903f,
	    1.000023f, 1.0f-0.000013f,
	    0.667979f, 1.0f-0.335851f,
	    0.999958f, 1.0f-0.336064f,
	    0.667979f, 1.0f-0.335851f,
	    0.336024f, 1.0f-0.671877f,
	    0.667969f, 1.0f-0.671889f,
	    1.000023f, 1.0f-0.000013f,
	    0.668104f, 1.0f-0.000013f,
	    0.667979f, 1.0f-0.335851f,
	    0.000059f, 1.0f-0.000004f,
	    0.335973f, 1.0f-0.335903f,
	    0.336098f, 1.0f-0.000071f,
	    0.667979f, 1.0f-0.335851f,
	    0.335973f, 1.0f-0.335903f,
	    0.336024f, 1.0f-0.671877f,
	    1.000004f, 1.0f-0.671847f,
	    0.999958f, 1.0f-0.336064f,
	    0.667979f, 1.0f-0.335851f,
	    0.668104f, 1.0f-0.000013f,
	    0.335973f, 1.0f-0.335903f,
	    0.667979f, 1.0f-0.335851f,
	    0.335973f, 1.0f-0.335903f,
	    0.668104f, 1.0f-0.000013f,
	    0.336098f, 1.0f-0.000071f,
	    0.000103f, 1.0f-0.336048f,
	    0.000004f, 1.0f-0.671870f,
	    0.336024f, 1.0f-0.671877f,
	    0.000103f, 1.0f-0.336048f,
	    0.336024f, 1.0f-0.671877f,
	    0.335973f, 1.0f-0.335903f,
	    0.667969f, 1.0f-0.671889f,
	    1.000004f, 1.0f-0.671847f,
	    0.667979f, 1.0f-0.335851f
	};

	for ( int i = 0; i < 18; i++ )
	{
		//gfx->addUV( g_uv_buffer_data[i], g_uv_buffer_data[i+1] );
		gfx->addUV( 0,0 );
		gfx->addUV( 0,1 );
		gfx->addUV( 1,0 );
		gfx->addUV( 1,0 );
		gfx->addUV( 0,1 );
		gfx->addUV( 1,1 );
	}

	for ( int i = 0; i < 108; i+=3 )
	{
		gfx->addVertex( g_vertex_buffer_data[i], g_vertex_buffer_data[i+1], g_vertex_buffer_data[i+2] );
	}

	for ( int i = 0; i < 36; i++ )
	{
		//gfx->addColour(1,1,1,1);
		gfx->addColour( 1, 1, 1, 1 );
	}

	for ( int i = 0; i < 36; i++ )
	{
		//gfx->addColour(1,1,1,1);
		gfx->addIndex( i );
	}

	gfx->calculateNormals( false );

	return gfx;


}

GfxObject* GfxBuilder::buildSegmentRectangle(float width, float height,
		int noSegments, vec4 colour1, vec4 colour2)
{
	GfxObject* gfx = new GfxObject();

	float width2 = width*0.5;
	float height2 = height*0.5;
	float segHeight = height/noSegments;

	for (int i=0; i<noSegments; i++)
	{
		gfx->addVertex( -width2 , segHeight*i - height2, 0);
		gfx->addVertex( width2 , segHeight*i - height2, 0);

		float ratio = (float)i / (float)(noSegments-1);
		vec4 c = Utils::interpolate(colour1, colour2, ratio);

		//Printer::print("Colour = ",c,"\n");
		//Printer::print("Vertex = ",vec3(width2,segHeight*i-height2,0),"\n");
		//Printer::print("Vertex = ",vec3(width2,segHeight*i-height2,0),"\n");
		gfx->addColour(c);
		gfx->addColour(c);
	}

	gfx->fillTriangles();

	return gfx;
}

GfxObject* GfxBuilder::buildQuad(float width, float height, vec4 colour1)
{
	float width2 = width*0.5;
	float height2 = height*0.5;

	GfxObject* gfx = new GfxObject();

	gfx->addVertex(-width2,-height2,0);
	gfx->addVertex(-width2,height2,0);
	gfx->addVertex(width2,height2,0);
	gfx->addVertex(width2,-height2,0);

	for(int i = 0; i < 4; i++)
		gfx->addColour(colour1);

	gfx->addUV( 0,1 );
	gfx->addUV( 0,0 );
	gfx->addUV( 1,0 );
	gfx->addUV( 1,1 );

	gfx->addTriangle(0,1,2);
	gfx->addTriangle(0,2,3);

	gfx->calculateNormals( false );

	return gfx;
}

void GfxBuilder::addQuad(GfxObject* gfx, vec2 bottomLeft, vec2 topRight)
{
	addQuad(gfx, bottomLeft, topRight, vec2(0,0), vec2(1,1));
}


GfxObject* GfxBuilder::buildPolygon(Polygon* outline, vec4 colour)
{
	GfxObject* gfx = new GfxObject();

	for (int i = 0 ; i < outline->size(); i++)
	{
		gfx->addVertex( vec3(outline->get(i),0) );
		gfx->addColour( colour );
		gfx->addIndex( i );
	}
	return gfx;
}

GfxObject* GfxBuilder::buildLinePolygon(Polygon* outline, vec4 colour)
{
	GfxObject* gfx = new GfxObject();
	float noPoints = outline->size();

	outline->updateTransformedOutline();

	for (int i = 0 ; i < noPoints; i++)
	{
		gfx->addVertex( vec3(outline->getTransformed(i),0) );
		gfx->addColour( colour );
		gfx->addIndex( i );
		if (!outline->getIsLine() || i<noPoints-1)
		{
			gfx->addIndex( (i+1) % outline->size() );
		}
	}
	return gfx;
}

void GfxBuilder::addQuad(GfxObject* gfx, vec2 bottomLeft, vec2 topRight,
		vec2 topLeftUV, vec2 bottomRight)
{
	int offset = gfx->getNoVertices();

	gfx->addVertex(bottomLeft.x ,bottomLeft.y ,0);
	gfx->addVertex(bottomLeft.x ,topRight.y ,0);
	gfx->addVertex(topRight.x , topRight.y ,0);
	gfx->addVertex(topRight.x , bottomLeft.y,0);

	gfx->addUV( topLeftUV.x,topLeftUV.y );
	gfx->addUV( topLeftUV.x,bottomRight.y );
	gfx->addUV( bottomRight.x,bottomRight.y );
	gfx->addUV( bottomRight.x,topLeftUV.y );

	gfx->addTriangle(offset, offset+1, offset+2);
	gfx->addTriangle(offset ,offset+2, offset+3);

	gfx->calculateNormals( false );
}

GfxObject* GfxBuilder::buildQuad(vec2 p1, vec2 p2, vec2 p3, vec2 p4, vec4 c)
{
	GfxObject* gfx = new GfxObject();
	addQuad(gfx,p1,p2,p3,p4,c);
	return gfx;
}

void GfxBuilder::addQuad(GfxObject* gfx, vec2 p1, vec2 p2, vec2 p3, vec2 p4,
		vec4 c)
{
	int offset = gfx->getNoVertices();

	gfx->addVertex(p1.x , p1.y ,0);
	gfx->addVertex(p2.x , p2.y ,0);
	gfx->addVertex(p3.x , p3.y ,0);
	gfx->addVertex(p4.x , p4.y ,0);
	gfx->addColour(c, 4);

	gfx->addTriangle(offset, offset+1, offset+2);
	gfx->addTriangle(offset ,offset+2, offset+3);
}
