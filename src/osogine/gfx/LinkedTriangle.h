/*
 * Triangle.h
 *
 *  Created on: 20 Oct 2014
 *      Author: mysticalOso
 */

#ifndef TRIANGLE_H_
#define TRIANGLE_H_

#include <osogine/utils/List.h>

class LinkedVertex;
class GfxObject;

class LinkedTriangle {
public:
	LinkedTriangle(LinkedVertex* v1, LinkedVertex* v2, LinkedVertex* v3);
	virtual ~LinkedTriangle();

	LinkedVertex* getVertex(int i){ return verts->get(i); }


	bool isMatch(LinkedTriangle* t);
	bool isMatch(LinkedVertex* v1, LinkedVertex* v2, LinkedVertex* v3);

	void addToGfx( GfxObject* g );

private:
	List< LinkedVertex* >* verts;
};

#endif /* TRIANGLE_H_ */
