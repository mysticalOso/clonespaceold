/*
 * Triangle.h
 *
 *  Created on: 8 Jan 2015
 *      Author: mysticalOso
 */

#ifndef SRC_OSOGINE_GFX_TRIANGLE_H_
#define SRC_OSOGINE_GFX_TRIANGLE_H_

#include <osogine/utils/List.h>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>

class LinkedTriangle;

class Triangle {
public:
	Triangle(vec3 p1, vec3 p2, vec3 p3, vec4 c);
	Triangle(LinkedTriangle* linkedTri);
	bool operator==(const Triangle&){ return false; }
	virtual ~Triangle();

	vec3 getVertex(int i){ return verts.get(i); }
	vec3 getNormal(){ return normal; }
	vec4 getColour(){ return colour; }

private:

	void updateNormal();
	void init(vec3 p1, vec3 p2, vec3 p3, vec4 c);
	List<vec3> verts;
	vec4 colour;
	vec3 normal;

};

#endif /* SRC_OSOGINE_GFX_TRIANGLE_H_ */
