/*
 * GfxObject.h
 *
 *  Created on: 9 Sep 2014
 *      Author: mysticalOso
 */

#ifndef GFXOBJECT_H_
#define GFXOBJECT_H_

#include <glm/fwd.hpp>
#include <glm/vec4.hpp>
#include <glm/vec3.hpp>
#include <glm/vec2.hpp>
#include <glm/geometric.hpp>
#include <osogine/gfx/GfxBuffer.h>
#include <osogine/utils/List.h>
#include <cstdio>

class Transform;

using namespace glm;

class GfxObject {
public:
	GfxObject();
	~GfxObject();

	void add( GfxObject* newGfx );

	void incrementIndices(int increment);

	void addIndex( int i ){ indices->add(i); }
	void setIndex( int index, int value ){ indices->set(index,value); }

	uint getIndex( int i ){ return indices->get(i); }

	void addVertex( vec3 vertex ){  vertices->add( vertex ); }
	void addVertex( float x, float y, float z ) { addVertex( vec3( x, y, z ) ); }
	void setVertex( int index, vec3 vertex ) { vertices->set(index, vertex); }

	void addNormal( vec3 normal ) { normals->add( normalize( normal ) ); }
	void addNormal( float x, float y, float z ) { addNormal( vec3( x, y, z ) ); }
	void setNormal( int index, vec3 normal ) { normals->set(index, normal); }
	vec3 getNormal( int index ){ return normals->get(index); }

	void addUV( vec2 uv ) { uvs->add( vec2(uv.x, 1-uv.y) ); }
	void addUV( float u, float v ){ addUV( vec2( u, v ) ); }
	void setUV( int index, vec2 uv ){ uvs->set(index, uv); }

	void addColour( vec4 colour, int n ) { for(int i=0; i<n; i++){colours->add( colour ); }}
	void addColour( vec4 colour ) { colours->add( colour ); }
	void addColour( float r, float g, float b, float a ) { addColour( vec4( r, g, b, a ) ); }
	void setColour( int index, vec4 colour ){ colours->set(index, colour); }

	vec4 getColour( int index ){ return colours->get(index); }

	void setColour( vec4 colour );
	void setAlpha( float alpha );

	float getAlpha();

	void addOutline( vec2 point ) { outline->add( point ); }
	void addOutline( float x, float y ) { addOutline( vec2 ( x, y ) ); }
	void setOutline( int index, vec2 point ) { outline->set(index, point); }

	void addTriangle( vec3 a, vec3 b, vec3 c );
	void addTriangle( vec3 a, vec3 b, vec3 c, vec4 colour );
	void setTriangle( int i, vec3 a, vec3 b, vec3 c, vec4 colour );
	void addTriangle( int i, int j, int k );
	void addTriangle(float x, float y, float size, float ratio, float angle,
			vec4 colour);
	void addQuad(float x, float y, float width, float height, vec4 c);
	void addQuad(vec2 a, vec2 b, vec2 c, vec2 d, vec4 colour);


	void addLine( vec3 a, vec3 b, vec4 colour);
	void fillTriangles();
	void fillLines(bool isLoop);

	void reColour(vec4 colour1, vec4 colour2);

	uint getNoVertices() { return vertices->size(); }

	uint getNoIndices() { return indices->size(); }

	void calculateNormals( bool isIndexed );

	vec3 getVertex( uint n ) { return vertices->get( n ); }

	GfxBuffer< vec3 >* getVertexBuffer(){ return vertices; }
	GfxBuffer< vec3 >* getNormalBuffer(){ return normals; }
	GfxBuffer< vec4 >* getColourBuffer(){ return colours; }
	GfxBuffer< vec2 >* getUVBuffer(){ return uvs; }
	GfxBuffer< short >* getIndexBuffer(){ return indices; }
	List< vec2 >* getOutline(){ return outline; }

	void setVertexBuffer( List< vec3 >* buffer ){ vertices->setVector( buffer->getVector() ); }
	void setNormalBuffer( List< vec3 >* buffer ){ normals->setVector( buffer->getVector() ); }
	void setColourBuffer( List< vec4 >* buffer ){ colours->setVector( buffer->getVector() ); }
	void setUVBuffer( List< vec2 >* buffer ){ uvs->setVector( buffer->getVector() ); }
	void setIndexBuffer( List< short >* buffer ){ indices->setVector( buffer->getVector() ); }
	void setOutline( List< vec2 >* list ){ outline->setVector( list->getVector() ); }

	void clearAll();

	void setDepthEnabled(bool b){ depthEnabled = b; }

	bool getDepthEnabled(){ return depthEnabled; }

	void setDepthMask(bool b){ depthMask = b; }

	bool getDepthMask(){ return depthMask; }
	void print();

	void applyTransform(Transform* transform);

	void applyLocalTransform(Transform* transform);

	void applyInverseTransform(Transform* transform);

	GfxObject* clone();

private:

    GfxBuffer< vec3 >* vertices;
	GfxBuffer< vec3 >* normals;
	GfxBuffer< vec2 >* uvs;
	GfxBuffer< short >* indices;
	GfxBuffer< vec4 >* colours;
	List< vec2 >* outline;

	bool depthEnabled;
	bool depthMask;


};

#endif /* GFXOBJECT_H_ */
