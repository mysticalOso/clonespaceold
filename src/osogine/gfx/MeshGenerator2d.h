/*
 * TriangleInterface.h
 *
 *  Created on: 23 Oct 2013
 *      Author: Adam Nasralla
 */

#ifndef MESHGENERATOR2D_H_
#define MESHGENERATOR2D_H_

extern "C" {
	#include <osogine/gfx/TriangleInterface.h>
}

#include <vector>
#include <glm/fwd.hpp>
#include <string>

class GfxObject;

class MeshGenerator2d
{
public:
	MeshGenerator2d();
	~MeshGenerator2d();
	void buildMesh(std::vector<glm::vec2> points, std::string area, glm::vec3 colour1, glm::vec3 colour2, bool is3d, bool useRows );
	GfxObject *gfx;

private:
	void addTriangle(int triangleIndex, glm::vec3 colour1, glm::vec3 colour2);
	void addTriangleFlat(int triangleIndex, glm::vec3 colour1, glm::vec3 colour2);
	void calculateHeight();
	void calculateHeightByRows();
	void calculateHeightByDistance();
	void callTriangulate(std::vector<glm::vec2> points, std::string area);
	void init();
	void setGfxVertices();
	void setGfxOutline();
	void initArrays();
	void freeMemory();
	int indexCount;
	int *visited;
	int *added;
	int *explored;
	int *row;
	float *height;

	std::vector<glm::vec2> outline;

	int totalAdded;
	int totalRows;

	struct triangulateio in, tri;
	bool useRows;
	bool is3D;

};

#endif /* MESHGENERATOR2D_H_ */
