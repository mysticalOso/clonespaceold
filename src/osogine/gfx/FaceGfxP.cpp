/*
 * FlatGfxP.cpp
 *
 *  Created on: 15 Oct 2014
 *      Author: Adam Nasralla
 */

#include <vector>
#include <glm/mat4x4.hpp>
#include <osogine/game/Entity.h>
#include <osogine/game/TextureEntity.h>
#include <osogine/gfx/FaceGfxP.h>
#include <osogine/gfx/GfxObject.h>


FaceGfxP::FaceGfxP()
{
}

FaceGfxP::~FaceGfxP()
{
}

void FaceGfxP::init()
{
	addUniform( "MVP" );
	addUniform( "M" );
	addUniform( "lightColour" );
	addUniform( "lightDirection" );
	addUniform( "numLights" );
	addUniform( "alpha" );
}

void FaceGfxP::execute()
{
	TextureEntity* tEntity = (TextureEntity*) entity;

	if (gfx->getDepthEnabled())
	{
		clearDepthBit();
		toggleDepthTest(true);
	}
	else
	{
		toggleDepthTest(false);
	}

	setUniformValue( "MVP", getMVP() );
	setUniformValue( "M", entity->getMatrix() );

	vector<vec3> colours;

	colours.push_back(vec3(1.0,1.0,1.0));
	colours.push_back(vec3(0.1 ,0 , 0.2));
	colours.push_back(vec3(0,0.1, 0.2 ));

	vector<vec3> directions;
	directions.push_back(vec3(-0.8,0.8,0.5));
	directions.push_back(vec3(1,0,0));
	directions.push_back(vec3(0,-1,0));

	int numLights = 3;

	setUniformValue( "lightColour", colours, numLights );
	setUniformValue( "lightDirection", directions, numLights );
	setUniformValue( "numLights", numLights );
	setUniformValue( "alpha", tEntity->getTextureAlpha());

	activateBuffer( gfx->getVertexBuffer(), 0 );
	activateBuffer( gfx->getNormalBuffer(), 1 );
	activateBuffer( gfx->getColourBuffer(), 2 );

	activateBuffer( gfx->getIndexBuffer(), 0 );

	draw();

	deactivateBuffer( 0 );
	deactivateBuffer( 1 );
	deactivateBuffer( 2 );
}
