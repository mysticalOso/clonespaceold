/*
 * DynamicTexture.cpp
 *
 *  Created on: 1 Nov 2014
 *      Author: mysticalOso
 */

#include <osogine/gfx/FloatTexture.h>
#include <osogine/utils/Dice.h>
#include <stdio.h>
FloatTexture::FloatTexture()
{
	id = 0;
	clear();
}

FloatTexture::~FloatTexture()
{

}

void FloatTexture::makeRandom()
{
	for (int i = 0; i < getSize(); i++)
	{
		for (int j = 0; j < getSize(); j++)
		{
			for (int k = 0; k < 3; k++)
						{
				floats[i][j][k] = (float) Dice::roll(0.0,1.0);
			}
		}
	}

}

void FloatTexture::clear()
{
	for (int i = 0; i < getSize(); i++)
	{
		for (int j = 0; j < getSize(); j++)
		{
			for (int k = 0; k < 3; k++)
			{
				floats[i][j][k] = 0;
			}
		}
	}
}
