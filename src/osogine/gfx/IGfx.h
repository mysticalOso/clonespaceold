/*
 * GfxInterface.h
 *
 *  Created on: 12 Aug 2014
 *      Author: mysticalOso
 */

#ifndef GFXINTERFACE_H_
#define GFXINTERFACE_H_

#include <glm/fwd.hpp>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <glm/mat4x4.hpp>

#include <string>
#include <vector>

using namespace glm;
using namespace std;

class IGfx {
public:
	IGfx(){}

	virtual ~IGfx(){}

	virtual void init(){}

	virtual void toggleDepthTest(bool isOn){}

	virtual void clearBufferBits(){}

	virtual void clearDepthBit(){}

	virtual void clearColour(float r, float g, float b){}

	virtual uint loadShaders(string vertex_file_path, string fragment_file_path){ return 0; }

	virtual uint declareUniform(uint programId, string name){ return 0; }

	virtual void setUniformValue(uint uniformId, int value){}
	virtual void setUniformValue(uint uniformId, float value){}
	virtual void setUniformValue(uint uniformId, vec2 value){}
	virtual void setUniformValue(uint uniformId, vec3 value){}
	virtual void setUniformValue(uint uniformId, vec4 value){}
	virtual void setUniformValue(uint uniformId, mat4 value){}
	virtual void setUniformValue(uint uniformId, vector<vec3> values, uint count){}
	virtual void setUniformValue(uint uniformId, vector<vec2> values, uint count){}
	virtual void setUniformValue(uint uniformId, vector<float> values, uint count){}
	virtual void setUniformValue(uint uniformId, vector<int> values, uint count){}
	virtual void uploadBuffer(uint* bufferId, uint size, void* buffer, bool isDynamic, bool isElementArray){}

	virtual void updateBuffer(uint* bufferId, uint size, void* buffer, bool isDynamic, bool isElementArray){}

	virtual void deleteBuffer(uint* id){}

	virtual void activateBuffer(uint id, uint location, uint noDimensions, bool isElementArray){}

	virtual void disableAttribArray(uint location){}

	virtual void useProgram(uint id){}

	virtual void drawTriangles( uint n, bool isIndexed ){}

	virtual uint loadTexture( string filename, int size ) { return 0; }

	virtual uint loadDynamicTexture(const void *pixels, int size){ return 0; }

	virtual uint loadFloatTexture(const void *floats, int size){ return 0; }

	virtual void updateTexture( uint texture, const void *pixels, int size){}

	virtual void updateFloatTexture(uint texture, const void *floats, int size){}

	virtual void activateTexture( uint texture, uint location  ) {}

	virtual void drawLines(uint n){}

	virtual void enableAdditiveBlend(){}
	virtual void enableAlphaBlend(){}
	virtual void toggleBlend(bool b){}
	virtual void toggleDepthMask(bool b){}

};

#endif /* GFXINTERFACE_H_ */
