/*
 * FlatGfxP.h
 *
 *  Created on: 15 Oct 2014
 *      Author: Adam Nasralla
 */

#ifndef FLATGFXP_H_
#define FLATGFXP_H_

#include <osogine/gfx/GfxProgram.h>

class FlatGfxP: public GfxProgram
{
public:
	FlatGfxP();
	~FlatGfxP();

	void init();
	void execute();
};

#endif /* FLATGFXP_H_ */
