/*
 * StandardGfxProgram.h
 *
 *  Created on: 4 Oct 2014
 *      Author: Adam Nasralla
 */

#ifndef STANDARDGFXP_H_
#define STANDARDGFXP_H_

#include <osogine/gfx/GfxProgram.h>

class StandardGfxP: public GfxProgram
{
public:
	StandardGfxP();
	~StandardGfxP();

	void init();
	void execute();
};

#endif /* STANDARDGFXP_H_ */
