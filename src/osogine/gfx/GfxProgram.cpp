/*
 * GfxProgram.cpp
 *
 *  Created on: 9 Sep 2014
 *      Author: mysticalOso
 */

#include <osogine/game/Camera.h>
#include <osogine/game/Entity.h>
#include <osogine/game/Light.h>
#include <osogine/gfx/FloatTexture.h>
#include <osogine/gfx/GfxObject.h>
#include <osogine/gfx/GfxProgram.h>
#include <osogine/gfx/IGfx.h>

//TODO rename to BaseGfxProgram
GfxProgram::GfxProgram()
{
	g = 0;
	id = 0;
	entity = 0;
	gfx = 0;
	camera = 0;
	lights = new EntityList();
}

GfxProgram::~GfxProgram()
{
}

void GfxProgram::load(IGfx* g1, string vertexShader, string fragmentShader)
{
	g = g1;
	id = g->loadShaders(vertexShader,fragmentShader);
	init();
}

void GfxProgram::render(Entity* entity)
{
	this->entity = entity;
	if (entity->getIsVisible())
	{
		camera = entity->getCamera();
		gfx = entity->getGfx();
		useProgram();
		execute();
	}
}

void GfxProgram::addUniform(string name)
{
	uniforms[name] = g->declareUniform(id,name);
}





mat4 GfxProgram::getMVP()
{
	mat4 MVP;
	if (entity->getDisableCamera()) MVP = camera->getProjectionMatrix() * entity->getMatrix();
	else MVP = camera->getViewProjectionMatrix() * entity->getMatrix();
	return MVP;
}

void GfxProgram::toggleDepthTest(bool b)
{
	g->toggleDepthTest(b);
}

void GfxProgram::deactivateBuffer(uint location)
{
	g->disableAttribArray(location);
}

void GfxProgram::activateTexture( uint texture, uint location  )
{
	g->activateTexture( texture, location );
}

void GfxProgram::addLight(Light* l)
{
	lights->add(l);
}

void GfxProgram::clearScreen()
{
	g->clearBufferBits();
}

void GfxProgram::addFloatTexture(FloatTexture* texture)
{
	texture->id = g->loadFloatTexture(texture->floats, texture->getSize());
}

void GfxProgram::updateFloatTexture(FloatTexture* texture)
{
	g->updateFloatTexture(texture->id, texture->floats, texture->getSize());
}

mat4 GfxProgram::getVP()
{
	return camera->getViewProjectionMatrix();
}

void GfxProgram::clearDepthBit()
{
	g->clearDepthBit();
}

void GfxProgram::useProgram()
{
	g->useProgram(id);
}

void GfxProgram::drawTriangles(uint noVertices)
{
	g->drawTriangles( noVertices, false );
}

void GfxProgram::draw()
{
	g->drawTriangles( gfx->getNoIndices(), true );
}


void GfxProgram::drawLines()
{
	g->drawLines( gfx->getNoIndices() );
}




//template <class T>
//void GfxProgram::setUniformValue(string name, T value)
//{
//	g->setUniformValue( uniforms[name], value );
//}

void GfxProgram::setUniformValue(string name, int value)
{
	g->setUniformValue( uniforms[name], value );
}

void GfxProgram::setUniformValue(string name, float value)
{
	g->setUniformValue( uniforms[name], value );
}

void GfxProgram::setUniformValue(string name, vec2 value)
{
	g->setUniformValue( uniforms[name], value );
}

void GfxProgram::setUniformValue(string name, vec3 value)
{
	g->setUniformValue( uniforms[name], value );
}

void GfxProgram::setUniformValue(string name, vec4 value)
{
	g->setUniformValue( uniforms[name], value );
}

void GfxProgram::setUniformValue(string name, mat4 value)
{
	g->setUniformValue( uniforms[name], value );
}

void GfxProgram::setUniformValue(string name, vector<int> value, uint count)
{
	g->setUniformValue( uniforms[name], value, count );
}

void GfxProgram::setUniformValue(string name, vector<float> value, uint count)
{
	g->setUniformValue( uniforms[name], value, count );
}


void GfxProgram::setUniformValue(string name, vector<vec3> value, uint count)
{
	g->setUniformValue( uniforms[name], value, count );
}

void GfxProgram::setUniformValue(string name, vector<vec2> value, uint count)
{
	g->setUniformValue( uniforms[name], value, count );
}


void GfxProgram::activateBuffer(GfxBuffer<short>* buffer, uint location)
{
	if ( !buffer->getIsUploaded() )
	{
		buffer->setIGfx(g);
		g->uploadBuffer( buffer->getIdPointer(), buffer->getByteSize(), &buffer->getVector()[0], false, true );
		buffer->setIsUploaded( true );
	}
	else if ( buffer->getHasChanged() )
	{
		g->updateBuffer( buffer->getIdPointer(), buffer->getByteSize(), &buffer->getVector()[0], true, true );
		buffer->setHasChanged( false );
	}
	g->activateBuffer(buffer->getId(), location, 1, true);
}

void GfxProgram::activateBuffer(GfxBuffer<float>* buffer, uint location)
{
	if ( !buffer->getIsUploaded() )
	{
		buffer->setIGfx(g);
		g->uploadBuffer( buffer->getIdPointer(), buffer->getByteSize(), &buffer->getVector()[0], false, false );
		buffer->setIsUploaded( true );
	}
	else if ( buffer->getHasChanged() )
	{
		g->updateBuffer( buffer->getIdPointer(), buffer->getByteSize(), &buffer->getVector()[0], true, false );
		buffer->setHasChanged( false );
	}
	g->activateBuffer(buffer->getId(), location, 1, false);
}


void GfxProgram::activateBuffer(GfxBuffer<vec2>* buffer, uint location)
{
	if ( !buffer->getIsUploaded() )
	{
		buffer->setIGfx(g);
		g->uploadBuffer( buffer->getIdPointer(), buffer->getByteSize(), &buffer->getVector()[0], false, false );
		buffer->setIsUploaded( true );
	}
	else if ( buffer->getHasChanged() )
	{
		g->updateBuffer( buffer->getIdPointer(), buffer->getByteSize(), &buffer->getVector()[0], true, false );
		buffer->setHasChanged( false );
	}
	g->activateBuffer(buffer->getId(), location, 2, false);
}

void GfxProgram::activateBuffer(GfxBuffer<vec3>* buffer, uint location)
{


	if ( !buffer->getIsUploaded() )
	{
		buffer->setIGfx(g);
		g->uploadBuffer( buffer->getIdPointer(), buffer->getByteSize(), &buffer->getVector()[0], false, false );
		buffer->setIsUploaded( true );
	}
	else if ( buffer->getHasChanged() )
	{
		g->updateBuffer( buffer->getIdPointer(), buffer->getByteSize(), &buffer->getVector()[0], true, false );
		buffer->setHasChanged( false );
	}
	g->activateBuffer( buffer->getId(), location, 3, false );
}

void GfxProgram::activateBuffer(GfxBuffer<vec4>* buffer, uint location)
{



	if ( !buffer->getIsUploaded() )
	{
		buffer->setIGfx(g);
		g->uploadBuffer( buffer->getIdPointer(), buffer->getByteSize(), &buffer->getVector()[0], false, false );
		buffer->setIsUploaded( true );
	}
	else if ( buffer->getHasChanged() )
	{
		g->updateBuffer( buffer->getIdPointer(), buffer->getByteSize(), &buffer->getVector()[0], true, false );
		buffer->setHasChanged( false );
	}
	g->activateBuffer(buffer->getId(), location, 4, false);
}


