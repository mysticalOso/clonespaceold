/*
 * GfxBuilder.h
 *
 *  Created on: 29 Sep 2014
 *      Author: Adam Nasralla
 */

#ifndef GFXBUILDER_H_
#define GFXBUILDER_H_

#include <glm/fwd.hpp>
#include <osogine/utils/List.h>

using namespace glm;

class GfxObject;
class GfxIndexer;
class Polygon;

class MeshGenerator2d;

class GfxBuilder
{
public:
	GfxBuilder();
	~GfxBuilder();

	GfxObject* buildTriMesh( List<vec2>* outline, float segmentArea, vec3 colour1, vec3 colour2, bool isRaised, bool isRidged);
	GfxObject* buildTriMesh( List<vec2>* outline, float segmentArea, bool isRaised, bool isRidged);

	GfxObject* buildCube(float size);
	GfxObject* buildCuboid(float width, float height, float depth);
	GfxObject* buildQuad(float width, float height, vec4 colour);
	void addQuad(GfxObject* gfx, vec2 bottomLeft, vec2 topRight);
	void addQuad(GfxObject* gfx, vec2 bottomLeft, vec2 topRight, vec2 topLeftUV, vec2 bottomRightUV);

	GfxObject* buildQuad(vec2 p1, vec2 p2, vec2 p3, vec2 p4, vec4 c);
	void addQuad(GfxObject* gfx, vec2 p1, vec2 p2, vec2 p3, vec2 p4, vec4 c);

	GfxObject* buildPolygon(Polygon* outline, vec4 colour);

	GfxObject* buildLinePolygon(Polygon* outline, vec4 colour);

	GfxObject* buildSegmentRectangle(float width, float height, int noSegments, vec4 colour1, vec4 colour2);

	GfxIndexer* getIndexer(){ return indexer; }

	MeshGenerator2d* meshGen;
	GfxIndexer* indexer;

};

#endif /* GFXBUILDER_H_ */
