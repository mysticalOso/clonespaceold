/*
 * CloneAnimation.cpp
 *
 *  Created on: 25 Jul 2014
 *      Author: Adam Nasralla
 */

#include <clone/CloneAnimation.h>
#include <clone/CloneModel.h>
#include <clone/KeyFrame.h>


CloneAnimation::CloneAnimation(std::string name1)
{
	name = name1;
	model = 0;
	currentFrame = 0;
}

CloneAnimation::~CloneAnimation()
{
	// TODO Auto-generated destructor stub
}

void CloneAnimation::play()
{
}

void CloneAnimation::addKeyFrame(KeyFrame* key)
{
	int frame = key->start;
	keyMap[frame].push_back(key);
}

bool CloneAnimation::nextFrame()
{
	currentFrame++;

	if (keyMap.count(currentFrame))
	{
		std::vector<KeyFrame*> newKeys = keyMap[currentFrame];
		currentKeys.insert(currentKeys.end(),newKeys.begin(),newKeys.end());
	}

	for (uint i = 0; i < currentKeys.size(); i++)
	{
		KeyFrame* key = currentKeys[i];


		if (currentFrame == key->start && key->changeParent)
		{
			model->changeParent(key->part,key->newParent);
		}

		model->tween(key,currentFrame);
		if (currentFrame==key->finish)
		{
			if (key->next->next==0)
			{
				currentKeys.erase(currentKeys.begin()+i);
				i--;
			}
			else
			{
				currentKeys[i] = key->next;
			}
		}
	}

	if (currentKeys.size() > 0)	return true;
	else
	{
		currentFrame = 0;
		return false;
	}

}
