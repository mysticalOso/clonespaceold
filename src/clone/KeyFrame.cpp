/*
 * KeyFrame.cpp
 *
 *  Created on: 25 Jul 2014
 *      Author: Adam Nasralla
 */

#include <osogine/utils/Utils.h>
#include "KeyFrame.h"

KeyFrame::KeyFrame(std::string part1, int start1, int inter1)
{
	part = part1;
	start = start1;
	inter = inter1;
	qRot = quat();
	trans = vec3(0,0,0);
	scaleV = vec3(1,1,1);
	next = 0;
	finish = 0;
	changeParent = false;
}

KeyFrame::~KeyFrame()
{
	// TODO Auto-generated destructor stub
}

KeyFrame* KeyFrame::setNextRot(float x, float y, float z, int start1, int inter1)
{
	KeyFrame* nextKey = getCopy();
	nextKey->setRotation(x,y,z);
	setNext(nextKey, start1, inter1);
	return nextKey;
}

KeyFrame* KeyFrame::setNextTrans(float x, float y, float z, int start1, int inter1)
{
	KeyFrame* nextKey = getCopy();
	nextKey->setTranslation(x,y,z);
	setNext(nextKey, start1, inter1);
	return nextKey;
}

KeyFrame* KeyFrame::getCopy()
{
	KeyFrame* copyKey = new KeyFrame(part,start,inter);
	copyKey->qRot = qRot;
	copyKey->trans = trans;
	copyKey->scaleV = scaleV;
	copyKey->next = next;
	copyKey->finish = finish;
	return copyKey;
}

void KeyFrame::setNext(KeyFrame* next1, int start1, int inter1)
{
	next1->start = start1;
	next1->inter = inter1;
	setNext(next1);
}

float KeyFrame::calcRatio(int frame)
{
	float ratio =  ((float)(frame-start)) / ((float)(finish-start));
	if (inter==0) ratio = Utils::sinInterpolate(0,1,ratio);
	if (inter==1) ratio = Utils::easeInInterpolate(0,1,ratio);
	if (inter==2) ratio = Utils::easeOutInterpolate(0,1,ratio);
	return ratio;
}
