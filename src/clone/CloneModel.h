/*
 * CloneModel.h
 *
 *  Created on: 22 Jul 2014
 *      Author: Adam Nasralla
 */

#ifndef CLONEMODEL_H_
#define CLONEMODEL_H_

#include <osogine/game/Entity.h>
#include <map>

class KeyFrame;
class CloneAnimation;
class Race;

class CloneModel : public Entity
{
public:
	CloneModel(Entity* parent);
	~CloneModel();
	void setAlpha(float a);
	void update();

	void setShooting(bool isOn){shouldShoot = isOn;}

	void setArmed(bool isOn){shouldArm = isOn;}

	void toggleArmed(){shouldArm = !shouldArm;}

	void changeParent(std::string part, std::string newParent);

	bool noAnims(){ return playingAnims.size() == 0; }

	void addAnim(std::string name, CloneAnimation* anim);
	void playAnim(std::string name);

	void tween(KeyFrame* key, int frame);

	EntityList* getParts();

	bool getIsArmed(){ return armed; }

private:

	map<string, Entity*> parts;
	map<string, CloneAnimation*> anims;
	void initParts();
	void initTransforms();
	void initAnims();

	vector< CloneAnimation* > playingAnims;

	Race* race;


	bool hasShot;
	bool shouldShoot;
	bool shouldArm;
	bool armed;


};

#endif /* CLONEMODEL_H_ */
