/*
 * CloneModel.cpp
 *
 *  Created on: 22 Jul 2014
 *      Author: Adam Nasralla
 */

#include <builders/AnimationBuilder.h>
#include <clone/Clone.h>
#include <clone/CloneAnimation.h>
#include <clone/CloneModel.h>
#include <clone/KeyFrame.h>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <osogine/gfx/GfxObject.h>
#include <osogine/gfx/ObjLoader/ObjLoader.h>
#include <osogine/utils/Transform.h>
#include <races/Race.h>
#include <string>
#include <vector>

using namespace glm;
using namespace std;

CloneModel::CloneModel(Entity* parent) : Entity(parent)
{
	race = ((Clone*)parent)->getRace();
	initParts();
	initTransforms();
	initAnims();
	hasShot = false;
	shouldShoot = false;
	shouldArm = true;
	armed = true;
	setRotationD(0,0,90);
}

CloneModel::~CloneModel()
{

}



void CloneModel::initParts()
{

	vec4 headC,bodyC,gunC,c2;

	std::string headO,foreArmO,upperArmO;


	if (race->isRace("sentient"))
	{
		headC = vec4(1.0f,0.7f,0.4f,1.0f);
		bodyC = vec4(0.5f,0.5f,1.0f,1.0f);
		gunC = vec4(1.0f,1.0f,1.0f,1.0f);
		headO = "head";
		foreArmO = "foreArm";
		upperArmO = "upperArm";
	}

	else
	{
		headC = vec4(1.0f,0.1f,0.1f,1.0f);
		bodyC = vec4(0.8f,0.5f,0.1f,1.0f);
		gunC = vec4(1.0f,1.0f,1.0f,1.0f);
		headO = "xacHead";
		foreArmO = "xacForeArm";
		upperArmO = "xacUpperArm";
	}

	gunC = vec4(1.0f,1.0f,1.0f,1.0f);

	parts["body"] = new Entity(this);

	parts["head"] = new Entity( parts["body"] );
	parts["head"]->setGfx(ObjLoader::getObject(headO));
	parts["head"]->getGfx()->reColour(headC,headC);

	parts["upperArmR"] = new Entity( parts["body"] );
	parts["upperArmR"]->setGfx(ObjLoader::getObject(upperArmO));
	parts["upperArmR"]->getGfx()->reColour(bodyC,bodyC);

	parts["upperArmL"] = new Entity( parts["body"] );
	parts["upperArmL"]->setGfx(ObjLoader::getObject(upperArmO));
	parts["upperArmL"]->getGfx()->reColour(bodyC,bodyC);

	parts["foreArmR"] = new Entity( parts["upperArmR"] );
	parts["foreArmR"]->setGfx(ObjLoader::getObject(foreArmO));
	parts["foreArmR"]->getGfx()->reColour(bodyC,bodyC);

	parts["foreArmL"] = new Entity( parts["upperArmL"] );
	parts["foreArmL"]->setGfx(ObjLoader::getObject(foreArmO));
	parts["foreArmL"]->getGfx()->reColour(bodyC,bodyC);

	parts["gun"] = new Entity( parts["body"] );
	parts["gun"]->setGfx(ObjLoader::getObject("gun"));
	parts["gun"]->getGfx()->reColour(gunC,gunC);


}

void CloneModel::update()
{
	Entity::update();

	for (uint i = 0 ; i < playingAnims.size(); i++)
	{
		if (!playingAnims[i]->nextFrame())
		{
			playingAnims.erase(playingAnims.begin()+i);
			i--;

		}
	}
	if ( noAnims() && !hasShot && shouldShoot && armed) {playAnim("shoot"); hasShot = true;}
	else if ( noAnims() && hasShot) {playAnim("shootRelease"); hasShot = false;}

	if ( noAnims() && !armed && shouldArm) {playAnim("draw"); armed = true;}
	else if (noAnims() && armed && !shouldArm) {playAnim("sheath"); armed = false;}
}

void CloneModel::initTransforms()
{

	parts["body"]->setRotationD(0,0,-20);

	parts["head"]->setPosition(0,0,150);
	parts["head"]->setRotationD(0,0,20);

	parts["upperArmR"]->setPosition(0,-240,0);
	parts["upperArmR"]->setRotationD(41.2,-17.7,-36);

	parts["upperArmL"]->setPosition(0,240,0);
	parts["upperArmL"]->setRotationD(63.4,7,263.9);

	parts["foreArmR"]->setPosition(0,-29.6,-293.6);
	parts["foreArmR"]->setRotationD(40,0,0);

	parts["foreArmL"]->setPosition(0,-29.6,-293.6);
	parts["foreArmL"]->setRotationD(45,0,0);

	parts["gun"]->setPosition(284.5,96.8,-88.7);
	parts["gun"]->setRotationD(-160,-90,0);
	parts["gun"]->setScale(1.2,1.2,1.2);
//
//	if (isEnemy) parts["body"]->setScale(1.4,1.4,1.4);
}

void CloneModel::setAlpha(float a)
{
	parts["head"]->setAlpha(a);
	parts["upperArmR"]->setAlpha(a);
	parts["upperArmL"]->setAlpha(a);
	parts["foreArmR"]->setAlpha(a);
	parts["foreArmL"]->setAlpha(a);
	parts["gun"]->setAlpha(a);
}



void CloneModel::initAnims()
{
	addAnim("shoot", AnimationBuilder::buildShootAnimation());
	addAnim("shootRelease", AnimationBuilder::buildShootReleaseAnimation());
	addAnim("draw", AnimationBuilder::buildDrawAnim());
	addAnim("sheath", AnimationBuilder::buildSheathAnim());
}

void CloneModel::addAnim(std::string name, CloneAnimation* anim)
{
	anim->setModel(this);
	anims[name] = anim;
}

void CloneModel::playAnim(std::string name)
{
	playingAnims.push_back(anims[name]);
}

void CloneModel::changeParent(std::string part, std::string newParent)
{
	Entity* oldParent = parts[part];
	oldParent->removeChild(parts[part]);
	parts[part]->setParent(parts[newParent]);
}

void CloneModel::tween(KeyFrame* key, int frame)
{
	Transform* trans = parts[key->part]->getTransform();
	trans->tween(key, frame);
}

EntityList* CloneModel::getParts()
{
	EntityList* list = new EntityList();

	typedef map<string, Entity*>::iterator it_type;
	for(it_type it = parts.begin(); it != parts.end(); it++)
	{
		Entity* part = it->second;
		list->add(part);
	}
	return list;
}
