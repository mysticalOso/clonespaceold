/*
 * Clone.h
 *
 *  Created on: 12 Jan 2015
 *      Author: mysticalOso
 */

#ifndef SRC_CLONE_CLONE_H_
#define SRC_CLONE_CLONE_H_

#include <core/Vessel.h>
#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>

class CloneAI;
class Race;
class ShipAI;
class CloneHub;
class Interior;
class Ship;
class CloneModel;
class Bullet;

class Clone: public Vessel {
public:
	Clone(Game* game);
	virtual ~Clone();

	void update();

	void setShipAI(ShipAI* shipAI){ this->shipAI = shipAI; }
	ShipAI* getShipAI(){ return shipAI; }

	void setAI(CloneAI* ai);

	void setHub(CloneHub* hub){ this->hub = hub; }
	CloneHub* getHub(){ return hub; }

	Ship* getShip();

	void hitBy( Bullet* bullet, vec2 collisionPoint );

	void stepBack();

	void setModel(CloneModel* model){this->model = model;}
	CloneModel* getModel(){ return model; }

	void setIsDriver(bool b);
	bool getIsDriver(){ return isDriver; }

	Hub* getBulletHub();

	void playerTakeControl();

	void playerLoseControl();

	void turnOnPrimaryWeapons(bool isOn);

	void toggleArmed();
	void use();

	void setArmed(bool b);
	void setAlpha(float a);


private:

	CloneModel* model;
	bool isDriver;
	CloneHub* hub;
	ShipAI* shipAI;
	CloneAI* ai;

	vec3 oldPosition;
	mat4 oldRotation;

};

#endif /* SRC_CLONE_CLONE_H_ */
