/*
 * CloneAnimation.h
 *
 *  Created on: 25 Jul 2014
 *      Author: Adam Nasralla
 */

#ifndef CLONESPACEOLD_SRC_CLONEANIMATION_H_
#define CLONESPACEOLD_SRC_CLONEANIMATION_H_

#include <map>
#include <string>
#include <vector>

class CloneModel;
class KeyFrame;

class CloneAnimation
{
public:
	CloneAnimation(std::string name);
	~CloneAnimation();

	void addKeyFrame(KeyFrame* key);

	void play();
	bool nextFrame(); //returns isFinished

	void setModel(CloneModel* model1){ model = model1; }

	std::string getName(){return name;}

private:

	CloneModel* model;
	std::map<int, std::vector<KeyFrame*> > keyMap;
	int currentFrame;
	std::vector<KeyFrame*> currentKeys;

	std::string name;

};

#endif /* CLONESPACEOLD_SRC_CLONEANIMATION_H_ */
