/*
 * Clone.cpp
 *
 *  Created on: 12 Jan 2015
 *      Author: mysticalOso
 */

#include <ai/CloneAI.h>
#include <ai/ShipAI.h>
#include <clone/Clone.h>
#include <clone/CloneModel.h>
#include <glm/mat4x4.hpp>
#include <hubs/CloneHub.h>
#include <interior/Interior.h>
#include <osogine/game/Game.h>
#include <ship/Engine.h>
#include <ship/Steering.h>
#include <glm/vec3.hpp>
#include <hubs/BulletHubInt.h>
#include <osogine/audio/SoundInterface.h>
#include <osogine/gfx/GfxObject.h>
#include <osogine/utils/Printer.h>
#include <ship/ModuleList.h>
#include <ship/Ship.h>
#include <weapons/Bullet.h>

Clone::Clone(Game* game) : Vessel(game)
{
	model = 0;
	ai = 0;
	hub = 0;
	race = 0;
	shipAI = 0;
	renderOrder = 2;
	isDriver = true;
	gfx = new GfxObject();
}

Clone::~Clone()
{
}

void Clone::update()
{
	if (ai!=0) ai->update();
	oldPosition = getLocalPosition();
	oldRotation = getLocalRotation();
	Entity::update();
//	if (name=="Osfro")
//	{
//		Printer::print("Osfro's pos = ",getPosition(),"\n");
//		Ship* ship = getShip();
//		if (ship!=0)
//		{
//			Printer::print("Osfro's ship's position = ",ship->getPosition(),"\n");
//		}
//	}
//	if (name=="Poco")
//	{
//		Printer::print("Poco's pos = ",getPosition(),"\n");
//		Ship* ship = getShip();
//		if (ship!=0)
//		{
//			Printer::print("Poco's ship's position = ",ship->getPosition(),"\n");
//		}
//	}
}

void Clone::setIsDriver(bool b)
{
	isDriver = b;
	Ship* ship = getShip();
	if (ship!=0)
	{
		if (b) ship->setDriver(this);
		else ship->setDriver(0);
	}

	if (!isDriver) model->setArmed(false);
	else  model->setArmed(false);
}

Ship* Clone::getShip()
{
	if (hub!=0) return hub->getShip();
	else return 0;
}

void Clone::stepBack()
{
	setPosition(oldPosition);
	setRotation(oldRotation);
	engines->turnOn(false);
	steering->turnOn(false);
	setVelocity(0,0,0);
	setSpin(0,0,0);
}

Hub* Clone::getBulletHub()
{
	return hub->getBulletHubInt();
}

void Clone::playerTakeControl()
{
	shipAI->deactivate();
}

void Clone::playerLoseControl()
{
	if (isDriver) shipAI->activate(getShip());
}

void Clone::hitBy(Bullet* bullet, vec2 collisionPoint)
{
	float damage = bullet->getDamage();

	health -= damage;

	if (health <= 0)
	{
		hub->destroyClone(this);
		SoundInterface::playDeath();
	}
	else
	{
		hub->cloneHit(this, collisionPoint);
	}
}

void Clone::use()
{
	hub->use(this);
}

void Clone::setAI(CloneAI* ai)
{
	this->ai = ai;
	ai->activate(this);
}

void Clone::turnOnPrimaryWeapons(bool isOn)
{
	if (model->getIsArmed())
	{
		Vessel::turnOnPrimaryWeapons(isOn);
		model->setShooting(isOn);
	}
}

void Clone::toggleArmed()
{
	model->toggleArmed();
}

void Clone::setArmed(bool b)
{
	model->setArmed(b);
}

void Clone::setAlpha(float a)
{
	model->setAlpha(a);
}
