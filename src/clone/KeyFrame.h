/*
 * KeyFrame.h
 *
 *  Created on: 25 Jul 2014
 *      Author: Adam Nasralla
 */

#ifndef CLONESPACEOLD_SRC_KEYFRAME_H_
#define CLONESPACEOLD_SRC_KEYFRAME_H_

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>
#include <string>

using namespace glm;

class KeyFrame
{
public:
	KeyFrame(std::string part1, int start1, int inter1);
	~KeyFrame();

	void setRotation(vec4 quat1){qRot = quat(quat1.x,quat1.y,quat1.z,quat1.a);}

	//Export -Z up -Y forward view Z up Y left X up
	void setRotation(float x, float y, float z){ qRot = quat(vec3(radians(x),radians(y),radians(z))); }

	void setTranslation(float x, float y, float z){ trans = vec3(x*100,y*100,z*100); }
	void setScale(vec3 scale1){ scaleV = scale1; }
	void setScale(float s){ scaleV = vec3(s,s,s); }

	KeyFrame* setNextRot(float x, float y, float z, int start, int inter);
	KeyFrame* setNextTrans(float x, float y, float z, int start, int inter);
	KeyFrame* getCopy();

	void setNext(KeyFrame* next1){next = next1; finish = next->start-1;}
	void setNext(KeyFrame* next1, int start1, int inter1);

	void setNewParent(std::string newParent1){changeParent = true; newParent = newParent1;}

	float calcRatio(int frame);

	std::string part;
	int start;
	int finish;
	int inter; //0 = sin, 1 = easeIn, 2 = easeOut
	quat qRot;
	vec3 trans;
	vec3 scaleV;
	KeyFrame* next;
	bool changeParent;
	std::string newParent;


};

#endif /* CLONESPACEOLD_SRC_KEYFRAME_H_ */
