/*
 * ExplosionBuilder.cpp
 *
 *  Created on: 10 Dec 2014
 *      Author: mysticalOso
 */

#include <builders/ExplosionBuilder.h>
#include <clone/Clone.h>
#include <clone/CloneModel.h>
#include <hubs/ExplosionHub.h>
#include <glm/vec4.hpp>
#include <osogine/gfx/GfxObject.h>
#include <osogine/utils/Polygon.h>
#include <ship/Ship.h>
#include <space/Explosion.h>
#include <space/Asteroid.h>
#include <osogine/utils/Printer.h>
#include <osogine/utils/Transform.h>
#include <osogine/utils/Utils.h>
#include <weapons/Bullet.h>

using namespace glm;

ExplosionBuilder::ExplosionBuilder(Game* game) : EntityBuilder(game)
{
	hub = 0;

}

ExplosionBuilder::~ExplosionBuilder()
{

}

void ExplosionBuilder::setHub(Hub* hub)
{
	this->hub = (ExplosionHub*) hub;
}

Explosion* ExplosionBuilder::explode(Asteroid* asteroid)
{
	Explosion* e = new Explosion(hub);
	GfxObject* gfx = asteroid->getGfx();
	e->addParticlesOf(asteroid->getGfx(),4,0.02,100);
	vec4 c1 = vec4(1,0,0,1);
	vec4 c2 = vec4(1,1,0,1);
	int noParticles = asteroid->getArea() / 200 + 250;
	//Printer::print("Area = ",,"\n");
	e->addParticlesInside( gfx, noParticles , 1, 6, 0.2, 100, c1, c2);
	e->setPosition(asteroid->getPosition());
	e->setRotation(asteroid->getRotation());
	e->addVelocity(asteroid->getVelocity());
	e->initGfx();


	return e;
}

Explosion* ExplosionBuilder::explode(Ship* ship)
{
	Explosion* e = new Explosion(hub);
	GfxObject* gfx = ship->getGfx();
	//e->addParticlesOf(ship->getGfx(),1,0.05,100);
	vec4 c1 = vec4(1,0,0,1);
	vec4 c2 = vec4(1,1,0,1);
	e->addParticlesInside( gfx, 400, 0.5, 4, 0.2, 100, c1, c2);
	e->addParticlesOf( gfx, 2, 0.1, 100 );
	e->setPosition(ship->getPosition());
	e->setRotation(ship->getRotation());
	e->addVelocity(ship->getVelocity() * 0.2f);
	//e->addVelocity(vec3(10,0,0));
	e->initGfx();


	return e;
}

Explosion* ExplosionBuilder::explode(Ship* ship, vec2 position)
{
	vec3 pos = vec3(position,0);
	Explosion* e = new Explosion(hub);
	vec4 c1;
	vec4 c2;
	if (!ship->shieldIsOn())
	{
		c1 = vec4(1,0,0,1);
		c2 = vec4(1,1,0,1);
	}
	else
	{
		c1 = vec4(0,0,0.8,1);
		c2 = vec4(0,0.5,1,1);
	}
	e->addParticles( pos, 20, 0.5, 3, 0.2, 100, c1, c2);

	//e->setPosition(ship->getPosition());
	//e->setRotation(ship->getRotation());
	//e->addVelocity(ship->getVelocity());
	//e->addVelocity(vec3(10,0,0));
	e->initGfx();


	return e;
}

Explosion* ExplosionBuilder::explode(Bullet* bullet)
{
	Explosion* e = new Explosion(hub);


	vec3 scale = bullet->getScale();

	vec4 c1 = vec4(0,0.5,0,1);
	vec4 c2 = vec4(0,1,0,1);
	e->addParticles( 20, 0.5, 2, 0.2, 30, c1, c2);
	e->initGfx();
	e->setPosition(vec3(bullet->getOutline()->getLastCollision(),0));
	e->setScale(scale);

	return e;
}

Explosion* ExplosionBuilder::explode(Asteroid* asteroid, List<Triangle> tris)
{
	Explosion* e = new Explosion(hub);

	vec3 centre = Utils::getCentre(tris);

	e->addParticlesOf(tris,centre,2,0.05,40);
	vec4 c1 = vec4(1,0,0,1);
	vec4 c2 = vec4(1,1,0,1);
	e->addParticles( centre, 20, 0.7, 3, 0.2, 40, c1, c2);
	e->setPosition(asteroid->getPosition());
	e->setRotation(asteroid->getRotation());
	e->addVelocity(asteroid->getVelocity());
	e->initGfx();

	return e;
}

void ExplosionBuilder::explode(Clone* clone)
{


	GfxObject* gfx = new GfxObject();
	Explosion* e = new Explosion(hub);

	vec4 c1 = vec4(0.6,0,0.1,1);
	vec4 c2 = vec4(0.3,0,0,1);

	CloneModel* model = clone->getModel();
	EntityList* parts = model->getParts();

	for (int k=0; k<parts->size(); k++)
	{

		Entity* entity = (Entity*) parts->get(k);
		GfxObject* partGfx = entity->getGfx();
		if (partGfx!=0 && k!=3 && k !=4)
		{
			GfxObject* gfx2 = partGfx->clone();
			Transform* transform = entity->getTransform();
			gfx2->applyTransform( transform );
			gfx->add(gfx2);
		}
	}

	gfx->applyInverseTransform( clone->getTransform() );
	e->addParticlesOf( gfx, 10, 0.01, 150 );
	e->addParticlesInside( gfx, 300, 10, 30, 0.3, 150, c1, c2);

	e->setPosition(clone->getPosition());
	e->setRotation(clone->getRotation());
	e->setScale(clone->getScale());

	e->initGfx();

	e->applyInverseOf(hub);


	gfx = new GfxObject();
	e = new Explosion(hub);


	for (int k=0; k<parts->size(); k++)
	{

		Entity* entity = (Entity*) parts->get(k);
		GfxObject* partGfx = entity->getGfx();
		if (partGfx!=0 && k==3)
		{
			GfxObject* gfx2 = partGfx->clone();
			Transform* transform = entity->getTransform();
			gfx2->applyTransform( transform );
			gfx->add(gfx2);
		}
	}

	gfx->applyInverseTransform( clone->getTransform() );

	e->addParticlesOf( gfx, 10, 0.01, 150 );

	e->setPosition(clone->getPosition());
	e->setRotation(clone->getRotation());
	e->setScale(clone->getScale());
	e->initGfx();

	e->applyInverseOf(hub);





	gfx = new GfxObject();
	e = new Explosion(hub);


	for (int k=0; k<parts->size(); k++)
	{

		Entity* entity = (Entity*) parts->get(k);
		GfxObject* partGfx = entity->getGfx();
		if (partGfx!=0 && k==4)
		{
			GfxObject* gfx2 = partGfx->clone();
			Transform* transform = entity->getTransform();
			gfx2->applyTransform( transform );
			gfx->add(gfx2);
		}
	}

	gfx->applyInverseTransform( clone->getTransform() );
	e->addParticlesInside( gfx, 300, 10, 30, 0.3, 150, c1, c2);
	e->addParticlesOf( gfx, 10, 0.01, 150 );

	e->setPosition(clone->getPosition());
	e->setRotation(clone->getRotation());
	e->setScale(clone->getScale());
	e->initGfx();

	e->applyInverseOf(hub);




}

Explosion* ExplosionBuilder::explode(Clone* clone, vec2 position)
{
	vec3 pos = vec3(position,0);

	//Printer::print("Explosion Pos = ",pos,"\n");
	Explosion* e = new Explosion(hub);
	vec4 c1;
	vec4 c2;

	c1 = vec4(0.6,0,0.1,1);
	c2 = vec4(0.3,0,0,1);

	//e->addParticles( pos, 30, 0.005, 0.03, 0.2, 100, c1, c2);

	e->addParticles( 50, 1, 4, 0.3, 40, c1, c2);
	e->initGfx();
	e->setPosition(vec3(position,0));
	e->setScale(0.01);


	e->initGfx();


	return e;

}
