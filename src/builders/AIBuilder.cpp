/*
 * AIBuilder.cpp
 *
 *  Created on: 12 Jan 2015
 *      Author: mysticalOso
 */

#include <ai/BasicShipAI.h>
#include <ai/ShipAI.h>
#include <builders/AIBuilder.h>

AIBuilder::AIBuilder(Game* game) : EntityBuilder(game)
{

}

AIBuilder::~AIBuilder()
{

}

ShipAI* AIBuilder::buildXacadienScoutAI()
{
	return new BasicShipAI(game);
}

ShipAI* AIBuilder::buildSentientAI()
{
	return new BasicShipAI(game);
}
