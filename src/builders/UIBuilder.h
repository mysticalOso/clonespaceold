/*
 * UIBuilder.h
 *
 *  Created on: 27 Feb 2015
 *      Author: Adam Nasralla
 */

#ifndef SRC_BUILDERS_UIBUILDER_H_
#define SRC_BUILDERS_UIBUILDER_H_

#include <osogine/game/EntityBuilder.h>
#include <glm/fwd.hpp>
#include <string>

using namespace glm;
using namespace std;

class ModuleIcon;
class Button;
class UIEntity;
class Polygon;
class TextureEntity;
class EntityList;
class CostBar;
class Entity;
class OutlinePoint;

class UIBuilder: public EntityBuilder
{
public:
	UIBuilder(Game* game);
	~UIBuilder();

	EntityList* buildModuleIcons(UIEntity* parent);

	ModuleIcon* buildModuleIcon(UIEntity* parent, vec2 code);

	Button* buildButton(UIEntity* parent, string name);

	vec3 getUIColour(string colour);


	EntityList* buildCostBars(Entity* parent);

	CostBar* buildCostBar(Entity* parent, string colour, int index );

	void buildStartingOutline(UIEntity* parent);

	OutlinePoint* buildOutlinePoint(UIEntity* parent, float x, float y);

private:

	Polygon* buildModuleIconOutline();
	TextureEntity* buildModuleIconEdge(ModuleIcon* parent);
	ModuleIcon* buildEmptyModuleIcon(UIEntity* parent, vec2 code);
};

#endif /* SRC_BUILDERS_UIBUILDER_H_ */
