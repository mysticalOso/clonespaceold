/*
 * LightBuilder.h
 *
 *  Created on: 23 Feb 2015
 *      Author: Adam Nasralla
 */

#ifndef SRC_BUILDERS_LIGHTBUILDER_H_
#define SRC_BUILDERS_LIGHTBUILDER_H_

#include <osogine/game/EntityBuilder.h>
#include <glm/vec2.hpp>
#include <osogine/utils/List.h>

using namespace glm;

class LightHub;
class RoomHub;
class EndPoint;
class Segment;
class Light;
class Walls;
class Polygon;
class GfxObject;
class Entity;

class LightBuilder: public EntityBuilder
{
public:
	LightBuilder(Game* game);
	~LightBuilder();

	Light* buildLight(vec3 position, vec3 colour);

	void updateLight(Light* light);

	void setHub(Hub* lightHub);

	void refresh();



private:

	void initSegments();

	void updateAngles(float x, float y);

	void addSegment(float x1, float y1, float x2, float y2);
	void updateSegment(int index, float x1, float y1, float x2, float y2);

	bool segmentInFront(Segment a, Segment b, vec2 relativeTo);

	bool leftOf(Segment s, vec2 p);
	vec2 lineIntersection(vec2 p1, vec2 p2, vec2 p3, vec2 p4);

	void addTriangle(float angle1, float angle2, Segment* segment);

	void sweep();

	void updateEdgeSegments();

	void addUVs();

	vec2 clamp(vec2 p, vec2 topRight, vec2 bottomLeft);

	void trimSegments();

	void clear();
	void clearSegments(List<Segment*> segments);
	void clearEndpoints(List<EndPoint*> endPoints);

	vec2 center;
	Walls* walls;
	Walls* walls2;
	LightHub* lightHub;
	RoomHub* roomHub;

	List<EndPoint*> endPoints2;
	List<Segment*> segments2;

	List<EndPoint*> endPoints;
	List<Segment*> segments;

	List<Segment*> open;

	Light* light;
	Polygon* outline;
	GfxObject* gfx;
	float lightSize;
};

#endif /* SRC_BUILDERS_LIGHTBUILDER_H_ */
