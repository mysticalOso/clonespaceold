/*
 * RaceBuilder.h
 *
 *  Created on: 12 Jan 2015
 *      Author: mysticalOso
 */

#ifndef SRC_BUILDERS_RACEBUILDER_H_
#define SRC_BUILDERS_RACEBUILDER_H_

#include <osogine/game/EntityBuilder.h>

class Race;
class RaceHub;

class RaceBuilder: public EntityBuilder {
public:
	RaceBuilder(Game* game);
	virtual ~RaceBuilder();

	Race* buildSentient();
	Race* buildXacadiens();

	void setHub(Hub* hub);

private:
	RaceHub* hub;
};

#endif /* SRC_BUILDERS_RACEBUILDER_H_ */
