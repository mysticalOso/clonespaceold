/*
 * BulletBuilder.cpp
 *
 *  Created on: 31 Oct 2014
 *      Author: Adam Nasralla
 */

#include <builders/BulletBuilder.h>
#include <osogine/gfx/GfxBuilder.h>
#include <osogine/gfx/GfxObject.h>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <osogine/game/Game.h>
#include <weapons/Bullet.h>
#include <osogine/utils/Polygon.h>
#include <osogine/utils/Utils.h>
#include <string>

BulletBuilder::BulletBuilder(Game* game) : EntityBuilder(game)
{
}

BulletBuilder::~BulletBuilder()
{
}

Bullet* BulletBuilder::buildBullet(float damage, float speed, float scale,  string colour)
{
	GfxObject* gfx = getBulletGfx(colour);
	Bullet* bullet = new Bullet(damage, speed, scale, game);
	bullet->setGfx(gfx);
	bullet->setProgram("simple");
	bullet->setOutline( getBulletOutline(speed,60) );
	return bullet;
}

GfxObject* BulletBuilder::getBulletGfx(string colourName)
{
	if (lazerGfx.count(colourName)==0)
	{
		vec3 colour = getColourByName( colourName );
		lazerGfx[colourName] = buildBulletGfx(colour);
		return lazerGfx[colourName];
	}
	else
	{
		return lazerGfx[colourName];
	}

}

GfxObject* BulletBuilder::buildBulletGfx(vec3 colour)
{

	vec4 colour1 = vec4(colour*0.5f,0);
	vec4 colour2 = vec4(colour,1);
	return gfxBuilder->buildSegmentRectangle(2,60,10,colour1,colour2);
}

vec3 BulletBuilder::getColourByName(string name)
{
	vec3 colour;
	if (name == "green") colour = vec3(0,1,0);
	if (name == "magenta") colour = vec3(1,0,1);
	return colour;
}

Polygon* BulletBuilder::getBulletOutline(float speed, float size)
{
	float halfSize = size*0.5;
	int noPoints = 2;
	vec2 start = vec2(0,halfSize);
	vec2 finish = vec2(0,-halfSize-speed);
	Polygon* poly = new Polygon( Utils::getPointsFromTo(start,finish,noPoints) );
	poly->setIsLine(true);
	return poly;
}
