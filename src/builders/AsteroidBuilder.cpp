/*
 * AsteroidBuilder.cpp
 *
 *  Created on: 23 Oct 2014
 *      Author: Adam Nasralla
 */

#include <builders/AsteroidBuilder.h>
#include <core/Constants.h>

#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>

#include <GL/glew.h>
#include <hubs/AsteroidHub.h>
#include <osogine/game/Entity.h>
#include <osogine/gfx/DynamicTexture.h>
#include <osogine/gfx/GfxBuilder.h>
#include <osogine/gfx/GfxObject.h>
#include <osogine/utils/Dice.h>
#include <osogine/utils/Polygon.h>
#include <space/Asteroid.h>
#include <test/NoiseMap.h>
#include <cmath>

using namespace glm;
using namespace std;

AsteroidBuilder::AsteroidBuilder(Game* game) : EntityBuilder(game)
{
	hub = 0;
	outlineScale = 0;
	blobMap = 0;
}

AsteroidBuilder::~AsteroidBuilder()
{

}

IEntity* AsteroidBuilder::build()
{
	Asteroid* asteroid;
	if (hub!=0)
	{
		asteroid = new Asteroid(hub);
	}
	else
	{
		asteroid = new Asteroid(game);
	}
	//List<vec2>* outline = buildOutline();

	outlineScale = Dice::roll(8.0,15.0);

	List<vec2>* outline = buildNoiseOutline()->getOutline();

	GfxObject* gfx = buildGfx(outline);

	modifyGfxWithNoise(gfx);

	//delete blobMap;

	asteroid->setGfx(gfx);
	asteroid->setProgram("flat");

	Polygon* p = new Polygon(outline);
	asteroid->setOutline(p);

	gfxs.add(gfx->clone());

	outlines.add(p->clone());

	asteroid->initMesh();

	return asteroid;
}

Asteroid* AsteroidBuilder::buildClone(int i)
{
	Asteroid* asteroid = new Asteroid(hub);

	asteroid->setGfx(gfxs.get(i)->clone());
	asteroid->setProgram("flat");

	asteroid->setOutline(outlines.get(i)->clone());

	asteroid->initMesh();

	return asteroid;
}

//TODO improve AsteroidBuilder::buildOutline
List<vec2>* AsteroidBuilder::buildOutline()
{
	List<vec2>* outline = new List<vec2>();

	float x1,y1,a,r;
	unsigned int i;

	r = 120;
	float startR = r;
	unsigned int noPoints =  10;

	float ea = Dice::roll(0.8,1.5);
	float eb = Dice::roll(0.8,1.5);

	bool ead = Dice::chance(2);
	bool ebd = Dice::chance(2);

	for (i=0; i<noPoints; i++)
	{
		a = (2*i*PI)/(noPoints);
		r += Dice::roll((-30.0),(30.0));
		if (i>(noPoints-7))
		{
			if (r>(startR+30))
			{
				r -= 20.0f;
			}
			if (r<(startR-30))
			{
				r += 20.0f;
			}
		}

		x1 = ea * r*cos(a);
		y1 = eb * r*sin(a);


		ea += Dice::roll(-0.2,0.2);
		eb += Dice::roll(-0.2,0.2);

//		if (ead) ea += Dice::roll(0.0,0.1);
//		else ea -= Dice::roll(0.0,0.1);
//
//		if (ebd) eb += Dice::roll(0.0,0.1);
//		else eb -= Dice::roll(0.0,0.1);
//
//		if (Dice::chance(5)) ead = !ead;
//		if (Dice::chance(5)) ebd = !ebd;

		if (eb > 1.5) eb = 1.5;
		if (eb < 0.8) eb = 0.8;
		if (ea > 1.5) ea = 1.5;
		if (ea < 0.8) ea = 0.8;

		outline->add(vec2(x1,y1));
	}

	return outline;
}

void AsteroidBuilder::setHub(Hub* hub)
{
	this->hub = (AsteroidHub*) hub;
}

GfxObject* AsteroidBuilder::buildGfx(List<vec2>* outline)
{
	vec3 colour1 = vec3(0.5,0.3,0.1);
	vec3 colour2 = vec3(0.6,0.4,0.1);

	GfxObject* gfx = gfxBuilder->buildTriMesh( outline, 200, colour1, colour2, true, true);

	gfx->reColour(vec4(colour1,1),vec4(colour2,1));
//	for (int i = 0; i < gfx->getNoVertices(); i++)
//	{
//		vec3 vert = gfx->getVertex(i);
//		vert.z += Dice::roll(-10,10);
//
//	}

	return gfx;
}

GfxObject* AsteroidBuilder::buildGfx()
{
	List<vec2>* outline = buildOutline();

	GfxObject* gfx = buildGfx(outline);



	return gfx;
}

Polygon* AsteroidBuilder::buildNoiseOutline()
{
	NoiseMap* noiseMap = new NoiseMap(32,32);
	noiseMap->generateNoiseMap(8,1,0,0.5,0.05,true);
	blobMap = new NoiseMap(32,32);
	blobMap->generateDistributedBlobs(7,1,8,0.2,1,true);
	NoiseMap* blobMap2 = new NoiseMap(32,32);
	blobMap2->generateDistributedBlobs(20,0.35,3,0.3,1,true);


	blobMap->multiplyMaps(noiseMap);

	blobMap->subtractMaps(blobMap2);


	DynamicTexture* texture = new DynamicTexture();

	Polygon* outline = new Polygon();

	for (int i = 0; i < 32; i++)
	{
		  for (int j = 0; j < 32; j++)
		  {
			  GLubyte b = (GLubyte) (blobMap->getValueAtXY(i,j) * 255);
			  //b = 0;
			  if (b<10) b = 0;
			  texture->pixels[i][j][0] = (GLubyte) b;
			  texture->pixels[i][j][1] = (GLubyte) b;
			  texture->pixels[i][j][2] = (GLubyte) b;
			  texture->pixels[i][j][3] = (GLubyte) 255;
		  }
	}

	for (int i=0; i<20; i++)
	{
		float k = 360.0f/20.0f;
		float a = (float)i*k/180.0f;
		float dx = cos(PI * a);
		float dy = sin(PI * a);
		float x = 0;
		float y = 0;
		for (int j=20; j>0; j--)
		{
			x = round(dx * j);
			y = round(dy * j);
			int a = x + 16;
			int b = y + 16;
			if (a>31) a = 31;
			if (a<0) a = 0;
			if (b>31) b = 31;
			if (b<0) b = 0;

			if (texture->pixels[b][a][0] > 10)
			{
				j = 0;
			}
		}
		outline->add(x*outlineScale,y*outlineScale);
	}

	for (int i=0; i<20; i++)
	{
		vec2 a = outline->get(i);
		vec2 b = outline->get((i+1)%20);
		vec2 d = a-b;
		a -= d*0.5f;
		outline->set(i,a);
	}

	//delete texture;
	//delete noiseMap;
	//delete blobMap2;
	return outline;
}

void AsteroidBuilder::modifyGfxWithNoise(GfxObject* gfx)
{
	float m = 1.0f/outlineScale;
	for (uint i=0; i<gfx->getNoVertices(); i++)
	{
		vec3 v = gfx->getVertex(i);
		int a = round(v.x*m +16);
		int b = round(v.y*m +16);
		if (a>31) a = 31;
		if (a<0) a = 0;
		if (b>31) b = 31;
		if (b<0) b = 0;
		v.z *= (blobMap->getValueAtXY(b,a)+0.5)*4;
		gfx->setVertex(i,v);
	}
}
