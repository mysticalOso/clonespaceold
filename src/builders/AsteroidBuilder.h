/*
 * AsteroidBuilder.h
 *
 *  Created on: 23 Oct 2014
 *      Author: Adam Nasralla
 */

#ifndef ASTEROIDBUILDER_H_
#define ASTEROIDBUILDER_H_

#include <glm/glm.hpp>
#include <osogine/game/EntityBuilder.h>
#include <osogine/utils/List.h>

class Asteroid;
class GfxObject;
class Hub;
class AsteroidHub;
class NoiseMap;
class Polygon;

class AsteroidBuilder : public EntityBuilder
{
public:
	AsteroidBuilder(Game* game);
	virtual ~AsteroidBuilder();
	void setHub(Hub* hub);

	void initOutlines();

	IEntity* build();
	Asteroid* buildClone(int i);

	List<vec2>* buildOutline();
	GfxObject* buildGfx();
	GfxObject* buildGfx(List<vec2>* outline);

private:

	Polygon* buildNoiseOutline();
	void modifyGfxWithNoise(GfxObject* gfx);

	AsteroidHub* hub;
	NoiseMap* blobMap;
	float outlineScale;

	List<Polygon*> outlines;
	List<GfxObject*> gfxs;


};

#endif /* ASTEROIDBUILDER_H_ */
