/*
 * ExplosionBuilder.h
 *
 *  Created on: 10 Dec 2014
 *      Author: mysticalOso
 */

#ifndef SRC_BUILDERS_EXPLOSIONBUILDER_H_
#define SRC_BUILDERS_EXPLOSIONBUILDER_H_

#include <osogine/game/EntityBuilder.h>
#include <osogine/gfx/Triangle.h>
#include <osogine/utils/List.h>

class Bullet;
class ExplosionHub;
class Explosion;
class Asteroid;
class Ship;
class Clone;

class ExplosionBuilder: public EntityBuilder {
public:
	ExplosionBuilder(Game* game);
	virtual ~ExplosionBuilder();
	void setHub(Hub* hub);

	Explosion* explode(Asteroid* asteroid);

	Explosion* explode(Ship* ship);

	Explosion* explode(Ship* ship, vec2 position);

	Explosion* explode(Bullet* bullet);

	Explosion* explode(Asteroid* asteroid, List<Triangle> tris);

	void explode(Clone* clone);

	Explosion* explode(Clone* clone, vec2 position);

private:

	ExplosionHub* hub;


};

#endif /* SRC_BUILDERS_EXPLOSIONBUILDER_H_ */
