/*
 * AnimationBuilder.cpp
 *
 *  Created on: 26 Jul 2014
 *      Author: Adam Nasralla
 */

#include <builders/AnimationBuilder.h>
#include <clone/CloneAnimation.h>
#include <clone/KeyFrame.h>

//Only specify next key frame (includes start frame of previous)

CloneAnimation* AnimationBuilder::buildShootAnimation()
{
	CloneAnimation* anim = new CloneAnimation("shoot");

	int length = 8;


	KeyFrame *key, *next;


	key = new KeyFrame("gun",1, 2);
	key->setTranslation(2.845,0.968,-0.887);
	key->setRotation(-160,-90,0);
	key->setNewParent("body");

	next = new KeyFrame("gun", length, 2);
	next->setTranslation(2.31,0.77,-0.31);
	next->setRotation(-180,-123.26,20);

	key->setNext(next);
	anim->addKeyFrame(key);


	key = new KeyFrame("foreArmR",1, 2);
	key->setTranslation(0,-0.296,-2.936);
	key->setRotation(69,0,0);


	next = new KeyFrame("foreArmR", length, 2);
	next->setTranslation(0,-0.296,-2.936);
	next->setRotation(90.5,0,0);

	key->setNext(next);
	anim->addKeyFrame(key);


	key = new KeyFrame("foreArmL",1, 2);
	key->setTranslation(0,-0.296,-2.936);
	key->setRotation(45,0,0);


	next = new KeyFrame("foreArmL", length, 2);
	next->setTranslation(0,-0.296,-2.936);
	next->setRotation(70,0,0);

	key->setNext(next);
	anim->addKeyFrame(key);


	key = new KeyFrame("upperArmL",1, 2);
	key->setTranslation(0,2.4,0);
	key->setRotation(63.4,7,263.9);

	next = new KeyFrame("upperArmL", length, 2);
	next->setTranslation(0,2.4,0);
	next->setRotation(80.3,7,-99);

	key->setNext(next);
	anim->addKeyFrame(key);


	key = new KeyFrame("body",1, 2);
	key->setTranslation(0,0,0);
	key->setRotation(0,0,-20);


	next = new KeyFrame("body", length, 2);
	next->setTranslation(-0.4,0,0);
	next->setRotation(0,0,-20);

	key->setNext(next);
	anim->addKeyFrame(key);




	anim->addKeyFrame(key);


	return anim;
}

CloneAnimation* AnimationBuilder::buildShootReleaseAnimation()
{
	CloneAnimation* anim = new CloneAnimation("shootRelease");

	int length = 20;


	KeyFrame *key, *next;


	key = new KeyFrame("gun",1, 1);
	key->setTranslation(2.31,0.77,-0.31);
	key->setRotation(-180,-123.26,20);
	key->setNewParent("body");

	next = new KeyFrame("gun", length, 1);
	next->setTranslation(2.845,0.968,-0.887);
	next->setRotation(-160,-90,0);


	key->setNext(next);
	anim->addKeyFrame(key);


	key = new KeyFrame("foreArmR",1, 1);
	key->setTranslation(0,-0.296,-2.936);
	key->setRotation(90.5,0,0);


	next = new KeyFrame("foreArmR", length, 1);
	next->setTranslation(0,-0.296,-2.936);
	next->setRotation(69,0,0);

	key->setNext(next);
	anim->addKeyFrame(key);


	key = new KeyFrame("foreArmL",1, 1);
	key->setTranslation(0,-0.296,-2.936);
	key->setRotation(70,0,0);


	next = new KeyFrame("foreArmL", length, 1);
	next->setTranslation(0,-0.296,-2.936);
	next->setRotation(45,0,0);

	key->setNext(next);
	anim->addKeyFrame(key);


	key = new KeyFrame("upperArmL",1, 1);
	key->setTranslation(0,2.4,0);
	key->setRotation(80.3,7,-99);

	next = new KeyFrame("upperArmL", length, 1);
	next->setTranslation(0,2.4,0);
	next->setRotation(63.4,7,263.9);

	key->setNext(next);
	anim->addKeyFrame(key);


	key = new KeyFrame("body",1, 1);
	key->setTranslation(-0.4,0,0);
	key->setRotation(0,0,-20);


	next = new KeyFrame("body", length, 1);
	next->setTranslation(0,0,0);
	next->setRotation(0,0,-20);

	key->setNext(next);
	anim->addKeyFrame(key);




	anim->addKeyFrame(key);


	return anim;
}

CloneAnimation* AnimationBuilder::buildDrawAnim()
{
	CloneAnimation* anim = new CloneAnimation("draw");

	KeyFrame *key1, *key2, *key3, *key4, *key5;


	key1 = new KeyFrame("upperArmR",1, 1);
	key1->setTranslation(0,-2.4,0);
	key1->setRotation(0,0,0);

	key2 = key1->setNextRot(91.8, -72.4, -72.5,  20, 2);

	key3 = key2->setNextRot(31.643, 162.643, 54.462,  40, 1);

	key4 = key3->setNextRot(127.5,-32.7,-100,  60, 2);

	key5 = key4->setNextRot(40.2,-11.2,-38,  80, 1);

	anim->addKeyFrame(key1);


	key1 = new KeyFrame("foreArmR",1, 1);
	key1->setTranslation(0,-0.296,-2.936);
	key1->setRotation(0,0,0);

	key2 = key1->setNextRot(43.7, 0, 0,  20, 2);

	key3 = key2->setNextRot(121.079, -10.827, 1.442,  40, 1);

	key4 = key3->setNextRot(55.527, 0, 0,  60, 2);

	key5 = key4->setNextRot(69, 0, 0,  80, 1);

	anim->addKeyFrame(key1);

	key1 = new KeyFrame("body",1, 0);
	key1->setTranslation(0,0,0);
	key1->setRotation(0,0,0);

	key2 = key1->setNextRot(0, 0, -20,   80, 0);

	anim->addKeyFrame(key1);


	key1 = new KeyFrame("head",1, 0);
	key1->setTranslation(0,0,0);
	key1->setRotation(0,0,0);

	key2 = key1->setNextRot(0, 0, 0,   40, 0);

	key3 = key2->setNextRot(0, 0, 20,  80, 0);

	anim->addKeyFrame(key1);


	key1 = new KeyFrame("foreArmL",1, 1);
	key1->setTranslation(0,-0.296,-2.936);
	key1->setRotation(0,0,0);

	key2 = key1->setNextRot(20.8, 0, 0,   40, 2);

	key3 = key2->setNextRot(45, 0, 0,  80, 1);

	anim->addKeyFrame(key1);

	key1 = new KeyFrame("upperArmL",1, 1);
	key1->setTranslation(0,2.4,0);
	key1->setRotation(0,0,0);

	key2 = key1->setNextRot(8, 0, -98.2,   40, 2);

	key3 = key2->setNextRot(63.4, 7, -96.1,  80, 1);

	anim->addKeyFrame(key1);

	key1 = new KeyFrame("gun",1, 0);
	key1->setTranslation(-2,-0.39,0);
	key1->setRotation(180,0,90);
	key1->setNewParent("body");

	key2 = key1->setNextRot(180,0,90, 39, 0);
	key2->setTranslation(-2,-0.38962,0);

	key3 = key2->setNextRot(168.534, 35.804, 200.269,  40, 0);
	key3->setTranslation(-0.0663, -0.00178, -3.19753);
	key3->setNewParent("foreArmR");

	key4 = key3->setNextRot(143.949, 18.841, 78.273, 80, 0);
	key4->setTranslation(-0.01151,0.93094,-3.20800);

	anim->addKeyFrame(key1);


	return anim;
}

//TODO finish and start frames should not be the same!
CloneAnimation* AnimationBuilder::buildSheathAnim()
{
	CloneAnimation* anim = new CloneAnimation("sheath");

	KeyFrame *key1, *key2, *key3, *key4, *key5;


	key1 = new KeyFrame("upperArmR",1, 1);
	key1->setTranslation(0,-2.4,0);
	key1->setRotation(40.2,-11.2,-38);

	key2 = key1->setNextRot(127.5,-32.7,-100,  20, 2);

	key3 = key2->setNextRot(31.643, 162.643, 54.462,  40, 1);

	key4 = key3->setNextRot(91.8, -72.4, -72.5,  60, 2);

	key5 = key4->setNextRot(0,0,0,  80, 1);

	anim->addKeyFrame(key1);


	key1 = new KeyFrame("foreArmR",1, 1);
	key1->setTranslation(0,-0.296,-2.936);
	key1->setRotation(69, 0, 0);

	key2 = key1->setNextRot(55.527, 0, 0,  20, 2);

	key3 = key2->setNextRot(121.079, -10.827, 1.442,  40, 1);

	key4 = key3->setNextRot(43.7, 0, 0,  60, 2);

	key5 = key4->setNextRot(0,0,0,  80, 1);

	anim->addKeyFrame(key1);


	key1 = new KeyFrame("body",1, 0);
	key1->setTranslation(0,0,0);
	key1->setRotation(0, 0, -20);

	key2 = key1->setNextRot(0, 0, 0,   80, 0);

	anim->addKeyFrame(key1);


	key1 = new KeyFrame("head",1, 0);
	key1->setTranslation(0,0,0);
	key1->setRotation(0, 0, 20);

	key2 = key1->setNextRot(0, 0, 0,   40, 0);

	key3 = key2->setNextRot(0, 0, 0,  80, 0);

	anim->addKeyFrame(key1);


	key1 = new KeyFrame("foreArmL",1, 1);
	key1->setTranslation(0,-0.296,-2.936);
	key1->setRotation(45, 0, 0);

	key2 = key1->setNextRot(20.8, 0, 0,   40, 2);

	key3 = key2->setNextRot(0,0,0,  80, 1);

	anim->addKeyFrame(key1);



	key1 = new KeyFrame("upperArmL",1, 1);
	key1->setTranslation(0,2.4,0);
	key1->setRotation(63.4, 7, -96.1);

	key2 = key1->setNextRot(8, 0, -98.2,   40, 2);

	key3 = key2->setNextRot(0,0,0,  80, 1);

	anim->addKeyFrame(key1);


	key1 = new KeyFrame("gun",1, 0);
	key1->setTranslation(-0.01151,0.93094,-3.20800);
	key1->setRotation(143.949, 18.841, 78.273);
	key1->setNewParent("foreArmR");

	key2 = key1->setNextRot(168.534, 35.804, 200.269,  40, 0);
	key2->setTranslation(-0.0663, -0.00178, -3.19753);

	key3 = key2->setNextRot(180,0,90, 41, 0);
	key3->setTranslation(-2,-0.38962,0);
	key3->setNewParent("body");

	key4 = key3->setNextRot(180,0,90, 80, 0);
	key4->setTranslation(-2,-0.39,0);

	anim->addKeyFrame(key1);


	return anim;
}
