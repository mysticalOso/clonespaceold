/*
 * ModuleBuilder.cpp
 *
 *  Created on: 31 Oct 2014
 *      Author: Adam Nasralla
 */

#include <builders/BulletBuilder.h>
#include <builders/ModuleBuilder.h>
#include <builders/ShieldBuilder.h>
#include <core/Constants.h>
#include <glm/vec2.hpp>
#include <osogine/game/EntityList.h>
#include <osogine/game/Game.h>
#include <ship/EnergyCore.h>
#include <ship/Engine.h>
#include <ship/GemTractor.h>
#include <ship/Shield.h>
#include <ship/Steering.h>
#include <ship/Teleporter.h>
#include <ui/ModuleIcon.h>
#include <weapons/EnergyWeapon.h>
#include <weapons/Missile.h>
#include <weapons/MissileLauncher.h>
#include <vector>

ModuleBuilder::ModuleBuilder(Game* game) : EntityBuilder(game)
{
	bulletBuilder = new BulletBuilder(game);
	shieldBuilder = (ShieldBuilder*) game->getBuilder("shield");
}

ModuleBuilder::~ModuleBuilder()
{

}

//Lazer = 0
//Missile = 1;
//Steering = 2;
//Engine = 3;
//Core = 4;
//Shield = 5;
//Gem Tractor = 6;
//Teleport = 7;

Module* ModuleBuilder::buildModule(vec2 code, vec2 pos)
{
	Module* module = 0;
	shieldBuilder = (ShieldBuilder*) game->getBuilder("shield");

	if (code == vec2(0,0))
	{
		Bullet* bullet = bulletBuilder->buildBullet(1,20,1,"green");
		module = new EnergyWeapon(10,1,bullet,game);
		module->setColour(getUIColour("red"));
		module->setIconName("lazerRoom");
		module->setName("Photon Lazer");
	}
	if (code == vec2(0,1))
	{
		Bullet* bullet = bulletBuilder->buildBullet(2,30,1,"magenta");
		module = new EnergyWeapon(7,1.5,bullet,game);
		module->setColour(getUIColour("red"));
		module->setIconName("lazerRoom");
		module->setName("Bozon Lazer!!");
	}
	if (code == vec2(1,0))
	{
		Bullet* bullet = new Missile( 0.1, 0.5, 0.98, MISSILE_DAMAGE, 10, 1, game );
		module = new MissileLauncher(50, 5, bullet, game);
		module->setColour(getUIColour("red"));
		module->setIconName("missileRoom");
		module->setName("Heat seeker");
	}
	if (code == vec2(2,0))
	{
		module = new Steering(0.05,game);
		module->setColour(getUIColour("cyan"));
		module->setIconName("steeringRoom");
		module->setName("Cockpit");
	}
	if (code == vec2(2,1))
	{
		module = new Steering(0.1,game);
		module->setColour(getUIColour("cyan"));
		module->setIconName("steeringRoom");
		module->setName("1st Class Cockpit");
	}
	if (code == vec2(3,0))
	{
		module = new Engine(0.3, 0.15, 0.15, 0.977, game);
		module->setColour(getUIColour("yellow"));
		module->setIconName("engineRoom");
		module->setName("Thruster");
	}
	if (code == vec2(3,1))
	{
		module = new Engine(0.6, 0.3, 0.3, 0.977, game);
		module->setColour(getUIColour("yellow"));
		module->setIconName("engineRoom");
		module->setName("Nitro Thruster");
	}
	if (code == vec2(4,0))
	{
		module = new EnergyCore(1000,3,200,game);
		module->setColour(getUIColour("green"));
		module->setIconName("energyRoom");
		module->setName("Energy Core");
	}
	if (code == vec2(4,1))
	{
		module = new EnergyCore(2000,5,200,game);
		module->setColour(getUIColour("green"));
		module->setIconName("energyRoom");
		module->setName("Fusion Core");
	}
	if (code == vec2(5,0))
	{
		module = shieldBuilder->buildShield();
		module->setColour(getUIColour("blue"));
		module->setIconName("shieldRoom");
		module->setName("Shield");
	}
	if (code == vec2(6,0))
	{
		module = new GemTractor(game);
		module->setColour(getUIColour("magenta"));
		module->setIconName("gemRoom");
		module->setName("Gem Tractor Beam");
	}
	if (code == vec2(7,0))
	{
		module = new Teleporter(game);
		module->setColour(getUIColour("magenta"));
		module->setIconName("teleportRoom");
		module->setName("Teleporter");
	}
	module->setPosition(vec3(pos,0));
	module->setCode(code.x,code.y);
	return module;
}

EntityList* ModuleBuilder::buildModules(EntityList* moduleIcons)
{
	EntityList* modules = new EntityList();
	for (int i=0; i<moduleIcons->size(); i++)
	{
		ModuleIcon* icon = (ModuleIcon*) moduleIcons->get(i);
		vec2 pos = vec2(icon->getPosition());
		pos.y -= 40;
		float scale = 2.0f / 47.0f;
		pos *= scale;
		vec2 code = icon->getCode();
		Module* module = buildModule(code,pos);
		modules->add(module);
	}
	return modules;
}

EntityList* ModuleBuilder::buildModules(vector<vec4> moduleCodes)
{
	EntityList* modules = new EntityList();
	for (int i=0; i<moduleCodes.size(); i++)
	{
		vec4 code = moduleCodes[i];
		vec2 pos = vec2(code.b,code.a);
		pos.y -= 40;
		float scale = 2.0f / 47.0f;
		pos *= scale;
		vec2 code1 = vec2(code.x,code.y);
		Module* module = buildModule(code1,pos);
		modules->add(module);
	}
	return modules;
}

vec3 ModuleBuilder::getUIColour(string colour)
{
	if (colour=="red") return vec3(0.5,0,0);
	if (colour=="green") return vec3(0,0.5,0);
	if (colour=="blue") return vec3(0.1,0.1,0.7);
	if (colour=="yellow") return vec3(0.5,0.5,0);
	if (colour=="cyan") return vec3(0,0.45,0.5);
	if (colour=="magenta") return vec3(0.45,0,0.45);
	if (colour=="grey") return vec3(0.4,0.4,0.4);
	return vec3(0,0,0);
}
