/*
 * ShipBuilder.cpp
 *
 *  Created on: 28 Oct 2014
 *      Author: mysticalOso
 */

#include <builders/BulletBuilder.h>
#include <builders/CloneBuilder.h>
#include <builders/InteriorBuilder.h>
#include <builders/ModuleBuilder.h>
#include <builders/ShieldBuilder.h>
#include <builders/ShipBuilder.h>
#include <osogine/game/Entity.h>
#include <osogine/game/Game.h>
#include <osogine/gfx/GfxBuilder.h>
#include <osogine/gfx/GfxObject.h>
#include <hubs/ShipHub.h>
#include <ship/EnergyCore.h>
#include <ship/Engine.h>
#include <ship/Ship.h>
#include <ship/Steering.h>
#include <weapons/Bullet.h>
#include <weapons/EnergyWeapon.h>

#include <glm/vec4.hpp>
#include <glm/vec3.hpp>
#include <glm/vec2.hpp>
#include <hubs/RaceHub.h>
#include <osogine/file/FileInterface.h>
#include <ship/DockingBay.h>
#include <ship/GemTractor.h>
#include <ship/Shield.h>
#include <ship/ShipTractor.h>
#include <weapons/Missile.h>
#include <weapons/MissileLauncher.h>
#include <vector>

#include <core/Constants.h>
#include <osogine/utils/Polygon.h>
#include <races/Race.h>
#include <ship/Module.h>

using namespace std;


ShipBuilder::ShipBuilder(Game* game) : EntityBuilder(game)
{
	interiorBuilder = 0;
	cloneBuilder = 0;
	hub = 0;
	moduleBuilder = (ModuleBuilder*) game->getBuilder("module");
}

ShipBuilder::~ShipBuilder()
{
}

Ship* ShipBuilder::buildShip(Polygon* outline, EntityList* moduleIcons)
{
	Ship* ship = new Ship(hub);

	GfxObject* gfx = buildGfx(outline->getOutline(),1);

	ship->setGfx(gfx);
	ship->setProgram("flat");
	ship->setOutline(outline);
	//ship->setPosition(100,100,0);
	ship->setPosition(0,40.0f/47.0f,0);
	ship->setName("new");

	//Printer::print("Area = ", ship->getArea(), "\n");

	ship->setMaxHealth(5);


	EntityList* modules = moduleBuilder->buildModules(moduleIcons);

	for (int i=0; i<modules->size(); i++)
	{
		Module* module = (Module*) modules->get(i);
		module->install(ship);
	}



	interiorBuilder = (InteriorBuilder*) game->getBuilder("interior");
	interiorBuilder->buildInterior(ship, modules);
//
	RaceHub* raceHub = (RaceHub*) game->getEntity("raceHub");
	Ship* playerShip = raceHub->getPlayerShip();
	hub->dock(ship,playerShip);

	return ship;

}

Ship* ShipBuilder::buildPlayer()
{
	Ship* ship = new Ship(hub);

	List<vec2>* outline = buildPlayerOutline();
	Polygon* p = new Polygon(outline);
	p->scaleOutline(1);

	GfxObject* gfx = buildGfx(outline);

	ship->setGfx(gfx);
	ship->setProgram("flat");
	ship->setOutline(outline);
	ship->setPosition(0,0,0);

	Module* core = moduleBuilder->buildModule(vec2(4,0),vec2(0,0));
	core->install(ship);

	Module* engine = moduleBuilder->buildModule(vec2(3,0),vec2(0,0));
	engine->install(ship);

	Module* steering = moduleBuilder->buildModule(vec2(2,0),vec2(0,0));
	steering->install(ship);

	//TODO should builders be made anywhere?
	Module* cannon = moduleBuilder->buildModule(vec2(0,0),vec2(0,0));
	cannon->setPosition(0,20,0);

	cannon->install(ship);


	Module* launcher = moduleBuilder->buildModule(vec2(1,0),vec2(0,0));
	launcher->install(ship);

	Module* shield = moduleBuilder->buildModule(vec2(5,0),vec2(0,0));
	shield->install(ship);

	Module* tractor = moduleBuilder->buildModule(vec2(6,0),vec2(0,0));
	tractor->install(ship);

	/*
	 * Module position = x = 0.000000  y = 20.000000  z = 0.000000
	Module code = x = 2.000000  y = 1.000000
	Module position = x = 672.000000  y = 200.000000  z = 0.000000
	Module code = x = 0.000000  y = 1.000000
	Module position = x = 0.000000  y = 140.000000  z = 0.000000
	Module code = x = 3.000000  y = 0.000000
	Module position = x = 0.000000  y = -100.000000  z = 0.000000
	Module code = x = 7.000000  y = 0.000000
	Module position = x = 108.000000  y = -160.000000  z = 0.000000
	Module code = x = 6.000000  y = 0.000000
	Module position = x = -108.000000  y = -160.000000  z = 0.000000
	 */

	EntityList* modules = new EntityList();

	vec3 pos = vec3(0,140,0);
	pos.y -= 20;
	float scale = 2.0f / 47.0f;
	pos *= scale;
	cannon->setPosition(pos);

	pos.y -= 240*scale;
	engine->setPosition(pos);

	pos.y -= 60*scale;
	pos.x += 108*scale;



	Module* teleporter = moduleBuilder->buildModule(vec2(7,0),vec2(0,0));
	teleporter->install(ship);

	teleporter->setPosition(pos);

	pos.x -= 216*scale;

	tractor->setPosition(pos);

	modules->add(teleporter);
	modules->add(steering);
	modules->add(engine);
	modules->add(tractor);
	modules->add(cannon);

	ship->setMaxHealth(80);
	ship->setHealth(80);


	for (int i=0; i<modules->size(); i++)
	{
		Module* module = (Module*) modules->get(i);
		module->move(0,75*scale,0);
	}

	interiorBuilder = (InteriorBuilder*) game->getBuilder("interior");
	interiorBuilder->buildInterior(ship, modules);

	ship->setAlpha(0.5);

	ship->setName("playerShip");

	return ship;

}

List<vec2>* ShipBuilder::buildPlayerOutline()
{
	List<vec2>* outline = new List<vec2>();
	outline->add(glm::vec2(0,20));
	outline->add(glm::vec2(7,0));
	outline->add(glm::vec2(15,-10));
	outline->add(glm::vec2(0,-7));
	outline->add(glm::vec2(-15,-10));
	outline->add(glm::vec2(-7,0));
	return outline;
}

void ShipBuilder::setHub(Hub* hub)
{
	this->hub = (ShipHub*) hub;
	init();
}

Ship* ShipBuilder::buildXacadianScout()
{
	Ship* ship = new Ship(hub);

	List<vec2>* outline = buildXacadianScoutOutline();

	GfxObject* gfx = buildGfx(outline);
	gfx->reColour(vec4(1,0,0,1),vec4(1,0,0.5,1));
	ship->setGfx(gfx);
	ship->setProgram("flat");
	ship->setOutline(outline);

	//TODO add ModuleBuilder!

	EnergyCore* core = new EnergyCore(1000,3,200,game);
	core->install(ship);

	Engine* engine = new Engine(0.4, 0.2, 0.2, 0.977,game);

	engine->install(ship);

	Steering* steering = new Steering(0.06,game);
	steering->install(ship);

	BulletBuilder* bulletBuilder = new BulletBuilder(game);
	Bullet* bullet = bulletBuilder->buildBullet(1,20,1,"green");
	EnergyWeapon* cannon = new EnergyWeapon(10,1,bullet,game);
	cannon->setPosition(0,20,0);

	cannon->install(ship);

	ship->setDriver( cloneBuilder->buildXacadienClone() );

	ship->setMaxHealth(3);

	ship->setName("scout");

	return ship;
}

Ship* ShipBuilder::buildXacadianMother()
{
	Ship* ship = new Ship(hub);

	List<vec2>* outline = buildXacadianMotherOutline();

	GfxObject* gfx = buildGfx(outline);
	gfx->reColour(vec4(0.7,0,0,1),vec4(1,0,0.3,1));
	ship->setGfx(gfx);
	ship->setProgram("flat");
	ship->setOutline(outline);

	//TODO add ModuleBuilder!

	Module* core = moduleBuilder->buildModule(vec2(4,0), vec2(0,0));
	core->install(ship);

	//Engine* engine = new Engine(1, 0.5, 0.5, 0.985,game);
	//Engine* engine = new Engine(1, 0.5, 0.5, 0.985, game);
	Module* engine = moduleBuilder->buildModule(vec2(3,1), vec2(0,-240));
	engine->install(ship);

	Module* steering = moduleBuilder->buildModule(vec2(2,0), vec2(0,240));
	steering->install(ship);

	Module* cannon = moduleBuilder->buildModule(vec2(0,0), vec2(-400,80));
	cannon->install(ship);

	Module* cannon2 = moduleBuilder->buildModule(vec2(0,0), vec2(400,80));
	cannon2->install(ship);

	ship->setMaxHealth(40);

	EntityList* modules = new EntityList();
	modules->add( steering );
	modules->add( cannon );
	modules->add( cannon2 );
	modules->add( engine );
	modules->add( core );

	float scale = 2.0f / 47.0f;

	for (int i=0; i<modules->size(); i++)
	{
		Module* module = (Module*) modules->get(i);

		//module->move(0,0,0);
		vec3 pos = module->getPosition();
		pos *= scale;
		module->setPosition(pos);
	}

	interiorBuilder = (InteriorBuilder*) game->getBuilder("interior");
	interiorBuilder->buildInterior(ship, modules);

	ship->setName("mother");

	//ship->setAlpha(0);

	return ship;
}

List<vec2>* ShipBuilder::buildXacadianScoutOutline()
{
	List<vec2>* outline = new List<vec2>();
	outline->add(glm::vec2(2,13));
	outline->add(glm::vec2(6,5));
	outline->add(glm::vec2(11,12));
	outline->add(glm::vec2(9,19));
	outline->add(glm::vec2(18,9));
	outline->add(glm::vec2(4,-8));
	outline->add(glm::vec2(-4,-8));
	outline->add(glm::vec2(-18,9));
	outline->add(glm::vec2(-9,19));
	outline->add(glm::vec2(-11,12));
	outline->add(glm::vec2(-6,5));
	outline->add(glm::vec2(-2,13));
	return outline;
}



List<vec2>* ShipBuilder::buildXacadianMotherOutline()
{
	List<vec2>* outline = new List<vec2>();
	outline->add(glm::vec2(4,16));
	outline->add(glm::vec2(12,6));
	outline->add(glm::vec2(22,18));
	outline->add(glm::vec2(16,34));
	outline->add(glm::vec2(32,24));
	outline->add(glm::vec2(22,-12));
	outline->add(glm::vec2(10,-8));
	outline->add(glm::vec2(6,-16));
	outline->add(glm::vec2(-6,-16));
	outline->add(glm::vec2(-10,-8));
	outline->add(glm::vec2(-22,-12));
	outline->add(glm::vec2(-32,24));
	outline->add(glm::vec2(-16,34));
	outline->add(glm::vec2(-22,18));
	outline->add(glm::vec2(-12,6));
	outline->add(glm::vec2(-4,16));
	return outline;
}



void ShipBuilder::init()
{
	cloneBuilder = (CloneBuilder*) game->getBuilder("clone");
}

Ship* ShipBuilder::buildPlayerStation()
{
	Ship* ship = new Ship(hub);

	List<vec2>* outline = buildPlayerStationOutline();

	GfxObject* gfx = buildGfx(outline,120);
	ship->setGfx(gfx);
	ship->setProgram("flat");
	ship->setOutline(outline);

	ship->setMaxHealth(1000000);
	RaceHub* raceHub = (RaceHub*) game->getEntity("raceHub");
	ship->setRace( raceHub->getRace("sentient") );

	DockingBay* dockingBay = new DockingBay(game);
	dockingBay->setPosition(0,0,0);
	dockingBay->install(ship);

	ShipTractor* shipTractor = new ShipTractor(game);
	shipTractor->install(ship);

	buildStationInterior(ship);
	ship->setName("station");

	ship->setPosition(-100,-100,0);

	ship->setRenderOrder(4);
	return ship;
}

List<vec2>* ShipBuilder::buildPlayerStationOutline()
{
//	vector<vec2> points = FileInterface::loadPoints(18);
//
//	for (unsigned int i = 0; i<points.size(); i++)
//	{
//		points[i].x += 2;
//		points[i] *= 0.4F;
//	}

	List<vec2>* outline = new List<vec2>();
	//list->setVector(points);

	outline->add(vec2(-144,-100));
	outline->add(vec2(-180,-40));
	outline->add(vec2(-168,-10));
	outline->add(vec2(-240,0));
	outline->add(vec2(-276,-40));
	outline->add(vec2(-348,-40));
	outline->add(vec2(-384,20));
	outline->add(vec2(-348,80));
	outline->add(vec2(-276,80));
	outline->add(vec2(-240,40));
	outline->add(vec2(-168,50));
	outline->add(vec2(-180,80));
	outline->add(vec2(-144,140));
	outline->add(vec2(-72,140));
	outline->add(vec2(-36,200));
	outline->add(vec2(36,200));
	outline->add(vec2(72,140));
	outline->add(vec2(144,140));
	outline->add(vec2(180,80));
	outline->add(vec2(168,50));
	outline->add(vec2(240,40));
	outline->add(vec2(276,80));
	outline->add(vec2(348,80));
	outline->add(vec2(384,20));
	outline->add(vec2(348,-40));
	outline->add(vec2(276,-40));
	outline->add(vec2(240,0));
	outline->add(vec2(168,-10));
	outline->add(vec2(180,-40));
	outline->add(vec2(144,-100));
	outline->add(vec2(72,-100));
	outline->add(vec2(36,-160));
	outline->add(vec2(-36,-160));
	outline->add(vec2(-72,-100));

	Polygon* p = new Polygon(outline);
	p->moveOutline(0,-20);
	p->scaleOutline(0.4);


	return outline;
}

void ShipBuilder::buildInterior(Ship* ship)
{
	interiorBuilder = (InteriorBuilder*) game->getBuilder("interior");
	interiorBuilder->buildInterior(ship);
}


void ShipBuilder::buildStationInterior(Ship* ship)
{
	interiorBuilder = (InteriorBuilder*) game->getBuilder("interior");
	interiorBuilder->buildStationInterior(ship);
}

GfxObject* ShipBuilder::buildGfx(Polygon* outline)
{
	return buildGfx(outline->getOutline(), 5);
}

GfxObject* ShipBuilder::buildGfx(List<vec2>* outline)
{
	return buildGfx(outline, 1);
}

GfxObject* ShipBuilder::buildGfx(List<vec2>* outline, float triSize)
{
	vec3 colour1 = vec3(0.7,0.8,1.0);
	vec3 colour2 = vec3(0.7,1.0,1.0);

	GfxObject* gfx = gfxBuilder->buildTriMesh(outline, triSize, colour1, colour2, true, false);

	gfx->reColour(vec4(colour1,1),vec4(colour2,1));

	return gfx;
}

Ship* ShipBuilder::buildShip(int type, Race* race)
{
	Ship* ship = new Ship(hub);


	vector<vec2> points = FileInterface::loadPoints(type);


	Polygon* p = new Polygon();



	for (uint i=0; i<points.size(); i++)
	{
		//Printer::print("Point = ",points[i],"\n");
		p->add(points[i].x, points[i].y);
	}

	p->moveOutline(0,-20);
	p->scaleOutline(2.0 / 47.0f);

	GfxObject* gfx = buildGfx(p->getOutline(),1);

	if (race->isRace("xacadiens")) gfx->reColour(vec4(0.7,0,0,1),vec4(1,0,0.3,1));

	ship->setGfx(gfx);
	ship->setProgram("flat");
	ship->setOutline(p);

	//ship->setName("new");

	//Printer::print("Area = ", ship->getArea(), "\n");

	ship->setMaxHealth(5);

	vector<vec4> moduleCodes = FileInterface::loadModules(type);


	EntityList* modules = moduleBuilder->buildModules(moduleCodes);

	for (int i=0; i<modules->size(); i++)
	{
		Module* module = (Module*) modules->get(i);
		module->install(ship);
	}



	interiorBuilder = (InteriorBuilder*) game->getBuilder("interior");
	interiorBuilder->buildInterior(ship, modules);


	return ship;
}
