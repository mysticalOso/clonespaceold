/*
 * CloudBuilder.h
 *
 *  Created on: 5 Nov 2014
 *      Author: Adam Nasralla
 */

#ifndef SRC_BUILDERS_CLOUDBUILDER_H_
#define SRC_BUILDERS_CLOUDBUILDER_H_

#include <osogine/game/EntityBuilder.h>
#include <osogine/utils/List.h>

class CloudHub;
class CloudGroup;
class DynamicTexture;

class CloudBuilder : public EntityBuilder
{
public:
	CloudBuilder(Game* game);
	~CloudBuilder();
	void setHub(Hub* hub);

	IEntity* build();
	CloudGroup* buildGroup();

	void initTextures(int count);


private:
	CloudHub* hub;
	List<string>* texturesA;
	List<string>* texturesB;
	CloudGroup* group;
	void buildTexture();
	void buildSimpleTexture(int index);
	string getTexture();
	float getDistAlpha(float x, float y);
};

#endif /* SRC_BUILDERS_CLOUDBUILDER_H_ */
