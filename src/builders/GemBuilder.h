/*
 * GemBuilder.h
 *
 *  Created on: 15 Jan 2015
 *      Author: mysticalOso
 */

#ifndef SRC_BUILDERS_GEMBUILDER_H_
#define SRC_BUILDERS_GEMBUILDER_H_

#include <glm/vec4.hpp>
#include <osogine/game/EntityBuilder.h>

using namespace glm;

class Polygon;
class Gem;
class GemHub;
class GfxObject;

class GemBuilder: public EntityBuilder {
public:
	GemBuilder(Game* game);
	virtual ~GemBuilder();
	void setHub(Hub* hub);

	Gem* buildGem(int colour);


private:
	Polygon* buildOutline(int quality, float scale, bool jitter);
	GfxObject* buildGfx(Polygon* outline, int colour);

	vec4 getColour(int colour);
	vec3 getBarRGB(int colour);


	GemHub* hub;
};

#endif /* SRC_BUILDERS_GEMBUILDER_H_ */
