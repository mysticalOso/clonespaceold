/*
 * InteriorBuilder.h
 *
 *  Created on: 3 Feb 2015
 *      Author: mysticalOso
 */

#ifndef SRC_BUILDERS_INTERIORBUILDER_H_
#define SRC_BUILDERS_INTERIORBUILDER_H_

#include <osogine/game/EntityBuilder.h>

class Interior;
class Ship;
class GfxObject;
class Polygon;
class RoomBuilder;
class Entity;
class RoomHub;
class EntityList;

class InteriorBuilder: public EntityBuilder {
public:
	InteriorBuilder(Game* game);
	virtual ~InteriorBuilder();

	Interior* buildStationInterior(Ship* ship);
	Interior* buildInterior(Ship* ship);

	Interior* buildInterior(Ship* ship, EntityList* modules);

private:

	GfxObject* buildInteriorGfx(Polygon* outline);

	Interior* buildEmptyInterior(Ship* ship);

	RoomBuilder* roomBuilder;


};

#endif /* SRC_BUILDERS_INTERIORBUILDER_H_ */
