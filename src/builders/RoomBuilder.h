/*
 * RoomBuilder.h
 *
 *  Created on: 7 Jul 2014
 *      Author: Adam Nasralla
 */

#ifndef ROOMBUILDER_H_
#define ROOMBUILDER_H_

#include <osogine/game/EntityBuilder.h>
#include <glm/vec2.hpp>
#include <vector>

using namespace std;
using namespace glm;

class Room;
class Interior;
class RoomHub;
class Module;

class RoomBuilder : public EntityBuilder
{
public:
	RoomBuilder(Game* game);
	~RoomBuilder();

	Room* buildRoom(float x1, float y1, vector<vec2> outline,
			RoomHub* hub, bool isExternal);

	Room* buildRoom(float x1, float y1, int type, float size,
			RoomHub* hub, bool isExternal);

	Room* buildRoom(Module* module, float size,
			RoomHub* hub);

	Room* buildRoom(Interior* interior);

};

#endif /* ROOMBUILDER_H_ */
