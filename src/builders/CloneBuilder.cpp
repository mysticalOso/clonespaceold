/*
 * CloneBuilder.cpp
 *
 *  Created on: 12 Jan 2015
 *      Author: mysticalOso
 */

#include <ai/CloneAI.h>
#include <builders/AIBuilder.h>
#include <builders/BulletBuilder.h>
#include <builders/CloneBuilder.h>
#include <clone/Clone.h>
#include <clone/CloneModel.h>
#include <hubs/CloneHub.h>
#include <hubs/RaceHub.h>
#include <osogine/game/Game.h>
#include <osogine/gfx/ObjLoader/ObjLoader.h>
#include <osogine/utils/ConvexHuller.h>
#include <osogine/utils/Polygon.h>
#include <ship/EnergyCore.h>
#include <ship/Engine.h>
#include <ship/Steering.h>
#include <weapons/Bullet.h>
#include <weapons/EnergyWeapon.h>

CloneBuilder::CloneBuilder(Game* game) : EntityBuilder(game)
{
	hub = 0;
	aiBuilder = 0;
	raceHub = 0;
}

CloneBuilder::~CloneBuilder()
{
}

Clone* CloneBuilder::buildSentientClone()
{
	Clone* clone = new Clone(game);
	clone->setRace( raceHub->getRace("sentient") );
	clone->setShipAI( aiBuilder->buildSentientAI() );
	//clone->addChild(new CloneModel(game));

	clone->setModel( new CloneModel(clone) ) ;
	clone->setIsDriver(false);
	clone->setProgram("clone");
	clone->setScale(0.0008,0.0008,0.0008);
	//clone->setRotationD(0,0,90);

	Engine* engine = new Engine(0.02,0.02,0.02,0.8,game);
	engine->toggleThrust(false);
	engine->install(clone);
	Steering* steering  = new Steering(0.1,game);
	steering->install(clone);

	clone->setTarget(0,10,false);

	//Polygon* p = new Polygon( ConvexHuller::getConvexHull( ObjLoader::getObject("passenger") ) );
	//p->scaleOutline(150);
	Polygon* p = new Polygon();
	p->add(-400,-50);
	p->add(0,550);
	p->add(400,-250);
	clone->setOutline( p );

	clone->setHealth(3);

	BulletBuilder* bulletBuilder = new BulletBuilder(game);
	Bullet* bullet = bulletBuilder->buildBullet(1,0.2,0.015,"green");
	EnergyWeapon* cannon = new EnergyWeapon(31,1,bullet,game);
	cannon->setPosition(0,0.5,0);

	EnergyCore* core = new EnergyCore(1000,3,200,game);
	core->install(clone);


	cannon->install(clone);
	//clone->setOutline( buildCloneOutline() );

	return clone;
}

Clone* CloneBuilder::buildXacadienClone()
{
	Clone* clone = new Clone(game);
	clone->setRace( raceHub->getRace("xacadiens") );
	clone->setShipAI( aiBuilder->buildSentientAI() );

	//clone->setPassengerGfx(ObjLoader::getObject("passenger"));
	//clone->setDriverGfx(ObjLoader::getObject("driver"));
	clone->setModel( new CloneModel(clone) ) ;
	clone->setIsDriver(false);
	clone->setProgram("clone");
	clone->setScale(0.001,0.001,0.001);
	//clone->setRotationD(0,0,90);

	clone->setHealth(3);

	Engine* engine = new Engine(0.02,0.02,0.02,0.8,game);
	engine->toggleThrust(false);
	engine->install(clone);
	Steering* steering  = new Steering(0.1,game);
	steering->install(clone);

	clone->setTarget(0,10,false);

	clone->setAI( new CloneAI(game) );

	Polygon* p = new Polygon();
	p->add(-400,-50);
	p->add(0,750);
	p->add(400,-250);
	clone->setOutline( p );

	BulletBuilder* bulletBuilder = new BulletBuilder(game);
	Bullet* bullet = bulletBuilder->buildBullet(1,0.2,0.015,"green");
	EnergyWeapon* cannon = new EnergyWeapon(31,1,bullet,game);
	cannon->setPosition(0,0.5,0);

	EnergyCore* core = new EnergyCore(1000,3,200,game);
	core->install(clone);


	cannon->install(clone);

	return clone;
}

void CloneBuilder::setHub(Hub* hub)
{
	this->hub = (CloneHub*) hub;
	init();
}

Polygon* CloneBuilder::buildCloneOutline()
{
	Polygon* polygon = new Polygon();
	float size = 4;
	polygon->add(vec2(-size,-size));
	polygon->add(vec2(-size,size));
	polygon->add(vec2(size,size));
	polygon->add(vec2(size,-size));
	return polygon;
}

void CloneBuilder::init()
{
	raceHub = (RaceHub*) game->getEntity("raceHub");
	aiBuilder = (AIBuilder*) game->getBuilder("ai");
}
