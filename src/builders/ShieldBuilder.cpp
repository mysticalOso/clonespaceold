/*
 * ShieldBuilder.cpp
 *
 *  Created on: 16 Jan 2015
 *      Author: mysticalOso
 */

#include <builders/ShieldBuilder.h>
#include <core/Constants.h>
#include <glm/vec2.hpp>
#include <glm/vec4.hpp>
#include <osogine/game/Entity.h>
#include <osogine/game/Game.h>
#include <osogine/gfx/GfxBuilder.h>
#include <osogine/gfx/GfxObject.h>
#include <osogine/utils/Polygon.h>
#include <ship/Shield.h>
#include <cmath>

ShieldBuilder::ShieldBuilder(Game* game) : EntityBuilder(game)
{

}

ShieldBuilder::~ShieldBuilder()
{

}

Shield* ShieldBuilder::buildShield()
{
	Polygon* outline = buildOutline();
	GfxObject* gfx = buildGfx(outline);

	Shield* shield = new Shield(game);

	shield->setGfx(gfx);
	shield->setOutline(outline);
	shield->setProgram("flat");

	return shield;
}

Polygon* ShieldBuilder::buildOutline()
{
	Polygon* outline = new Polygon();
	int i = 0;
	int noPoints =  20;
	float r = 27;
	float x1,y1,a;

	for (i=0; i<noPoints; i++)
	{
		a = (2*i*PI)/(noPoints);
		x1 = r*cos(a);
		y1 = r*sin(a);
		outline->add(vec2(x1,y1));
	}
	return outline;
}

GfxObject* ShieldBuilder::buildGfx(Polygon* outline)
{
	GfxObject* gfx = gfxBuilder->buildTriMesh(
			outline->getOutline(), 50 , true, false);

	gfx->reColour(vec4(0,0,0.8,1),vec4(0,0.5,1,1));


	gfx->setDepthEnabled(false);

	return gfx;
}
