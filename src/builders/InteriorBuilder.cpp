/*
 * InteriorBuilder.cpp
 *
 *  Created on: 3 Feb 2015
 *      Author: mysticalOso
 */

#include <builders/InteriorBuilder.h>
#include <builders/LightBuilder.h>
#include <builders/RoomBuilder.h>
#include <clone/Clone.h>
#include <hubs/RoomHub.h>
#include <hubs/LightHub.h>
#include <interior/Interior.h>
#include <interior/Room.h>
#include <osogine/game/Game.h>
#include <osogine/gfx/GfxBuilder.h>
#include <osogine/gfx/GfxObject.h>
#include <osogine/utils/Dice.h>
#include <osogine/utils/Polygon.h>
#include <osogine/utils/Printer.h>
#include <ship/Ship.h>
#include <core/Constants.h>
#include <hubs/UIHub.h>
#include <interior/Pod.h>
#include <ship/Incubator.h>
#include <ui/ModuleIcon.h>
#include <ui/ShipDesigner.h>

InteriorBuilder::InteriorBuilder(Game* game) : EntityBuilder(game)
{
	roomBuilder = new RoomBuilder(game);
}

InteriorBuilder::~InteriorBuilder() {
	// TODO Auto-generated destructor stub
}

Interior* InteriorBuilder::buildInterior(Ship* ship)
{
	Interior* interior = new Interior(ship);

	Polygon* outline = ship->getOutline();
	interior->setOutline( outline );

	interior->setGfx( buildInteriorGfx(outline) );

	interior->setProgram("flat");

//	RoomHub* roomHub = interior->getRoomHub();

	roomBuilder->buildRoom(interior);
//
//	roomBuilder->buildRoom(0,1.5,0,0.1,roomHub,false);
//	roomBuilder->buildRoom(0,2.5,0,0.1,roomHub,false);
//
//	roomBuilder->buildRoom(0,-0.25,0,0.1,roomHub,false);
//
//	roomBuilder->buildRoom(-1,-0.75,0,0.1,roomHub,false);
//	roomBuilder->buildRoom(1,-0.75,0,0.1,roomHub,false);
//	roomBuilder->buildRoom(-2,-1.25,0,0.1,roomHub,false);
//	roomBuilder->buildRoom(2,-1.25,0,0.1,roomHub,false);


	//roomBuilder->buildRoom(0,3.85,0,0.1,roomHub,false);
	if (LIGHTS)
	{
		LightHub* lightHub = interior->getLightHub();
		lightHub->refresh();
		lightHub->initLights();
	}
	return interior;

}

GfxObject* InteriorBuilder::buildInteriorGfx(Polygon* outline)
{
	vec3 colour1 = vec3(0.7,0.9,1.0) * 0.4f;
	vec3 colour2 = vec3(0.7,1.0,1.0) * 0.4f;

	GfxObject* gfx = gfxBuilder->buildTriMesh(
			outline->getOutline(), 100, colour1, colour2, false, false);

	gfx->reColour(vec4(colour1,1),vec4(colour2,1));

	return gfx;
}

Interior* InteriorBuilder::buildStationInterior(Ship* ship)
{
	Interior* interior = buildEmptyInterior(ship);

	RoomHub* roomHub = interior->getRoomHub();

	Room* room = roomBuilder->buildRoom(0,0.9,0,1,roomHub,false);
	room->positionPod(0,-21.75);
	Pod* pod = room->getPod();
	Entity* icon = room->getIcon();
	icon->setIsVisible(false);
	UIHub* uiHub = (UIHub*) game->getEntity("uiHub");
	pod->setUI(uiHub->getShipDesigner());


	room = roomBuilder->buildRoom(12,-40,0,0.3,roomHub,false);
	Pod* iPod = new Incubator(room);
	Pod* iPod2 = new Incubator(room);
	room->changePod(iPod,iPod2);
	icon = room->getIcon();
	icon->setIsVisible(false);

	room = roomBuilder->buildRoom(-12,-40,0,0.3,roomHub,false);
	icon = room->getIcon();
	icon->setIsVisible(false);

//	if (LIGHTS)
//	{
//		LightHub* lightHub = interior->getLightHub();
//		lightHub->refresh();
//		lightHub->initLights();
//	}


	return interior;

}

Interior* InteriorBuilder::buildInterior(Ship* ship, EntityList* modules)
{
	Interior* interior = buildEmptyInterior(ship);
	RoomHub* roomHub = interior->getRoomHub();

	for (int i=0; i<modules->size(); i++)
	{
		Module* module = (Module*) modules->get(i);
		roomBuilder->buildRoom(module, 6.0f / 46.0f ,roomHub);
	}

	if (LIGHTS)
	{
		LightHub* lightHub = interior->getLightHub();
		lightHub->refresh();
		lightHub->initLights();
	}
	return interior;
}

Interior* InteriorBuilder::buildEmptyInterior(Ship* ship)
{
	Interior* interior = new Interior(ship);
	Polygon* outline = ship->getOutline();
	interior->setOutline( outline );
	interior->setGfx( buildInteriorGfx(outline) );
	interior->setProgram("flat");
	roomBuilder->buildRoom(interior);

	return interior;
}
