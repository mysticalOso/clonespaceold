/*
 * AIBuilder.h
 *
 *  Created on: 12 Jan 2015
 *      Author: mysticalOso
 */

#ifndef SRC_BUILDERS_AIBUILDER_H_
#define SRC_BUILDERS_AIBUILDER_H_

#include <osogine/game/EntityBuilder.h>

class ShipAI;

class AIBuilder: public EntityBuilder {
public:
	AIBuilder(Game* game);
	virtual ~AIBuilder();

	ShipAI* buildXacadienScoutAI();
	ShipAI* buildSentientAI();
private:

};

#endif /* SRC_BUILDERS_AIBUILDER_H_ */
