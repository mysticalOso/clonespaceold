/*
 * AnimationBuilder.h
 *
 *  Created on: 26 Jul 2014
 *      Author: Adam Nasralla
 */

#ifndef CLONESPACEOLD_SRC_ANIMATIONBUILDER_H_
#define CLONESPACEOLD_SRC_ANIMATIONBUILDER_H_

class CloneAnimation;

class AnimationBuilder
{
public:
	AnimationBuilder(){}
	~AnimationBuilder(){}

	static CloneAnimation* buildShootAnimation();
	static CloneAnimation* buildShootReleaseAnimation();
	static CloneAnimation* buildDrawAnim();
	static CloneAnimation* buildSheathAnim();

};

#endif /* CLONESPACEOLD_SRC_ANIMATIONBUILDER_H_ */
