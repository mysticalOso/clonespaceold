/*
 * BulletBuilder.h
 *
 *  Created on: 31 Oct 2014
 *      Author: Adam Nasralla
 */

#ifndef BULLETBUILDER_H_
#define BULLETBUILDER_H_

#include <osogine/game/EntityBuilder.h>
#include <glm/fwd.hpp>

#include <map>

using namespace std;
using namespace glm;

class Bullet;
class GfxObject;
class Polygon;

class BulletBuilder: public EntityBuilder
{
public:
	BulletBuilder(Game* game);
	virtual ~BulletBuilder();

	Bullet* buildBullet(float speed, float scale, float damage, string colour);

private:

	GfxObject* getBulletGfx(string colourName);
	GfxObject* buildBulletGfx(vec3 colour);
	vec3 getColourByName(string name);
	Polygon* getBulletOutline(float speed, float size);

	map< string, GfxObject* > lazerGfx;

};

#endif /* BULLETBUILDER_H_ */
