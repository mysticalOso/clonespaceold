/*
 * LightBuilder.cpp
 *
 *  Created on: 23 Feb 2015
 *      Author: Adam Nasralla
 */

#include <builders/LightBuilder.h>
#include <glm/vec4.hpp>
#include <glm/vec3.hpp>
#include <glm/vec2.hpp>
#include <hubs/LightHub.h>
#include <hubs/RoomHub.h>
#include <interior/Door.h>
#include <interior/Room.h>
#include <interior/Walls.h>
#include <interior/WallSection.h>
#include <osogine/game/Entity.h>
#include <osogine/game/EntityList.h>
#include <osogine/game/Light.h>
#include <osogine/gfx/GfxObject.h>
#include <osogine/utils/List.h>
#include <osogine/utils/Polygon.h>
#include <osogine/utils/Printer.h>
#include <osogine/utils/Utils.h>
#include <test/EndPoint.h>
#include <test/Segment.h>
#include <cmath>
#include <vector>

LightBuilder::LightBuilder(Game* game) : EntityBuilder(game)
{
	gfx = 0;
	outline = 0;
	walls = 0;
	walls2 = 0;
	light = 0;
	lightHub = 0;
	lightSize = 5;
	roomHub = 0;
}

LightBuilder::~LightBuilder()
{
	// TODO Auto-generated destructor stub
}

Light* LightBuilder::buildLight(vec3 position, vec3 colour)
{
	light = new Light(lightHub);
	light->setLightPosition(position);
	light->setColour(colour);
	light->setTexture("light");
	light->setProgram("light");
	light->setTextureAlpha(0.7);
	center = vec2(position);
	gfx = new GfxObject();
	outline = new Polygon();
	updateEdgeSegments();
	updateAngles(center.x,center.y);
	sweep();
	light->setGfx(gfx);
	light->setOutline(outline);
	//Printer::print("Outline Size",outline->size(),"\n");
	//light->getGfx()->print();
	light->setRenderOrder(2);
	return light;
}

void LightBuilder::updateLight(Light* light)
{
	center = vec2(light->getLightPosition());
	gfx = light->getGfx();
	gfx->clearAll();
	outline = light->getOutline();
	outline->clear();
	updateEdgeSegments();
	trimSegments();
	updateAngles(center.x,center.y);
	sweep();
}

void LightBuilder::refresh()
{
	initSegments();
}


bool compByAngle(EndPoint* a, EndPoint* b)
{
	return ((a->angle < b->angle) || (a->angle == b->angle && a->begin && !b->begin));
}

void LightBuilder::updateAngles(float x, float y)
{
	for (int i=0; i<segments2.size(); i++)
	{
		Segment* segment = segments2.get(i);
		float dx = 0.5f * (segment->p1->x + segment->p2->x) - x;
		float dy = 0.5f * (segment->p1->y + segment->p2->y) - y;

		segment->d = dx*dx + dy*dy;

		// NOTE: future optimization: we could record the quadrant
		// and the y/x or x/y ratio, and sort by (quadrant,
		// ratio), instead of calling atan2. See
		// <https://github.com/mikolalysenko/compare-slope> for a
		// library that does this. Alternatively, calculate the
		// angles and use bucket sort to get an O(N) sort.
		// also remove doubles!

		segment->p1->angle = atan2(segment->p1->y - y, segment->p1->x - x);
		segment->p2->angle = atan2(segment->p2->y - y, segment->p2->x - x);

		//Printer::print("P1Angle", segment->p1->angle, "\n");
		//Printer::print("P2Angle", segment->p2->angle, "\n");

		float dAngle = segment->p2->angle - segment->p1->angle;
		if (dAngle <= -PI) { dAngle += 2*PI; }
		if (dAngle > PI) { dAngle -= 2*PI; }
		segment->p1->begin = (dAngle > 0.0);
		segment->p2->begin = !segment->p1->begin;
	}



	vector<EndPoint*> toSort;
	//Printer::print("EndPoint Size = ",endPoints.size(),"\n");
	//Printer::print("EndPoint2 Size = ",endPoints2.size(),"\n");
	for (int i=0; i<endPoints2.size(); i++)
	{
		toSort.push_back(endPoints2.get(i));
	}
	sort(toSort.begin(), toSort.end(), compByAngle);

	endPoints2.setVector(toSort);

}

void LightBuilder::addSegment(float x1, float y1, float x2, float y2)
{

    Segment* segment = new Segment();
    EndPoint* p1 = new EndPoint();
    EndPoint* p2 = new EndPoint();

    p1->x = x1; p1->y = y1;
    p2->x = x2; p2->y = y2;

    p1->segment = segment;
    p2->segment = segment;
    segment->p1 = p1;
    segment->p2 = p2;
    segment->d = 0.0;

    segments.add(segment);
    endPoints.add(p1);
    endPoints.add(p2);

}

bool LightBuilder::segmentInFront(Segment a, Segment b, vec2 relativeTo)
{
	vec2 p1 = vec2(b.p1->x,b.p1->y);
	vec2 p2 = vec2(b.p2->x,b.p2->y);
	bool A1 = leftOf(a, Utils::interpolate(p1, p2, 0.01));
	bool A2 = leftOf(a, Utils::interpolate(p2, p1, 0.01));
	bool A3 = leftOf(a, relativeTo);
	p1 = vec2(a.p1->x,a.p1->y);
	p2 = vec2(a.p2->x,a.p2->y);
	bool B1 = leftOf(b, Utils::interpolate(p1, p2, 0.01));
	bool B2 = leftOf(b, Utils::interpolate(p2, p1, 0.01));
	bool B3 = leftOf(b, relativeTo);

	// NOTE: this algorithm is probably worthy of a short article
	// but for now, draw it on paper to see how it works. Consider
	// the line A1-A2. If both B1 and B2 are on one side and
	// relativeTo is on the other side, then A is in between the
	// viewer and B. We can do the same with B1-B2: if A1 and A2
	// are on one side, and relativeTo is on the other side, then
	// B is in between the viewer and A.
	if (B1 == B2 && B2 != B3) return true;
	if (A1 == A2 && A2 == A3) return true;
	if (A1 == A2 && A2 != A3) return false;
	if (B1 == B2 && B2 == B3) return false;

	// If A1 != A2 and B1 != B2 then we have an intersection.
	// Expose it for the GUI to show a message. A more robust
	// implementation would split segments at intersections so
	// that part of the segment is in front and part is behind.
	return false;

}

bool LightBuilder::leftOf(Segment s, vec2 p)
{
	float cross = (s.p2->x - s.p1->x) * (p.y - s.p1->y)
                          - (s.p2->y - s.p1->y) * (p.x - s.p1->x);
	return cross < 0;

}

vec2 LightBuilder::lineIntersection(vec2 p1, vec2 p2, vec2 p3, vec2 p4)
{
//	if ( ((p4.y - p3.y) * (p2.x - p1.x) - (p4.x - p3.x) * (p2.y - p1.y)) == 0 )
//	{
//		float i = 0;
//	}
	float s = ((p4.x - p3.x) * (p1.y - p3.y) - (p4.y - p3.y) * (p1.x - p3.x))
                    / ((p4.y - p3.y) * (p2.x - p1.x) - (p4.x - p3.x) * (p2.y - p1.y));
	return vec2(p1.x + s * (p2.x - p1.x), p1.y + s * (p2.y - p1.y));
}

void LightBuilder::addTriangle(float angle1, float angle2, Segment* segment)
{
    vec2 p1 = center;
    vec2 p2 = vec2(center.x + cos(angle1), center.y + sin(angle1));
    vec2 p3 = vec2(0.0, 0.0);
    vec2 p4 = vec2(0.0, 0.0);

    if (segment != 0) {
        // Stop the triangle at the intersecting segment
        p3.x = segment->p1->x;
        p3.y = segment->p1->y;
        p4.x = segment->p2->x;
        p4.y = segment->p2->y;
    } else {
        // Stop the triangle at a fixed distance; this probably is
        // not what we want, but it never gets used in the demo
        p3.x = center.x + cos(angle1) * lightSize;
        p3.y = center.y + sin(angle1) * lightSize;
        p4.x = center.x + cos(angle2) * lightSize;
        p4.y = center.y + sin(angle2) * lightSize;
    }

    vec2 pBegin = lineIntersection(p3, p4, p1, p2);

    p2.x = center.x + cos(angle2);
    p2.y = center.y + sin(angle2);
    vec2 pEnd = lineIntersection(p3, p4, p1, p2);

    vec2 topRight = center + vec2(lightSize,lightSize);
    vec2 bottomLeft = center - vec2(lightSize,lightSize);

    pBegin = clamp(pBegin,topRight,bottomLeft);
    pEnd = clamp(pEnd,topRight,bottomLeft);
    gfx->addTriangle(vec3(center,0),vec3(pBegin,0),vec3(pEnd,0),vec4(0.3,0.3,0.5,1));


    outline->add(center);
    outline->add(pBegin);
    outline->add(pEnd);

    //    outline->add(center);
//    outline->add(pBegin);
//    outline->add(pEnd);
//    outline->add(center);
}

void LightBuilder::sweep()
{
	open.clear();

	gfx->clearAll();

	outline->clear();

	//outline->setIsLine(true);
	float beginAngle = 0;


	for (int pass = 0; pass < 2; pass++)
	{
		for (int i = 0; i < endPoints2.size(); i++)
		{
			EndPoint p = *(endPoints2.get(i));
			Segment* currentOld = 0;
			if (open.size()>0) currentOld = open.get(0);

            if (p.begin) {
                // Insert into the right place in the list
                Segment* node = 0;
    			if (open.size()>0) node = open.get(0);
                int index = 0;
                while (node != 0 && segmentInFront(*p.segment, *node, center) && index<open.size()-1)
                {
                	index++;
                    node = open.get(index);

                }
                if (node == 0) {
                    open.add(p.segment);
                } else {
                    open.insert(index, p.segment);
                }
            }
            else {
                open.remove(p.segment);
            }


            Segment* currentNew = 0;
            if (open.size()>0) currentNew = open.get(0);
            if (currentOld != currentNew) {
                if (pass == 1) {
                    addTriangle(beginAngle, p.angle, currentOld);
                }
                beginAngle = p.angle;
            }
		}
	}
	addUVs();

	//Printer::print("Outline size = ",outline->size(),"\n");

	//outline->print();
	//light->program->setLightMap(outline);
	//gfx->print();
	//Printer::print("Centre ",center,"\n");
}

void LightBuilder::updateSegment(int index, float x1, float y1, float x2,
		float y2)
{
    Segment* segment = segments.get(index);
    EndPoint* p1 = segment->p1;
    EndPoint* p2 = segment->p2;

    p1->x = x1; p1->y = y1;
    p2->x = x2; p2->y = y2;
}

void LightBuilder::updateEdgeSegments()
{
	int n = segments.size();
	float cx = center.x;
	float cy = center.y;

	float lightSize = 5;

	updateSegment(n-4,cx-lightSize,cy-lightSize,cx+lightSize,cy-lightSize);
	updateSegment(n-3,cx+lightSize,cy-lightSize,cx+lightSize,cy+lightSize);
	updateSegment(n-2,cx+lightSize,cy+lightSize,cx-lightSize,cy+lightSize);
	updateSegment(n-1,cx-lightSize,cy+lightSize,cx-lightSize,cy-lightSize);

}

void LightBuilder::addUVs()
{
	vec2 topLeft = center - vec2(lightSize,lightSize);
	vec2 bottomRight = center + vec2(lightSize,lightSize);
	for (uint i=0; i<gfx->getNoVertices(); i++)
	{
		vec2 v = vec2(gfx->getVertex(i));
		vec2 uv = (v-topLeft) / (bottomRight-topLeft);
		gfx->addUV(uv);
	}
}

void LightBuilder::initSegments()
{

	clearSegments( segments );
	clearEndpoints( endPoints );
	segments.clear();
	endPoints.clear();
	open.clear();

	int totalSize = 0;
	for (int i=0; i<roomHub->size(); i++)
	{
		Room* room = roomHub->getRoom(i);
		Walls* walls = room->getWalls();
		EntityList* sections = walls->getSections();
		Transform* trans = walls->getTransform();

		for (int i=0; i<sections->size(); i++)
		{
			WallSection* section = (WallSection*) sections->get(i);
			Polygon* outline = section->getOutline();
			//totalSize += outline->size();
			outline->setTransform(trans);
			outline->updateTransformedOutline();
			for (int j=0; j<outline->size(); j++)
			{
				vec2 p = outline->getTransformed(j);
				vec2 p2 = outline->getTransformed( (j+1)%outline->size() );
				addSegment(p.x,p.y,p2.x,p2.y);
			}
		}

		EntityList* doors = room->getDoors();
		for (int i=0; i<doors->size(); i++)
		{
			Door* door = (Door*) doors->get(i);
			door->updateSections();
			Polygon* lSection = door->getLeftSection();
			lSection->setTransform(trans);
			lSection->updateTransformedOutline();
			totalSize += lSection->size();
			for (int j=0; j<lSection->size(); j++)
			{
				vec2 p = lSection->getTransformed(j);
				vec2 p2 = lSection->getTransformed( (j+1)%lSection->size() );
				addSegment(p.x,p.y,p2.x,p2.y);
			}
			Polygon* rSection = door->getRightSection();
			rSection->setTransform(trans);
			rSection->updateTransformedOutline();
			totalSize += rSection->size();
			for (int j=0; j<rSection->size(); j++)
			{
				vec2 p = rSection->getTransformed(j);
				vec2 p2 = rSection->getTransformed( (j+1)%lSection->size() );
				addSegment(p.x,p.y,p2.x,p2.y);
			}
		}

	}
	addSegment(0,0,0,0);
	addSegment(0,0,0,0);
	addSegment(0,0,0,0);
	addSegment(0,0,0,0);

	//Printer::print("Total Size = ",totalSize,"\n");
}

void LightBuilder::setHub(Hub* hub)
{
	lightHub = (LightHub*) hub;
	roomHub = lightHub->getRoomHub();
}

vec2 LightBuilder::clamp(vec2 p, vec2 topRight, vec2 bottomLeft)
{
	p -= center;
	topRight -= center;
	bottomLeft -= center;
	if (p.x > topRight.x)
	{
		p.y *= (topRight.x / p.x);
		p.x = topRight.x;
	}
	if (p.y > topRight.y)
	{
		p.x *= (topRight.y / p.y);
		p.y = topRight.y;
	}
	if (p.x < bottomLeft.x)
	{
		p.y *= (bottomLeft.x / p.x);
		p.x = bottomLeft.x;
	}
	if (p.y < bottomLeft.y)
	{
		p.x *= (bottomLeft.y / p.y);
		p.y = bottomLeft.y;
	}
	p += center;
	return p;
}

void LightBuilder::trimSegments()
{
	segments2.clear();
	endPoints2.clear();
	for (int i=0; i<segments.size(); i++)
	{
		Segment* segment = segments.get(i);
		EndPoint* p1 = segment->p1;
		EndPoint* p2 = segment->p2;
		bool p1Inside = false;
		bool p2Inside = false;
		if (p1->x <= center.x + 5 && p1->x >= center.x - 5 && p1->y <= center.y + 5 && p1->y >= center.y - 5)
		{
			p1Inside = true;
		}
		if (p2->x <= center.x + 5 && p2->x >= center.x - 5 && p2->y <= center.y + 5 && p2->y >= center.y - 5)
		{
			p2Inside = true;
		}
		if (p1Inside || p2Inside)
		{
			endPoints2.add(p1);
			endPoints2.add(p2);
			segments2.add(segment);
		}
	}

}

void LightBuilder::clear()
{

}

void LightBuilder::clearSegments(List<Segment*> segments)
{
	while(segments.size()>0)
	{
		Segment* s = segments.get(0);
		segments.erase(0);
		delete s;
	}
	segments.clear();
}

void LightBuilder::clearEndpoints(List<EndPoint*> endPoints)
{
	while(endPoints.size()>0)
	{
		EndPoint* p = endPoints.get(0);
		endPoints.erase(0);
		delete p;
	}
	endPoints.clear();
}
