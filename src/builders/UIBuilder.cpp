/*
 * UIBuilder.cpp
 *
 *  Created on: 27 Feb 2015
 *      Author: Adam Nasralla
 */

#include <builders/UIBuilder.h>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <osogine/game/EntityList.h>
#include <osogine/game/TextureEntity.h>
#include <osogine/gfx/GfxBuilder.h>
#include <osogine/utils/Polygon.h>
#include <ui/Button.h>
#include <ui/CostBar.h>
#include <ui/ModuleIcon.h>
#include <core/Constants.h>
#include <ui/OutlinePoint.h>

UIBuilder::UIBuilder(Game* game) : EntityBuilder(game)
{
	// TODO Auto-generated constructor stub

}

UIBuilder::~UIBuilder()
{
	// TODO Auto-generated destructor stub
}

EntityList* UIBuilder::buildModuleIcons(UIEntity* parent)
{
	EntityList* icons = new EntityList();
	icons->add( buildModuleIcon( parent, vec2(0,0) ) );
	icons->add( buildModuleIcon( parent, vec2(0,1) ) );
	icons->add( buildModuleIcon( parent, vec2(1,0) ) );
	icons->add( buildModuleIcon( parent, vec2(2,0) ) );
	icons->add( buildModuleIcon( parent, vec2(2,1) ) );
	icons->add( buildModuleIcon( parent, vec2(3,0) ) );
	icons->add( buildModuleIcon( parent, vec2(3,1) ) );
	icons->add( buildModuleIcon( parent, vec2(4,0) ) );
	icons->add( buildModuleIcon( parent, vec2(4,1) ) );
	icons->add( buildModuleIcon( parent, vec2(5,0) ) );
	icons->add( buildModuleIcon( parent, vec2(6,0) ) );
	icons->add( buildModuleIcon( parent, vec2(7,0) ) );
	return icons;
}

ModuleIcon* UIBuilder::buildModuleIcon(UIEntity* parent, vec2 code)
{
	ModuleIcon* icon = buildEmptyModuleIcon(parent, code);

	if (code == vec2(0,0))
	{
		icon->initQuad(10,85,144,120);
		icon->setTopLeft(1416,220);
		icon->setTextureColour( getUIColour("red") );
		icon->setName("Photon Lazer");
	}
	if (code == vec2(0,1))
	{
		icon->initQuad(10,85,144,120);
		icon->setTopLeft(1416,100);
		icon->setTextureColour( getUIColour("red") );
		icon->setGlowColour( getUIColour("blue") );
		icon->setName("Bozon Lazer!!");
	}
	if (code == vec2(1,0))
	{
		icon->initQuad(460,85,144,120);
		icon->setTopLeft(1308,40);
		icon->setTextureColour( getUIColour("red") );
		icon->setName("Heat seeker");
	}
	if (code == vec2(2,0))
	{
		icon->initQuad(760,85,144,120);
		icon->setTopLeft(1524,400);
		icon->setTextureColour( getUIColour("cyan") );
		icon->setName("Cockpit");
	}

	if (code == vec2(2,1))
	{
		icon->initQuad(760,85,144,120);
		icon->setTopLeft(1524,280);
		icon->setTextureColour( getUIColour("cyan") );
		icon->setGlowColour( vec3(1,0,0) );
		icon->setName("1st Class Cockpit");
	}
	if (code == vec2(3,0))
	{
		icon->initQuad(160,215,144,120);
		icon->setTopLeft(1524,160);
		icon->setTextureColour( getUIColour("yellow") );
		icon->setName("Thruster");
	}
	if (code == vec2(3,1))
	{
		icon->initQuad(160,215,144,120);
		icon->setTopLeft(1524,40);
		icon->setTextureColour( getUIColour("yellow") );
		icon->setGlowColour( getUIColour("red") );
		icon->setName("Nitro Thruster");
	}
	if (code == vec2(4,0))
	{
		icon->initQuad(310,215,144,120);
		icon->setTopLeft(1632,220);
		icon->setTextureColour( getUIColour("green") );
		icon->setName("Energy Core");
	}
	if (code == vec2(4,1))
	{
		icon->initQuad(310,215,144,120);
		icon->setTopLeft(1632,100);
		icon->setTextureColour(  getUIColour("green") );
		icon->setGlowColour( getUIColour("blue") );
		icon->setName("Fusion Core");
	}
	if (code == vec2(5,0))
	{
		icon->initQuad(160,85,144,120);
		icon->setTopLeft(1632,340);
		icon->setTextureColour( getUIColour("blue") );
		icon->setName("Shield");
	}
	if (code == vec2(6,0))
	{
		icon->initQuad(610,85,144,120);
		icon->setTopLeft(1740,400);
		icon->setTextureColour( getUIColour("magenta") );
		icon->setName("Gem Tractor Beam");
	}
	if (code == vec2(7,0))
	{
		icon->initQuad(310,85,144,120);
		icon->setTopLeft(1740,280);
		icon->setTextureColour( getUIColour("magenta") );
		icon->setName("Teleporter");
	}


	return icon;
}

void UIBuilder::buildStartingOutline(UIEntity* parent)
{
	buildOutlinePoint(parent,-252,-200);
	buildOutlinePoint(parent, 0,300);
	buildOutlinePoint(parent,252,-200);
}

OutlinePoint* UIBuilder::buildOutlinePoint(UIEntity* parent, float x, float y)
{
	OutlinePoint* point = new OutlinePoint(parent);
	point->initQuad(519,15,40,32);
	point->setAlphaModifier(0.5);
	Polygon* outline = new Polygon();
	outline->add(-19,1);
	outline->add(-10,16);
	outline->add(9,15);
	outline->add(18,0);
	outline->add(8,-15);
	outline->add(-10,-15);
	point->setOutline(outline);
	point->setPosition(x,y,0);
	return point;
}

ModuleIcon* UIBuilder::buildEmptyModuleIcon(UIEntity* parent, vec2 code)
{
	ModuleIcon* icon = new ModuleIcon(parent, code);
	icon->setEdge( buildModuleIconEdge(icon) );
	icon->setOutline( buildModuleIconOutline() );
	return icon;
}

Polygon* UIBuilder::buildModuleIconOutline()
{
	Polygon* outline = new Polygon();
	outline->add(-35,-58);
	outline->add(33,-57);
	outline->add(67,-1);
	outline->add(34,57);
	outline->add(-34,57);
	outline->add(-69,0);
	return outline;
}

TextureEntity* UIBuilder::buildModuleIconEdge(ModuleIcon* parent)
{
	UIEntity* edge = new UIEntity(parent);
	edge->setTexture("uiIcons",1024);
	edge->setDisableCamera(true);
	edge->initQuad(7,212,150,126);
	return edge;
}

vec3 UIBuilder::getUIColour(string colour)
{
	if (colour=="red") return vec3(0.5,0,0);
	if (colour=="green") return vec3(0,0.5,0);
	if (colour=="blue") return vec3(0.1,0.1,0.7);
	if (colour=="yellow") return vec3(0.5,0.5,0);
	if (colour=="cyan") return vec3(0,0.45,0.5);
	if (colour=="magenta") return vec3(0.45,0,0.45);
	if (colour=="grey") return vec3(0.4,0.4,0.4);
	return vec3(0,0,0);
}


Button* UIBuilder::buildButton(UIEntity* parent, string name)
{
	Button* button;
	if (name == "exit" )
	{
		button = new Button(parent,"quit");
		button->initQuad(6,5,244,71);
		button->addOutline(-122,36);
		button->addOutline(121,36);
		button->addOutline(98,-6);
		button->addOutline(121,-35);
		button->addOutline(-81,-36);
		button->setTopLeft(684,1001);
	}
	if (name == "build" )
	{
		button = new Button(parent,"build");
		button->initQuad(258,5,244,71);
		button->addOutline(-122,35);
		button->addOutline(121,35);
		button->addOutline(79,-35);
		button->addOutline(-119,-34);
		button->addOutline(-99,-3);
		button->setTopLeft(996,1001);
	}
	return button;
}


EntityList* UIBuilder::buildCostBars(Entity* parent)
{
	EntityList* bars = new EntityList();
	bars->add( buildCostBar(parent, "grey", 6));
	bars->add( buildCostBar(parent, "red", 5));
	bars->add( buildCostBar(parent, "green", 4));
	bars->add( buildCostBar(parent, "blue", 3));
	bars->add( buildCostBar(parent, "yellow", 2));
	bars->add( buildCostBar(parent, "cyan", 1));
	bars->add( buildCostBar(parent, "magenta", 0));
	return bars;
}

CostBar* UIBuilder::buildCostBar(Entity* parent, string colour, int index)
{
	vec2 offset = vec2(index * 36 - WIDTH2, HEIGHT2);
	vec2 p1 = vec2(134,-535) + offset;
	vec2 p2 = vec2(158,-535) + offset;
	vec2 p3 = vec2(422,-975) + offset;
	vec2 p4 = vec2(398,-975) + offset;
	vec3 c = getUIColour(colour);
	GfxObject* gfx = gfxBuilder->buildQuad(p1,p2,p3,p4, vec4(c,0.4) );

	CostBar* bar = new CostBar(parent);
	bar->setGfx(gfx);
	bar->setProgram("simple");
	return bar;
}


