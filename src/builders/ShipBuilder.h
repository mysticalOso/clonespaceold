/*
 * ShipBuilder.h
 *
 *  Created on: 28 Oct 2014
 *      Author: mysticalOso
 */

#ifndef SHIPBUILDER_H_
#define SHIPBUILDER_H_

#include <osogine/game/EntityBuilder.h>
#include <osogine/utils/List.h>

class Ship;
class GfxObject;
class Hub;
class ShipHub;
class CloneBuilder;
class Polygon;
class EntityList;
class InteriorBuilder;
class ModuleBuilder;
class Race;

class ShipBuilder: public EntityBuilder
{
public:
	ShipBuilder(Game* game);
	~ShipBuilder();
	void setHub(Hub* hub);
	Ship* buildPlayer();
	Ship* buildXacadianScout();
	Ship* buildXacadianMother();
	Ship* buildPlayerStation();
	Ship* buildShip(Polygon* outline, EntityList* moduleIcons);
	Ship* buildShip(int type, Race* race);
private:

	void init();

	InteriorBuilder* interiorBuilder;
	CloneBuilder* cloneBuilder;
	ModuleBuilder* moduleBuilder;
	ShipHub* hub;
	List<vec2>* buildPlayerOutline();
	List<vec2>* buildXacadianScoutOutline();
	List<vec2>* buildXacadianMotherOutline();
	List<vec2>* buildPlayerStationOutline();

	GfxObject* buildGfx(Polygon* outline);
	GfxObject* buildGfx(List<vec2>* outline);
	GfxObject* buildGfx(List<vec2>* outline, float triSize);
	void buildInterior(Ship* ship);
	void buildStationInterior(Ship* ship);
};

#endif /* SHIPBUILDER_H_ */
