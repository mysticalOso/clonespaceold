/*
 * CloudBuilder.cpp
 *
 *  Created on: 5 Nov 2014
 *      Author: Adam Nasralla
 */

#include <builders/CloudBuilder.h>
#include <glm/vec4.hpp>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <hubs/CloudHub.h>
#include <osogine/game/Entity.h>
#include <osogine/game/Game.h>
#include <osogine/gfx/DynamicTexture.h>
#include <osogine/gfx/GfxBuilder.h>
#include <osogine/gfx/GfxHub.h>
#include <osogine/utils/Dice.h>
#include <osogine/utils/Printer.h>
#include <osogine/utils/SimplexNoise.h>
#include <osogine/utils/Utils.h>
#include <space/Cloud.h>
#include <space/CloudGroup.h>

using namespace glm;

CloudBuilder::CloudBuilder(Game* game) : EntityBuilder(game)
{
	hub = 0;
	group = 0;
	texturesA = new List<string>();
	texturesB = new List<string>();
}

CloudBuilder::~CloudBuilder()
{
}


CloudGroup* CloudBuilder::buildGroup()
{
	group = new CloudGroup(game);
	group->setScale(2,2,1);
	float range = 100;
	float speed = 1;
	for (int i = 0; i < 5; i++)
	{
		Cloud* cloud = (Cloud*) build();
		cloud->setPosition( Dice::roll(vec3(-range,-range,0),vec3(range,range,0)) );
		cloud->setVelocity( Dice::roll(vec3(-speed,-speed,0),vec3(speed,speed,0)) );
	}
	return group;
}

IEntity* CloudBuilder::build()
{
	float size = 512;
	Cloud* cloud;
	if (group!=0)
	{
		//Printer::print("Building with hub\n");
		cloud = new Cloud(group);
	}
	else
	{
		//Printer::print("Building with game\n");
		cloud = new Cloud(game);
	}
	GfxObject* gfx = gfxBuilder->buildQuad(size,size,vec4(1,1,1,1));
	cloud->setGfx( gfx );
	cloud->setTexture(getTexture());
	cloud->setProgram("texture");

	return cloud;
}

void CloudBuilder::buildTexture()
{
	DynamicTexture* texture = new DynamicTexture();


	float	t1 = 12;
	float 	t2 = 1000;
	float 	n1 = 0.5;
	float	n2 = 0.05;
	float thickness = 40;

	int i, j, c, d, e;

	for (i = 0; i < 128; i++) {
	  for (j = 0; j < 128; j++) {
		// c = ((((i&0x8)==0)^((j&0x8))==0))*255;
		 double x = (double)j/((double)512);
		 double y = (double)i/((double)512);



		 // Typical Perlin noise
		 double n = (1+n1)*SimplexNoise::noise(x*t1,  y*t1, 0.5)-(n2+n1);
		 double m = (1+n1)*SimplexNoise::noise(x*t1,  y*t1, 1.5)-(n2+n1);
		 double p = (1+n1)*SimplexNoise::noise(x*t1,  y*t1, 1.0)-(n2+n1);
		 double q = n2*SimplexNoise::noise(x*t2, y*t2, 3); //+ 0.1*pn.noise(x*t2*2, y*t2*2, 5) + 0.1*pn.noise(x*t2*4, y*t2*4, 7);
		 if (n<0) n = 0; else n += q;
		 if (m<0) m = 0; else m += q;
		 if (p<0) p = 0; else p += q;
		 //n = 20 * pn.noise(x, y, 0.8);
		 //n = n - floor(n);

//		 if (n>0.8)
//		 {
//			 d = e = c = 255;
//		 }
//		 else
//		 {
			 c = floor(255 * n);
			 d = floor(255 * m);
			 e = floor(255 * p);
			 int f = floor(thickness * (n + m + p) * getDistAlpha(i,j));
			 if (f>255) f = 255;
//		 }


		texture->pixels[i][j][0] = (GLubyte) d;
		texture->pixels[i][j][1] = (GLubyte) e;
		texture->pixels[i][j][2] = (GLubyte) c;
		texture->pixels[i][j][3] = (GLubyte) f;
	  }
	}

	GfxHub* gfxHub = game->getGfxHub();
	gfxHub->addDynamicTexture(texture,"cloud");
}

float CloudBuilder::getDistAlpha(float x, float y)
{
	float h = 128 * 0.5;
	float d = Utils::dist(vec2(h,h), vec2(x,y));
	if (d<(h-50))
	{
		return 1;
	}
	else
	{
		if (d>h) return 0;
		else
		{
			return 1 - ( (d - (h-50.0f)) / 50.0f);
		}
	}
}

void CloudBuilder::setHub(Hub* hub)
{
	this->hub = (CloudHub*) hub;
}

void CloudBuilder::buildSimpleTexture(int index)
{
	DynamicTexture* texture = new DynamicTexture();
	int r,g,b;
	float value = 0;
	float a;
	float colourShift = 1;

	float noiseScale = 0.01;

	//float cutoff = 0;
	//float multipler = 255.f * (1.f-cutoff);

	float x = 0;
	float y = 0;
	float z = Dice::roll(-1000.0,1000.0);
	float startZ = z;
	float scatter = 0.03;


	for (int i = 0; i < 128; i++)
	{

		for (int j = 0; j < 128; j++)
		{

			//scatter = SimplexNoise::noise(i,  j, z) * 0.05;
			value = SimplexNoise::noise(x,  y, z) + Dice::roll(-scatter,scatter);
			if (value < 0) value = 0;
			r = (value * 255);
			a = value;

			z += colourShift;
			value = SimplexNoise::noise(x,  y, z ) + Dice::roll(-scatter,scatter);;
			if (value < 0) value = 0;
			g = (value * 255);
			a *= value;

			z += colourShift;
			value = SimplexNoise::noise(x,  y, z ) + Dice::roll(-scatter,scatter);;
			if (value < 0) value = 0;
			b = (value * 255);
			a *= value * 255;

			z = startZ;

			int f = floor(12*getDistAlpha(i,j));
			if (f>12) f = 12;

			texture->pixels[i][j][0] = (GLubyte) r;
			texture->pixels[i][j][1] = (GLubyte) g;
			texture->pixels[i][j][2] = (GLubyte) b;
			texture->pixels[i][j][3] = (GLubyte) f;

			y += noiseScale;
		}
		y = 0;
		x += noiseScale;

	}


	GfxHub* gfxHub = game->getGfxHub();
	string s = "cloud" + Utils::toString(index);
	gfxHub->addDynamicTexture(texture,s);
	texturesA->add(s);
}

void CloudBuilder::initTextures(int count)
{
	for (int i = 0; i < count; i++)
	{
		buildSimpleTexture(i);
	}
}

string CloudBuilder::getTexture()
{
	int i;
	string t;
	if (texturesA->size()>0)
	{
		i = Dice::roll(0,texturesA->size()-1);
		t = texturesA->get(i);
		texturesA->remove(t);
		texturesB->add(t);
	}
	else if (texturesB->size()>0)
	{
		i = Dice::roll(0,texturesB->size()-1);
		t = texturesB->get(i);
		texturesB->remove(t);
		texturesA->add(t);
	}
	else
	{
		t = "";
	}
	return t;
}
