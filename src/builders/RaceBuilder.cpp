/*
 * RaceBuilder.cpp
 *
 *  Created on: 12 Jan 2015
 *      Author: mysticalOso
 */

#include <builders/RaceBuilder.h>
#include <hubs/RaceHub.h>
#include <races/AlienRace.h>
#include <races/Sentient.h>

RaceBuilder::RaceBuilder(Game* game) : EntityBuilder(game)
{
	hub = 0;
}

RaceBuilder::~RaceBuilder()
{

}

Race* RaceBuilder::buildSentient()
{
	return new Sentient(hub);
}

Race* RaceBuilder::buildXacadiens()
{
	return new AlienRace(hub);
}

void RaceBuilder::setHub(Hub* hub)
{
	this->hub = (RaceHub*) hub;
}
