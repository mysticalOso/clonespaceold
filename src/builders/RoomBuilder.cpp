#include <builders/RoomBuilder.h>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <interior/Door.h>
#include <interior/Interior.h>
#include <interior/Room.h>
#include <interior/Walls.h>
#include <osogine/game/Entity.h>
#include <osogine/utils/List.h>
#include <osogine/utils/Polygon.h>
#include <osogine/utils/Printer.h>
#include <ship/Module.h>


RoomBuilder::RoomBuilder(Game* game) : EntityBuilder(game)
{
}

RoomBuilder::~RoomBuilder() {
}


Room* RoomBuilder::buildRoom(float x1, float y1, std::vector<vec2> outline,
		RoomHub* hub, bool isExternal)
{
	Room* room = new Room(hub);
	Walls* walls = new Walls(room);
	room->setIsExternal(isExternal);

	room->setPosition(x1,y1,0);

	for (unsigned int i = 0; i < outline.size(); i++)
	{
		vec2 p = outline[i];
		walls->addExternal(p.x,p.y);
		walls->addGap(i,0.5,1.5);
	}

	walls->init();


	for (unsigned int i = 0; i < outline.size(); i++)
	{
		Door* d = new Door(walls->getGap(i),room);
		d->setIsExternal(true);
		d->setIsLocked(false);
	}

//	if (tileSize!="0")
//	{
//		room->addFloor(tileSize);
//	}
	//room->addLight(new Light(x1,y1,vec3(0.5,0.5,0.5),room));

	return room;
}

Room* RoomBuilder::buildRoom(float x1, float y1, int type, float size,
		RoomHub* hub, bool isExternal)
{

	std::vector<vec2> outline;


	if (type==1 || type==0)
	{
		outline.push_back(vec2(-24,0) * size);
		outline.push_back(vec2(-12,20) * size);
		outline.push_back(vec2(12,20) * size);
		outline.push_back(vec2(24,0) * size);
		outline.push_back(vec2(12,-20) * size);
		outline.push_back(vec2(-12,-20) * size);
	}
	if (type==2)
	{
		outline.push_back(vec2(-24,0) * size);
		outline.push_back(vec2(0,40) * size);
		outline.push_back(vec2(24,0) * size);
		outline.push_back(vec2(12,-20) * size);
		outline.push_back(vec2(-12,-20) * size);
	}

	return buildRoom( x1, y1, outline,hub,isExternal);

}

Room* RoomBuilder::buildRoom(Interior* interior)
{
	vector<vec2> outline = interior->getOutline()->getOutline()->getVector();
	return buildRoom(0,0,outline,interior->getRoomHub(),true);
}

Room* RoomBuilder::buildRoom(Module* module, float size,
		RoomHub* hub)
{
	vec2 pos = vec2(module->getPosition());
	Room* room = buildRoom(pos.x,pos.y,0,size,hub,false);
	room->setModule(module);
	return room;
}


