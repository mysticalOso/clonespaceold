/*
 * WeaponBuilder.h
 *
 *  Created on: 31 Oct 2014
 *      Author: Adam Nasralla
 */

#ifndef SRC_BUILDERS_WEAPONBUILDER_H_
#define SRC_BUILDERS_WEAPONBUILDER_H_

#include <osogine/game/EntityBuilder.h>

class Game;

class WeaponBuilder: public EntityBuilder
{
public:
	WeaponBuilder(Game* game);
	virtual ~WeaponBuilder();
};

#endif /* SRC_BUILDERS_WEAPONBUILDER_H_ */
