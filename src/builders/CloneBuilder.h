/*
 * CloneBuilder.h
 *
 *  Created on: 12 Jan 2015
 *      Author: mysticalOso
 */

#ifndef SRC_BUILDERS_CLONEBUILDER_H_
#define SRC_BUILDERS_CLONEBUILDER_H_

#include <osogine/game/EntityBuilder.h>

class Clone;
class CloneHub;
class RaceHub;
class AIBuilder;
class Polygon;

class CloneBuilder: public EntityBuilder {
public:
	CloneBuilder(Game* game);
	virtual ~CloneBuilder();

	Clone* buildSentientClone();
	Clone* buildXacadienClone();

	void setHub(Hub* hub);

	void init();

private:

	Polygon* buildCloneOutline();


	CloneHub* hub;
	RaceHub* raceHub;
	AIBuilder* aiBuilder;
};

#endif /* SRC_BUILDERS_CLONEBUILDER_H_ */
