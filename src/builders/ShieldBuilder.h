/*
 * ShieldBuilder.h
 *
 *  Created on: 16 Jan 2015
 *      Author: mysticalOso
 */

#ifndef SRC_BUILDERS_SHIELDBUILDER_H_
#define SRC_BUILDERS_SHIELDBUILDER_H_

#include <osogine/game/EntityBuilder.h>

class GfxObject;
class Polygon;
class Shield;

class ShieldBuilder: public EntityBuilder {
public:
	ShieldBuilder(Game* game);
	virtual ~ShieldBuilder();

	Shield* buildShield();

private:
	Polygon* buildOutline();
	GfxObject* buildGfx(Polygon* outline);


};

#endif /* SRC_BUILDERS_SHIELDBUILDER_H_ */
