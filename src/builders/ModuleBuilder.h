/*
 * ModuleBuilder.h
 *
 *  Created on: 31 Oct 2014
 *      Author: Adam Nasralla
 */

#ifndef SRC_BUILDERS_MODULEBUILDER_H_
#define SRC_BUILDERS_MODULEBUILDER_H_

#include <osogine/game/EntityBuilder.h>
#include <glm/vec2.hpp>
#include <vector>

using namespace glm;

class Module;
class Game;
class EntityList;
class BulletBuilder;
class ShieldBuilder;

class ModuleBuilder: public EntityBuilder
{
public:
	ModuleBuilder(Game* game);
	virtual ~ModuleBuilder();

	Module* buildModule(vec2 code, vec2 pos);

	EntityList* buildModules(EntityList* moduleIcons);
	EntityList* buildModules(vector<vec4> moduleCodes);

	vec3 getUIColour(string colour);

private:
	BulletBuilder* bulletBuilder;
	ShieldBuilder* shieldBuilder;
};

#endif /* SRC_BUILDERS_MODULEBUILDER_H_ */
