/*
 * GemBuilder.cpp
 *
 *  Created on: 15 Jan 2015
 *      Author: mysticalOso
 */

#include <builders/GemBuilder.h>
#include <core/Constants.h>
#include <glm/vec2.hpp>
#include <hubs/GemHub.h>
#include <osogine/game/Entity.h>
#include <osogine/gfx/GfxBuilder.h>
#include <osogine/gfx/GfxObject.h>
#include <osogine/utils/Dice.h>
#include <osogine/utils/Polygon.h>
#include <space/Gem.h>
#include <cmath>


GemBuilder::GemBuilder(Game* game) : EntityBuilder(game)
{
	hub = 0;
}

GemBuilder::~GemBuilder()
{
}

void GemBuilder::setHub(Hub* hub)
{
	this->hub = (GemHub*) hub;
}

Gem* GemBuilder::buildGem(int colour)
{
	Polygon* outline = buildOutline(1,1,true);
	GfxObject* gfx = buildGfx(outline,colour);

	Gem* gem = new Gem(hub);
	gem->setGfx(gfx);
	gem->setOutline(outline);
	gem->setColour(colour);
	gem->setProgram("flat");
	gem->setFriction(0.95);

	return gem;

}

Polygon* GemBuilder::buildOutline(int quality, float scale, bool jitter)
{
	Polygon* outline = new Polygon();
	float radius,a,x1,y1,dr;

	int noPoints = quality + 3;

	if (noPoints == 6) noPoints = 6;
	else if (noPoints == 7) noPoints = 8;
	else if (noPoints == 8) noPoints = 10;


	//dr = 2.0 - (float)(noPoints-3)/3.0f;

	dr = 0;
	float da = PI / (noPoints*3);
	float baseR = 7;
	for (int i=0; i<noPoints; i++)
	{
		a = (2*i*PI)/(noPoints);
		if (jitter) a += Dice::roll(-da,da);
		if (noPoints > 5)
		{
			if (i%2 == 0 )
			{
				if (noPoints == 6) baseR = 3;
				if (noPoints == 8) baseR = 3.5;
				if (noPoints == 10) baseR = 4;
				dr = 0;
			}//{baseR = 1.5 + ((float)noPoints/3.0); dr = 0;}
			else {baseR = 10; dr = 0;}
		}
		radius = baseR * scale;

		if (jitter) radius += Dice::roll(-dr,dr);

		x1 = radius*cos(a);
		y1 = radius*sin(a);

		outline->add(vec2(x1,y1));
	}

	return outline;
}

GfxObject* GemBuilder::buildGfx(Polygon* outline, int colour1)
{

	GfxObject* gfx = gfxBuilder->buildTriMesh(
			outline->getOutline(), 1 , true, false);

	vec4 colour = getColour(colour1);
	gfx->reColour(colour,colour);

	return gfx;
}

vec4 GemBuilder::getColour(int colour1)
{
	float r1, g1, b1;
	r1 = g1 = b1 = 0.5;
	if (colour1==1) r1 = 1;
	if (colour1==2) g1 = 1;
	if (colour1==3) b1 = 1;
	if (colour1==4) {r1=1; g1=1; b1=0.6;}
	if (colour1==5) {b1=1; g1=1; b1=0.6;}
	if (colour1==6) {r1=1; b1=1; b1=0.6;}
	return vec4(r1,g1,b1,1);
}

vec3 GemBuilder::getBarRGB(int colour1)
{

		vec3 c1;
		if (colour1==0) c1 = vec3(0.55,0.55,0.5);
		if (colour1==1) c1 = vec3(0.9,0,0);
		if (colour1==2) c1 = vec3(0,0.9,0);
		if (colour1==3) c1 = vec3(0.6,0.6,1);
		if (colour1==4) c1 = vec3(1,1,0);
		if (colour1==5) c1 = vec3(0,1,1);
		if (colour1==6) c1 = vec3(1,0.3,1);
		return c1;

}
