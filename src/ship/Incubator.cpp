/*
 * Incubator.cpp
 *
 *  Created on: 6 Mar 2015
 *      Author: Adam Nasralla
 */

#include <clone/Clone.h>
#include <hubs/CloneHub.h>
#include <hubs/LightHub.h>
#include <interior/Room.h>
#include <osogine/game/Light.h>
#include <osogine/gfx/GfxObject.h>
#include <ship/Incubator.h>

Incubator::Incubator(Entity* parent) : Pod(parent)
{
	room = (Room*) parent;
	setAlpha(1);
	spinTime = 250;
	hatched = false;
	count = 0;
}

Incubator::~Incubator()
{

}

void Incubator::update()
{
	if (!hatched) cloneVeryNear = true;
	else {cloneVeryNear = false; turnOff();}
	Pod::update();
	updateGfx();

	if (count>0)
	{
		count++;
		if (count==200)
		{
			hatched = true;
			Incubator* pod2 = (Incubator*) room->getPod2();
			pod2->setHatched(true);
		}
	}

}

void Incubator::use(Clone* clone)
{
	if (clone!=0)
	{
		checkClone(clone);
		if (cloneVeryNear)
		{

			if (LIGHTS)
			{
				CloneHub* cloneHub = clone->getHub();
				if (cloneHub!=0)
				{
					LightHub* lightHub = cloneHub->getLightHub();
					if (lightHub!=0)
					{
						Light* light = lightHub->addLight(getLocalPosition(), vec3(0,0,0));
						light->explode(200,300,vec3(1,1,1));
						light = lightHub->addLight(getLocalPosition(), vec3(0,0,0));
						light->explode(100,500,vec3(0,1,0));
					}
				}
			}
			count = 1;
		}
	}
}

void Incubator::updateGfx()
{
	for (int i=0; i<gfx->getNoVertices(); i++)
	{
		vec4 c = gfx->getColour(i);
		c.g *= 2;
		if (c.g>1) c.g = 1;
		gfx->setColour(i,c);
	}
}
