/*
 * EnergyCore.h
 *
 *  Created on: 30 Jan 2014
 *      Author: Adam Nasralla
 */

#ifndef ENERGYCORE_H_
#define ENERGYCORE_H_

#include <ship/Module.h>

class Ship;
class Clone;

class EnergyCore: public Module
{
public:
	EnergyCore(float maxEnergy1, float rechargeRate1, float recoveryTime1, Game* game);
	~EnergyCore();
	void update();
	bool drain(float e);
	int getEnergy(){return energy;}
	float getRatioRemaining(){ return energy / maxEnergy; }
	bool hasEnergy( float e );
	void install(Ship* ship);
	void install(Clone* clone);

private:
	int recoveryTime;
	int recoveryCount;
	float energy;
	float maxEnergy;
	float rechargeRate;
	bool isDrained;

};

#endif /* ENERGYCORE_H_ */
