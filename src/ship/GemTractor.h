/*
 * GemTractor.h
 *
 *  Created on: 25 Jan 2015
 *      Author: mysticalOso
 */

#ifndef SRC_SHIP_GEMTRACTOR_H_
#define SRC_SHIP_GEMTRACTOR_H_

#include <ship/Module.h>

class GemBeam;
class GemHub;
class Gem;

class GemTractor: public Module {
public:
	GemTractor(Game* game);
	virtual ~GemTractor();
	void update();
	void render();
	void turnOn(bool isOn1);
	void install(Ship* ship);

	void addGem(Gem* gem);
	EntityList* getGems(){ return gems; }
	GemHub* getGemHub(){ return gemHub; }

	int getRemaining();


private:

	void scanGems();

	EntityList* gems;
	GemHub* gemHub;

};

#endif /* SRC_SHIP_GEMTRACTOR_H_ */
