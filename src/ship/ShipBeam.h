/*
 * ShipBeam.h
 *
 *  Created on: 17 Feb 2015
 *      Author: Adam Nasralla
 */

#ifndef SRC_SHIP_SHIPBEAM_H_
#define SRC_SHIP_SHIPBEAM_H_

#include <ship/Beam.h>

class Ship;
class ShipTractor;

class ShipBeam: public Beam
{
public:
	ShipBeam(ShipTractor* tractor, Ship* ship);
	~ShipBeam();

	void update();

private:
	Ship* ship;
	ShipTractor* tractor;
	int count;

	void updateShip();
	bool shipNeedsBeam();

};

#endif /* SRC_SHIP_SHIPBEAM_H_ */
