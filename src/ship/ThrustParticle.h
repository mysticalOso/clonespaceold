/*
 * ThrustParticle.h
 *
 *  Created on: 19 Nov 2014
 *      Author: mysticalOso
 */

#ifndef SRC_SHIP_THRUSTPARTICLE_H_
#define SRC_SHIP_THRUSTPARTICLE_H_

#include <osogine/game/Entity.h>
#include <glm/vec4.hpp>
class Thrust;

class ThrustParticle: public Entity {
public:
	ThrustParticle(Thrust* thrust, float startAge);
	virtual ~ThrustParticle();

	void update();

	vec4 getColour();
	float getSize();

private:
	float power;
	Thrust* thrust;


};

#endif /* SRC_SHIP_THRUSTPARTICLE_H_ */
