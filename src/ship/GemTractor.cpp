/*
 * GemTractor.cpp
 *
 *  Created on: 25 Jan 2015
 *      Author: mysticalOso
 */

#include <hubs/GemHub.h>
#include <hubs/ShipHub.h>
#include <osogine/audio/SoundInterface.h>
#include <osogine/game/EntityList.h>
#include <osogine/game/Game.h>
#include <ship/GemBeam.h>
#include <ship/GemTractor.h>
#include <ship/Ship.h>
#include <space/Gem.h>

GemTractor::GemTractor(Game* game) : Module(game)
{
	gems = new EntityList();
	gemHub = 0;
}

GemTractor::~GemTractor()
{

}

void GemTractor::update()
{
	Module::update();
	if (children->size() < getRemaining() && children->size() < 5) scanGems();

}

void GemTractor::turnOn(bool isOn1)
{
}

void GemTractor::install(Ship* ship)
{
	this->ship = ship;
	gemHub = ship->getHub()->getGemHub();
	ship->setGemTractor(this);
}

void GemTractor::render()
{
}

int GemTractor::getRemaining()
{
	if (gems->size() >= 20) game->event("storageFull");
	return (30 - gems->size());
}

void GemTractor::scanGems()
{
	Gem* gem = (Gem*) gemHub->getClosest(this, 100);
	if (gem!=0)
	{
		gemHub->remove(gem);
		children->add( new GemBeam(this, gem) );
	}
}

void GemTractor::addGem(Gem* gem)
{
	//Printer::print("GEM COUNT = ",gems->size(),"\n");
	SoundInterface::collectGem();
	gems->add(gem);
}

