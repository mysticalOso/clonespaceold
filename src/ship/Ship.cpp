/*
 * Ship.cpp
 *
 *  Created on: 15 Oct 2014
 *      Author: Adam Nasralla
 */

#include <ai/ShipAI.h>
#include <builders/GemBuilder.h>
#include <clone/Clone.h>
#include <hubs/BulletHub.h>
#include <hubs/ShipHub.h>
#include <interior/Interior.h>
#include <osogine/game/Game.h>
#include <osogine/game/IEntity.h>
#include <osogine/utils/List.h>
#include <osogine/utils/Polygon.h>
#include <osogine/utils/Printer.h>
#include <races/Race.h>
#include <ship/Shield.h>
#include <ship/Ship.h>
#include <ship/ShipRoof.h>
#include <space/Gem.h>
#include <weapons/Bullet.h>


Ship::Ship(ShipHub* hub) : Vessel(hub)
{
	this->hub = hub;
	interior = 0;
	ai = 0;
	driver = 0;
	roof = new ShipRoof(this);
	maxHealth = 0;
	health = 0;
	renderOrder = 3;
	takingOff = false;

}

Ship::~Ship()
{
}

void Ship::update()
{
	if (ai!=0) ai->update();	//TODO give AI a proper home
	Entity::update();
	//if (shield!=0) shield->setIsVisible(false);
	//setIsVisible(false);
	//getCamera()->setTarget(this);
}

void Ship::setDriver(Clone* clone)
{
	driver = clone;
	if (driver!=0)
	{
		setRace(driver->getRace());
		ai = clone->getShipAI();
		ai->activate(this);
	}
	else if (ai!=0)
	{
		ai->deactivate();
	}
}



void Ship::hitBy(Bullet* bullet, vec2 collisionPoint)
{
	float damage = bullet->getDamage();
	if ( shieldIsOn() )
	{
		shield->hit(damage);
	}
	else
	{
		health -= damage;
	}

	if (health <= 0)
	{
		if (getRace()->isRace("xacadiens")) game->event("enemyDestroyed");
		hub->destroyShip(this);
	}
	else
	{
		hub->shipHit(this, collisionPoint);
	}
}



void Ship::setOutline(List<vec2>* outline)
{
	setOutline(new Polygon(outline));
}

void Ship::setOutline(Polygon* outline)
{
	Entity::setOutline(outline);
	setMass( outline->getArea() / 315.0f );
}

void Ship::setInterior(Interior* interior1)
{
	if (interior!=0) removeChild(interior);
	interior = interior1;
	addChild(interior);
}

void Ship::fadeRoofIn() { roof->fadeIn(); }
void Ship::fadeRoofOut() {
	roof->fadeOut();
}

Hub* Ship::getBulletHub()
{
	return hub->getBulletHub();
}

void Ship::takeOff(Clone* c)
{
	takingOff = true;
	hub->launch(this);
	if (maxHealth > 1000)
	{
		setMaxHealth(50);
	}
	c->setIsDriver(true);
	c->playerTakeControl();
	game->event("takeOff");
}

void Ship::setAlpha(float a)
{
	Entity::setAlpha(a);
//	if (name=="new")
//	{
//		Printer::print("Alpha = ",a,"\n");
//	}
}

Clone* Ship::getClone(string name)
{
	return interior->getClone(name);
}
