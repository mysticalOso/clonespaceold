/*
 * ShipTractor.h
 *
 *  Created on: 17 Feb 2015
 *      Author: Adam Nasralla
 */

#ifndef SRC_SHIP_SHIPTRACTOR_H_
#define SRC_SHIP_SHIPTRACTOR_H_

#include <ship/Module.h>

class Gem;

class ShipTractor: public Module
{
public:
	ShipTractor(Game* game);
	virtual ~ShipTractor();
	void update();
	void install(Ship* ship);
	void render(){}
	void addGem(Gem* gem);

	EntityList* getGems(){ return gems; }

	ShipHub* getShipHub(){ return shipHub; }

	bool shipNeedsBeam(Ship* ship);

private:

	void scanShips();

	EntityList* gems;
	ShipHub* shipHub;

};

#endif /* SRC_SHIP_SHIPTRACTOR_H_ */
