/*
 * DockingBay.h
 *
 *  Created on: 9 Feb 2015
 *      Author: mysticalOso
 */

#ifndef SRC_SHIP_DOCKINGBAY_H_
#define SRC_SHIP_DOCKINGBAY_H_

#include <ship/Module.h>

class DockingBay: public Module {
public:
	DockingBay(Game* game);
	virtual ~DockingBay();

	void update();

	void install(Ship* ship);

	void setEnabled(bool b){ enabled = b; }



private:

	void checkDistance();
	void fadeRoof();
	void checkLanding();
	bool enabled;
	Ship* playerShip;
	ShipHub* shipHub;
	float dist;
};

#endif /* SRC_SHIP_DOCKINGBAY_H_ */
