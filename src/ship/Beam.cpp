/*
 * Beam.cpp
 *
 *  Created on: 17 Feb 2015
 *      Author: Adam Nasralla
 */

#include <core/Constants.h>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <osogine/game/Game.h>
#include <osogine/gfx/GfxBuilder.h>
#include <osogine/gfx/GfxObject.h>
#include <osogine/utils/Polygon.h>
#include <osogine/utils/Transform.h>
#include <ship/Beam.h>
#include <cmath>


Beam::Beam(Entity* parent, Entity* target) : Entity(parent)
{
	this->target = target;
	width = 0;
	range = 0;
}

Beam::~Beam()
{
}

void Beam::initGfx()
{
	setProgram("simple");

	GfxBuilder* gfxBuilder = game->getGfxBuilder();

	Polygon* outline = new Polygon();
	outline->add(vec2(0,0));
	outline->add(vec2(100,100));
	outline->add(vec2(-100,100));

	gfx = gfxBuilder->buildPolygon( outline, vec4(0,0,1,0.6) );
}



void Beam::updateGfx()
{
	transform->zeroRotation();

	vec3 gemPos = target->getPosition();
	vec3 pos = getPosition();

	float dx = gemPos.x - pos.x;
	float dy = gemPos.y - pos.y;

	float a = atan2(dy, dx);

	float x1 = dx + width*cos(a+PI/4);
	float y1 = dy + width*sin(a+PI/4);
	float x2 = dx + width*cos(a-PI/4);
	float y2 = dy + width*sin(a-PI/4);

	gfx->setVertex(1, vec3(x1, y1, 0));
	gfx->setVertex(2, vec3(x2, y2, 0));
}

bool Beam::isInRange()
{
	return (target->distTo2(this) <= range*range);
}
