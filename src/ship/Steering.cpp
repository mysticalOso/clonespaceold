/*
 * Steering.cpp
 *
 *  Created on: 31 Jan 2014
 *      Author: Adam Nasralla
 */



#include <clone/Clone.h>
#include <core/Vessel.h>
#include <ship/Ship.h>
#include <ship/Steering.h>
#include <osogine/utils/Printer.h>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <osogine/utils/Transform.h>


Steering::Steering(float turningPower1, Game* game) : Module(game)
{

	entity = 0;
	turningPower = turningPower1;
//	if (ship!=0) turningPower *= (315.0 / ship->getArea());
	targetA = 0;
	dir = vec2(0,0);
	updateOrder = 1;
	useMouseTarget = false;
}

Steering::~Steering()
{
}

void Steering::update()
{
	updateTarget();

	if (dir != vec2(0,0) && !entity->getIsDocked())
	{
		float da = entity->angBetween(dir);
		bool isClockwise = entity->isClockwiseTo(dir);

		if (isClockwise)
		{
			if (da>turningPower) entity->rotateZ(turningPower);
			else entity->rotateZ(da);
		}
		else
		{
			if (da>turningPower) entity->rotateZ(-turningPower);
			else entity->rotateZ(-da);
		}
	}

}

void Steering::setTarget(float x1, float y1, bool isMouse)
{


	useMouseTarget = isMouse;

	if (isMouse)
	{
		mouseTarget.x = x1;
		mouseTarget.y = y1;
	}
	else
	{
		target.x = x1;
		target.y = y1;
	}

	updateTarget();

}

void Steering::install(Ship* s)
{
	ship = s;
	ship->setSteering(this);
	entity = ship;
//	turningPower *= (315.0 / ship->getArea());
}

void Steering::install(Clone* c)
{
	entity = c;
	c->setSteering(this);
}

void Steering::updateTarget()
{
	float dy;
	float dx;

	if (!useMouseTarget)
	{
		dy = target.y - entity->getLocalPosition().y;
		dx = target.x - entity->getLocalPosition().x;
	}
	else
	{
		dx = mouseTarget.x;
		dy = mouseTarget.y;
	}



	dir = vec2(dx,dy);

	IEntity* parent = (IEntity*) entity->getParent();
	if (parent!=0)
	{
		Transform* parentTrans = parent->getTransform();
		if (parentTrans!=0)
		{
			dir = parentTrans->applyRotation(dir);
		}
	}
}

void Steering::use(Clone* c)
{
	if (ship!=0)
	{
		ship->takeOff(c);
	}
}
