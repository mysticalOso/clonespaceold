/*
 * ShipGrower.h
 *
 *  Created on: 2 Mar 2015
 *      Author: Adam Nasralla
 */

#ifndef SRC_SHIP_SHIPGROWER_H_
#define SRC_SHIP_SHIPGROWER_H_

#include <osogine/game/Entity.h>
#include <vector>

class Ship;
class CameraCS;
class GfxObject;
class LinkedMesh;
class LinkedVertex;

class ShipGrower: public Entity
{
public:
	ShipGrower(Entity* parent);
	~ShipGrower();

	void grow(Ship* ship);

	void updateGrow(){}

	void update();

	void render(){}

	void callBack(string s);

private:
	void initGrow();
	void updateAlphas();
	void addAlpha();
	void addAlpha(LinkedVertex* v);

	int count;
	Ship* ship;
	CameraCS* camera;
	vector<float> alphas;
	LinkedMesh* linkedMesh;
	List<LinkedVertex*> added;
	List<LinkedVertex*> current;


};

#endif /* SRC_SHIP_SHIPGROWER_H_ */
