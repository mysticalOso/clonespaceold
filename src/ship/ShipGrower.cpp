/*
 * ShipGrower.cpp
 *
 *  Created on: 2 Mar 2015
 *      Author: Adam Nasralla
 */

#include <clone/Clone.h>
#include <core/CameraCS.h>
#include <ship/ShipGrower.h>
#include <osogine/game/Game.h>
#include <interior/Interior.h>
#include <glm/vec4.hpp>
#include <hubs/LightHub.h>
#include <hubs/RaceHub.h>
#include <races/Sentient.h>
#include <hubs/RoomHub.h>
#include <osogine/gfx/GfxObject.h>
#include <osogine/gfx/LinkedMesh.h>
#include <osogine/gfx/LinkedVertex.h>
#include <osogine/utils/Dice.h>
#include <osogine/utils/Fader.h>
#include <osogine/utils/FunctionTimer.h>
#include <osogine/utils/Printer.h>
#include <ship/Ship.h>


ShipGrower::ShipGrower(Entity* parent) : Entity(parent)
{
	camera = (CameraCS*) game->getCamera();
	ship = 0;
	gfx = 0;
	count = 0;
	linkedMesh = 0;
}

ShipGrower::~ShipGrower()
{
}

void ShipGrower::grow(Ship* ship)
{
	this->ship = ship;
	Interior* interior = ship->getInterior();
	interior->setIsVisible(false);
	RoomHub* roomHub = interior->getRoomHub();
	LightHub* lightHub = interior->getLightHub();
	ship->fadeRoofIn();
	RaceHub* raceHub = (RaceHub*) game->getEntity("raceHub");
	Ship* playerShip = raceHub->getPlayerShip();
	Interior* playerInterior = playerShip->getInterior();
	RoomHub* playerRoomHub = playerInterior->getRoomHub();
	LightHub* playerLightHub = playerInterior->getLightHub();

	playerRoomHub->toggleOwnerRooms(roomHub, false);
	playerLightHub->toggleOwnerLights(lightHub, false);

	initGrow();
}

void ShipGrower::update()
{
	count++;
	if (gfx!=0 && count==5 && added.size()<linkedMesh->getNoVertices())
	{
		addAlpha();
		count = 0;

	}
	else if (added.size()==linkedMesh->getNoVertices() && count==50)
	{
		//Printer::print("fadingOut");
		ship->fadeRoofOut();
		Interior* interior = ship->getInterior();
		interior->setIsVisible(true);
		RoomHub* roomHub = interior->getRoomHub();
		LightHub* lightHub = interior->getLightHub();
		RaceHub* raceHub = (RaceHub*) game->getEntity("raceHub");
		Ship* playerShip = raceHub->getPlayerShip();
		Interior* playerInterior = playerShip->getInterior();
		RoomHub* playerRoomHub = playerInterior->getRoomHub();
		LightHub* playerLightHub = playerInterior->getLightHub();

		playerRoomHub->toggleOwnerRooms(roomHub, true);
		playerLightHub->toggleOwnerLights(lightHub, true);
		//



	}
	if (added.size()==linkedMesh->getNoVertices() && count==120)
	{
		RaceHub* raceHub = (RaceHub*) game->getEntity("raceHub");
		Sentient* sentient = raceHub->getSentient();
		Clone* clone = sentient->getCurrentClone();
		camera->setTargetClone(clone);
		camera->clearFixedTarget();
		game->event("buildFinished");
	}
	if (count<50)
	{
		updateAlphas();
		linkedMesh->updateGfx();
	}
}

void ShipGrower::initGrow()
{
	//Printer::print("INIT GROW");
	gfx = ship->getGfx();

	linkedMesh = new LinkedMesh(gfx);
	linkedMesh->updateGfx();
	for (int i=0; i<linkedMesh->getNoVertices(); i++)
	{
		vec4 c = linkedMesh->getVertex(i)->getColour();
		c.a = 0;
		linkedMesh->getVertex(i)->setColour(c);
		alphas.push_back(0);
	}
	current.add(linkedMesh->getVertex(0));

	new FunctionTimer(this,"grow",10,true);


}


void ShipGrower::updateAlphas()
{
	for (int i=0; i<added.size(); i++)
	{
		vec4 colour = added.get(i)->getColour();
		colour.a = alphas[i];
		added.get(i)->setColour(colour);
	}
}

void ShipGrower::addAlpha()
{
	List<LinkedVertex*> next;

	for (int i=0; i<current.size(); i++)
	{
		addAlpha(current.get(i));
		List<LinkedVertex*>* links = current.get(i)->getLinks();
		int size = links->size();

		for (int i=0; i<size;i++)
		{
			if (!next.contains(links->get(i))) next.add(links->get(i));
		}
	}

	current.clear();

	for (int i=0; i<next.size(); i++)
	{
		if (!added.contains(next.get(i))) current.add(next.get(i));
	}

}

void ShipGrower::callBack(string s)
{
	if (s=="grow") updateGrow();
}

void ShipGrower::addAlpha(LinkedVertex* v)
{
	if (!added.contains(v))
	{
		added.add(v);
		int i = added.size()-1;
		Fader* fader = new Fader(this,&(alphas[i]),0,1,Dice::roll(50,100));
		fader->fadeIn();
	}

}
