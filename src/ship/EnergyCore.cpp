/*
 * EnergyCore.cpp
 *
 *  Created on: 30 Jan 2014
 *      Author: Adam Nasralla
 */

#include <clone/Clone.h>
#include "EnergyCore.h"
#include "Ship.h"

EnergyCore::EnergyCore(float maxEnergy1, float rechargeRate1, float recoveryTime1, Game* game) : Module(game)
{
	energy = maxEnergy1;
	maxEnergy = maxEnergy1;
	recoveryCount = 0;
	recoveryTime = recoveryTime1;
	recoveryTime = 10;
	isDrained = false;
	rechargeRate = rechargeRate1;
}

EnergyCore::~EnergyCore()
{

}

void EnergyCore::update()
{
	if ( energy < maxEnergy )
	{
		if ( !isDrained )
		{
			energy += rechargeRate;
			if (energy > maxEnergy) energy = maxEnergy;
		}
		else
		{
			recoveryCount++;
			if (recoveryCount==recoveryTime)
			{
				isDrained = false;
				recoveryCount = 0;
			}
		}
	}
}

bool EnergyCore::drain( float e )
{

	if (e <= energy)
	{
		energy -= e;
		if ( energy <= 0 )
		{
			energy = 0;
			isDrained = true;
		}
		return true;
	}
	else
	{
		energy = 0;
		isDrained = true;
		return false;
	}
}

//TODO fix empty energy core
bool EnergyCore::hasEnergy( float e )
{
	return ( energy >= e );
}

void EnergyCore::install(Ship* ship)
{
	ship->setEnergyCore(this);
}

void EnergyCore::install(Clone* clone)
{
	clone->setEnergyCore(this);
}
