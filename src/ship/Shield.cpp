/*
 * Shield.cpp
 *
 *  Created on: 30 Jan 2014
 *      Author: Adam Nasralla
 */

#include <osogine/game/Game.h>
#include <osogine/gfx/GfxObject.h>
#include <osogine/utils/Printer.h>
#include <osogine/utils/Transform.h>
#include <races/Race.h>
#include <ship/EnergyCore.h>
#include <ship/Shield.h>
#include <ship/Ship.h>



//TODO parameterise shield with ShieldBuilder
Shield::Shield( Game* game ) : Module(game)
{
	ship = 0;
	core = 0;
	energyCost = 4;
	hitEnergyCost = 100;

	renderOrder = 3;
	alpha = 0;
	alphaModifier = 1;
	maxAlpha = 0.5;
	alphaAnimSpeed = 0.05;
	updateOrder = 2;

}

Shield::~Shield()
{
}

void Shield::update()
{
	Entity::update();

	transform->zeroRotation();

	if (core==0) core = ship->getEnergyCore();

	if (core!=0)
	{

		if (!core->hasEnergy( energyCost )) isOn = false;

		if (isOn)
		{
			core->drain( energyCost );
		}

	}

	updateAlpha();
}

void Shield::hit( float damage )
{
	if (core!=0)
	{
		core->drain( hitEnergyCost * damage );
		if (!core->hasEnergy( energyCost )) isOn = false;
	}



	if (ship->getRace()->isRace("sentient")) game->event("shieldBlock");
}

void Shield::updateAlpha()
{

	if ( isOn )
	{
		alpha += alphaAnimSpeed;
		if ( alpha >= maxAlpha ) alpha = maxAlpha;
	}
	else
	{
		alpha -= alphaAnimSpeed;
		if ( alpha <= 0 ) alpha = 0;
	}


	float alphaModifier2;
	float modifiedAlpha;

	if (core!=0)
	{
		alphaModifier2 = core->getRatioRemaining() * 0.8 + 0.2;
		modifiedAlpha = alpha * alphaModifier * alphaModifier2;
	}
	else
	{
		modifiedAlpha = 0;
	}
	setAlpha( modifiedAlpha );

}

void Shield::turnOn(bool isOn1)
{
	setPosition(0,0,0);
	isOn = false;
	if (core!=0)
	{
		if (core->hasEnergy( energyCost )) isOn = isOn1;
	}
	else isOn = false;
}

void Shield::install(Ship* ship1)
{
	ship = ship1;
	ship->setShield(this);
	//gfx->applyInverseTransform(getTransform());
	//Printer::print("INSTALL SHIELD!");
}





