/*
 * Module.cpp
 *
 *  Created on: 30 Jan 2014
 *      Author: Adam Nasralla
 */

#include <ship/Module.h>
#include <ship/Ship.h>
#include <glm/vec3.hpp>

Module::Module(Game* game) : Entity(game)
{
	//TODO change module type to string?
	grade = 0;
	type = 0;
	ship = 0;
	isOn = false;
	gfx = 0;
	updateOrder = 1;
}

Module::~Module()
{
}

vec3 Module::getVelocity()
{
	if (ship!=0) return ship->getVelocity();
	else return vec3(0,0,0);
}

ShipHub* Module::getShipHub()
{
	return ship->getHub();
}
//Cost Module::getCost()
//{
//	return ModuleLookup::getCost(type,grade);
//}

//std::string Module::getTitle()
//{
//	return ModuleLookup::getTitle(type,grade);
//}

//std::string Module::getDescription()
//{
//	return ModuleLookup::getDescription(type,grade);
//}

/*
Module* Module::clone()
{
	return ModuleLookup::getModule(type,grade);
}*/

