/*
 * Ship.h
 *
 *  Created on: 15 Oct 2014
 *      Author: Adam Nasralla
 */

#ifndef SHIP_H_
#define SHIP_H_

#include <core/Vessel.h>
#include <glm/fwd.hpp>

class Bullet;
class Clone;
class Interior;
class Race;
class ShipAI;
class ShipHub;
class ShipRoof;


class Ship: public Vessel
{
public:
	Ship(ShipHub* hub);
	~Ship();

	void update();

	void setDriver(Clone* clone);

	void setInterior(Interior* interior);
	Interior* getInterior(){ return interior; }

	void setAI(ShipAI* a){ ai = a; }

	ShipHub* getHub(){ return hub; }
	void setHub(ShipHub* hub){this->hub = hub;}

	void hitBy( Bullet* bullet, vec2 collisionPoint );

	void fadeRoofIn();
	void fadeRoofOut();

	void setOutline(Polygon* outline);
	void setOutline(List<vec2>* outline);

	Clone* getDriver(){ return driver; }

	void takeOff(Clone* c);
	Hub* getBulletHub();

	void setAlpha(float a);

	bool getTakingOff(){ return takingOff; }
	void setTakingOff(bool b){ takingOff = b; }

	Clone* getClone(string name);

private:

	bool takingOff;
	ShipHub* hub;
	ShipAI* ai;
	Interior* interior;
	ShipRoof* roof;
	Clone* driver;



};

#endif /* SHIP_H_ */
