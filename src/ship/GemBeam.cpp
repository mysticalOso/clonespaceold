/*
 * GemBeam.cpp
 *
 *  Created on: 25 Jan 2015
 *      Author: mysticalOso
 */

#include <hubs/GemHub.h>
#include <osogine/game/Game.h>
#include <osogine/game/Hub.h>
#include <ship/GemBeam.h>
#include <ship/GemTractor.h>
#include <space/Gem.h>

GemBeam::GemBeam(GemTractor* tractor, Gem* gem) : Beam(tractor,gem)
{
	this->tractor = tractor;
	gemHub = tractor->getGemHub();
	currentGem = gem;
	gem->setBeam(this);
	renderOrder = 1;
	updateOrder = 2;
	count = 25;
	width = 15;
	range = 100;
	initGfx();
	updateGfx();
}

GemBeam::~GemBeam()
{

}

void GemBeam::update()
{
	if (isInRange())
	{
		updateGem();
		updateGfx();

		count--;

		if (count<=0)
		{
			beamUpGem();
		}
	}
	else
	{
		releaseGem();
	}
}


void GemBeam::render()
{
	if (currentGem!=0) currentGem->render();
	Entity::render();

}

void GemBeam::updateGem()
{
	float alpha = (float)count / 25.0f;
	currentGem->setAlpha( alpha );
	currentGem->update();
}


void GemBeam::beamUpGem()
{
	game->event("gemCollected");
	currentGem->setBeam(0);
	tractor->addGem(currentGem);
	tractor->removeChild(this);
	//delete this;
}



void GemBeam::releaseGem()
{
	currentGem->setBeam(0);
	gemHub->add(currentGem);
	tractor->removeChild(this);
	//delete this;
}

