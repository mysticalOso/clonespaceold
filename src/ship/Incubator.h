/*
 * Incubator.h
 *
 *  Created on: 6 Mar 2015
 *      Author: Adam Nasralla
 */

#ifndef SRC_SHIP_INCUBATOR_H_
#define SRC_SHIP_INCUBATOR_H_

#include <interior/Pod.h>

class Room;

class Incubator: public Pod
{
public:
	Incubator(Entity* parent);
	~Incubator();

	void update();

	void use(Clone* clone);

	void setHatched(bool b){ hatched = b; }


private:
	void updateGfx();
	bool hatched;
	Room* room;
	int count;
};

#endif /* SRC_SHIP_INCUBATOR_H_ */
