/*
 * Thrust.cpp
 *
 *  Created on: 19 Nov 2014
 *      Author: mysticalOso
 */

#include <osogine/gfx/GfxObject.h>
#include <ship/Engine.h>
#include <ship/Thrust.h>
#include <ship/ThrustParticle.h>
#include <glm/glm.hpp>
#include <osogine/utils/Printer.h>
#include <osogine/utils/Transform.h>
#include <osogine/utils/Utils.h>
#include <glm/vec4.hpp>

Thrust::Thrust(Entity* engine) : Entity(engine)
{
	isOn = false;
	this->engine = engine;
	currentPower = 0;
	maxPower = 15;
	gfx = new GfxObject();
	setProgram("simple");
	transform->setParent(0);
	updateOrder = 1;
	propel = 15;

	colour1 = vec4(1,0.8,0,0.8);
	colour2 = vec4(1,0,0,0.8);

	width = 4;
}

Thrust::~Thrust()
{
}

void Thrust::update()
{
	Entity::update();

	if (isOn)
	{
		currentPower++;
		if (currentPower>=maxPower) currentPower = maxPower;
		//addParticle();
	}
	else
	{
		currentPower--;
		if (currentPower<0) currentPower = 0;
	}
	if (currentPower>0) addParticle();

	updateGfx();
}



void Thrust::addParticle()
{
	ThrustParticle* p = new ThrustParticle(this,currentPower);
	p->setPosition(engine->getPosition());
	//Printer::print("EV = ",engine->getVelocity(), "\n");
	if (propel!=0)
	{
		p->setVelocity(engine->getDirection()*-propel + engine->getVelocity());
		p->setFriction(0.8);
	}
	//p->update();
	//p->setFriction(0.9);
}

void Thrust::updateGfx()
{
	gfx->clearAll();

	vec3 p1 = engine->getPosition();

	for (int i = children->size()-1; i >= 0 ; i--)
	{
		ThrustParticle* p = (ThrustParticle*) children->get(i);
		vec3 p2 = p->getPosition();
		vec3 diff = p2 - p1;
		vec3 perp = vec3(Utils::getUnitPerpendicular(vec2(diff)),0);

		float size = p->getSize();
		vec4 colour = p->getColour();

		vec3 a = p1 - perp*size;
		vec3 b = p1 + perp*size;

		gfx->addVertex(a);
		gfx->addVertex(b);

		gfx->addColour(colour);
		gfx->addColour(colour);

		gfx->addNormal(0,0,1);
		gfx->addNormal(0,0,1);

		p1 = p2;
	}

	gfx->fillTriangles();
}
