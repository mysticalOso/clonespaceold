/*
 * ShipRoof.cpp
 *
 *  Created on: 13 Mar 2014
 *      Author: Adam Nasralla
 */

#include <core/Constants.h>
#include <ship/Shield.h>
#include <ship/Ship.h>
#include <ship/ShipRoof.h>
#include <cmath>


ShipRoof::ShipRoof(Ship* ship1) : Entity(ship1)
{
	ship = ship1;
	shield = 0;
	fadeLevel = 0;
	fadeRate = 0;
}

ShipRoof::~ShipRoof()
{
}

void ShipRoof::update()
{
	shield = ship->getShield();
	if (fadeRate != 0)
	{



		fadeLevel += fadeRate;

		if (fadeLevel >= 1)
		{
			fadeLevel = 1;
			fadeRate = 0;
		}
		if  (fadeLevel <=0)
		{
			fadeLevel = 0;
			fadeRate = 0;
		}

		float a = (0.5 + (cos( fadeLevel * PI )/2.0f));
		setAlpha(a);
	}

}


void ShipRoof::fadeIn()
{
	fadeRate = -0.05;
}

void ShipRoof::fadeOut()
{
	fadeRate = 0.02;
}

void ShipRoof::setAlpha(float a)
{
	if (ship!=0) ship->setAlpha(a);
	if (shield!=0) shield->setAlphaModifier( a/2.0f + 0.5 );
}

void ShipRoof::stopFade()
{
	fadeRate = 0;
}

