/*
 * Beam.h
 *
 *  Created on: 17 Feb 2015
 *      Author: Adam Nasralla
 */

#ifndef SRC_SHIP_BEAM_H_
#define SRC_SHIP_BEAM_H_

#include <osogine/game/Entity.h>

class Beam: public Entity
{
public:
	Beam(Entity* parent, Entity* target);
	~Beam();

protected:
	float width;
	Entity* target;
	float range;
	void initGfx();
	void updateGfx();
	bool isInRange();

};

#endif /* SRC_SHIP_BEAM_H_ */
