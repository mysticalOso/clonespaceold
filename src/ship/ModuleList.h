/*
 * ModuleList.h
 *
 *  Created on: 5 Feb 2014
 *      Author: Adam Nasralla
 */

#ifndef MODULELIST_H_
#define MODULELIST_H_

#include <osogine/game/EntityList.h>
#include <string>

class Module;

class ModuleList : public EntityList
{
public:
	ModuleList();
	~ModuleList();
	void turnOn(bool isOn1);
	void setThrust(string direction, bool isOn);
	int getAmmo();
	void addAmmo();
	bool needsAmmo();

	Module* getModule(string name);

};

#endif /* MODULELIST_H_ */
