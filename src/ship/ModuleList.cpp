/*
 * ModuleList.cpp
 *
 *  Created on: 5 Feb 2014
 *      Author: Adam Nasralla
 */

#include <ship/ModuleList.h>
#include <ship/Module.h>

ModuleList::ModuleList()
{
}

ModuleList::~ModuleList()
{
}

void ModuleList::turnOn(bool isOn1)
{
	for( int i=0; i<size(); i++ )
	{
		((Module*)get(i))->turnOn(isOn1);
	}
}

int ModuleList::getAmmo()
{
	int total = 0;
	for( int i=0; i<size(); i++ )
	{
		total = ((Module*)get(i))->getAmmo();
	}
	return total;
}

void ModuleList::setThrust(string direction, bool isOn)
{
	for( int i=0; i<size(); i++ )
	{
		((Module*)get(i))->setMoveDir(direction,isOn);
	}
}

void ModuleList::addAmmo()
{
	for( int i=0; i<size(); i++ )
	{
		((Module*)get(i))->addAmmo();
	}
}

bool ModuleList::needsAmmo()
{
	bool need = false;
	for( int i=0; i<size(); i++ )
	{
		if (((Module*)get(i))->needsAmmo())
		{
			need = true;
		}
	}
	return need;
}

Module* ModuleList::getModule(string name)
{
	Module* module = 0;
	for( int i=0; i<size(); i++ )
	{
		Module* m = ((Module*)get(i));
		if (m->getName() == name) module = m;
	}
	return module;
}
