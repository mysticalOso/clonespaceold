/*
 * Shield.h
 *
 *  Created on: 30 Jan 2014
 *      Author: Adam Nasralla
 */

#ifndef SHIELD_H_
#define SHIELD_H_

#include <ship/Module.h>

class EnergyCore;

class Shield : public Module
{
public:
	Shield(Game* game);
	~Shield();
	void update();
	void hit(float damage);
	void setAlphaModifier(float a){alphaModifier = a;}
	void turnOn(bool isOn1);
	void install(Ship* ship);

private:
	float maxAlpha;
	float alpha;
	float alphaModifier;
	float alphaAnimSpeed;
	float energyCost;
	float hitEnergyCost;
	EnergyCore *core;

	void updateAlpha();
	void initGfx();


};
#endif /* SHIELD_H_ */
