/*
 * Steering.h
 *
 *  Created on: 31 Jan 2014
 *      Author: Adam Nasralla
 */

#ifndef STEERING_H_
#define STEERING_H_

#include <ship/Module.h>
#include <glm/vec2.hpp>

class Clone;
class Vessel;

class Steering : public Module
{
public:
	Steering(float turningPower1, Game* game);
	~Steering();
	void update();
	void setTarget(float x1, float y1, bool isMouse);

	void install(Ship *s);
	void install(Clone* c);

	void use(Clone* c);


private:
	void updateTarget();

	Vessel* entity;
	float turningPower;
	float targetA;
	vec2 dir;
	vec2 target;
	vec2 mouseTarget;
	bool useMouseTarget;
};

#endif /* STEERING_H_ */
