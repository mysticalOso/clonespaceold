/*
 * ShipRoof.h
 *
 *  Created on: 13 Mar 2014
 *      Author: Adam Nasralla
 */

#ifndef SHIPROOF_H_
#define SHIPROOF_H_

#include <osogine/game/Entity.h>


class Ship;
class Shield;

class ShipRoof: public Entity
{
public:
	ShipRoof(Ship* ship);
	~ShipRoof();
	void update();
	void fadeIn();
	void fadeOut();
	void setAlpha(float a);
	void stopFade();

private:
	float fadeLevel;
	float fadeRate;
	Ship* ship;
	Shield* shield;


};

#endif /* SHIPROOF_H_ */
