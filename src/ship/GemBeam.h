/*
 * GemBeam.h
 *
 *  Created on: 25 Jan 2015
 *      Author: mysticalOso
 */

#ifndef SRC_SHIP_GEMBEAM_H_
#define SRC_SHIP_GEMBEAM_H_

#include <ship/Beam.h>

class GemTractor;
class GemHub;
class Gem;

class GemBeam: public Beam {
public:
	GemBeam(GemTractor* tractor, Gem* gem);
	virtual ~GemBeam();
	void update();
	void render();

private:

	void updateGem();
	void beamUpGem();
	void releaseGem();
	GemTractor* tractor;
	GemHub* gemHub;
	Gem* currentGem;
	int count;
};

#endif /* SRC_SHIP_GEMBEAM_H_ */
