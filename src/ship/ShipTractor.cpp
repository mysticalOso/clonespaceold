/*
 * ShipTractor.cpp
 *
 *  Created on: 17 Feb 2015
 *      Author: Adam Nasralla
 */

#include <hubs/ShipHub.h>
#include <osogine/game/Game.h>
#include <osogine/utils/Printer.h>
#include <ship/Ship.h>
#include <ship/ShipBeam.h>
#include <ship/ShipTractor.h>
#include <space/Gem.h>

ShipTractor::ShipTractor(Game* game) : Module(game)
{
	shipHub = 0;
	gems = new EntityList();
	setIsVisible(true);
}

ShipTractor::~ShipTractor()
{

}

void ShipTractor::update()
{
	Module::update();
	if (children->size()<1) scanShips();

	if (children->size()>0) game->event("shipBeamOn");
	else game->event("shipBeamOff");
}


void ShipTractor::install(Ship* ship)
{
	this->ship = ship;
	shipHub = ship->getHub();
	ship->setShipTractor(this);
}

void ShipTractor::scanShips()
{
	//Printer::print("Num Ship Beams = ",children->size(), "\n");
	Ship* targetShip = (Ship*) shipHub->getClosestAlly(ship);
	if (targetShip!=0 &&
			targetShip->distTo2(ship) < 400*400 && shipNeedsBeam(targetShip))
	{
		children->add( new ShipBeam(this, targetShip));
	}
}

bool ShipTractor::shipNeedsBeam(Ship* ship)
{
	if (ship!=0)
	{
		EntityList* gems = ship->getGems();
		int gemCount = 0;
		if (gems!=0) gemCount = gems->size();
		return (!ship->hasMaxHealth() || gemCount>0 || ship->needsAmmo());
	}
	else
	{
		return false;
	}
}

void ShipTractor::addGem(Gem* gem)
{
	gems->add(gem);
}
