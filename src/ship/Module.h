/*
 * Module.h
 *
 *  Created on: 30 Jan 2014
 *      Author: Adam Nasralla
 */

#ifndef MODULE_H_
#define MODULE_H_

#include <osogine/game/Entity.h>
#include <glm/vec2.hpp>

using namespace glm;

class Ship;
class ShipHub;
class Clone;

class Module : public Entity
{
public:
	Module(Game* game);
	virtual ~Module();

	virtual bool getIsOn(){return isOn;}
	virtual void turnOn(bool isOn1){isOn = isOn1;}
	virtual int getAmmo(){return 0;}
	virtual void install(Ship* ship){this->ship = ship;}
	//virtual vec2 getCode(){ return vec2(type,grade); }
	virtual void setCode(int type1, int grade1){ type = type1; grade = grade1; }
	virtual void addAmmo(){}
	virtual void setMoveDir(string direction, bool isOn){}

	virtual Ship* getShip(){ return ship; }
	virtual ShipHub* getShipHub();

	virtual vec3 getVelocity();
	//Cost getCost();
	virtual std::string getTitle(){return "";}

	virtual void use(Clone* clone){}

	vec2 getCode(){ return vec2(type,grade); }

	void setIconName(string name){ iconName = name; }
	string getIconName(){ return iconName; }

	vec3 getColour(){ return colour; }
	void setColour(vec3 colour){ this->colour = colour; }

	virtual bool needsAmmo(){return false; }

	void setName(string name){ this->name = name; }

	string getName(){ return name; }
	//std::string getDescription();
	//virtual const char* getImageFile();

	//virtual Module* clone();

protected:

	Ship *ship;
	bool isOn;
	int type;
	int grade;
	vec3 colour;
	string iconName;
	string name;



};

#endif /* MODULE_H_ */
