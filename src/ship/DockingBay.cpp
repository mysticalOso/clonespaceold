/*
 * DockingBay.cpp
 *
 *  Created on: 9 Feb 2015
 *      Author: mysticalOso
 */

#include <hubs/ShipHub.h>
#include <osogine/utils/Printer.h>
#include <ship/DockingBay.h>
#include <ship/Ship.h>

DockingBay::DockingBay(Game* game) : Module(game)
{
	shipHub = 0;
	playerShip = 0;
	dist = 0;
	name = "dockingBay";
	enabled = false;
}

DockingBay::~DockingBay()
{

}

void DockingBay::update()
{
	Module::update();

	//ship->rotateZ(0.002);

	if (enabled)
	{
		playerShip = shipHub->getPlayerShip();

		//Printer::print(playerShip->getName());

		if (playerShip!=0 && playerShip!=ship)
		{
			checkDistance();
		}
	}

	//ship->setAlpha(1);
}



void DockingBay::checkDistance()
{
	dist = distTo(playerShip);
	if (dist < 100)
	{
		fadeRoof();
		checkLanding();
	}
	else
	{
		ship->setTakingOff(false);
		ship->setAlpha(1);
	}
}

void DockingBay::fadeRoof()
{
	if (dist > 50)
	{
		ship->setAlpha( (dist - 50.0f) / 50.0f );
	}
	else
	{
		ship->setAlpha(0);
	}

}

void DockingBay::checkLanding()
{
	if (dist < 40 && !playerShip->getTakingOff())
	{
		if (playerShip->getSpeed() < 5)
		{
			shipHub->dock(playerShip, ship);
		}
	}
}


void DockingBay::install(Ship* ship)
{
	this->ship = ship;
	shipHub = ship->getHub();
	ship->addPassiveModule(this);
}
