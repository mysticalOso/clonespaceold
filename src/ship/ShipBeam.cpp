/*
 * ShipBeam.cpp
 *
 *  Created on: 17 Feb 2015
 *      Author: Adam Nasralla
 */

#include <osogine/utils/Printer.h>
#include <ship/Ship.h>
#include <ship/ShipBeam.h>
#include <ship/ShipTractor.h>
#include <space/Gem.h>



ShipBeam::ShipBeam(ShipTractor* tractor, Ship* ship) : Beam(tractor, ship)
{
	//Printer::print("NEW SHIP BEAM\n");
	this->tractor = tractor;
	this->ship = ship;
	target = ship;
	renderOrder = 4;
	updateOrder = 2;
	width = 30;
	range = 400;
	count = 0;
	setIsVisible(true);
	initGfx();
	updateGfx();
}

ShipBeam::~ShipBeam()
{
}

void ShipBeam::update()
{
	if ((isInRange()) && tractor->shipNeedsBeam(ship))
	{
		updateShip();
		updateGfx();
	}
	else
	{
		tractor->removeChild(this);
		//delete this;
	}
}

void ShipBeam::updateShip()
{
	count++;
	ship->heal(0.1);
	if ((count%10)==0)
	{
		EntityList* gems = ship->getGems();
		if (gems!=0)
		{
			if (gems->size()>0)
			{
				Gem* gem = (Gem*) gems->get(0);
				gems->remove(gem);
				tractor->addGem(gem);
			}
		}
	}
	if (count==50)
	{
		count = 0;
		ship->addAmmo();

	}
}


