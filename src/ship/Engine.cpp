/*
 * Engine.cpp
 *
 *  Created on: 30 Jan 2014
 *      Author: Adam Nasralla
 */

#include <clone/Clone.h>
#include <glm/vec3.hpp>
#include <osogine/game/Game.h>
#include <ship/Ship.h>
#include <ship/Engine.h>
#include <ship/Thrust.h>
#include <osogine/utils/Transform.h>

using namespace glm;


Engine::Engine(float forwardPower1, float sidePower1,
		float backPower1, float friction1, Game* game) : Module(game)
{
	ship = 0;
	entity = 0;

	forwardPower = forwardPower1;
	sidePower = sidePower1;
	backPower = backPower1;
	friction = friction1;

	setPosition(vec3(0,-6,0));

	forwardOn = leftOn = rightOn = backOn = false;

	thrust = new Thrust(this);

	updateOrder = 1;

	isCloneEngine = false;
}


Engine::~Engine()
{
}

void Engine::update()
{
	Module::update();

	float forceX = 0;
	float forceY = 0;

	vec3 dir = getDirection();

	if (isCloneEngine) dir = vec3(0,1,0);

	float modifier = 1;
	if (leftOn || rightOn)
	{
		modifier = 0.5;
		if (isCloneEngine) modifier = 0.7;
	}



	if (forwardOn)
	{
		game->event("shipForward");
		forceX += dir.x*forwardPower*modifier;
		forceY += dir.y*forwardPower*modifier;
	}
	if (backOn)
	{
		forceX -= dir.x*backPower*modifier;
		forceY -= dir.y*backPower*modifier;
	}

	if (!isCloneEngine) modifier = 1;

	if (leftOn)
	{
		forceX -= dir.y*sidePower*modifier;
		forceY += dir.x*sidePower*modifier;
	}
	if (rightOn)
	{
		forceX += dir.y*sidePower*modifier;
		forceY -= dir.x*sidePower*modifier;
	}

	if (!forwardOn) thrust->turnOn(false);

	entity->applyForce( forceX, forceY, 0 );



}

void Engine::setMoveDir(string dir, bool isOn1)
{
	if (dir=="forward")
	{
		forwardOn=isOn1;
		thrust->turnOn(isOn1);
	}
	if (dir=="back")
	{
		backOn=isOn1;
	}
	if (dir=="left")
	{
		leftOn=isOn1;
	}
	if (dir=="right")
	{
		rightOn=isOn1;
	}
	isOn = (forwardOn || backOn || leftOn || rightOn);
}

void Engine::turnOn(bool isOn1)
{
	if (!isOn1)
	{
		forwardOn = false;
		backOn = false;
		leftOn = false;
		rightOn = false;
	}
	isOn = isOn1;
}

void Engine::install(Ship* ship1)
{
	ship = ship1;
	ship->addEngine(this);
	ship->setFriction(friction);
	parent = ship;
	entity = ship;
	adjustForArea();
}

void Engine::install(Clone* clone)
{
	clone->addEngine(this);
	clone->setFriction(friction);
	isCloneEngine = true;
	parent = clone;
	entity = clone;

}

void Engine::toggleThrust(bool isOn)
{
	if (isOn) addChild(thrust);
	else removeChild(thrust);
}

//TODO replace adjustForArea with mass
void Engine::adjustForArea()
{
//	forwardPower *= (315.0/ship->getArea());
//	sidePower *= (315.0/ship->getArea());
//	backPower *= (315.0/ship->getArea());
}

vec3 Engine::getDirection()
{
	return entity->getTransform()->getLocalDirection();
}
