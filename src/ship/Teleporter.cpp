/*
 * Teleporter.cpp
 *
 *  Created on: 1 Mar 2015
 *      Author: Adam Nasralla
 */

#include <clone/Clone.h>
#include <core/CameraCS.h>
#include <core/Vessel.h>
#include <hubs/CloneHub.h>
#include <hubs/LightHub.h>
#include <hubs/ShipHub.h>
#include <interior/Interior.h>
#include <osogine/audio/SoundInterface.h>
#include <osogine/game/Game.h>
#include <osogine/game/Light.h>
#include <osogine/utils/Utils.h>
#include <ship/Ship.h>
#include <ship/Teleporter.h>

Teleporter::Teleporter(Game* game) : Module(game)
{
	camera = (CameraCS*) game->getCamera();
	count = 0;
	clone = 0;
}

Teleporter::~Teleporter()
{

}

void Teleporter::install(Ship* ship)
{
	this->ship = ship;
	ship->addPassiveModule(this);
}

void Teleporter::use(Clone* clone)
{
	ShipHub* hub = ship->getHub();
	this->clone = clone;
	Ship* targetShip = (Ship*) hub->getClosestEnemy(ship,ship->getRace());
	//Ship* targetShip = (Ship*) hub->getClosestAlly(ship);
	if (targetShip!=0 && targetShip->getInterior()!=0)
	{
		count = 1;
	}
}

void Teleporter::update()
{
	Module::update();
	if (count>0)
	{
		count++;
		if (count==2)
		{
			CloneHub* startHub = clone->getHub();
			SoundInterface::teleport();
			if (LIGHTS)
			{
				LightHub* lightHub = startHub->getLightHub();
				Light* light = lightHub->addLight(clone->getLocalPosition(), vec3(0,0,0));
				light->explode(30,60,vec3(1,0.5,1));
			}
		}

		if (count<=60)
		{
			float ratio = (float)count/60.0f;
			clone->setAlpha( Utils::sinInterpolate(1,0,ratio));
			clone->setAlpha(0);

		}

		if (count==60)
		{
			ShipHub* hub = ship->getHub();
			Ship* targetShip = (Ship*) hub->getClosestEnemy(ship,ship->getRace());
			if (targetShip!=0)
			{
				ship->fadeRoofIn();
				targetShip->fadeRoofOut();
				CloneHub* startHub = clone->getHub();
				CloneHub* targetHub = targetShip->getInterior()->getCloneHub();
				startHub->remove(clone);
				targetHub->add(clone);
				clone->setPosition(2,-4,0.2);
				game->event("teleported");
			}
		}
		if (count==130)
		{
			CloneHub* endHub = clone->getHub();
			if (LIGHTS)
			{
				LightHub* lightHub = endHub->getLightHub();
				Light* light = lightHub->addLight(clone->getLocalPosition(), vec3(0,0,0));
				light->explode(30,60,vec3(1,0.5,1));
			}
		}
		if (count>130 && count<210)
		{
			float ratio = (float)(count-130)/80.0f;
			clone->setAlpha( Utils::sinInterpolate(0,1,ratio));
		}
		if (count==210) count = 0;
	}
}
