/*
 * ThrustParticle.cpp
 *
 *  Created on: 19 Nov 2014
 *      Author: mysticalOso
 */

#include <osogine/game/Game.h>
#include <osogine/gfx/GfxBuilder.h>
#include <osogine/gfx/GfxHub.h>
#include <osogine/gfx/GfxObject.h>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <ship/Thrust.h>
#include <ship/ThrustParticle.h>
#include <osogine/utils/List.h>
#include <osogine/utils/Printer.h>
#include <osogine/utils/Transform.h>
#include <osogine/utils/Utils.h>


ThrustParticle::ThrustParticle(Thrust* thrust, float startPower) : Entity(thrust)
{
	this->thrust = thrust;
	power = startPower;
	float s = power/thrust->getMaxPower();
	setScale(s,s,s);
	transform->setParent(0);

	//TODO remove tp gfx
	GfxBuilder* builder = game->getGfxHub()->getBuilder();
	List<vec2>* outline = new List<vec2>();
	outline->add(vec2(-5,0));
	outline->add(vec2(0,5));
	outline->add(vec2(5,0));
	outline->add(vec2(0,-5));

	setGfx( builder->buildTriMesh(outline,10,vec3(1,1,1),vec3(1,1,1),false,false));

	gfx->setColour(getColour());
	//setProgram("flat");
}

ThrustParticle::~ThrustParticle()
{
}

void ThrustParticle::update()
{
	Entity::update();
	power--;
	float s = power/thrust->getMaxPower();
	if (s<0.2) s = 0.2;
	setScale(s,s,s);
	gfx->setColour(getColour());
	if (power<=0)
	{

		thrust->removeChild(this);
		//delete this;
	}
}

vec4 ThrustParticle::getColour()
{
	vec4 col;
	float midPoint = thrust->getMaxPower() * 0.3;
	if (power >= midPoint)
	{
		float c = (power-midPoint) / (thrust->getMaxPower()-midPoint);
		//Printer::print("c",c,"\n");
		col = Utils::interpolate(thrust->getColour2(),thrust->getColour1(),c);
	}
	else
	{
		float c = power / midPoint;
		col = Utils::interpolate(vec4(0,0,0,0),thrust->getColour2(),c);
		//col =  vec4(c*0.9+0.1,0,0,c*0.8);
	}
	return col;


}

float ThrustParticle::getSize()
{
	float s = power/thrust->getMaxPower() * thrust->getWidth();
	if (s<1) s = 1;
	return s;
}
