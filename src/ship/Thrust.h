/*
 * Thrust.h
 *
 *  Created on: 19 Nov 2014
 *      Author: mysticalOso
 */

#ifndef SRC_SHIP_THRUST_H_
#define SRC_SHIP_THRUST_H_

#include <osogine/game/Entity.h>
#include <glm/vec4.hpp>

class Thrust: public Entity {
public:
	Thrust(Entity* engine);
	virtual ~Thrust();
	void update();
	float getMaxPower(){return maxPower;}
	void turnOn(bool isOn){ this->isOn = isOn; }

	void setPropel(float propel){ this->propel = propel; }
	void setMaxPower(float maxPower){ this->maxPower = maxPower; }
	void setStartPower(float startPower){ currentPower = startPower; }
	void setWidth(float width){ this->width = width; }
	void setColour1(vec4 colour1){ this->colour1 = colour1; }
	void setColour2(vec4 colour2){ this->colour2 = colour2; }
	void setColour3(vec4 colour3){ this->colour2 = colour3; }
	float getWidth(){ return width; }
	vec4 getColour1(){ return colour1; }
	vec4 getColour2(){ return colour2; }
	vec4 getColour3(){ return colour3; }
private:
	Entity* engine;
	bool isOn;
	float width;
	float currentPower;
	float maxPower;
	float propel;

	vec4 colour1;
	vec4 colour2;
	vec4 colour3;

	void addParticle();
	void updateGfx();
};

#endif /* SRC_SHIP_THRUST_H_ */
