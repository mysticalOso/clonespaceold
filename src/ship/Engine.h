/*
 * Engine.h
 *
 *  Created on: 30 Jan 2014
 *      Author: Adam Nasralla
 */

#ifndef ENGINE_H_
#define ENGINE_H_

#include <ship/Module.h>

class Ship;
class Thrust;
class Clone;

class Engine : public Module
{
public:
	Engine(float forwardPower1, float sidePower1,
			float backPower1, float friction, Game* game);
	~Engine();
	void update();
	void setMoveDir(string, bool);
	void turnOn(bool isOn1);
	void install(Ship *ship1);
	void install(Clone *clone);
	vec3 getDirection();
	bool getForwardIsOn(){return forwardOn;}

	void toggleThrust(bool b);

private:

	void adjustForArea();

	float forwardPower;
	float sidePower;
	float backPower;
	float friction;

	bool forwardOn;
	bool leftOn;
	bool rightOn;
	bool backOn;

	bool isCloneEngine;

	Entity* entity;

	Thrust* thrust;



};

#endif /* ENGINE_H_ */
