/*
 * Teleporter.h
 *
 *  Created on: 1 Mar 2015
 *      Author: Adam Nasralla
 */

#ifndef SRC_SHIP_TELEPORTER_H_
#define SRC_SHIP_TELEPORTER_H_

#include <ship/Module.h>

class CameraCS;

class Teleporter: public Module
{
public:
	Teleporter(Game* game);
	~Teleporter();

	void update();

	void install(Ship* ship);

	void use(Clone* clone);

private:
	CameraCS* camera;
	int count;
	Clone* clone;

};

#endif /* SRC_SHIP_TELEPORTER_H_ */
