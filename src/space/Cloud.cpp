/*
 * Cloud.cpp
 *
 *  Created on: 1 Nov 2014
 *      Author: mysticalOso
 */

#include <space/CloudGroup.h>
#include <space/Cloud.h>
#include <osogine/gfx/GfxObject.h>
#include <osogine/utils/Dice.h>

Cloud::Cloud(CloudGroup* group) : TextureEntity(group)
{
	this->group = group;
	setPosition(0,0,0);
	//setScale(2,2,2);
	setTextureAlpha(1);
	renderOrder = 6;
	setVelocity(Dice::roll(vec3(-10,-10,0),vec3(10,10,0)));
	//setIsVisible(false);
}

Cloud::~Cloud()
{

}


void Cloud::update()
{
	if (group!=0)
	{
		vec3 pos = getPosition() - group->getPosition();
		pos.z = 0;
		pos = (-pos) * 0.00002f;
		setForce(pos);
	}
	Entity::update();

}

void Cloud::render()
{
	Entity::render();
}

Cloud::Cloud(Game* game) : TextureEntity(game)
{
	group = 0;
	setPosition(0,0,1000);
}
