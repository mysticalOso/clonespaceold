/*
 * Space.cpp
 *
 *  Created on: 15 Oct 2014
 *      Author: Adam Nasralla
 */

#include <hubs/AsteroidHub.h>
#include <hubs/BulletHub.h>
#include <hubs/CloudHub.h>
#include <hubs/ExplosionHub.h>
#include <hubs/ShipHub.h>
#include <space/Space.h>
#include <space/StarSystem.h>
#include <test/SpaceTest.h>
#include <core/Constants.h>
#include <hubs/GemHub.h>
#include <osogine/game/Game.h>

#include <osogine/gfx/GfxBuilder.h>

Space::Space(Game* game) : Entity(game)
{
	gemHub = 0;
	asteroidHub = 0;
	shipHub = 0;
	bulletHub = 0;
	cloudHub = 0;
	starSystem = 0;
	explosionHub = 0;
	tempBack = 0;

	//tempBack = new Entity(this);
	//initTempBack();
}

void Space::init()
{
	//
	//	//TODO all hubs init separate to construct to ensure all have access to eachother
	//

	if (!TEST_MODE)
	{
		bulletHub = new BulletHub(this);
		asteroidHub = new AsteroidHub(this);
		shipHub = new ShipHub(this);
		starSystem = new StarSystem(this);
		explosionHub = new ExplosionHub(this);
		cloudHub = new CloudHub(this);
		gemHub = new GemHub(this);
//
		asteroidHub->init();
		bulletHub->init();
		shipHub->init();

	}
	else
	{
		SpaceTest* test = new SpaceTest(this);
		shipHub = new ShipHub(this);
		gemHub = new GemHub(this);
		test->init();
	}
}



Space::~Space()
{
	//delete asteroidHub;
	//delete shipHub;
	//delete bulletHub;
}

void Space::initStars()
{
	starSystem->init();
}

void Space::initTempBack()
{
	tempBack->setProgram("simple");
	GfxBuilder* builder = game->getGfxBuilder();
	tempBack->setGfx( builder->buildQuad(WIDTH,HEIGHT,vec4(0,1,0,1)));
}
