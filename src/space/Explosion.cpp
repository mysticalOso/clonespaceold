/*
 * Explosion.cpp
 *
 *  Created on: 10 Dec 2014
 *      Author: mysticalOso
 */

#include <osogine/game/EntityList.h>
#include <osogine/gfx/GfxObject.h>
#include <osogine/gfx/GfxProgram.h>
#include <glm/glm.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <glm/matrix.hpp>
#include <hubs/ExplosionHub.h>
#include <space/Explosion.h>
#include <space/ExplosionParticle.h>
#include <osogine/utils/Dice.h>
#include <osogine/utils/Printer.h>
#include <osogine/utils/Utils.h>



Explosion::Explosion(ExplosionHub* hub) : Entity(hub)
{
	this->hub = hub;
	//applyInverseOf(hub);

	maxTime = 0;
	isLines = false;

	time = 0;
	gfx = new GfxObject();
	velocityBuffer = new GfxBuffer<vec3>();
	forceBuffer = new GfxBuffer<vec3>();
	axisBuffer = new GfxBuffer<vec3>();
	spinBuffer = new GfxBuffer<float>();
	lifeBuffer = new GfxBuffer<float>();
	deathBuffer = new GfxBuffer<float>();
	offsetBuffer = new GfxBuffer<vec3>();

	renderOrder = 5;

	setProgram("explosion");
}

Explosion::~Explosion()
{
	//delete gfx;
	//delete velocityBuffer;
	//delete forceBuffer;
	//delete axisBuffer;
	//delete spinBuffer;
	//delete lifeBuffer;
	//delete deathBuffer;
	//delete offsetBuffer;

}

void Explosion::update()
{

	time++;
	if (time==maxTime)
	{
		hub->removeAndDelete(this);
	}

}

void Explosion::render()
{

	if (program!=0 && gfx!=0)
	{
		program->render( this );
	}


}



void Explosion::addParticlesInside(GfxObject* objectGfx,
		int number, float size, float speed, float spinSpeed,
		float maxLife, vec4 colour1, vec4 colour2)
{
	if (maxLife>maxTime) maxTime = maxLife;
	for (int i = 0; i < number; i++)
	{
		ExplosionParticle* p = new ExplosionParticle(this);
		vec3 offset;

		vec3 vertex = objectGfx->getVertex(Dice::roll(0,objectGfx->getNoVertices()-1));

		offset = Utils::interpolate(vec3(0,0,0),vertex,Dice::roll(0.7,0.8));

		p->setSize(size);
		p->setOffset(offset, speed);
		p->setMaxLife(maxLife);
		p->setSpinSpeed(spinSpeed);
		p->setColour(colour1, colour2);
	}
}

void Explosion::addParticlesOf(GfxObject* objectGfx,
		float speed, float spinSpeed, float maxLife)
{
	if (maxLife>maxTime) maxTime = maxLife;
	for (unsigned int i = 0; i < objectGfx->getNoIndices(); i+=3)
	{
		ExplosionParticle* p = new ExplosionParticle(this);
		vec3 p1 = objectGfx->getVertex(objectGfx->getIndex(i));
		vec3 p2 = objectGfx->getVertex(objectGfx->getIndex(i+1));
		vec3 p3 = objectGfx->getVertex(objectGfx->getIndex(i+2));

		p->setPoints(p1,p2,p3,speed);
		p->setMaxLife(maxLife);
		p->setSpinSpeed(spinSpeed);
		p->setNormal(objectGfx->getNormal(objectGfx->getIndex(i)));
		p->setColour(objectGfx->getColour(objectGfx->getIndex(i)));
	}
}

void Explosion::addParticlesOf(List<Triangle> tris, vec3 centre,
		float speed, float spinSpeed, float maxLife)
{
	if (maxLife>maxTime) maxTime = maxLife;
	for (unsigned int i = 0; i < tris.size(); i++)
	{
		ExplosionParticle* p = new ExplosionParticle(this);
		Triangle tri = tris.get(i);
		vec3 p1 = tri.getVertex(0);
		vec3 p2 = tri.getVertex(1);
		vec3 p3 = tri.getVertex(2);

		p->setPoints(p1,p2,p3,speed);
		p->setSpeed(speed,centre);
		p->setMaxLife(maxLife);
		p->setSpinSpeed(spinSpeed);
		p->setNormal(tri.getNormal());
		p->setColour(tri.getColour());
	}
}



void Explosion::addParticles(int number, float size, float speed,
		float spinSpeed, float maxLife, vec4 colour1, vec4 colour2)
{
	if (maxLife>maxTime) maxTime = maxLife;
	for (int i = 0; i < number; i++)
	{
		ExplosionParticle* p = new ExplosionParticle(this);

		p->setSize(size);
		p->setSpeed(speed);
		p->setMaxLife(maxLife);
		p->setSpinSpeed(spinSpeed);
		p->setColour(colour1, colour2);
	}

}

void Explosion::addParticles(vec3 centre, int number, float size, float speed,
		float spinSpeed, float maxLife, vec4 colour1, vec4 colour2)
{
	if (maxLife>maxTime) maxTime = maxLife;
	for (int i = 0; i < number; i++)
	{
		ExplosionParticle* p = new ExplosionParticle(this);

		p->setSize(size);
		p->setOffset(centre,0);
		p->setSpeed(speed,centre);
		p->setMaxLife(maxLife);
		p->setSpinSpeed(spinSpeed);
		p->setColour(colour1, colour2);
	}
}


void Explosion::reset()
{

	gfx->clearAll();
	velocityBuffer->clear();
	forceBuffer->clear();
	spinBuffer->clear();
	axisBuffer->clear();
	lifeBuffer->clear();
	deathBuffer->clear();
	offsetBuffer->clear();
	children->empty();

}

void Explosion::initGfx()
{

	for (int i = 0; i < children->size(); i++)
	{
		ExplosionParticle* p = (ExplosionParticle*) children->get(i);
		int noVertices;
		if (!isLines)
		{
			gfx->addTriangle( p->getPoint(0), p->getPoint(1), p->getPoint(2), p->getColour() );
			noVertices = 3;
		}
		else
		{
			gfx->addLine( p->getPoint(0), p->getPoint(1), vec4(1,0,0,0.2) );
			gfx->addLine( p->getPoint(1), p->getPoint(2), vec4(1,0,0,0.2) );
			gfx->addLine( p->getPoint(2), p->getPoint(0), vec4(1,0,0,0.2) );
			noVertices = 6;
		}
		for (int j = 0; j < noVertices; j++)
		{
			gfx->addNormal( p->getNormal() );
			velocityBuffer->add( p->getVelocity() );
			forceBuffer->add( p->getForce() );
			spinBuffer->add( p->getSpin() );
			axisBuffer->add( p->getAxis() );
			lifeBuffer->add( p->getLife() );
			deathBuffer->add( p->getDeath() );
			offsetBuffer->add( p->getOffset() );
		}
	}
	if (time == 200) time = 0;
}

void Explosion::addVelocity(vec3 velocity)
{
	velocity = vec3( inverse(getRotation()) * vec4(velocity,0) );
	for (int i = 0; i < children->size(); i++)
		{
			ExplosionParticle* p = (ExplosionParticle*) children->get(i);
			p->addVelocity( velocity );
		}
}

void Explosion::addParticlesOf(GfxObject* objectGfx, vec3 centre, float speed,
		float spinSpeed, float maxLife)
{

	if (maxLife>maxTime) maxTime = maxLife;
	for (unsigned int i = 0; i < objectGfx->getNoIndices(); i+=3)
	{
		ExplosionParticle* p = new ExplosionParticle(this);
		vec3 p1 = objectGfx->getVertex(objectGfx->getIndex(i));
		vec3 p2 = objectGfx->getVertex(objectGfx->getIndex(i+1));
		vec3 p3 = objectGfx->getVertex(objectGfx->getIndex(i+2));

		p->setPoints(p1,p2,p3,speed);
		p->setSpeed(speed,centre);
		p->setMaxLife(maxLife);
		p->setSpinSpeed(spinSpeed);
		p->setNormal(objectGfx->getNormal(objectGfx->getIndex(i)));
		p->setColour(objectGfx->getColour(objectGfx->getIndex(i)));
		//Printer::print("Particle Velocity = ",p->getVelocity(),"\n");
	}

}
