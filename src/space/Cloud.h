/*
 * Cloud.h
 *
 *  Created on: 1 Nov 2014
 *      Author: mysticalOso
 */

#ifndef SRC_SPACE_CLOUD_H_
#define SRC_SPACE_CLOUD_H_

#include <osogine/game/TextureEntity.h>

class CloudGroup;
class Game;

class Cloud: public TextureEntity {

public:
	Cloud(CloudGroup* group);
	Cloud(Game* game);
	virtual ~Cloud();
	virtual void update();
	virtual void render();

private:

	CloudGroup* group;
};

#endif /* SRC_SPACE_CLOUD_H_ */
