/*
 * Gem.cpp
 *
 *  Created on: 15 Jan 2015
 *      Author: mysticalOso
 */

#include <hubs/GemHub.h>
#include <space/Gem.h>

Gem::Gem(GemHub* hub) : Entity(hub)
{
	this->hub = hub;
	colour = 0;
	beam = 0;
}

Gem::~Gem() {
	// TODO Auto-generated destructor stub
}

void Gem::update()
{
	Entity::update();

	if (beam==0)
	{
		float alpha = getAlpha();
		if (alpha < 1)
		{
			alpha += 0.02;
			setAlpha(alpha);
		}
	}

}

vec3 Gem::getBarRGB()
{
	vec3 c1;
	if (colour==0) c1 = vec3(0.55,0.55,0.5);
	if (colour==1) c1 = vec3(0.9,0,0);
	if (colour==2) c1 = vec3(0,0.9,0);
	if (colour==3) c1 = vec3(0.6,0.6,1);
	if (colour==4) c1 = vec3(1,1,0);
	if (colour==5) c1 = vec3(0,1,1);
	if (colour==6) c1 = vec3(1,0.3,1);
	return c1;
}
