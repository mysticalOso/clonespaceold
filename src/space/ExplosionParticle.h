/*
 * ExplosionParticle.h
 *
 *  Created on: 10 Dec 2014
 *      Author: mysticalOso
 */

#ifndef SRC_SPACE_EXPLOSIONPARTICLE_H_
#define SRC_SPACE_EXPLOSIONPARTICLE_H_

#include <osogine/game/IEntity.h>
#include <glm/fwd.hpp>
#include <glm/vec4.hpp>
#include <glm/vec3.hpp>
#include <osogine/utils/List.h>

using namespace glm;

class Explosion;

class ExplosionParticle: public IEntity {
public:
	ExplosionParticle(Explosion* parent);
	virtual ~ExplosionParticle();

	void setSpinSpeed(float spinSpeed);
	void setMaxLife(float maxLife);
	void setSize(float size);
	void setSpeed(float speed);
	void setSpeed(float speed, vec3 centre);
	void setOffset(vec3 offset, float speed);
	void setPoints(vec3 p1, vec3 p2, vec3 p3, float speed);

	void addVelocity(vec3 v) { velocity += v; }

	void setColour(vec4 c){colour = c;}
	void setColour(vec4 c1, vec4 c2);

	vec3 getVelocity(){ return velocity; }
	vec3 getForce(){ return force; }
	float getSpin(){ return spin; }
	vec3 getAxis(){ return axis; }
	vec4 getColour(){ return colour; }
	float getLife(){ return life; }
	float getDeath(){ return death; }
	vec3 getPoint(int i){ return points->get(i); }

	vec3 getNormal(){ return normal; }
	void setNormal(vec3 n){ normal = n; }

	void setVelocity(vec3 v){ velocity = v; }
	void setForce(vec3 f){ force = f; }
	void setSpin(float s){ spin = s; }
	void addShift(vec3 shift);

	vec3 getOffset(){ return offset; }

private:
	vec3 rotatePointAboutAxis(vec3 p, float theta, vec3 p1, vec3 p2);
	vec3 velocity;
	vec3 force;
	List<vec3>* points;
	vec4 colour;
	vec3 axis;
	vec3 offset;
	vec3 normal;
	float rotation;
	float spin;
	float life;
	float death;
};

#endif /* SRC_SPACE_EXPLOSIONPARTICLE_H_ */
