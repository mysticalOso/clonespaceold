/*
 * Star.cpp
 *
 *  Created on: 6 Nov 2014
 *      Author: Adam Nasralla
 */

#include <osogine/game/Camera.h>
#include <glm/glm.hpp>
#include <space/Star.h>
#include <osogine/utils/Dice.h>
#include <stdlib.h>
#include <core/Constants.h>

Star::Star(Camera* cam)
{
	camera = cam;
	init();
}

Star::~Star()
{

}

vec3 Star::getVertex(int i, bool flip, bool flip2)
{
	vec3 p;
	if (flip)
	{
		if (i == 0) p = pos + vec3(Dice::roll(vec3(-2,2,0),vec3(2,2,0)));
		if (i == 1) p = pos + vec3(Dice::roll(vec3(2,-2,0),vec3(2,0,0)));
		if (i == 2) p = pos + vec3(Dice::roll(vec3(-2,-2,0),vec3(-2,0,0)));
	}
	else
	{
		if (i == 0) p = pos - vec3(Dice::roll(vec3(-2,2,0),vec3(2,2,0)));
		if (i == 1) p = pos - vec3(Dice::roll(vec3(2,-2,0),vec3(2,0,0)));
		if (i == 2) p = pos - vec3(Dice::roll(vec3(-2,-2,0),vec3(-2,0,0)));
	}

//	if (flip2)
//	{
//		vec3 p2 = p;
//		p2.x = p.y;
//		p2.y = p.x;
//		p = p2;
//	}
	return p;
}

void Star::update()
{

	vec2 clipPos = camera->getClipPos(pos);

	if (clipPos.x > 1 || clipPos.x < -1 || clipPos.y > 1 || clipPos.y < -1)
	{
		pos = vec3( camera->getX()*2 - pos.x, camera->getY()*2 - pos.y, pos.z );
	}



}

void Star::init()
{
	pos = Dice::roll(vec3(-WIDTH2,-HEIGHT2,0),vec3(WIDTH2,HEIGHT2,223.6)) + vec3(camera->getX(),camera->getY(),0);
	pos.z = pos.z * pos.z;
	pos.z =   pos.z - 25000;
	float s = (50000.f-pos.z) / 50000.f;
	pos.x *= s;
	pos.y *= s;
	if (pos.z>0)
	{
		brightness = 1 - pos.z/25000.f;
	}
	else
	{
		brightness = 1;
	}
	//brightness = 1;
	newStar = true;
}
