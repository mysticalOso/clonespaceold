/*
 * Gem.h
 *
 *  Created on: 15 Jan 2015
 *      Author: mysticalOso
 */

#ifndef SRC_SPACE_GEM_H_
#define SRC_SPACE_GEM_H_

#include <osogine/game/Entity.h>

class GemHub;
class GemBeam;

class Gem: public Entity {
public:
	Gem(GemHub* hub);
	//Gem(Game* game);
	virtual ~Gem();

	void setColour(int c){ colour = c; }
	int getColour(){ return colour; }

	void update();

	void setBeam(GemBeam* beam){this->beam = beam;}

	vec3 getBarRGB();

private:
	int colour;
	GemHub* hub;
	GemBeam* beam;
};

#endif /* SRC_SPACE_GEM_H_ */
