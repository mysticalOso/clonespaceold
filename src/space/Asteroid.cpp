/*
 * Asteroid.cpp
 *
 *  Created on: 23 Oct 2014
 *      Author: Adam Nasralla
 */

#include <hubs/AsteroidHub.h>
#include <osogine/game/Entity.h>
#include <space/Asteroid.h>
#include <osogine/utils/Dice.h>
#include <osogine/game/Game.h>
#include <osogine/gfx/LinkedMesh.h>
#include <osogine/gfx/Triangle.h>
#include <osogine/utils/List.h>
#include <osogine/utils/Polygon.h>
#include <osogine/utils/Printer.h>
#include <osogine/utils/Transform.h>

Asteroid::Asteroid(AsteroidHub* hub) : Entity(hub)
{
	this->hub = hub;
	init();
}

Asteroid::Asteroid(Game* game) : Entity(game)
{
	hub = 0;
	health = 6;
	mesh = 0;
}


Asteroid::~Asteroid()
{

}

void Asteroid::update()
{
	Entity::update();
}

void Asteroid::init()
{
	setSpin(0,0, Dice::roll(-0.002,0.002) );

	setVelocity( Dice::roll(0.1,0.2), Dice::roll(-0.1,0.1), 0 );

	health = 6;
	mesh = 0;
}

void Asteroid::hitBy(Bullet* bullet, vec2 point)
{
	health--;
	if (health<=0)
	{
		hub->explode(this);
	}
	else
	{
		chipAway(point);
	}
}

void Asteroid::setOutline(Polygon* p)
{
	Entity::setOutline(p);
	health += p->getArea() / 5000.f;
}

void Asteroid::initMesh()
{
	mesh = new LinkedMesh(gfx);
	mesh->updateGfx();
	gfx = mesh->getGfx();
	Entity::setOutline( mesh->getOutline() );
}

void Asteroid::chipAway(vec2 point)
{
	point = getTransform()->applyInverseTo(point);
	vec2 nearest = outline->getClosestVertex(point);
	List<Triangle> triangles = mesh->removeTrianglesNear(nearest);
	hub->chipAway(this, triangles);
	mesh->updateGfx();
	gfx = mesh->getGfx();
	Entity::setOutline( mesh->getOutline() );
}
