/*
 * Space.h
 *
 *  Created on: 15 Oct 2014
 *      Author: Adam Nasralla
 */

#ifndef SPACE_H_
#define SPACE_H_

#include <osogine/game/Entity.h>

class StarSystem;
class AsteroidHub;
class ShipHub;
class BulletHub;
class CloudHub;
class ExplosionHub;
class GemHub;

class Space : public Entity
{
public:
	Space(Game* game);
	~Space();

	void init();

	ShipHub* getShipHub(){ return shipHub; }
	AsteroidHub* getAsteroidHub(){ return asteroidHub; }
	ExplosionHub* getExplosionHub(){ return explosionHub; }
	BulletHub* getBulletHub(){ return bulletHub; }
	GemHub* getGemHub(){ return gemHub; }

	void initStars();


private:
	AsteroidHub* asteroidHub;
	ShipHub* shipHub;
	BulletHub* bulletHub;
	CloudHub* cloudHub;
	GemHub* gemHub;
	ExplosionHub* explosionHub;
	StarSystem* starSystem;

	Entity* tempBack;

	void initTempBack();

};

#endif /* SPACE_H_ */
