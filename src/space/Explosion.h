/*
 * Explosion.h
 *
 *  Created on: 10 Dec 2014
 *      Author: mysticalOso
 */

#ifndef SRC_SPACE_EXPLOSION_H_
#define SRC_SPACE_EXPLOSION_H_

#include <osogine/game/Entity.h>
#include <osogine/gfx/GfxBuffer.h>
#include <osogine/gfx/Triangle.h>

class ExplosionHub;

class Explosion: public Entity {
public:

	Explosion(ExplosionHub* hub);
	virtual ~Explosion();

	void update();
	void render();

	int getTime(){ return time; }

	bool getIsLines(){ return isLines; }
	void setIsLines(bool b){ isLines = b; }

	void addVelocity(vec3 velocity);

	GfxBuffer<vec3>* getVelocityBuffer(){ return velocityBuffer; }
	GfxBuffer<vec3>* getForceBuffer(){ return forceBuffer; }
	GfxBuffer<vec3>* getAxisBuffer(){ return axisBuffer; }
	GfxBuffer<float>* getSpinBuffer(){ return spinBuffer; }
	GfxBuffer<float>* getLifeBuffer(){ return lifeBuffer; }
	GfxBuffer<float>* getDeathBuffer(){ return deathBuffer; }
	GfxBuffer<vec3>* getOffsetBuffer(){ return offsetBuffer; }

	void addParticles(vec3 centre, int number, float size,
			float speed, float spinSpeed,
			float maxLife, vec4 colour1, vec4 colour2);

	void addParticles(int number, float size,
			float speed, float spinSpeed,
			float maxLife, vec4 colour1, vec4 colour2);

	void addParticlesInside(GfxObject* gfx,
			int number, float size, float speed, float spinSpeed,
			float maxLife, vec4 colour1, vec4 colour2);

	void addParticlesOf(GfxObject* gfx,
			float speed, float spinSpeed, float maxLife);

	void addParticlesOf(GfxObject* gfx, vec3 centre,
			float speed, float spinSpeed, float maxLife);

	void addParticlesOf(List<Triangle> points, vec3 centre,
			float speed, float spinSpeed, float maxLife);

	void reset();

	void initGfx();

private:

	int time;
	bool isLines;

	GfxBuffer<vec3>* velocityBuffer;
	GfxBuffer<vec3>* forceBuffer;
	GfxBuffer<vec3>* axisBuffer;
	GfxBuffer<float>* spinBuffer;
	GfxBuffer<float>* lifeBuffer;
	GfxBuffer<float>* deathBuffer;
	GfxBuffer<vec3>* offsetBuffer;

	int maxTime;
	ExplosionHub* hub;

};

#endif /* SRC_SPACE_EXPLOSION_H_ */
