/*
 * CloudGroup.cpp
 *
 *  Created on: 11 Dec 2014
 *      Author: mysticalOso
 */

#include <hubs/CloudHub.h>
#include <space/CloudGroup.h>

CloudGroup::CloudGroup(CloudHub* hub) : Entity(hub)
{
	this->hub = hub;
}

CloudGroup::CloudGroup(Game* game) : Entity(game)
{
	hub = 0;
}

CloudGroup::~CloudGroup() {
	// TODO Auto-generated destructor stub
}


