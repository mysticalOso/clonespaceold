/*
 * Asteroid.h
 *
 *  Created on: 23 Oct 2014
 *      Author: Adam Nasralla
 */

#ifndef ASTEROID_H_
#define ASTEROID_H_

#include <osogine/game/Entity.h>

class AsteroidHub;
class Polygon;
class Bullet;
class LinkedMesh;

class Asteroid: public Entity
{
public:
	Asteroid(AsteroidHub* hub);
	Asteroid(Game* game);
	~Asteroid();

	void update();

	void setOutline(Polygon* p);

	void initMesh();

	void hitBy(Bullet* bullet, vec2 point);

private:
	AsteroidHub* hub;
	void init();
	void chipAway(vec2 point);
	LinkedMesh* mesh;

	int health;



};

#endif /* ASTEROID_H_ */
