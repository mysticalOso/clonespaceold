/*
 * ExplosionParticle.cpp
 *
 *  Created on: 1 Dec 2014
 *      Author: mysticalOso
 */

#include <space/ExplosionParticle.h>
#include <osogine/utils/Transform.h>
#include <space/Explosion.h>
#include <osogine/utils/Dice.h>
#include <osogine/utils/Utils.h>
#include <glm/geometric.hpp>

ExplosionParticle::ExplosionParticle(Explosion* parent)
{
	parent->addChild(this);
	points = new List<vec3>();
	rotation = 0;
	velocity = force = normal = vec3(0,0,0);
	spin = life = death = 0;
	axis = Dice::roll(vec3(-1,-1,-1),vec3(1,1,1));
	normal = vec3(0,0,-1);
	force = Dice::roll(vec3(-0.001,-0.001,-0.001),vec3(0.001,0.001,0.001));
}


ExplosionParticle::~ExplosionParticle()
{
	//delete points;
}



void ExplosionParticle::setColour(vec4 c1, vec4 c2)
{
	colour = Dice::roll(c1,c2);
}

void ExplosionParticle::setSpeed(float speed)
{
	velocity = Dice::roll(vec3(-1,-1,-1),vec3(1,1,1));
	velocity = normalize(velocity) * Dice::roll( speed*0.25 ,speed );
	velocity = rotatePointAboutAxis(velocity, Dice::roll(5.0f), vec3(0,0,0),
				Dice::roll(vec3(-1,-1,-1),vec3(1,1,1)));

	velocity.z *= 20;
}

void ExplosionParticle::setSpinSpeed(float spinSpeed)
{
	spin = Dice::roll(spinSpeed*0.5,spinSpeed);
}

void ExplosionParticle::setMaxLife(float maxLife)
{
	life = Dice::roll(maxLife*0.25,maxLife*0.5);
	death = Dice::roll(maxLife*0.25,maxLife*0.5);
}

void ExplosionParticle::setOffset(vec3 offset, float speed)
{
	this->offset = offset;

	velocity = offset + Dice::roll(vec3(-10,-10,10),vec3(10,10,10));
	velocity = glm::normalize(velocity)  * Dice::roll(speed*0.4,speed);

	velocity.z *= 20;

}

void ExplosionParticle::setPoints(vec3 p1, vec3 p2, vec3 p3, float speed)
{
	vec3 centre = ( p1 + p2 + p3 ) / 3.0f;
	p1.z = centre.z;
	p2.z = centre.z;
	p3.z = centre.z;
	points->add( p1 - centre);
	points->add( p2 - centre);
	points->add( p3 - centre);
	setOffset(centre, speed);
}





void ExplosionParticle::setSize(float size)
{
	points->clear();

	points->add( (vec3(-3,-3,0) + Dice::roll(vec3(-1,-1,0),vec3(1,1,0))) * size );
	points->add( (vec3(3,-3,0) + Dice::roll(vec3(-1,-1,0),vec3(1,1,0))) * size );
	points->add( (vec3(0,3,0) + Dice::roll(vec3(-1,-1,0),vec3(1,1,0))) * size );

	addShift(Dice::roll(vec3(-2,-2,-2),vec3(2,2,2)) * size);
}



void ExplosionParticle::addShift(vec3 shift)
{
	points->set(0, points->get(0) + shift);
	points->set(1, points->get(1) + shift);
	points->set(2, points->get(2) + shift);
}




//TODO move this to Utils
vec3 ExplosionParticle::rotatePointAboutAxis(vec3 p, float theta, vec3 p1,
		vec3 p2)
{
	return Utils::rotatePointAboutAxis(p,theta,p1,p2);


}

void ExplosionParticle::setSpeed(float speed, vec3 centre)
{
	velocity = (offset-centre) + Dice::roll(vec3(-10,-10,10),vec3(10,10,10));
	velocity = glm::normalize(velocity)  * Dice::roll(speed*0.4,speed);

	velocity.z *= 20;
}
