/*
 * StarSystem.cpp
 *
 *  Created on: 6 Nov 2014
 *      Author: Adam Nasralla
 */

#include <osogine/game/EntityList.h>
#include <osogine/gfx/GfxObject.h>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <space/Space.h>
#include <space/Star.h>
#include <space/StarSystem.h>
#include <osogine/utils/Dice.h>

StarSystem::StarSystem(Space* space) : Entity(space)
{
	updateOrder = 1;
	stars = 0;
	first = true;
}

StarSystem::~StarSystem()
{
}

void StarSystem::initStars(int numStars)
{
	stars = new EntityList();
	for (int i = 0; i < numStars; i++)
	{
		stars->add(new Star(getCamera()));
	}
}

void StarSystem::updateGfx()
{
	for (int i = 0; i < stars->size(); i++)
	{
		bool flip = Dice::chance(2);
		bool flip2 = Dice::chance(2);
		Star* s = (Star*) stars->get(i);

		float c = 0;
		if (!Dice::chance(200))
		{
			c = Dice::roll(0.3,0.5);
		}
		else
		{
			if (Dice::chance(2))
			{
				c = Dice::roll(0.0,0.3);
			}
			else
			{
				c = Dice::roll(0.5,1.0);
			}
		}

		//c = Dice::roll(0.2,0.7);

		for (int j = 0; j < 3; j++)
		{
			gfx->setColour(i*3+j,vec4(c,c,c,c*s->getBrightness()));
			gfx->setVertex(i*3+j,s->getVertex(j,flip,flip2));
		}
	}
}

void StarSystem::initGfx()
{
	gfx = new GfxObject();
	for (int i = 0; i < stars->size(); i++)
	{
		Star* s = (Star*) stars->get(i);
		bool flip = Dice::chance(2);
		bool flip2 = Dice::chance(2);
		for (int j = 0; j < 3; j++)
		{
			gfx->addVertex(vec3(s->getVertex(j,flip,flip2)));
			gfx->addIndex(i*3 + j);
			gfx->addColour(vec4(1,1,1,0.3));
		}
	}
	setProgram("simple");
}

void StarSystem::update()
{

	Entity::update();
	stars->update();
	updateGfx();

}

void StarSystem::init()
{
	if (first)
	{
		initStars(1000);
		initGfx();
		first = false;
	}
	else
	{
		reset();
		updateGfx();
	}
}

void StarSystem::reset()
{
	for (int i = 0; i < 1000; i++)
	{
		Star* star = (Star*) stars->get(i);
		star->init();
	}
}
