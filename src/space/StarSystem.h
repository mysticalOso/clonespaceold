/*
 * StarSystem.h
 *
 *  Created on: 6 Nov 2014
 *      Author: Adam Nasralla
 */

#ifndef SRC_SPACE_STARSYSTEM_H_
#define SRC_SPACE_STARSYSTEM_H_

#include <osogine/game/Entity.h>

class Space;

class StarSystem: public Entity
{
public:
	StarSystem(Space* space);
	~StarSystem();
	void init();

	void reset();

private:
	void initStars(int numStars);
	void updateGfx();
	void initGfx();
	void update();
	EntityList* stars;

	bool first;
};

#endif /* SRC_SPACE_STARSYSTEM_H_ */
