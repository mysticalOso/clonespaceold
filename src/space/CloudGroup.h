/*
 * CloudGroup.h
 *
 *  Created on: 11 Dec 2014
 *      Author: mysticalOso
 */

#ifndef SRC_SPACE_CLOUDGROUP_H_
#define SRC_SPACE_CLOUDGROUP_H_

#include <osogine/game/Entity.h>

class CloudHub;
class Game;

class CloudGroup: public Entity {
public:
	CloudGroup(CloudHub* hub);
	CloudGroup(Game* game);
	virtual ~CloudGroup();

private:
	CloudHub* hub;
};

#endif /* SRC_SPACE_CLOUDGROUP_H_ */
