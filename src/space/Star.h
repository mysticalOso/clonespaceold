/*
 * Star.h
 *
 *  Created on: 6 Nov 2014
 *      Author: Adam Nasralla
 */

#ifndef SRC_SPACE_STAR_H_
#define SRC_SPACE_STAR_H_

#include <osogine/game/IEntity.h>

#include <glm/vec3.hpp>

class Camera;

using namespace glm;

class Star: public IEntity
{
public:
	Star(Camera* cam);
	~Star();

	vec3 getVertex(int i, bool flip, bool flip2);

	float getBrightness(){ return brightness; }

	void update();

	void init();
private:

	vec3 pos;
	Camera* camera;
	float brightness;
	bool newStar;
};

#endif /* SRC_SPACE_STAR_H_ */
