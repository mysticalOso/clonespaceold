/*
 * CloneAI.cpp
 *
 *  Created on: 3 Mar 2015
 *      Author: Adam Nasralla
 */

#include <ai/CloneAI.h>
#include <clone/Clone.h>
#include <core/Vessel.h>
#include <glm/vec2.hpp>
#include <hubs/CloneHub.h>
#include <hubs/RoomHub.h>
#include <osogine/game/EntityList.h>
#include <osogine/utils/List.h>
#include <osogine/utils/Polygon.h>
#include <osogine/utils/Printer.h>
#include <osogine/utils/Transform.h>
#include <osogine/utils/Utils.h>



CloneAI::CloneAI(Game* game)
{
	clone = 0;
	hub = 0;
	interior = 0;
	count = 0;
	wasDriver = false;
}

CloneAI::~CloneAI()
{

}

void CloneAI::activate(Clone* clone)
{
	this->clone = clone;
}

void CloneAI::deactivate()
{
	if (clone!=0) clone->killEngines();
	clone = 0;
}

void CloneAI::update()
{
	if (clone!=0)
	{
		clone->turnOnPrimaryWeapons(false);
		hub = clone->getHub();
		if (hub->containsEnemy(clone))
		{
			if (clone->getIsDriver() || wasDriver)
			{
				if (!hub->containsAlly(clone))
				{
					wasDriver = true;
					clone->setIsDriver(false);
					clone->setArmed(true);

					if (isSouth())
					{
						clone->setMoveDir("back",false);
						attackClosestEnemy();
					}
					else
					{
						clone->setTarget(0,0,false);
						moveSouth();
					}

				}
			}
			else
			{
				clone->setArmed(true);
				attackClosestEnemy();
			}
		}
	}
}

void CloneAI::attackClosestEnemy()
{
	Clone* enemy = (Clone*) getClosestVisibleEnemy();//hub->getClosestEnemy(clone,clone->getRace());
	if (enemy!=0)
	{
//		Printer::print("Xaccy X position = ", clone->getX(),"\n");
//		Printer::print("Xaccy Y position = ", clone->getY(),"\n");
//		Printer::print("Osfro X position = ", enemy->getX(),"\n");
//		Printer::print("Osfro Y position = ", enemy->getY(),"\n");
		clone->setTarget(enemy->getLocalPosition().x,enemy->getLocalPosition().y,false);
		clone->turnOnPrimaryWeapons(true);
	}
}

bool CloneAI::isSouth()
{
	vec2 subjectPos = vec2(clone->getPosition());
	Transform* trans = hub->getTransform();
	vec2 pos = vec2(0,5);
	pos = trans->applyTo(pos);
	float dist = Utils::dist2(subjectPos,pos);
	//Printer::print("Dist = ", dist, "\n");
	return dist<1;
}

void CloneAI::moveSouth()
{
	CloneHub* hub = clone->getHub();
	Transform* trans = hub->getTransform();
	vec2 pos = vec2(0,5);
	pos = trans->applyTo(pos);
	clone->setTarget(pos.x,pos.y,false);
	count++;
	if (count>50) clone->setMoveDir("back",true);
}

Clone* CloneAI::getClosestVisibleEnemy()
{
	EntityList* enemies = hub->getEnemies(clone->getRace());
	vec2 startPos = vec2(clone->getPosition());
	RoomHub* roomHub = hub->getRoomHub();
	List<Polygon*>* obstacles = roomHub->getAllPolygons();
	EntityList* visibleEnemies = new EntityList();
	bool hidden = false;
	for (int i=0; i<enemies->size(); i++)
	{
		Clone* enemy = (Clone*) enemies->get(i);
		vec2 endPos = vec2(enemy->getPosition());
		Polygon* line = new Polygon();
		line->add(startPos);
		line->add(endPos);
		line->setIsLine(true);
		for (int j=0; j<obstacles->size(); j++)
		{
			Polygon* p = obstacles->get(j);
			if (p->intersectWith(line))
			{
				hidden = true;
				j = obstacles->size();
			}
		}
		if (!hidden) visibleEnemies->add(enemy);
		//delete line;
	}
	//delete obstacles;
	return (Clone*)clone->getClosest(visibleEnemies);
}




