/*
 * CloneAI.h
 *
 *  Created on: 3 Mar 2015
 *      Author: Adam Nasralla
 */

#ifndef SRC_AI_CLONEAI_H_
#define SRC_AI_CLONEAI_H_

#include <osogine/game/IEntity.h>

class Clone;
class CloneHub;
class Interior;

class CloneAI : public IEntity
{
public:
	CloneAI(Game* game);
	~CloneAI();

	void activate(Clone* clone);
	void deactivate();

	void update();

private:

	void attackClosestEnemy();

	bool isSouth();
	void moveSouth();
	Clone* getClosestVisibleEnemy();

	Clone* clone;
	CloneHub* hub;
	Interior* interior;
	bool wasDriver;
	int count;

};

#endif /* SRC_AI_CLONEAI_H_ */
