/*
 * XakadienScoutAI.h
 *
 *  Created on: 12 Jan 2015
 *      Author: mysticalOso
 */

#ifndef SRC_AI_BASICSHIPAI_H_
#define SRC_AI_BASICSHIPAI_H_

#include <ai/ShipAI.h>

class BasicShipAI : public ShipAI {
public:
	BasicShipAI(Game* game);
	virtual ~BasicShipAI();

	void update();


};

#endif /* SRC_AI_BASICSHIPAI_H_ */
