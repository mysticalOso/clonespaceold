/*
 * ShipAI.cpp
 *
 *  Created on: 12 Jan 2015
 *      Author: mysticalOso
 */

#include <ai/ShipAI.h>
#include <osogine/utils/Targeter.h>
#include <glm/vec2.hpp>
#include <glm/geometric.hpp>
#include <glm/trigonometric.hpp>
#include <hubs/CloneHub.h>
#include <hubs/GemHub.h>
#include <hubs/ShipHub.h>
#include <interior/Interior.h>
#include <osogine/game/Game.h>
#include <osogine/utils/Dice.h>
#include <osogine/utils/Printer.h>
#include <osogine/utils/Utils.h>
#include <ship/Ship.h>
#include <space/Space.h>
#include <weapons/Weapon.h>

ShipAI::ShipAI(Game* game)
{
	ship = 0;
	targetShip = 0;
	currentWeapon = 0;
	count = 0;
	delay = 5;
	spread = 400;
	sideRight = Dice::chance(2);
	angDiff = 0;
	distToTarget = 0;
	forwardThrust = false;
	rightThrust = false;
	leftThrust = false;
	backThrust = false;
	this->game = game;
	this->space = (Space*) game->getEntity("space");
	this->shipHub = space->getShipHub();
}

ShipAI::~ShipAI()
{
}

void ShipAI::activate(Ship* ship)
{
	playerSlot = Dice::roll(vec3(-WIDTH2*0.3,-HEIGHT2*0.3,0), vec3(WIDTH2*0.3,HEIGHT2*0.3,0));
	this->ship = ship;
}

void ShipAI::deactivate()
{
	if (ship!=0) ship->killEngines();
	ship = 0;
}

Ship* ShipAI::getClosestEnemy()
{
	return (Ship*) shipHub->getClosestEnemy(ship, ship->getRace());
}

vec2 ShipAI::getTargetPos()
{
	vec2 pos = vec2(0,0);
	if (hasTarget() && isActive() && currentWeapon!=0)
	{
		pos = Targeter::getTarget(ship,currentWeapon,
				targetShip, spread, 1.5);
	}
	return pos;
}

void ShipAI::updateTargetPos()
{
	count++;
	if (count == delay)
	{
		count = 0;
		targetPos = getTargetPos();
		updateDistToTarget();
		updateAngDiff();
	}
}

void ShipAI::updateThrust()
{
	ship->setMoveDir("forward",forwardThrust);
	ship->setMoveDir("back",backThrust);
	ship->setMoveDir("left",leftThrust);
	ship->setMoveDir("right",rightThrust);
}

void ShipAI::targetClosestEnemy()
{
	targetShip = getClosestEnemy();
	if (targetShip!=0)
	{
		updateTargetPos();
	}
}

void ShipAI::randomSideThrust(int changeChance)
{
	if (chance(1,changeChance)) sideRight = !sideRight;
	if (sideRight)
	{
		rightThrust = true;
	}
	else
	{
		leftThrust = true;
	}

}

bool ShipAI::targetInRange(float min, float max)
{
	min = min*min;
	if (max==-1) max = 99999999;
	else max = max*max;
	return (distToTarget >= min && distToTarget <= max);
}

bool ShipAI::angleInRange(float min, float max)
{
	return (angDiff >= min && angDiff <= max);
}

bool ShipAI::chance(int nominator, int denominator)
{
	return Dice::chance(nominator,denominator);
}

void ShipAI::updateDistToTarget()
{
	distToTarget = ship->distTo2(targetShip);
}

void ShipAI::faceTarget()
{
	//Printer::print("TargetPos = ",targetPos,"\n");
	ship->setTarget(targetPos.x,targetPos.y,false);
}

//TODO updateAngDiff will not work for all angles
void ShipAI::updateAngDiff()
{
	vec2 shipDir = vec2(ship->getDirection());
	vec2 dirToTarget = vec2(vec3(targetPos,0) - ship->getPosition());
	dirToTarget = dirToTarget / glm::length(dirToTarget);

	float cosAng = glm::dot(shipDir,dirToTarget);

	angDiff = glm::acos(cosAng);
	if (cosAng>1) angDiff = 0;
}

void ShipAI::resetShip()
{
	forwardThrust = false;
	backThrust = false;
	leftThrust = false;
	rightThrust = false;
	ship->turnOnPrimaryWeapons(false);
	//ship->turnOnSecondaryWeapons(false);
}



void ShipAI::usePrimaryWeapon()
{
	currentWeapon = ship->getPrimaryWeapon();
}

void ShipAI::firePrimaryWeapon()
{
	ship->turnOnPrimaryWeapons(true);
}

bool ShipAI::gemsNearBy()
{
	GemHub* gemHub = shipHub->getGemHub();
	if (gemHub->size()>0)
	{
		Entity* gem = ship->getClosest(gemHub->getGems());
		if (gem!=0)
		{
			if (ship->distTo(gem)<1000)
			{
				return true;
			}
		}
	}

	return false;
}

void ShipAI::approachGems()
{
	GemHub* gemHub = shipHub->getGemHub();
	if (gemHub->size()>0)
	{
		Entity* gem = ship->getClosest(gemHub->getGems());
		vec2 pos = vec2(gem->getPosition());
		if (ship->distTo(gem)<1000)
		{
			ship->setTarget(pos.x,pos.y,false);

			vec2 shipDir = vec2(ship->getDirection());
			vec2 dirToTarget = vec2(vec3(pos,0) - ship->getPosition());
			dirToTarget = dirToTarget / glm::length(dirToTarget);

			float cosAng = glm::dot(shipDir,dirToTarget);
			float angDiff2 = glm::acos(cosAng);
			if (cosAng>1) angDiff2 = 0;
			//Printer::print("AngDiff2 = ",angDiff2,"\n");

			float dist = ship->distTo(gem);
			if (dist>80 && ship->getSpeed()<(dist/100.0f) && angDiff2 < 0.2)
			{
				forwardThrust = true;
			}
		}

	}
}

void ShipAI::approachPlayerSlot()
{
	Ship* playerShip = shipHub->getPlayerShip();
	if (playerShip!=0)
	{
		vec2 pos = vec2(shipHub->getPlayerShip()->getPosition() + playerSlot);
		float dist = Utils::dist2(vec2(ship->getPosition()),pos);

		ship->setTarget(pos.x,pos.y,false);

		vec2 shipDir = vec2(ship->getDirection());
		vec2 dirToTarget = vec2(vec3(pos,0) - ship->getPosition());
		dirToTarget = dirToTarget / glm::length(dirToTarget);

		float cosAng = glm::dot(shipDir,dirToTarget);
		float angDiff2 = glm::acos(cosAng);
		if (cosAng>1) angDiff2 = 0;

		//Printer::print("Ship Speed = ",ship->getSpeed(),"\n");

		if (dist>2000 && ship->getSpeed()<(dist/40000.0f) && angDiff2 < 0.2)
		{
			forwardThrust = true;
		}
		else
		{
			forwardThrust = false;
	}
	}

}

bool ShipAI::targetHasAllies()
{
	Interior* interior = targetShip->getInterior();
	if (interior!=0)
	{
		CloneHub* cloneHub = interior->getCloneHub();
		return (cloneHub->getRaceCount(ship->getRace())>0);
	}
	return false;
}
