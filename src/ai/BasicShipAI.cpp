/*
 * XakadienScoutAI.cpp
 *
 *  Created on: 12 Jan 2015
 *      Author: mysticalOso
 */

#include <ai/BasicShipAI.h>
#include <osogine/utils/Printer.h>
#include <ship/Ship.h>

BasicShipAI::BasicShipAI(Game* game) : ShipAI(game)
{

}

BasicShipAI::~BasicShipAI() {
}
//TODO use vectors instead of angles in AI
void BasicShipAI::update()
{
	if (isActive())
	{
		resetShip();
		usePrimaryWeapon();
		targetClosestEnemy();
		if (hasTarget() && !targetHasAllies())
		{
			faceTarget();

			if (chance(1,100)) ship->turnOnShield(true);
			if (chance(1,400)) ship->turnOnShield(false);

			if (chance(1,10)) ship->turnOnSecondaryWeapons(false);

			if (angleInRange(-0.2,0.2) && chance(1,10) && distToTarget<(HEIGHT*HEIGHT)) firePrimaryWeapon();

			if (angleInRange(-0.2,0.2) && chance(1,100) && distToTarget<(HEIGHT*HEIGHT)) ship->turnOnSecondaryWeapons(true);

			if (angleInRange(-0.2,0.2) && targetInRange(330, -1))
			{
				forwardThrust = true;
			}
			else if (targetInRange(0, 330))
			{
				backThrust = true;
			}

			if (targetInRange(300,400)) randomSideThrust(100);
		}
		else if(ship->hasGemTractor() && gemsNearBy())
		{
			approachGems();
		}
		else if (!hasTarget())
		{
			ship->turnOnShield(false);
			ship->turnOnSecondaryWeapons(false);
			approachPlayerSlot();
		}
		updateThrust();
	}
}
