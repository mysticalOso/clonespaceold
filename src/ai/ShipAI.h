/*
 * ShipAI.h
 *
 *  Created on: 12 Jan 2015
 *      Author: mysticalOso
 */

#ifndef SRC_AI_SHIPAI_H_
#define SRC_AI_SHIPAI_H_

#include <osogine/game/IEntity.h>
#include <glm/fwd.hpp>
#include <glm/vec2.hpp>
#include <string>


using namespace std;
using namespace glm;

class Ship;
class ShipHub;
class Space;
class Weapon;

class ShipAI: public IEntity {
public:
	ShipAI(Game* game);
	virtual ~ShipAI();
	void updateTargetPos();
	void activate(Ship* ship);
	void deactivate();


protected:

	Ship* getClosestEnemy();
	vec2 getTargetPos();
	bool targetInRange(float min, float max);
	bool angleInRange(float min, float max);
	bool isActive(){ return ship!=0 ; }
	bool hasTarget(){ return targetShip!=0; }
	bool targetHasAllies();
	void updateThrust();
	void resetShip();
	void updateDistToTarget();
	void targetClosestEnemy();
	void faceTarget();
	void updateAngDiff();
	void usePrimaryWeapon();
	void firePrimaryWeapon();
	void randomSideThrust(int changeChance);
	bool chance(int nominator, int denominator);
	void approachGems();
	void approachPlayerSlot();
	bool gemsNearBy();

	Game* game;
	Ship* ship;
	Ship* targetShip;
	vec2 targetPos;
	Weapon* currentWeapon;
	Space* space;
	ShipHub* shipHub;
	bool forwardThrust;
	bool backThrust;
	bool leftThrust;
	bool rightThrust;

	vec3 playerSlot;

	float distToTarget;
	float spread;
	float angDiff;
	bool sideRight;
	int delay;
	int count;
};

#endif /* SRC_AI_SHIPAI_H_ */
