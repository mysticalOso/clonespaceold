/*
 * UIController.cpp
 *
 *  Created on: 26 Feb 2015
 *      Author: Adam Nasralla
 */

#include <controllers/UIController.h>
#include <ui/UIEntity.h>

UIController::UIController(Game* game) : Controller(game)
{
	ui = 0;
	shiftDown = false;
}

UIController::~UIController()
{
}

void UIController::setEntity(Entity* e)
{
	ui = (UIEntity*) e;
}

void UIController::keyPressed(string key)
{
	if (ui!=0)
	{
		if (key == "lmb")
		{
			ui->mouseDown();
		}
		if (key == "shift")
		{
			shiftDown = true;
		}
		if (key == "1")
		{
			ui->saveLoad(1,shiftDown);
		}
		if (key == "2")
		{
			ui->saveLoad(2,shiftDown);
		}
		if (key == "3")
		{
			ui->saveLoad(3,shiftDown);
		}
		if (key == "4")
		{
			ui->saveLoad(4,shiftDown);
		}
		if (key == "5")
		{
			ui->saveLoad(5,shiftDown);
		}
		if (key == "6")
		{
			ui->saveLoad(6,shiftDown);
		}
		if (key == "7")
		{
			ui->saveLoad(7,shiftDown);
		}
		if (key == "8")
		{
			ui->saveLoad(8,shiftDown);
		}
		if (key == "9")
		{
			ui->saveLoad(9,shiftDown);
		}
		if (key == "0")
		{
			ui->saveLoad(0,shiftDown);
		}
		if (key == "secondaryFire")
		{
			//ui->toggleAction();
		}
	}
}

void UIController::keyReleased(string key)
{
	if (ui!=0)
	{
		if (key == "lmb")
		{
			ui->mouseUp();
		}
		if (key == "shift")
		{
			shiftDown = false;
		}
	}
}

void UIController::mouseMoved(float x, float y)
{
	if (ui!=0)
	{
		ui->mouseMoved(x,y);
	}
}
