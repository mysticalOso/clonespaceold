/*
 * UIController.h
 *
 *  Created on: 26 Feb 2015
 *      Author: Adam Nasralla
 */

#ifndef SRC_CONTROLLERS_UICONTROLLER_H_
#define SRC_CONTROLLERS_UICONTROLLER_H_

#include <osogine/input/Controller.h>

class UIEntity;

class UIController: public Controller
{
public:
	UIController(Game* game);
	~UIController();

	void setEntity(Entity* e);

	void keyPressed(string key);

	void keyReleased(string key);

	void mouseMoved(float x, float y);

	void toggleAction(){}

private:
	UIEntity* ui;
	bool shiftDown;
};

#endif /* SRC_CONTROLLERS_UICONTROLLER_H_ */
