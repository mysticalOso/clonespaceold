/*
 * CameraCtrl.h
 *
 *  Created on: 28 Oct 2014
 *      Author: Adam Nasralla
 */

#ifndef CAMERACTRL_H_
#define CAMERACTRL_H_

#include <osogine/input/Controller.h>
#include <glm/fwd.hpp>
#include <glm/vec3.hpp>

class Transform;
class CameraCS;

class CameraCtrl: public Controller
{
public:
	CameraCtrl(Game* game);
	~CameraCtrl();

	void setEntity(Entity* e);

	void keyPressed(string key);

	void keyReleased(string key);

	void mouseMoved(float x, float y);

	void mouseScrolled(float x, float y);

private:
	CameraCS* camera;

};
#endif /* CAMERACTRL_H_ */
