/*
 * ShipCtrl.cpp
 *
 *  Created on: 29 Oct 2014
 *      Author: Adam Nasralla
 */

#include <controllers/ShipCtrl.h>
#include <ship/Ship.h>
#include <osogine/utils/Printer.h>
#include <glm/vec2.hpp>
#include <osogine/game/Game.h>


ShipCtrl::ShipCtrl(Game* game) : Controller(game)
{
	ship = 0;
}

ShipCtrl::~ShipCtrl()
{

}

void ShipCtrl::setEntity(Entity* e)
{
	ship = (Ship*) e;
}

void ShipCtrl::keyPressed(string key)
{
	if (ship != 0)
	{
		if (key == "forward")
		{
			ship->setMoveDir("forward",true);
		}
		if (key == "back")
		{
			ship->setMoveDir("back", true);
		}
		if (key == "left")
		{
			ship->setMoveDir("left", true);
		}
		if (key == "right")
		{
			ship->setMoveDir("right", true);
		}
		if (key == "lmb")
		{
			game->event("shipLazer");
			ship->turnOnPrimaryWeapons(true);
		}
		if (key == "rmb")
		{
			ship->turnOnShield(true);
			game->event("shieldsActive");
		}
		if (key == "secondaryFire")
		{
			game->event("missilesFired");
			ship->turnOnSecondaryWeapons(true);
		}
	}
}

void ShipCtrl::keyReleased(string key)
{
	if (ship != 0)
	{
		if (key == "forward")
		{
			ship->setMoveDir("forward",false);
		}
		if (key == "back")
		{
			ship->setMoveDir("back", false);
		}
		if (key == "left")
		{
			ship->setMoveDir("left", false);
		}
		if (key == "right")
		{
			ship->setMoveDir("right", false);
		}
		if (key == "lmb")
		{
			ship->turnOnPrimaryWeapons(false);
		}
		if (key == "rmb")
		{
			ship->turnOnShield(false);
		}
		if (key == "secondaryFire")
		{
			ship->turnOnSecondaryWeapons(false);
		}
	}
}

void ShipCtrl::mouseMoved(float x, float y)
{
	game->event("mouseMoved");
	if (ship!=0)
	{
		//Printer::print("Mouse Pos : ",vec2(x,y),"\n");
		ship->setTarget(x ,y , true);
	}
}



void ShipCtrl::mouseScrolled(float x, float y)
{
}
