/*
 * CameraCtrl.cpp
 *
 *  Created on: 17 Oct 2014
 *      Author: Adam Nasralla
 */

#include <controllers/CameraCtrl.h>
#include <core/CameraCS.h>
#include <osogine/game/Game.h>
#include <osogine/utils/Printer.h>

CameraCtrl::CameraCtrl(Game* game) : Controller(game)
{
	camera = 0;
}


CameraCtrl::~CameraCtrl()
{
}

void CameraCtrl::keyPressed(string key)
{
	if (key == "tab")
	{
		camera->targetNextClone();
		game->event("tabPressed");
	}
}


void CameraCtrl::keyReleased(string key)
{
}

void CameraCtrl::mouseMoved(float x, float y)
{
}


void CameraCtrl::mouseScrolled(float x, float y)
{
//	Printer::print("\n");
//	Printer::print("Mouse Scroll X = ",x,"\n");
//	Printer::print("Mouse Scroll Y = ",y,"\n");
//	Printer::print("\n");

	if (y>0)
	{
		game->event("mouseWheel");
		camera->increaseZoom();
	}
	else
	{
		game->event("mouseWheel");
		camera->decreaseZoom();
	}
}

void CameraCtrl::setEntity(Entity* e)
{
	camera = (CameraCS*) e;
}

