/*
 * ShipCtrl.h
 *
 *  Created on: 29 Oct 2014
 *      Author: Adam Nasralla
 */

#ifndef SHIPCTRL_H_
#define SHIPCTRL_H_

#include <osogine/input/Controller.h>

class Ship;

class ShipCtrl : public Controller
{
public:
	ShipCtrl(Game* game);
	~ShipCtrl();

	void setEntity(Entity* e);

	void keyPressed(string key);

	void keyReleased(string key);

	void mouseMoved(float x, float y);

	void mouseScrolled(float x, float y);

private:
	Ship* ship;

};

#endif /* SHIPCTRL_H_ */
