/*
 * RaceCtrl.h
 *
 *  Created on: 3 Mar 2015
 *      Author: Adam Nasralla
 */

#ifndef SRC_CONTROLLERS_RACECTRL_H_
#define SRC_CONTROLLERS_RACECTRL_H_

#include <osogine/input/Controller.h>
#include <string>

class Sentient;
class AlienRace;
class CommLink;
class HexScreen;

class RaceCtrl: public Controller
{
public:
	RaceCtrl(Game* game);
	~RaceCtrl();

	void setEntity(Entity* e);

	void setSentient(Sentient* s){ sentient = s; }

	void keyPressed(string key);

private:

	AlienRace* xacadiens;
	Sentient* sentient;
	CommLink* commLink;
	HexScreen* hex;
	int count;
};

#endif /* SRC_CONTROLLERS_RACECTRL_H_ */
