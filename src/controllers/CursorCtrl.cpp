/*
 * CursorCtrl.cpp
 *
 *  Created on: 12 Mar 2015
 *      Author: mysticalOso
 */

#include <controllers/CursorCtrl.h>
#include <osogine/game/Entity.h>

CursorCtrl::CursorCtrl(Game* game) : Controller(game)
{
	cursor = 0;
}

CursorCtrl::~CursorCtrl()
{
}

void CursorCtrl::mouseMoved(float x, float y)
{
	if (cursor!=0)
	{
		cursor->setPosition(x,y,0);
	}
}
