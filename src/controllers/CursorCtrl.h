/*
 * CursorCtrl.h
 *
 *  Created on: 12 Mar 2015
 *      Author: mysticalOso
 */

#ifndef SRC_CONTROLLERS_CURSORCTRL_H_
#define SRC_CONTROLLERS_CURSORCTRL_H_

#include <osogine/input/Controller.h>

class CursorCtrl: public Controller {
public:
	CursorCtrl(Game* game);
	virtual ~CursorCtrl();

	void setEntity(Entity* e){cursor = e;}
	void mouseMoved(float x, float y);

private:
	Entity* cursor;
};

#endif /* SRC_CONTROLLERS_CURSORCTRL_H_ */
