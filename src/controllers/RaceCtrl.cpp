/*
 * RaceCtrl.cpp
 *
 *  Created on: 3 Mar 2015
 *      Author: Adam Nasralla
 */

#include <controllers/RaceCtrl.h>
#include <core/CameraCS.h>
#include <hubs/UIHub.h>
#include <osogine/game/Entity.h>
#include <osogine/game/Game.h>
#include <osogine/utils/Printer.h>
#include <races/AlienRace.h>
#include <races/Sentient.h>
#include <ui/CommLink.h>
#include <ui/HexScreen.h>
#include <ui/Tutorial.h>


RaceCtrl::RaceCtrl(Game* game)
{
	this->game = game;
	xacadiens = 0;
	sentient = 0;
	commLink = 0;
	hex = 0;
	count = 3;
}

RaceCtrl::~RaceCtrl()
{
	// TODO Auto-generated destructor stub
}

void RaceCtrl::setEntity(Entity* e)
{
	xacadiens = (AlienRace*) e;

}

void RaceCtrl::keyPressed(string key)
{
	//Printer::print(key);
	if (key == "j")
	{
		xacadiens->addScout();
	}
	if (key == "k")
	{
		xacadiens->addMother();
	}
	if (key == "l")
	{
		xacadiens->addKiller();
	}
	if (key == "u")
	{
		sentient->sendTeleporterShip();
	}
	if (key == "i")
	{
		sentient->sendScout();
	}
	if (key == "o")
	{
		sentient->sendMother();
	}
	if (key == "y")
	{
		sentient->addStationClone();
	}
	if (key == "esc")
	{
		Tutorial* tutorial = (Tutorial*) game->getEntity("tutorial");
		if (tutorial!=0)
		{
			tutorial->reset(5);
		}
	}
	if (key == "b")
	{
		Tutorial* tutorial = (Tutorial*) game->getEntity("tutorial");
		if (tutorial!=0)
		{
			tutorial->turnOff();
		}
	}
//
//	if (key == "f" && count == 0)
//	{
//		xacadiens->addScout();
//		count++;
//	}
//	else if (key == "f" && count == 1)
//	{
//		xacadiens->addScout();
//		xacadiens->addScout();
//		count++;
//	}
//	else if (key == "f" && count == 2)
//	{
//		xacadiens->addMother();
//		count++;
//	}
//	else if (key == "f" && count == 3)
//	{
//		UIHub* uiHub = (UIHub*) game->getEntity("uiHub");
//		commLink = uiHub->getCommLink();
//		commLink->showMessage("Warning!! Xacadien armada approacing!", 450);
//		count++;
//	}
//	else if (key == "f" && count == 4)
//	{
//		UIHub* uiHub = (UIHub*) game->getEntity("uiHub");
//		commLink = uiHub->getCommLink();
//		commLink->changeMessage("Sending reinforcements...");
//		count++;
//	}
//	else if (key == "f" && count == 5)
//	{
//		sentient->sendReinforcements();
//		count++;
//	}
//	else if (key == "f" && count == 6)
//	{
//		UIHub* uiHub = (UIHub*) game->getEntity("uiHub");
//		commLink = uiHub->getCommLink();
//		commLink->toggleFace();
//		commLink->showMessage("Hi my name is Xaccy the Xacadien, nice to meet you, prepare to die! P.S. I'm gay...", 300);
//		count++;
//	}
//	else if (key == "f" && count == 7)
//	{
//		xacadiens->addWave(1);
//		count++;
//	}
//	else if (key == "f" && count == 8)
//	{
//		xacadiens->addWave(2);
//		count++;
//	}
//	else if (key == "f" && count == 9)
//	{
//		CameraCS* camera = (CameraCS*) game->getCamera();
//		camera->setZoom(50000);
//		count++;
//	}
//	else if (key == "f" && count == 10)
//	{
//		UIHub* uiHub = (UIHub*) game->getEntity("uiHub");
//		hex = new HexScreen(uiHub);
//		hex->show();
//		count++;
//	}
//	else if (key == "f" && count == 11)
//	{
//		xacadiens->removeAllShips();
//		hex->hide();
//		count++;
//	}
//	else if (key == "f" && count == 12)
//	{
//		sentient->birth();
//		count++;
//	}
//	else if (key == "f" && count == 13)
//	{
//		UIHub* uiHub = (UIHub*) game->getEntity("uiHub");
//		commLink = uiHub->getCommLink();
//		commLink->toggleFace();
//		commLink->showMessage("Clone incubation complete.", 300);
//		count++;
//	}
//	else if (key == "f" && count == 14)
//	{
//		CameraCS* camera = (CameraCS*) game->getCamera();
//
//		camera->setTargetPosition(5000,0,50000,0.49f,0.1f);
//		count++;
//	}

//	if (key == "g")
//	{
//		CameraCS* camera = (CameraCS*) game->getCamera();
//		camera->setTargetPosition(5000,0,50000,0.49f,0.1f);
//	}

}
