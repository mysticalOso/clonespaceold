/*
 * CloneCtrl.cpp
 *
 *  Created on: 6 Feb 2015
 *      Author: mysticalOso
 */

#include <clone/Clone.h>
#include <clone/CloneModel.h>
#include <controllers/CloneCtrl.h>
#include <osogine/game/Game.h>
#include <osogine/utils/Printer.h>

CloneCtrl::CloneCtrl(Game* game) : Controller(game)
{
	clone = 0;

}

CloneCtrl::~CloneCtrl() {
}

void CloneCtrl::setEntity(Entity* e)
{
	clone = (Clone*) e;
}

void CloneCtrl::keyPressed(string key)
{
	if (clone != 0)
	{
		if (key == "forward")
		{
			game->event("cloneMoved");
			clone->setMoveDir("forward",true);
		}
		if (key == "back")
		{
			game->event("cloneMoved");
			clone->setMoveDir("back", true);
		}
		if (key == "left")
		{
			game->event("cloneMoved");
			clone->setMoveDir("left", true);
		}
		if (key == "right")
		{
			game->event("cloneMoved");
			clone->setMoveDir("right", true);
		}
		if (key == "lmb")
		{
			clone->turnOnPrimaryWeapons(true);
		}
		if (key == "secondaryFire")
		{
			clone->use();
		}
		if (key == "rmb")
		{
			game->event("cloneDraw");
			clone->toggleArmed();
		}
//		if (key == "secondaryFire")
//		{
//			clone->turnOnSecondaryWeapons(true);
//		}
	}
}

void CloneCtrl::keyReleased(string key)
{
	if (clone != 0)
	{
		if (key == "forward")
		{
			clone->setMoveDir("forward",false);
		}
		if (key == "back")
		{
			clone->setMoveDir("back", false);
		}
		if (key == "left")
		{
			clone->setMoveDir("left", false);
		}
		if (key == "right")
		{
			clone->setMoveDir("right", false);
		}
		if (key == "lmb")
		{
			clone->turnOnPrimaryWeapons(false);
		}
//		if (key == "rmb")
//		{
//			clone->turnOnShield(false);
//		}
//		if (key == "secondaryFire")
//		{
//			clone->turnOnSecondaryWeapons(false);
//		}
	}
}

void CloneCtrl::mouseMoved(float x, float y)
{
	if (clone!=0)
	{
		clone->setTarget(x,y,true);
	}
}
