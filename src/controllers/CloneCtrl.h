/*
 * CloneCtrl.h
 *
 *  Created on: 6 Feb 2015
 *      Author: mysticalOso
 */

#ifndef SRC_CONTROLLERS_CLONECTRL_H_
#define SRC_CONTROLLERS_CLONECTRL_H_

#include <osogine/input/Controller.h>

class Clone;

class CloneCtrl: public Controller {
public:
	CloneCtrl(Game* game);
	virtual ~CloneCtrl();

	void setEntity(Entity* e);

	void keyPressed(string key);

	void keyReleased(string key);

	void mouseMoved(float x, float y);

private:
	Clone* clone;
};

#endif /* SRC_CONTROLLERS_CLONECTRL_H_ */
