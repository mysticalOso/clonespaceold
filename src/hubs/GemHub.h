/*
 * GemHub.h
 *
 *  Created on: 15 Jan 2015
 *      Author: mysticalOso
 */

#ifndef SRC_HUBS_GEMHUB_H_
#define SRC_HUBS_GEMHUB_H_

#include <osogine/game/Hub.h>

class GemBuilder;
class Space;
class Asteroid;
class Gem;

class GemHub: public Hub {
public:
	GemHub(Space* space);
	virtual ~GemHub();
	void init();

	//TODO Use GemSpectrum class
	void addGems(int number);

	void addGemsFrom(Asteroid* asteroid);

	EntityList* getGems(){ return children; }

	Gem* getGem(int i);


private:
	Space* space;
	GemBuilder* gemBuilder;
};

#endif /* SRC_HUBS_GEMHUB_H_ */
