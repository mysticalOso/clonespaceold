/*
 * ShipHubInt.cpp
 *
 *  Created on: 8 Feb 2015
 *      Author: mysticalOso
 */

#include <hubs/BulletHubInt.h>
#include <hubs/CloneHub.h>
#include <hubs/LightHub.h>
#include <hubs/RoomHub.h>
#include <hubs/ShipHubInt.h>
#include <interior/Interior.h>
#include <osogine/game/Game.h>
#include <osogine/utils/Transform.h>
#include <ship/Ship.h>


ShipHubInt::ShipHubInt(Interior* interior) : Hub(interior)
{
	this->interior = interior;
	cloneHub = 0;
	roomHub = 0;
	bulletHub = 0;
	lightHub = 0;
}

ShipHubInt::~ShipHubInt() {
}

void ShipHubInt::dock(Ship* childShip)
{
	childShip->setIsDocked(true);
	childShip->setVelocity(0,0,0);
	childShip->killEngines();
	childShip->fadeRoofOut();
	if (childShip->getDriver()!=0)
	{
		addWithMove(childShip);
	}
	else
	{
		add(childShip);
	}
//	Transform* childTransform = childShip->getTransform();
//	childTransform->applyInverseOf(getTransform());
//	add(childShip);
	cloneHub->dock(childShip);
	roomHub->dock(childShip);
	lightHub->dock(childShip);
}

void ShipHubInt::launch(Ship* childShip)
{
	childShip->setIsDocked(false);
	cloneHub->launch(childShip);
	roomHub->launch(childShip);
	lightHub->launch(childShip);
	remove(childShip);
}

void ShipHubInt::init()
{
	cloneHub = interior->getCloneHub();
	bulletHub = interior->getBulletHubInt();
	roomHub = interior->getRoomHub();
	lightHub = interior->getLightHub();
}

void ShipHubInt::collideWith(Hub* hub, string collisionName)
{
	for (int i = 0; i < size(); i++)
	{
		Ship* ship = (Ship*) get(i);
		RoomHub* roomHub = ship->getInterior()->getRoomHub();
		roomHub->collideWith(hub,collisionName);
	}
}

void ShipHubInt::update()
{
	Hub::update();

	//collideWith(bulletHub,"bullet");
}

void ShipHubInt::check(Clone* clone)
{
	for (int i = 0; i < size(); i++)
	{
		Ship* ship = (Ship*) get(i);
		RoomHub* roomHub = ship->getInterior()->getRoomHub();
		roomHub->check(clone);
	}
}


