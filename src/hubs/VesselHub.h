/*
 * VesselHub.h
 *
 *  Created on: 3 Mar 2015
 *      Author: Adam Nasralla
 */

#ifndef SRC_HUBS_VESSELHUB_H_
#define SRC_HUBS_VESSELHUB_H_

#include <osogine/game/Hub.h>

class Race;
class Vessel;

class VesselHub: public Hub
{
public:
	VesselHub(Entity* parent);
	~VesselHub();

	Vessel* getClosestEnemy(Entity* subject, Race* race);

	Vessel* getClosestAlly(Vessel* vessel);

	EntityList* getEnemies(Race* race);

	EntityList* getAllies(Vessel* vessel);

	EntityList* getRaceList(Race* race);

	bool containsEnemy(Vessel* vessel);

	bool containsAlly(Vessel* vessel);

	int getRaceCount(Race* race);
};

#endif /* SRC_HUBS_VESSELHUB_H_ */
