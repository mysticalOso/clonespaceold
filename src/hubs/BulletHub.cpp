/*
 * BulletHub.cpp
 *
 *  Created on: 30 Oct 2014
 *      Author: Adam Nasralla
 */

#include <hubs/BulletHub.h>
#include <space/Space.h>
#include <hubs/AsteroidHub.h>
#include <hubs/ExplosionHub.h>
#include <hubs/ShipHub.h>
#include <osogine/game/Collision.h>
#include <osogine/utils/Printer.h>
#include <races/Race.h>
#include <ship/Ship.h>
#include <space/Asteroid.h>
#include <weapons/Bullet.h>
#include <core/Constants.h>
#include <osogine/game/Game.h>


BulletHub::BulletHub(Space* space) : Hub(space)
{
	this->space = space;
	asteroidHub = 0;
	explosionHub = 0;
	shipHub = 0;
}

BulletHub::~BulletHub()
{

}

void BulletHub::update()
{
	Hub::update();
	collideWith(asteroidHub, "asteroid");
	collideWith(shipHub, "ship");
}

void BulletHub::handleCollision(Collision* collision)
{
	Bullet* bullet = (Bullet*) collision->getA();

	if (collision->nameIs("asteroid"))
	{
		handleAsteroidCollision(bullet,collision);
	}
	else if (collision->nameIs("ship"))
	{
		handleShipCollision(bullet,collision);
	}


}

void BulletHub::init()
{
	asteroidHub = space->getAsteroidHub();
	explosionHub = space->getExplosionHub();
	shipHub = space->getShipHub();
}

void BulletHub::handleAsteroidCollision(Bullet* bullet, Collision* collision)
{
	Asteroid* asteroid = (Asteroid*) collision->getB();
	vec2 collisionPoint = collision->getPoint();
	asteroid->hitBy( bullet, collision->getPoint() );
	bullet->setPosition(collisionPoint.x,collisionPoint.y,0);
	explosionHub->explode(bullet);
	removeAndDelete(bullet);
}


void BulletHub::handleShipCollision(Bullet* bullet, Collision* collision)
{

	Ship* ship = (Ship*) collision->getB();
	Race* bulletRace = bullet->getRace();
	Race* shipRace = ship->getRace();

	if ( bulletRace->isEnemy(shipRace) )
	{
		vec2 collisionPoint = collision->getPoint();
		bullet->setPosition(collisionPoint.x,collisionPoint.y,0);
		ship->hitBy( bullet, collision->getPoint() );
		explosionHub->explode(bullet)	;

		if (bullet->getDamage()==MISSILE_DAMAGE) game->event("enemyHitByMissile");

		removeAndDelete(bullet);
	}

}


void BulletHub::add(IEntity* entity)
{
	Hub::add(entity);
	Bullet* bullet = (Bullet*) entity;
	Race* race = bullet->getRace();
	if (race==0) {}// Printer::print("NO RACE!");
	else if (bullet->getRace()->isRace("sentient")
			&& bullet->getDamage()!=MISSILE_DAMAGE) game->event("shipLazer");
}
