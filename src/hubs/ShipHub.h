/*
 * ShipHub.h
 *
 *  Created on: 28 Oct 2014
 *      Author: mysticalOso
 */

#ifndef SHIPHUB_H_
#define SHIPHUB_H_

#include <hubs/VesselHub.h>

class Space;
class BulletHub;
class ExplosionHub;
class Entity;
class ShipBuilder;
class Ship;
class GemHub;
class Race;
class RaceHub;

class ShipHub: public VesselHub
{
public:
	ShipHub(Space* space);
	~ShipHub();

	void dock(Ship* childShip, Ship* motherShip);
	void launch(Ship* childShip);

	BulletHub* getBulletHub(){ return bulletHub; }

	GemHub* getGemHub(){ return gemHub; }

	Ship* getPlayerShip();

	void shipHit(Ship* ship, vec2 position);
	void destroyShip(Ship* ship);

	void removeAllShipsOf(Race* race);


	void init();

private:

	GemHub* gemHub;
	BulletHub* bulletHub;
	ExplosionHub* explosionHub;
	Space* space;
	ShipBuilder* shipBuilder;
	RaceHub* raceHub;
};

#endif /* SHIPHUB_H_ */
