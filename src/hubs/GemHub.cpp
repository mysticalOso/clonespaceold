/*
 * GemHub.cpp
 *
 *  Created on: 15 Jan 2015
 *      Author: mysticalOso
 */

#include <builders/GemBuilder.h>
#include <hubs/GemHub.h>
#include <osogine/utils/Dice.h>
#include <space/Asteroid.h>
#include <space/Gem.h>
#include <space/Space.h>
#include <glm/vec3.hpp>

GemHub::GemHub(Space* space) : Hub(space)
{
	this->space = space;
	setBuilder("gem");
	gemBuilder = (GemBuilder*) builder;
}


GemHub::~GemHub() {
}

void GemHub::init() {
}

void GemHub::addGems(int number)
{
	for (int i = 0; i < number; i++)
	{
		int colour = Dice::roll(1,6);
		gemBuilder->buildGem(colour);
	}
}

void GemHub::addGemsFrom(Asteroid* asteroid)
{
	int number = asteroid->getArea() / 10000 + 1;
	for (int i = 0; i < number; i++)
	{
		int colour = Dice::roll(1,6);
		Gem* g = gemBuilder->buildGem(colour);
		g->setPosition( asteroid->getPosition() );
		vec3 speed = vec3(5,5,5);
		g->setVelocity(Dice::roll(-speed,speed));
	}
}

Gem* GemHub::getGem(int i)
{
	return (Gem*) children->get(i);
}
