/*
 * ShipHubInt.h
 *
 *  Created on: 8 Feb 2015
 *      Author: mysticalOso
 */

#ifndef SRC_HUBS_SHIPHUBINT_H_
#define SRC_HUBS_SHIPHUBINT_H_

#include <osogine/game/Hub.h>

class Interior;
class Ship;
class CloneHub;
class Clone;
class BulletHubInt;
class RoomHub;
class LightHub;

class ShipHubInt: public Hub {
public:
	ShipHubInt(Interior* interior);
	virtual ~ShipHubInt();

	void update();

	void dock(Ship* childShip);
	void launch(Ship* childShip);

	void collideWith(Hub* hub, string collisionName);

	void init();

	void check(Clone* clone);

private:
	Interior* interior;
	CloneHub* cloneHub;
	RoomHub* roomHub;
	BulletHubInt* bulletHub;
	LightHub* lightHub;

};

#endif /* SRC_HUBS_SHIPHUBINT_H_ */
