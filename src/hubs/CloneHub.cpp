/*
 * CloneHub.cpp
 *
 *  Created on: 12 Jan 2015
 *      Author: mysticalOso
 */

#include <clone/Clone.h>
#include <core/CameraCS.h>
#include <core/CloneGfxP.h>
#include <core/Vessel.h>
#include <hubs/CloneHub.h>
#include <hubs/ExplosionHub.h>
#include <hubs/RaceHub.h>
#include <hubs/RoomHub.h>
#include <interior/Interior.h>
#include <osogine/audio/SoundInterface.h>
#include <osogine/game/EntityList.h>
#include <osogine/game/Game.h>
#include <osogine/utils/Polygon.h>
#include <races/Race.h>
#include <races/Sentient.h>
#include <ship/Ship.h>

CloneHub::CloneHub(Interior* interior) : VesselHub(interior)
{
	this->interior = interior;
	roomHub = 0;
	shipHubInt = 0;
	bulletHubInt = 0;
	cloneGfxP = 0;
	explosionHub = 0;
	lightHub = 0;
}

CloneHub::~CloneHub() {
	// TODO Auto-generated destructor stub
}

Ship* CloneHub::getShip()
{
	return interior->getShip();
}

void CloneHub::init()
{
	roomHub = interior->getRoomHub();
	shipHubInt = interior->getShipHubInt();
	bulletHubInt = interior->getBulletHubInt();
	cloneGfxP = (CloneGfxP*) game->getGfxProgram("clone");
	explosionHub = interior->getExplosionHub();
	lightHub = interior->getLightHub();
}

void CloneHub::update()
{
	Hub::update();

	checkRoom();

}

void CloneHub::add(IEntity* entity)
{
	Entity::addChild(entity);
	Clone* clone = (Clone*) entity;
	clone->setHub(this);
}

void CloneHub::handleCollision(Collision* collision)
{
	//Clone* clone = (Clone*) collision->getA();

}


void CloneHub::checkRoom()
{
	for (int i = 0; i < children->size(); i++)
	{
		Clone* clone = (Clone*) children->get(i);
		roomHub->check(clone);
		//shipHubInt->check(clone);
	}
}

void CloneHub::dock(Ship* childShip)
{
	CloneHub* childHub = childShip->getInterior()->getCloneHub();
	Clone* driver = childShip->getDriver();
	if (driver!=0) driver->setIsDriver(false);
	transferFrom(childHub);
}

void CloneHub::launch(Ship* childShip)
{
	CloneHub* childHub = childShip->getInterior()->getCloneHub();
	Polygon* shipOutline = childShip->getOutline();
	for (int i=0; i<children->size(); i++)
	{
		Clone* clone = (Clone*) children->get(i);
		if ( shipOutline->isInside(vec2(clone->getPosition())) )
		{
			childHub->transferFrom(this, clone);
			i--;
		}

	}

}


void CloneHub::use(Clone* clone)
{
	roomHub->use(clone);
}

void CloneHub::destroyClone(Clone* clone)
{
	SoundInterface::shipExplode();
	explosionHub->explode(clone);
	Race* race = clone->getRace();
	if (clone->getName() == "Xaccy") game->event("XaccyKilled");
	if (clone->getName() == "Fredric") game->event("FredricKilled");
	if (race->isRace("sentient"))
	{
		Sentient* sentient = (Sentient*) race;
		if (sentient->getCurrentClone()==clone)
		{
			CameraCS* camera = (CameraCS*) game->getCamera();
			camera->targetNextClone();
		}
	}
	remove(clone);
}

void CloneHub::cloneHit(Clone* clone, vec2 position)
{
	SoundInterface::shipHit();
	explosionHub->explode(clone, position);
}

bool CloneHub::containsActivePlayer()
{
	RaceHub* raceHub =  (RaceHub*) game->getEntity("raceHub");
	Sentient* sentient = (Sentient*) raceHub->getSentient();
	Clone* player = sentient->getCurrentClone();
	Ship* ship = getInterior()->getShip();
	if (this->contains(player) && !player->getIsDriver()) return true;
	else
	{
		if (!ship->getIsDocked()) ship->fadeRoofIn();
	}


	CameraCS* camera = (CameraCS*) game->getCamera();

	if (this->contains(player) && camera->isTransitioning()) return true;

	Clone* oldPlayer = camera->getOldTargetClone();

	if (oldPlayer!=0 && camera->isTransitioning())
	{
		if (this->contains(oldPlayer)) return true;
	}

	Ship* oldShip = camera->getOldTargetShip();
	if (ship->getName()=="station" && camera->isTransitioning())
	{
		//Printer::print("TRANSITIONING");
		return true;
	}

	return false;

}

void CloneHub::removeAllClones()
{
	RaceHub* raceHub =  (RaceHub*) game->getEntity("raceHub");
	Sentient* sentient = (Sentient*) raceHub->getSentient();
	for (int i=0; i<size(); i++)
	{
		Clone* c = (Clone*) children->get(i);
		sentient->removeClone(c);
	}
	children->clear();
}

Clone* CloneHub::getClone(string name)
{
	for (int i=0; i<size(); i++)
	{
		Clone* c = (Clone*) children->get(i);
		if (c->getName() == name) return c;
	}
	return 0;

}
