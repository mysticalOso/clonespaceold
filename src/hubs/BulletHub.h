/*
 * BulletHub.h
 *
 *  Created on: 30 Oct 2014
 *      Author: Adam Nasralla
 */

#ifndef BULLETHUB_H_
#define BULLETHUB_H_

#include <osogine/game/Hub.h>

class Space;
class AsteroidHub;
class ExplosionHub;
class ShipHub;
class Bullet;

class BulletHub: public Hub {
public:
	BulletHub(Space* space);
	virtual ~BulletHub();
	void update();
	void init();
	void handleCollision(Collision* collision);

	void add(IEntity* entity);

private:

	void handleAsteroidCollision(Bullet* bullet, Collision* collision);
	void handleShipCollision(Bullet* bullet, Collision* collision);
	Space* space;
	AsteroidHub* asteroidHub;
	ShipHub* shipHub;
	ExplosionHub* explosionHub;

};

#endif /* BULLETHUB_H_ */
