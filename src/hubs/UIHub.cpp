/*
 * UIHub.cpp
 *
 *  Created on: 12 Feb 2015
 *      Author: Adam Nasralla
 */

#include <hubs/RaceHub.h>
#include <hubs/UIHub.h>
#include <osogine/game/Game.h>
#include <ship/EnergyCore.h>
#include <ship/Ship.h>
#include <ui/CommLink.h>
#include <ui/Cursor.h>
#include <ui/Hud.h>
#include <ui/ShipDesigner.h>

UIHub::UIHub(Game* game) : Hub(game)
{
	commLink = new CommLink(this);
	shipDesigner = new ShipDesigner(this);
	hud = new Hud(this);
	cursor = new Cursor(this);
	hudOn = false;
}

UIHub::~UIHub()
{

}

float UIHub::getHealth()
{
	return getPlayerShip()->getHealthRatio();
}

float UIHub::getEnergy()
{
	EnergyCore* core = getPlayerShip()->getEnergyCore();
	if (core!=0) return core->getRatioRemaining();
	else return 0;
}

int UIHub::getAmmo()
{
	Ship* ship = getPlayerShip();
	if (ship!=0)
	{
		return ship->getAmmo();
	}
	else return 0;
}

EntityList* UIHub::getGems()
{
	Ship* ship = getPlayerShip();
	if (ship!=0)
	{
		return ship->getGems();
	}
	else return 0;
}

EntityList* UIHub::getShipGems()
{
	Ship* ship = getPlayerShip();
	if (ship!=0)
	{
		return ship->getShipGems();
	}
	else return 0;
}

Ship* UIHub::getPlayerShip()
{
	RaceHub* raceHub = (RaceHub*) game->getEntity("raceHub");
	return raceHub->getPlayerShip();
}

void UIHub::addTarget(vec2 pos, vec3 colour)
{
	hud->addTarget(pos,colour);
}

void UIHub::removeTarget(int index)
{
	hud->removeTarget(index);
}

void UIHub::setDesignerText(string string)
{
	shipDesigner->setText(string);
}

void UIHub::toggleHud()
{
	hudOn = !hudOn;

	if (hudOn) hud->fadeIn();
	else hud->fadeOut();
}

void UIHub::toggleHud(bool b)
{
	hudOn = b;
	if (hudOn) hud->fadeIn();
	else hud->fadeOut();
}

void UIHub::toggleFace()
{
	commLink->toggleFace();
}

void UIHub::addTarget(Entity* target, vec3 colour)
{
	hud->addTarget(target,colour);
}

void UIHub::toggleBuildEnabled(bool b)
{
	shipDesigner->toggleBuildEnabled(b);
}

