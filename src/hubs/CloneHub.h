/*
 * CloneHub.h
 *
 *  Created on: 12 Jan 2015
 *      Author: mysticalOso
 */

#ifndef SRC_HUBS_CLONEHUB_H_
#define SRC_HUBS_CLONEHUB_H_


#include <hubs/VesselHub.h>

class LightHub;
class CloneBuilder;
class Interior;
class Ship;
class Clone;
class RoomHub;
class ShipHubInt;
class BulletHubInt;
class CloneGfxP;
class ExplosionHub;

class CloneHub: public VesselHub {
public:
	CloneHub(Interior* interior);
	virtual ~CloneHub();

	void init();

	void update();

	Interior* getInterior(){ return interior; }
	Ship* getShip();

	void add(IEntity* entity);
	void addChild(IEntity* entity){ add(entity); }

	void handleCollision(Collision* collision);

	void dock(Ship* childShip);
	void launch(Ship* childShip);


	BulletHubInt* getBulletHubInt(){ return bulletHubInt; }
	RoomHub* getRoomHub(){ return roomHub; }

	void destroyClone(Clone* clone);
	void cloneHit(Clone* clone, vec2 position);

	void use(Clone* clone);

	LightHub* getLightHub(){ return lightHub; }

	bool containsActivePlayer();


	void removeAllClones();

	Clone* getClone(string name);

private:

	void checkRoom();

	Interior* interior;
	RoomHub* roomHub;
	ShipHubInt* shipHubInt;
	BulletHubInt* bulletHubInt;
	ExplosionHub* explosionHub;
	CloneGfxP* cloneGfxP;
	LightHub* lightHub;
};

#endif /* SRC_HUBS_CLONEHUB_H_ */
