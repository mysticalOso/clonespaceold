/*
 * AsteroidHub.h
 *
 *  Created on: 24 Oct 2014
 *      Author: Adam Nasralla
 */

#ifndef ASTEROIDHUB_H_
#define ASTEROIDHUB_H_

#include <osogine/game/Hub.h>
#include <glm/fwd.hpp>
#include <osogine/gfx/Triangle.h>
#include <osogine/utils/List.h>

class Bullet;

class Asteroid;
class Space;
class ShipHub;
class ExplosionHub;
class GemHub;

using namespace glm;

class AsteroidHub: public Hub
{
public:
	AsteroidHub(Space* space);
	~AsteroidHub();
	void init();
	void update();
	void handleCollision(Collision* collision);

	void explode(Asteroid* a);

	void chipAway(Asteroid* asteroid, List<Triangle> tris);



private:
	void initAsteroids(int i);
	Asteroid* buildAsteroid();
	ShipHub* shipHub;
	ExplosionHub* explosionHub;
	GemHub* gemHub;
	Space* space;
};

#endif /* ASTEROIDHUB_H_ */
