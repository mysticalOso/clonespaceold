/*
 * BulletHubInt.cpp
 *
 *  Created on: 8 Feb 2015
 *      Author: mysticalOso
 */

#include <clone/Clone.h>
#include <hubs/BulletHubInt.h>
#include <hubs/CloneHub.h>
#include <hubs/LightHub.h>
#include <interior/Interior.h>
#include <osogine/game/Light.h>
#include <osogine/utils/Printer.h>
#include <osogine/utils/Transform.h>
#include <races/Race.h>
#include <weapons/Bullet.h>


BulletHubInt::BulletHubInt(Interior* interior)  : Hub(interior)
{
	cloneHub = 0;
	this->interior = interior;
	updateOrder = 1;
	lightHub = 0;
}

BulletHubInt::~BulletHubInt() {
}

void BulletHubInt::add(IEntity* bullet)
{
	Hub::add(bullet);
	Bullet* b = (Bullet*) bullet;
	b->setRenderOrder(3);
	vec3 pos = b->getLocalPosition();
	//pos = getTransform()->applyInverseTo(pos);
	if (LIGHTS)
	{
		Light* light = lightHub->addLight(pos, vec3(0,0.3,0));
		b->setLight(light);
	}
}

void BulletHubInt::init()
{
	cloneHub = interior->getCloneHub();
	lightHub = interior->getLightHub();
}

void BulletHubInt::update()
{
	collideWith(cloneHub,"clone");
}

void BulletHubInt::handleCollision(Collision* collision)
{
	Bullet* bullet = (Bullet*) collision->getA();
	if (collision->nameIs("clone"))
	{
		handleCloneCollision(bullet, collision);
	}
}

void BulletHubInt::handleCloneCollision(Bullet* bullet, Collision* collision)
{
	//Printer::print("BULLET!!");
	Clone* clone = (Clone*) collision->getB();
	Race* bulletRace = bullet->getRace();
	Race* cloneRace = clone->getRace();

	if ( bulletRace->isEnemy(cloneRace) )
	{
		clone->hitBy( bullet, collision->getPoint() );
		Light* light = bullet->getLight();
		if (light!=0)
		{
			if (clone->getHealth()>0) light->explode(10,20,vec3(0.8,0,0));
			else light->explode(10,100,vec3(1,0,0));
		}
		removeAndDelete(bullet);
	}
}
