/*
 * BulletHubInt.h
 *
 *  Created on: 8 Feb 2015
 *      Author: mysticalOso
 */

#ifndef SRC_HUBS_BULLETHUBINT_H_
#define SRC_HUBS_BULLETHUBINT_H_

#include <osogine/game/Hub.h>

class CloneHub;
class Interior;
class Bullet;
class LightHub;

class BulletHubInt: public Hub {
public:
	BulletHubInt(Interior* interior);
	virtual ~BulletHubInt();
	void add(IEntity* bullet);
	void init();
	void update();
	void handleCollision(Collision* collision);

private:
	void handleCloneCollision(Bullet* bullet, Collision* collision);
	Interior* interior;
	CloneHub* cloneHub;
	LightHub* lightHub;
};

#endif /* SRC_HUBS_BULLETHUBINT_H_ */
