/*
 * ExplosionHub.h
 *
 *  Created on: 23 Oct 2014
 *      Author: Adam Nasralla
 */

#ifndef EXPLOSIONHUB_H_
#define EXPLOSIONHUB_H_

#include <osogine/game/Hub.h>
#include <osogine/gfx/Triangle.h>
#include <osogine/utils/List.h>

class Bullet;
class Asteroid;
class Ship;
class Clone;
class ExplosionBuilder;

class ExplosionHub : public Hub
{
public:
	ExplosionHub(Entity* parent);
	~ExplosionHub();

	void explode(Asteroid* asteroid);
	void explode(Ship* ship);
	void explode(Ship* ship, vec2 position);
	void explode(Clone* ship);
	void explode(Clone* ship, vec2 position);
	void explode(Bullet* bullet);
	void explode(Asteroid* asteroid, List<Triangle> tris);

private:

	ExplosionBuilder* explosionBuilder;
};

#endif /* EXPLOSIONHUB_H_ */
