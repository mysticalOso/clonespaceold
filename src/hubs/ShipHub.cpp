/*
 * ShipHub.cpp
 *
 *  Created on: 28 Oct 2014
 *      Author: mysticalOso
 */

#include <builders/ShipBuilder.h>
#include <core/CameraCS.h>
#include <osogine/game/EntityBuilder.h>
#include <hubs/BulletHub.h>
#include <hubs/CloneHub.h>
#include <hubs/ExplosionHub.h>
#include <hubs/RaceHub.h>
#include <hubs/ShipHub.h>
#include <hubs/ShipHubInt.h>
#include <interior/Interior.h>
#include <osogine/audio/SoundInterface.h>
#include <osogine/game/Game.h>
#include <osogine/utils/Printer.h>
#include <races/Race.h>
#include <ship/Ship.h>
#include <space/Space.h>



ShipHub::ShipHub(Space* space) : VesselHub(space)
{
	this->space = space;
	setBuilder("ship");
	bulletHub = 0;
	explosionHub = 0;
	shipBuilder = 0;
	gemHub = 0;
	raceHub = 0;
}

Ship* ShipHub::getPlayerShip()
{
	Ship* playerShip = raceHub->getPlayerShip();
	if (contains(playerShip)) return playerShip;
	else return 0;
}

void ShipHub::dock(Ship* childShip, Ship* motherShip)
{
	remove(childShip);
	motherShip->fadeRoofOut();
	Interior* interior = motherShip->getInterior();
	ShipHubInt* shipHubInt = interior->getShipHubInt();
	shipHubInt->dock(childShip);
	childShip->setIsDocked(true);
}

void ShipHub::launch(Ship* childShip)
{
	if (!children->contains(childShip))
	{
		Ship* motherShip = raceHub->getPlayerShip();
		childShip->fadeRoofIn();
		Interior* interior = motherShip->getInterior();
		ShipHubInt* shipHubInt = interior->getShipHubInt();
		shipHubInt->launch(childShip);
		addWithMove(childShip);
		childShip->setIsDocked(false);
		CameraCS* camera = (CameraCS*) game->getCamera();
		camera->setOldTargetShip(motherShip);
	}
}


void ShipHub::init()
{
	bulletHub = space->getBulletHub();
	gemHub = space->getGemHub();
	explosionHub = space->getExplosionHub();
	shipBuilder = (ShipBuilder*) builder;
	raceHub = (RaceHub*) game->getEntity("raceHub");
	//shipBuilder->buildPlayer();
}

ShipHub::~ShipHub()
{

}

void ShipHub::shipHit(Ship* ship, vec2 position)
{
	SoundInterface::shipHit();
	explosionHub->explode(ship, position);
}

void ShipHub::destroyShip(Ship* ship)
{
	SoundInterface::shipExplode();

	explosionHub->explode(ship);
	Interior* interior = ship->getInterior();



	if (ship->getName() == "starter") game->event("starterKilled");

	if (interior!=0)
	{
		interior->getCloneHub()->removeAllClones();
	}
	remove(ship);

	if (getRaceCount(raceHub->getRace("xacadiens")) == 0 )
	{
		game->event("xacadiensKilled");
	}
}

void ShipHub::removeAllShipsOf(Race* race)
{
	for(int i=0;i<children->size();i++)
	{
		Ship* ship = (Ship*) children->get(i);
		if (ship->getRace()==race)
		{
			children->remove(ship);
			i--;
		}
	}
}
