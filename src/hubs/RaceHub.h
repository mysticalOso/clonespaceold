/*
 * RaceHub.h
 *
 *  Created on: 12 Jan 2015
 *      Author: mysticalOso
 */

#ifndef SRC_HUBS_RACEHUB_H_
#define SRC_HUBS_RACEHUB_H_

#include <osogine/game/Hub.h>
#include <string>
#include <map>

using namespace std;

class Race;
class Sentient;
class Ship;
class Clone;

class RaceHub: public Hub {
public:
	RaceHub(Game* game);
	virtual ~RaceHub();
	void init();
	Race* getRace(string name){ return races[name]; }
	void addRace(string name, Race* race){ races[name] = race; }
	void activateRace(string name);
	void deactivateRace(string name);
	Ship* getPlayerShip();
	Clone* getPlayerClone();
	Sentient* getSentient();

	vec2 getPlayerPos();

private:

	map<string, Race*> races;
};

#endif /* SRC_HUBS_RACEHUB_H_ */
