/*
 * LightHub.cpp
 *
 *  Created on: 24 Feb 2015
 *      Author: Adam Nasralla
 */

#include <builders/LightBuilder.h>
#include <glm/vec3.hpp>
#include <hubs/CloneHub.h>
#include <hubs/LightHub.h>
#include <hubs/RoomHub.h>
#include <interior/Interior.h>
#include <interior/Room.h>
#include <osogine/game/EntityList.h>
#include <osogine/game/Light.h>
#include <osogine/utils/Dice.h>
#include <core/Constants.h>
#include <interior/Pod.h>
#include <ship/Ship.h>

LightHub::LightHub(Interior* interior) : Hub(interior)
{
	this->interior = interior;
	cloneHub = 0;
	roomHub = 0;
	updateOrder = 2;
	builder = new LightBuilder(game);
}

LightHub::~LightHub()
{

}


void LightHub::init()
{
	roomHub = interior->getRoomHub();
	cloneHub = interior->getCloneHub();
	builder->setHub(this);
}



Light* LightHub::getLight(int i)
{
	if (children->size()>i) return (Light*) children->get(i);
	else return 0;
}

void LightHub::initLights()
{
	int size = roomHub->size();
	for (int i=0; i<size; i++)
	{
		Room* room = roomHub->getRoom(i);
		if (!room->getIsExternal())
		{
			vec3 pos;
			Pod* pod = room->getPod();
			if (pod!=0)
			{
				pos = pod->getPosition();
			}
			else
			{
				pos = room->getPosition();
			}
			pos.z = 2;
			builder->buildLight(pos, room->getLightColour());

		}

	}
}

void LightHub::refresh()
{
	if (LIGHTS) builder->refresh();
}

void LightHub::update()
{
if (LIGHTS)
{
	Hub::update();
	builder->refresh();


	for (int i=0; i<size(); i++)
	{
		builder->updateLight( getLight(i) );
		//getLight(i)->setIsVisible(false);
	}
}


//	Light* light = getLight(0);
//	vec3 pos = light->getLocalLightPos();
//	pos.x += 0.01;
//	light->setLightPosition(pos);
//	builder->updateLight(light);

}

Light* LightHub::addLight(vec3 pos, vec3 colour)
{
	Light* light = builder->buildLight(pos, colour);
	return light;
}

void LightHub::dock(Ship* childShip)
{
	LightHub* childHub = childShip->getInterior()->getLightHub();
	transferFrom(childHub);
}

void LightHub::toggleOwnerLights(LightHub* owner, bool isVisible)
{
	for(int i = 0; i < children->size(); i++)
	{
		Light* light = (Light*) children->get(i);
		if (light->getOwner()==owner) light->setIsVisible(isVisible);
	}
}

void LightHub::launch(Ship* childShip)
{
	LightHub* childHub = childShip->getInterior()->getLightHub();
	for(int i = 0; i < children->size(); i++)
	{

		Light* light = (Light*) children->get(i);
		if (light->getOwner() == childHub)
		{
			//Printer::print("LightOwnerMatch\n");
			childHub->transferFrom(this, light);
			i--;
		}
	}
}
