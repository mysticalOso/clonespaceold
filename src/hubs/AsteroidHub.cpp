/*
 * AsteroidHub.cpp
 *
 *  Created on: 24 Oct 2014
 *      Author: Adam Nasralla
 */

#include <core/Constants.h>
#include <hubs/AsteroidHub.h>
#include <hubs/ExplosionHub.h>
#include <hubs/GemHub.h>
#include <hubs/ShipHub.h>
#include <osogine/audio/SoundInterface.h>
#include <osogine/game/Collision.h>
#include <osogine/game/EntityBuilder.h>
#include <osogine/game/Game.h>
#include <osogine/utils/Dice.h>
#include <races/Race.h>
#include <ship/Ship.h>
#include <space/Asteroid.h>
#include <space/Space.h>

AsteroidHub::AsteroidHub(Space* space) : Hub(space)
{
	this->space = space;
	setBuilder("asteroid");
	initAsteroids(25);


}




void AsteroidHub::initAsteroids(int numAsteroids)
{
	for (int i = 0; i < numAsteroids; i++)
	{
		Asteroid* a = buildAsteroid();
		bool tooClose = false;
		do
		{
			a->setPosition( i*500 - 7250 + Dice::roll(-100,100), Dice::roll(HEIGHT2*8,HEIGHT2*10), 0 );
			tooClose = false;
			for (int i=0; i<size(); i++)
			{
				Asteroid* b = (Asteroid*) children->get(i);
				if (a!=b)
				{
					float dist = a->distTo2(b);
					if (dist<300000)
					{
						tooClose = true;
					}
				}
			}
		}
		while(tooClose);

	}
//
//	for (int i = 0; i < numAsteroids; i++)
//	{
//		Asteroid* a = buildAsteroid();
//		bool tooClose = false;
//		do
//		{
//			a->setPosition( Dice::roll(vec3(7000,-2000,0),vec3(9000,2000,0) ));
//			tooClose = false;
//			for (int i=0; i<size(); i++)
//			{
//				Asteroid* b = (Asteroid*) children->get(i);
//				if (a!=b)
//				{
//					float dist = a->distTo2(b);
//					if (dist<300000)
//					{
//						tooClose = true;
//					}
//				}
//			}
//		}
//		while(tooClose);
//
//	}
}

AsteroidHub::~AsteroidHub()
{
}

Asteroid* AsteroidHub::buildAsteroid()
{
	Asteroid* a = (Asteroid*) builder->build();
	return a;
}

void AsteroidHub::init()
{
	shipHub = space->getShipHub();
	explosionHub = space->getExplosionHub();
	gemHub = space->getGemHub();
}

void AsteroidHub::update()
{
	Hub::update();
	collideWith(shipHub, "shipCollision");
}

void AsteroidHub::handleCollision(Collision* collision)
{
	Ship* ship = (Ship*)collision->getB();
	if (ship->getRace()->isRace("xacadiens")) game->event("enemyCrashed");
	shipHub->destroyShip(ship);
}

void AsteroidHub::explode(Asteroid* a)
{
	SoundInterface::asteroidExplode();
	explosionHub->explode(a);
	game->event("asteroidDestroyed");
	gemHub->addGemsFrom(a);
	removeAndDelete(a);

}

void AsteroidHub::chipAway(Asteroid* asteroid, List<Triangle> tris)
{
	SoundInterface::asteroidHit();
	explosionHub->explode(asteroid, tris);
}
