/*
 * VesselHub.cpp
 *
 *  Created on: 3 Mar 2015
 *      Author: Adam Nasralla
 */

#include <core/Vessel.h>
#include <hubs/VesselHub.h>
#include <osogine/game/EntityList.h>
#include <races/Race.h>

VesselHub::VesselHub(Entity* parent) : Hub(parent)
{

}

VesselHub::~VesselHub()
{

}

Vessel* VesselHub::getClosestEnemy(Entity* subject, Race* race)
{
	EntityList* enemies =  getEnemies(race);
	Vessel* closest = (Vessel*) subject->getClosest(enemies);
	//delete enemies;
	return closest;
}

Vessel* VesselHub::getClosestAlly(Vessel* vessel)
{
	EntityList* allies =  getAllies(vessel);
	Vessel* closest = (Vessel*) vessel->getClosest(allies);
	//delete allies;
	return closest;
}

EntityList* VesselHub::getEnemies(Race* race)
{
	Race* subjectRace = race;
	EntityList* enemies = new EntityList();
	for (int i = 0; i < children->size(); i++)
	{
		Vessel* object = (Vessel*) children->get(i);
		Race* objectRace = object->getRace();
		if (subjectRace->isEnemy(objectRace))
		{
			enemies->add(object);
		}
	}
	return enemies;
}

EntityList* VesselHub::getAllies(Vessel* vessel)
{
	Race* subjectRace = vessel->getRace();
	EntityList* allies = new EntityList();
	for (int i = 0; i < children->size(); i++)
	{
		Vessel* object = (Vessel*) children->get(i);
		Race* objectRace = object->getRace();
		if (!subjectRace->isEnemy(objectRace) && object!=vessel)
		{
			allies->add(object);
		}
	}
	return allies;
}

EntityList* VesselHub::getRaceList(Race* race)
{
	EntityList* vessels = new EntityList();
	for (int i = 0; i < children->size(); i++)
	{
		Vessel* vessel = (Vessel*) children->get(i);
		if (vessel->getRace() == race) vessels->add(vessel);
	}
	return vessels;
}

bool VesselHub::containsEnemy(Vessel* vessel)
{
	EntityList* vessels = getEnemies(vessel->getRace());
	return (vessels->size()!=0);
}

int VesselHub::getRaceCount(Race* race)
{
	EntityList* Vessels = getRaceList(race);
	int count = Vessels->size();
	//delete Vessels;
	return count;
}

bool VesselHub::containsAlly(Vessel* vessel)
{
	EntityList* vessels = getAllies(vessel);
	return (vessels->size()!=0);
}
