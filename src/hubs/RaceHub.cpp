/*
 * RaceHub.cpp
 *
 *  Created on: 12 Jan 2015
 *      Author: mysticalOso
 */

#include <builders/RaceBuilder.h>
#include <clone/Clone.h>
#include <hubs/RaceHub.h>
#include <osogine/game/Game.h>
#include <races/Race.h>
#include <races/Sentient.h>
#include <ship/Ship.h>

RaceHub::RaceHub(Game* game) : Hub(game)
{
	setBuilder("race");
}

RaceHub::~RaceHub()
{
}

void RaceHub::activateRace(string name)
{
	add(getRace(name));
}

void RaceHub::init()
{
	RaceBuilder* builder = (RaceBuilder*) game->getBuilder("race");

	Race* sentient = builder->buildSentient();
	addRace("sentient",sentient);
	activateRace("sentient");

	Race* xacadiens = builder->buildXacadiens();
	addRace("xacadiens",xacadiens);
	activateRace("xacadiens");

	xacadiens->addEnemy(sentient);
	sentient->addEnemy(xacadiens);

	Hub::init();

}

void RaceHub::deactivateRace(string name)
{
	remove(getRace(name));
}


Ship* RaceHub::getPlayerShip()
{
	Sentient* sentient = getSentient();
	if (sentient!=0)
	{
		return sentient->getPlayerShip();
	}
	else return 0;
}

Sentient* RaceHub::getSentient()
{
	return (Sentient*) getRace("sentient");
}

Clone* RaceHub::getPlayerClone()
{
	Sentient* sentient = getSentient();
	return sentient->getCurrentClone();
}

vec2 RaceHub::getPlayerPos()
{
	vec2 pos = vec2(0,0);
	Sentient* sentient = getSentient();
	if (sentient!=0)
	{
		Clone* clone = sentient->getCurrentClone();
		if (clone!=0)
		{
			if (clone->getIsDriver())
			{
				Ship* ship = getPlayerShip();
				if (ship!=0) pos = vec2( ship->getPosition() );
			}
			else
			{
				pos = vec2( clone->getLocalPosition() );
			}
		}
	}
	return pos;
}
