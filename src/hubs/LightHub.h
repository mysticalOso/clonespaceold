/*
 * LightHub.h
 *
 *  Created on: 24 Feb 2015
 *      Author: Adam Nasralla
 */

#ifndef SRC_HUBS_LIGHTHUB_H_
#define SRC_HUBS_LIGHTHUB_H_

#include <osogine/game/Hub.h>

class CloneHub;
class Interior;
class Light;
class LightBuilder;
class RoomHub;
class Ship;

class LightHub: public Hub
{
public:
	LightHub(Interior* interior);
	~LightHub();

	Light* addLight(vec3 pos, vec3 colour);
	void refresh();
	Light* getLight(int i);
	void init();
	void initLights();
	void update();
	RoomHub* getRoomHub(){ return roomHub; }

	void toggleOwnerLights(LightHub* owner, bool isVisible);

	void dock(Ship* childShip);

	void launch(Ship* childShip);

private:
	Interior* interior;
	RoomHub* roomHub;
	CloneHub* cloneHub;
	LightBuilder* builder;
};

#endif /* SRC_HUBS_LIGHTHUB_H_ */
