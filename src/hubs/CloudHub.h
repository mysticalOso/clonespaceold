/*
 * CloudHub.h
 *
 *  Created on: 5 Nov 2014
 *      Author: Adam Nasralla
 */

#ifndef SRC_HUBS_CLOUDHUB_H_
#define SRC_HUBS_CLOUDHUB_H_

#include <osogine/game/Hub.h>
#include <glm/fwd.hpp>

using namespace glm;

class Space;
class CloudBuilder;
class CloudGroup;

class CloudHub: public Hub
{
public:
	CloudHub(Space* space);
	virtual ~CloudHub();

	void update();



private:
	CloudBuilder* cBuilder;
	Space* space;

	float xBMax;
	float xBMin;
	float yBMax;
	float yBMin;
	float speed;

	void moveCloudLeft(CloudGroup* cloud);
	void moveCloudRight(CloudGroup* cloud);
	void moveCloudUp(CloudGroup* cloud);
	void moveCloudDown(CloudGroup* cloud);

	vec3 getCamPos();
};

#endif /* SRC_HUBS_CLOUDHUB_H_ */
