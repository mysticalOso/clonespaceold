/*
 * CloudHub.cpp
 *
 *  Created on: 5 Nov 2014
 *      Author: Adam Nasralla
 */

#include <builders/CloudBuilder.h>
#include <osogine/game/EntityList.h>
#include <osogine/game/Game.h>
#include <osogine/game/Camera.h>
#include <glm/vec3.hpp>
#include <hubs/CloudHub.h>
#include <space/CloudGroup.h>
#include <space/Space.h>
#include <osogine/utils/Dice.h>
#include <osogine/utils/Printer.h>

CloudHub::CloudHub(Space* space) : Hub(space)
{
	this->space = space;
	setBuilder("cloud");
	cBuilder = (CloudBuilder*) builder;

	cBuilder->initTextures(20);
	speed = 1;
	float range = 1500;

	xBMax = 4000;
	xBMin = 3000;
	yBMax = 2000;
	yBMin = 1500;

	CloudGroup* cloud;

	for (int i = 0; i < 20; i++)
	{
		cloud = cBuilder->buildGroup();
		cloud->setPosition( Dice::roll(vec3(-range*2,-range,0),vec3(range*2,range,20000)) );
		//cloud->setPosition( Dice::roll(vec3(-range,-range,0),vec3(range,range,0)) );
		//cloud->setScale(3,3,3);
		cloud->setVelocity(Dice::roll(vec3(-speed,-speed,0),vec3(speed,speed,0)));
		addChild(cloud);
	}
}

CloudHub::~CloudHub()
{

}

void CloudHub::update()
{
	for (int i = 0; i < children->size(); i++)
	{
		CloudGroup* cloud = (CloudGroup*) children->get(i);
		vec3 pos = cloud->getPosition() - getCamPos();

		//Printer::print("Cloud pos = ",pos,"\n");

		if (pos.x > xBMax) moveCloudLeft(cloud);
		if (pos.x < -xBMax) moveCloudRight(cloud);
		if (pos.y > yBMax) moveCloudDown(cloud);
		if (pos.y < -yBMax) moveCloudUp(cloud);
	}
	Hub::update();
}


void CloudHub::moveCloudLeft(CloudGroup* cloud)
{

	cloud->setPosition( Dice::roll(vec3(-xBMin,-yBMin,1000),vec3(-xBMin,yBMin,1000)) + getCamPos() );
	//cloud->setVelocity(Dice::roll(vec3(-speed,-speed,0),vec3(speed,speed,0)));
}

void CloudHub::moveCloudRight(CloudGroup* cloud)
{
	cloud->setPosition( Dice::roll(vec3(xBMin,-yBMin,1000),vec3(xBMin,yBMin,1000)) + getCamPos() );
	//cloud->setVelocity(Dice::roll(vec3(-speed,-speed,0),vec3(speed,speed,0)));
}

void CloudHub::moveCloudUp(CloudGroup* cloud)
{
	cloud->setPosition( Dice::roll(vec3(-xBMin,yBMin,1000),vec3(xBMin,yBMin,1000)) + getCamPos() );
	//cloud->setVelocity(Dice::roll(vec3(-speed,-speed,0),vec3(speed,speed,0)));
}

void CloudHub::moveCloudDown(CloudGroup* cloud)
{
	cloud->setPosition( Dice::roll(vec3(-xBMin,-yBMin,1000),vec3(xBMin,-yBMin,1000)) + getCamPos());
	//cloud->setVelocity(Dice::roll(vec3(-speed,-speed,0),vec3(speed,speed,0)));
}

vec3 CloudHub::getCamPos()
{
	vec3 c = game->getCamera()->getPosition();
	c.z = 0;
	return c;
}
