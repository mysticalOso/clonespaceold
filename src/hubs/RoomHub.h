/*
 * RoomHub.h
 *
 *  Created on: 8 Feb 2015
 *      Author: mysticalOso
 */

#ifndef SRC_HUBS_ROOMHUB_H_
#define SRC_HUBS_ROOMHUB_H_

#include <osogine/game/Hub.h>

class Interior;
class Clone;
class CloneHub;
class BulletHubInt;
class Collision;
class ExplosionHub;
class Ship;
class Room;
class Pod;

class RoomHub: public Hub {
public:
	RoomHub(Interior* interior);
	virtual ~RoomHub();

	void check(Clone* clone);

	void update();

	void init();

	void handleCollision(Collision* collision);

	void dock(Ship* childShip);

	void launch(Ship* childShip);

	void collideWith(Hub* hub, string collisionName);

	Room* getRoom(int i);

	void use(Clone* clone);

	void toggleOwnerRooms(RoomHub* owner, bool isVisible);

	void print();

	Pod* getPod(vec2 code);



	List<Polygon*>* getAllPolygons();
private:

	void handleBulletCollision(Collision* collision);
	void handleCloneCollision(Collision* collision);
	Interior* interior;
	CloneHub* cloneHub;
	BulletHubInt* bulletHub;
	ExplosionHub* explosionHub;
};

#endif /* SRC_HUBS_ROOMHUB_H_ */
