/*
 * RoomHub.cpp
 *
 *  Created on: 8 Feb 2015
 *      Author: mysticalOso
 */

#include <clone/Clone.h>
#include <hubs/BulletHubInt.h>
#include <hubs/CloneHub.h>
#include <hubs/ExplosionHub.h>
#include <hubs/RoomHub.h>
#include <interior/Door.h>
#include <interior/Interior.h>
#include <interior/Pod.h>
#include <interior/Room.h>
#include <interior/Walls.h>
#include <interior/WallSection.h>
#include <osogine/audio/SoundInterface.h>
#include <osogine/game/Collision.h>
#include <osogine/game/Entity.h>
#include <osogine/game/EntityList.h>
#include <osogine/utils/List.h>
#include <osogine/utils/Polygon.h>
#include <osogine/utils/Printer.h>
#include <ship/Module.h>
#include <ship/Ship.h>
#include <weapons/Bullet.h>

RoomHub::RoomHub(Interior* interior) : Hub(interior)
{
	this->interior = interior;
	cloneHub = 0;
	bulletHub = 0;
	explosionHub = 0;
	updateOrder = 1;
}

RoomHub::~RoomHub() {
}

void RoomHub::check(Clone* clone)
{
	for(int i = 0; i < children->size(); i++)
	{
		Room* room = (Room*) children->get(i);
		room->check(clone);
	}
}

void RoomHub::update()
{
	Hub::update();
	if (INTERIOR_COLLISIONS)
	{
		collideWith(bulletHub, "bullet");
		collideWith(cloneHub, "clone");
	}
}


void RoomHub::init()
{
	cloneHub = interior->getCloneHub();
	bulletHub = interior->getBulletHubInt();
	explosionHub = interior->getExplosionHub();
}

void RoomHub::handleCollision(Collision* collision)
{
	if (collision->nameIs("bullet"))
	{
		handleBulletCollision(collision);
	}
	if (collision->nameIs("clone"))
	{
		handleCloneCollision(collision);
	}
}

void RoomHub::toggleOwnerRooms(RoomHub* owner, bool isVisible)
{
	for(int i = 0; i < children->size(); i++)
	{
		Room* room = (Room*) children->get(i);
		if (room->getOwner()==owner) room->setIsVisible(isVisible);
	}
}
//TODO optimse and use for lighting
List<Polygon*>* RoomHub::getAllPolygons()
{
	List<Polygon*>* polygons = new List<Polygon*>();
	for (int i=0; i<size(); i++)
	{
		Room* room = getRoom(i);
		Walls* walls = room->getWalls();
		EntityList* sections = walls->getSections();
		Transform* trans = walls->getTransform();

		for (int i=0; i<sections->size(); i++)
		{
			WallSection* section = (WallSection*) sections->get(i);
			Polygon* outline = section->getOutline();
			outline->setTransform(trans);
			outline->updateTransformedOutline();
			polygons->add(outline);
		}

		EntityList* doors = room->getDoors();
		for (int i=0; i<doors->size(); i++)
		{
			Door* door = (Door*) doors->get(i);
			door->updateSections();
			Polygon* lSection = door->getLeftSection();
			lSection->setTransform(trans);
			lSection->updateTransformedOutline();
			polygons->add(lSection);
			Polygon* rSection = door->getRightSection();
			rSection->setTransform(trans);
			rSection->updateTransformedOutline();
			polygons->add(rSection);
		}
	}
	return polygons;
}

void RoomHub::handleBulletCollision(Collision* collision)
{
	Bullet* b = (Bullet*) collision->getA();
	bulletHub->remove(b);
	explosionHub->explode(b);
	SoundInterface::shipHit();
}

Pod* RoomHub::getPod(vec2 code)
{
	for(int i = 0; i < children->size(); i++)
	{
		Room* room = (Room*) children->get(i);
		Module* module = room->getModule();
		if (module!=0)
		{
			if (module->getCode() == code) return room->getPod();
		}
		else
		{
			if (code == vec2(-1,-1) )
			{
				Pod* pod = room->getPod();
				if (pod->getUI() != 0) return pod;
			}
		}

	}
	return 0;
}

void RoomHub::handleCloneCollision(Collision* collision)
{
	Clone* c = (Clone*) collision->getA();
	c->stepBack();
}

void RoomHub::dock(Ship* childShip)
{
	RoomHub* childHub = childShip->getInterior()->getRoomHub();
	transferFrom(childHub);
}



Room* RoomHub::getRoom(int i)
{
	if (children->size()>i) return (Room*) children->get(i);
	else return 0;
}

void RoomHub::use(Clone* clone)
{
	for(int i = 0; i < children->size(); i++)
	{
		Room* room = (Room*) children->get(i);
		room->use(clone);
	}
}

void RoomHub::launch(Ship* childShip)
{

	RoomHub* childHub = childShip->getInterior()->getRoomHub();
	for(int i = 0; i < children->size(); i++)
	{
		Room* room = (Room*) children->get(i);
		if (room->getOwner() == childHub)
		{
			//Printer::print("ownerMatch\n");
			childHub->transferFrom(this, room);
			i--;
		}
	}

}

void RoomHub::print()
{
//	Printer::print("printingTransfer\n");
//	for(int i = 0; i < children->size(); i++)
//		{
//		Room* room = (Room*) children->get(i);
//				Module* module = room->getPod()->getModule();
//				if (module!=0) Printer::print("Module Code = ",module->getCode(), "\n");
//		}
}

void RoomHub::collideWith(Hub* hub, string collisionName)
{
	if (hub!=0)
	{
		EntityList* children2 = hub->getChildren();
		List<Polygon*>* obstacles = getAllPolygons();
		for (int i=0; i<children2->size(); i++)
		{
			Entity* e = (Entity*) children2->get(i);
			for (int j=0; j<obstacles->size(); j++)
			{
				Polygon* p = obstacles->get(j);
				if (p->intersectWith(e->getOutline()))
				{
					e->getOutline()->setLastCollision(p->getLastCollision());
					collisions->add(new Collision(e,this,collisionName));
				}
			}
		}
		for (int i = 0; i < collisions->size(); i++)
		{
			handleCollision(collisions->get(i));
		}

		for (int i = 0; i < collisions->size(); i++)
		{
			//delete collisions->get(i);
		}
		collisions->clear();
	}
}

