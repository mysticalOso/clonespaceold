/*
 * UIHub.h
 *
 *  Created on: 12 Feb 2015
 *      Author: Adam Nasralla
 */

#ifndef SRC_HUBS_UIHUB_H_
#define SRC_HUBS_UIHUB_H_

#include <osogine/game/Hub.h>
#include <string>

class CommLink;
class ShipDesigner;
class Hud;
class Ship;
class Cursor;

class UIHub: public Hub
{
public:
	UIHub(Game* game);
	~UIHub();

	float getHealth();
	float getEnergy();
	int getAmmo();
	EntityList* getGems();
	EntityList* getShipGems();

	Ship* getPlayerShip();

	void addTarget(Entity* target, vec3 colour);
	void addTarget(vec2 pos, vec3 colour);
	void removeTarget(int index);

	CommLink* getCommLink(){ return commLink; }
	ShipDesigner* getShipDesigner(){ return shipDesigner; }

	void setDesignerText(string string);

	void toggleHud();

	void toggleHud(bool b);

	void toggleFace();

	void reset();


	void toggleBuildEnabled(bool b);


private:
	CommLink* commLink;
	ShipDesigner* shipDesigner;
	Hud* hud;
	Cursor* cursor;
	bool hudOn;
};

#endif /* SRC_HUBS_UIHUB_H_ */
