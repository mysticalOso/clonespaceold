/*
 * ExplosionHub.cpp
 *
 *  Created on: 23 Oct 2014
 *      Author: Adam Nasralla
 */

#include <builders/ExplosionBuilder.h>
#include <hubs/ExplosionHub.h>
#include <osogine/game/Light.h>
#include <ship/Ship.h>
#include <space/Explosion.h>
#include <space/Space.h>
#include <weapons/Bullet.h>

ExplosionHub::ExplosionHub(Entity* parent) : Hub(parent)
{
	explosionBuilder = new ExplosionBuilder(game);
	explosionBuilder->setHub(this);
}

ExplosionHub::~ExplosionHub()
{
}

void ExplosionHub::explode(Asteroid* asteroid)
{
	explosionBuilder->explode(asteroid);
}

void ExplosionHub::explode(Ship* ship)
{
	explosionBuilder->explode(ship);
}

void ExplosionHub::explode(Bullet* bullet)
{
	Explosion* e = explosionBuilder->explode(bullet);
	e->applyInverseOf(this);
	Light* light = bullet->getLight();
	if (light!=0) light->explode(10,20,vec3(0.7,0.8,0));
}

void ExplosionHub::explode(Ship* ship, vec2 position)
{
	explosionBuilder->explode(ship,position);
}


void ExplosionHub::explode(Asteroid* asteroid, List<Triangle> tris)
{
	explosionBuilder->explode(asteroid, tris);
}

void ExplosionHub::explode(Clone* clone)
{
	explosionBuilder->explode(clone);
	//e->applyInverseOf(this);
}

void ExplosionHub::explode(Clone* clone, vec2 position)
{
	Explosion* e = 	explosionBuilder->explode(clone,position);
	e->applyInverseOf(this);
}
