/*
 * SentientFace.cpp
 *
 *  Created on: 19 Feb 2015
 *      Author: Adam Nasralla
 */

#include <osogine/gfx/GfxObject.h>
#include <osogine/gfx/ObjLoader/ObjLoader.h>
#include <osogine/utils/Dice.h>
#include <races/SentientFace.h>

SentientFace::SentientFace(Entity* parent) : TextureEntity(parent)
{
	initGfx();
	trail = new EntityList();
	trailSize = 10;
	initTrail();
	speed = Dice::roll(0.01,0.04);
	friction = Dice::roll(0.9,1.0);
	renderOrder = 8;
	gfx->setDepthEnabled(false);
}

SentientFace::~SentientFace()
{

}

void SentientFace::update()
{
	TextureEntity::update();
	applyTorque(Dice::roll(vec3(-speed,-speed,-speed),vec3(speed,speed,speed)));
	setFriction(friction);


	if (Dice::chance(50)) friction = Dice::roll(0.9,1.0);
	if (Dice::chance(50)) speed = Dice::roll(0.01,0.04);
	if (Dice::chance(50)) setRotation(Dice::roll(100),Dice::roll(100),Dice::roll(100));
//	if (Dice::chance(50))
//	{
//		float r = 1.8;
//		if (Dice::chance(2)) r = Dice::roll(1.6,1.8);
//		setScale(r,r,r);
//	}
	//setScale(1.6,1.6,1.6);
//
//	if (Dice::chance(200)) setScale(1.6, Dice::roll(0.8,2.0), 1.6);
//	if (Dice::chance(200)) setScale(Dice::roll(0.8,2.0), 1.6, 1.6);
//	if (Dice::chance(200)) setScale(Dice::roll(0.8,2.0), Dice::roll(0.8,2.0), 1.6);

	updateColour();
	updateTrail();
}

void SentientFace::initGfx()
{
	setGfx(ObjLoader::getObject("sentient"));
	setProgram("face");
	setDisableCamera(true);
	setScale(1.8,1.8,1.8);
	setPosition(-300,-400,0);

	for (uint i=0; i<gfx->getNoVertices(); i++)
	{
		colourDir.add(Dice::chance(2));
	}
}

void SentientFace::initTrail()
{
	for (int i=0; i<trailSize; i++)
	{
		TextureEntity* t = new TextureEntity((Entity*)parent);
		trail->add( t );
		t->setGfx( getGfx() );
		t->setProgram("face");
		t->setDisableCamera(true);
		t->setScale(1.8,1.8,1.8);
		t->setPosition(-300,-400,0);
		t->setTextureAlpha(0);
		t->setAlphaModifier(0.25 - (float)i/(float)(trailSize*4));
		t->setRenderOrder(8);
	}
}

void SentientFace::updateTrail()
{
	for (int i=trailSize-1; i>=0; i--)
	{
		TextureEntity* t = (TextureEntity*) trail->get(i);
		TextureEntity* b;
		if (i==0)
		{
			b = this;
		}
		else
		{
			b = (TextureEntity*) trail->get(i-1);
		}
		t->setRotation(b->getRotation());
		t->setScale(b->getScale());
		t->setTextureAlpha(getTextureAlpha() * t->getAlphaModifier());
	}
}

void SentientFace::updateColour()
{
	for (uint i=0; i<gfx->getNoVertices(); i++)
	{
		float r = Dice::roll(-0.05,0.1);
		vec4 c = gfx->getColour(i);
		float b = c.r;
		bool dir = colourDir.get(i);

		if (dir) b += r;
		else b -= r;
		if (b>1)
		{
			colourDir.set(i, !dir);
			b = 1;
		}
		if (b<0.5)
		{
			colourDir.set(i, !dir);
			b = 0.5;
		}

		c.r = b;
		c.g = b;

		gfx->setColour(i,c);

	}
}
