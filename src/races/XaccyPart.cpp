/*
 * XaccyPart.cpp
 *
 *  Created on: 5 Mar 2015
 *      Author: Adam Nasralla
 */

#include <osogine/game/Entity.h>
#include <osogine/game/EntityList.h>
#include <osogine/game/IEntity.h>
#include <osogine/gfx/GfxObject.h>
#include <osogine/gfx/ObjLoader/ObjLoader.h>
#include <osogine/utils/Dice.h>
#include <osogine/utils/List.h>
#include <races/Xaccy.h>
#include <races/XaccyPart.h>

XaccyPart::XaccyPart(Entity* parent, string gfxName) : TextureEntity(parent)
{
	ratio = 0;
	current = 0;
	hi = 0;
	low = 0;
	size = 0;
	trail = new EntityList();
	trailSize = 10;
	renderOrder = 7;
	initGfx(gfxName);
	initTrail();
	gfx->setDepthEnabled(false);

}

XaccyPart::~XaccyPart()
{
	// TODO Auto-generated destructor stub
}

void XaccyPart::update()
{
	TextureEntity::update();
	if (gfxName=="xaccy")
	{
		setPosition(Dice::roll(vec3(-5,-5,-100),vec3(5,5,100)) * size*1.5f + pos);
		if (Dice::chance(20)) setPosition(Dice::roll(vec3(-20,-20,-1000),vec3(20,20,1000)) * size*1.5f + pos);
	}
	updateColour();
	updateTrail();
}

void XaccyPart::initTrail()
{
	for (int i=0; i<trailSize; i++)
	{
		TextureEntity* t = new TextureEntity((Entity*)parent);
		trail->add( t );
		t->setGfx( getGfx() );
		t->setProgram("face");
		t->setDisableCamera(true);
		if (gfxName=="xaccy")
		{
			t->setScale(0.075,0.075,0.075);
		}
		t->setPosition(getPosition());
		t->setTextureAlpha(0);
		t->setAlphaModifier(0.25 - (float)i/(float)(trailSize*4));
		t->setRenderOrder(7);
	}
}

void XaccyPart::initGfx(string gfxName)
{
	this->gfxName = gfxName;
	gfx = ObjLoader::getObject(gfxName);
	setProgram("face");
	setDisableCamera(true);
	//updateGfx();
	if (gfxName=="xaccy")
	{
		float size = ((Xaccy*)parent)->getSize();
		setScale(0.075*size,0.075*size,0.075*size);
	}
	setTextureAlpha(1);
	for (uint i=0; i<gfx->getNoVertices(); i++)
	{
		colourDir.add(Dice::chance(2));
	}
}

void XaccyPart::updateColour()
{
	for (uint i=0; i<gfx->getNoVertices(); i++)
	{
		float r = Dice::roll(-0.05,0.1);
		vec4 c = gfx->getColour(i);
		float b = c.r;
		bool dir = colourDir.get(i);

		if (dir) b += r;
		else b -= r;
		if (b>1)
		{
			colourDir.set(i, !dir);
			b = 1;
		}
		if (b<0)
		{
			colourDir.set(i, !dir);
			b = 0;
		}

		c.r = b;
		c.g = b*Dice::roll(0.0,0.2);
		c.b = b*Dice::roll(0.0,0.5);

		gfx->setColour(i,c);

	}
}



void XaccyPart::updateTrail()
{
	for (int i=trailSize-1; i>=0; i--)
	{
		TextureEntity* t = (TextureEntity*) trail->get(i);
		TextureEntity* b;
		if (i==0)
		{
			b = this;
		}
		else
		{
			b = (TextureEntity*) trail->get(i-1);
		}
		if (gfxName=="xaccy")
		{

			t->setRotation(b->getRotation());
			t->setScale(b->getScale());
			t->setPosition(b->getLocalPosition());
		}
		else
		{
			t->setRotation(b->getLocalRotation());
			t->setPosition(b->getLocalPosition());
		}
		t->setTextureAlpha(getTextureAlpha() * t->getAlphaModifier());
	}
}

void XaccyPart::updateGfx()
{
}
