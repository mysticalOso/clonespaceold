/*
 * Xaccy.h
 *
 *  Created on: 2 Mar 2015
 *      Author: Adam Nasralla
 */

#ifndef SRC_RACES_XACCY_H_
#define SRC_RACES_XACCY_H_

#include <races/SentientFace.h>
#include <map>

class XaccyPart;

class Xaccy: public TextureEntity
{
public:
	Xaccy(Entity* parent,float size);
	~Xaccy();
	void update();

	float getSize(){ return size; }
	void setSize(float s);
	void setPos(vec3 p);
	void setTextureAlpha(float alpha);

private:


	void updateParts();
	void initParts();

	float size;

	map<string, XaccyPart*> parts;
	map<string, float> ratio;
	map<string, float> current;

	vec3 pos;

};

#endif /* SRC_RACES_XACCY_H_ */
