/*
 * Sentient.cpp
 *
 *  Created on: 10 Jan 2015
 *      Author: mysticalOso
 */

#include <builders/CloneBuilder.h>
#include <builders/RoomBuilder.h>
#include <builders/ShipBuilder.h>
#include <clone/Clone.h>
#include <controllers/CloneCtrl.h>
#include <controllers/RaceCtrl.h>
#include <controllers/ShipCtrl.h>
#include <core/CameraCS.h>
#include <hubs/CloneHub.h>
#include <hubs/UIHub.h>
#include <interior/Interior.h>
#include <interior/Pod.h>
#include <interior/Room.h>
#include <osogine/game/Game.h>
#include <osogine/utils/Dice.h>
#include <races/Sentient.h>
#include <ship/DockingBay.h>
#include <ship/Ship.h>
#include <space/Space.h>
#include <ui/ShipDesigner.h>

Sentient::Sentient(RaceHub* hub) : Race(hub)
{
	station = 0;
	controlEnabled = true;
	currentClone = 0;
	camera = 0;
	count = 0;
	shipCtrl = (ShipCtrl*) game->getController("ship");
	cloneCtrl = (CloneCtrl*) game->getController("clone");
	cloneBuilder = (CloneBuilder*) game->getBuilder("clone");
	startShip = 0;
	RaceCtrl* ctrl = (RaceCtrl*) game->getController("race");
	ctrl->setSentient(this);
}

Sentient::~Sentient()
{

}

void Sentient::init()
{
	Race::init();

	cloneBuilder->init();

	camera = (CameraCS*) game->getCamera();
	camera->setSentient(this);

	reset();


	Space* space = (Space*) game->getEntity("space");

	space->initStars();




}

void Sentient::update()
{
	updateCtrl();
	camera->setTargetClone(  getCurrentClone() );

//	count++;
//	if (count == 500)
//	{
//		count = 0;
//		Ship* scout = shipBuilder->buildPlayer();
//		vec3 pos = Dice::roll(vec3(-WIDTH2,-HEIGHT2,0),vec3(WIDTH2,HEIGHT2,0));
//		Clone* clone = cloneBuilder->buildSentientClone();
//		CloneHub* cloneHub = scout->getInterior()->getCloneHub();
//		clone->setIsDriver(true);
//		scout->setDriver(clone);
//		cloneHub->add(clone);
//		addClone(clone);
//		scout->setPosition(pos);
//	}
}

Ship* Sentient::getPlayerShip()
{
	Clone* clone = getCurrentClone();
	if (clone!=0) return getCurrentClone()->getShip();
	else return 0;
}

void Sentient::addClones(Interior* interior)
{

	CloneHub* cloneHub = interior->getCloneHub();

	//interior->getShip()->setDriver(clone);

	Clone* clone = cloneBuilder->buildSentientClone();
	clone->setIsDriver(false);
	clone->setPosition(0,-45,0.2);
	cloneHub->add(clone);
	clone->setName("Poco");
	addClone(clone);

}

Clone* Sentient::getNextClone()
{
	getCurrentClone()->playerLoseControl();
	currentClone++;
	if (currentClone>=clones->size()) currentClone=0;
	Printer::print("Current Clone = ",currentClone,"\n");
	getCurrentClone()->playerTakeControl();
	updateCtrl();
	return getCurrentClone();
}

Clone* Sentient::addCloneToShip(Ship* ship, float x, float y, bool isDriver, string name)
{
	CloneHub* cloneHub = ship->getInterior()->getCloneHub();
	Clone* clone = cloneBuilder->buildSentientClone();
	clone->setIsDriver(isDriver);
	clone->setPosition(x,y,0.2);
	cloneHub->add(clone);
	addClone(clone);
	Printer::print("Clone Count = ",clones->size(),"\n");
	clone->setMaxHealth(20);
	clone->setName(name);
	if (isDriver) ship->setDriver(clone);
	return clone;
}

void Sentient::enableDocking(bool b)
{
	DockingBay* bay = (DockingBay*) station->getPassiveModule("dockingBay");
	bay->setEnabled(b);
}

void Sentient::addStationClone()
{
	addClones( station->getInterior() );
}


void Sentient::reset()
{

	removeAllShips();
	removeAllClones();

	Ship* ship = shipBuilder->buildPlayer();

	addCloneToShip(ship,0,3,true,"Oso");
	//addCloneToShip(ship,3,-4.5,false,"Osfro");

	//ship->setController("ship");
	ship->setAI(0);
	ship->setDriver( getCurrentClone() );

	//ship->setPosition(3000,-3000,0);
	ship->setPosition(6000,-2000,0);
	ship->setName("starter");

	currentClone = 0;

	getCurrentClone()->playerTakeControl();

	station = shipBuilder->buildPlayerStation();
	station->setName("station");

	startShip = ship;

	//camera->setPosition(startShip->getX(), startShip->getY(), 10000);


	//camera->setTarget(ship);

}

void Sentient::updateCtrl()
{
	if (controlEnabled)
	{
		Clone* clone = getCurrentClone();
		Ship* ship = clone->getShip();
		clone->playerTakeControl();
		if (clone->getIsDriver())
		{
			cloneCtrl->setEntity(0);
			shipCtrl->setEntity(ship);
		}
		else
		{
			cloneCtrl->setEntity(clone);
			shipCtrl->setEntity(0);
		}
	}
	else
	{
		cloneCtrl->setEntity(0);
		shipCtrl->setEntity(0);
	}
}

Ship* Sentient::addShip(int type, float x, float y)
{
	Ship* ship = shipBuilder->buildShip(type, this);
	ship->setPosition(x,y,0);
	ship->setHealth(20);
	addCloneToShip(ship,0,0,true,"sento");
	startShip = ship;
	return ship;
}

void Sentient::sendReinforcements()
{
	vec3 playerPos = getPlayerShip()->getPosition();
//	for (int i=0; i<3; i++)
//	{
//		vec3 pos = playerPos  + Dice::roll(vec3(-WIDTH*2,-HEIGHT*4,0),vec3(WIDTH*2,-HEIGHT*2,0));
//		addShip(0,pos.x,pos.y);
//	}
	for (int i=0; i<8; i++)
	{
		int k = (i%2) + 1;
		vec3 pos = playerPos  + Dice::roll(vec3(-WIDTH2*2,-HEIGHT*1,0),vec3(WIDTH2*2,-HEIGHT*1,0));
		addShip(k,pos.x,pos.y);
	}

//	for (int i=0; i<3; i++)
//	{
//		vec3 pos = playerPos  + Dice::roll(vec3(-WIDTH*2,-HEIGHT*4,0),vec3(WIDTH*2,-HEIGHT*2,0));
//		addShip(8,pos.x,pos.y);
//	}
}

void Sentient::birth()
{
	CloneHub* cloneHub = station->getInterior()->getCloneHub();
	Clone* clone = cloneBuilder->buildSentientClone();
	clone = cloneBuilder->buildSentientClone();
	clone->setIsDriver(false);
	clone->setPosition(12,-40,0.2);
	cloneHub->add(clone);
	clone->setName("Osus");
	addClone(clone);
	currentClone = 0;
	camera->setTargetClone(  getCurrentClone() );
}


void Sentient::removeClone(Clone* clone)
{
	if(clones->contains(clone)) clones->remove(clone);
	if (clones->size() == 0)
	{
		game->event("allDead");
//		CloneHub* cloneHub = station->getInterior()->getCloneHub();
//		Clone* clone = cloneBuilder->buildSentientClone();
//		clone = cloneBuilder->buildSentientClone();
//		clone->setIsDriver(false);
//		clone->setPosition(100,-5,0.5);
//		cloneHub->add(clone);
//		clone->setName("Devlos");
//		addClone(clone);
	}
}

void Sentient::sendTeleporterShip()
{
	Printer::print("Sending Teleporter");
	vec3 playerPos = getPlayerShip()->getPosition();
	vec3 pos = playerPos  + Dice::roll(vec3(-WIDTH,HEIGHT2,0),vec3(-WIDTH,0,0));
	Printer::print("Adding ship");
	Ship* ship = addShip(3,pos.x,pos.y);
	ship->setMaxHealth(100);
	ship->setName("teleporter");
	Printer::print("Adding clone");
	Clone* c = addCloneToShip(ship,0,7.9,false,"Pocoleen");
	c->setArmed(true);
}

void Sentient::sendScout()
{
	vec3 playerPos = getPlayerShip()->getPosition();
	vec3 pos = playerPos  + Dice::roll(vec3(-WIDTH,HEIGHT2,0),vec3(-WIDTH,0,0));

	Ship* ship = addShip(1,pos.x,pos.y);
	ship->setMaxHealth(10);
	ship->setName("SentientScout");
}

void Sentient::sendMother()
{
	vec3 playerPos = getPlayerShip()->getPosition();
	vec3 pos = playerPos  + Dice::roll(vec3(-WIDTH,HEIGHT2,0),vec3(-WIDTH,0,0));

	Ship* ship = addShip(2,pos.x,pos.y);
	ship->setMaxHealth(30);
	ship->setName("SentientMother");
}
