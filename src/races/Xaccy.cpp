/*
 * Xaccy.cpp
 *
 *  Created on: 2 Mar 2015
 *      Author: Adam Nasralla
 */

#include <races/Xaccy.h>
#include <osogine/gfx/GfxObject.h>
#include <osogine/gfx/ObjLoader/ObjLoader.h>
#include <osogine/utils/Dice.h>
#include <osogine/utils/Transform.h>
#include <osogine/utils/Utils.h>
#include <races/XaccyPart.h>


Xaccy::Xaccy(Entity* parent, float size) : TextureEntity(parent)
{
	this->size = size;
	initParts();

}

Xaccy::~Xaccy()
{

}

void Xaccy::update()
{

	TextureEntity::update();
	updateParts();



}





void Xaccy::updateParts()
{


	ratio["rightTeeth"] += Dice::roll(-0.05,0.2);
	ratio["leftTeeth"] += Dice::roll(-0.05,0.2);
	ratio["leftJaw"] += Dice::roll(-0.05,0.2);
	ratio["rightJaw"] += Dice::roll(-0.05,0.2);

	current["rightTeeth"] = Utils::sinInterpolate(15.0,80.0,ratio["rightTeeth"]);
	current["leftTeeth"] = Utils::sinInterpolate(15.0,80.0,ratio["leftTeeth"]);
	current["leftJaw"] = Utils::sinInterpolate(-50.0,20.0,ratio["leftJaw"]);
	current["rightJaw"] = Utils::sinInterpolate(-70.0,-10.0,ratio["rightJaw"]);

	parts["rightTeeth"]->setRotationD(current["rightTeeth"],0,0);
	parts["leftTeeth"]->setRotationD(current["leftTeeth"],0,0);
	parts["leftJaw"]->setRotationD(current["leftJaw"],66,-291);
	parts["rightJaw"]->setRotationD(current["rightJaw"],274,-34);
}

void Xaccy::setTextureAlpha(float alpha)
{
	textureAlpha = alpha;

	parts["head"]->setTextureAlpha(alpha);
	parts["rightJaw"]->setTextureAlpha(alpha);
	parts["leftJaw"]->setTextureAlpha(alpha);
	parts["rightTeeth"]->setTextureAlpha(alpha);
	parts["leftTeeth"]->setTextureAlpha(alpha);
}

void Xaccy::setSize(float s)
{
	parts["head"]->setSize(s);
}

void Xaccy::setPos(vec3 p)
{
	parts["head"]->setPos(p);
}

void Xaccy::initParts()
{

	parts["head"] = new XaccyPart(this,"xaccy");
	parts["head"]->setSize(size);
	parts["head"]->setPos(pos);
	//parts["head"]->setScale(0.075);
	parts["rightJaw"] = new XaccyPart(parts["head"] , "xaccyJaw");
	parts["leftJaw"] = new XaccyPart(parts["head"], "xaccyJaw"  );
	parts["rightTeeth"] = new XaccyPart( parts["rightJaw"], "xaccyTeeth"  );
	parts["leftTeeth"] = new XaccyPart( parts["leftJaw"], "xaccyTeeth"  );

	parts["rightJaw"]->setProgram("face");
	parts["rightJaw"]->setPosition(-1229,-980,169);
	parts["rightJaw"]->setRotationD(-53,274,-34);
	parts["rightJaw"]->setTextureAlpha(1);

	parts["rightTeeth"]->setProgram("face");
	parts["rightTeeth"]->setPosition(0,0,-500);
	parts["rightTeeth"]->setRotationD(65,0,0);
	parts["rightTeeth"]->setTextureAlpha(1);

	parts["leftJaw"]->setProgram("face");
	parts["leftJaw"]->setPosition(1223,-1144,230);
	parts["leftJaw"]->setRotationD(-12,66,-291);
	parts["leftJaw"]->setTextureAlpha(1);

	parts["leftTeeth"]->setProgram("face");
	parts["leftTeeth"]->setPosition(0,0,-500);
	parts["leftTeeth"]->setRotationD(65,0,0);
	parts["leftTeeth"]->setTextureAlpha(1);

	current["rightTeeth"] = Dice::roll(15.0,65.0);
	current["leftTeeth"] = Dice::roll(15.0,65.0);
	current["leftJaw"] = Dice::roll(-50.0,10.0);
	current["rightJaw"] = Dice::roll(-80.0,-20.0);

	ratio["rightTeeth"] = Dice::roll(0.0,1.0);
	ratio["leftTeeth"] = Dice::roll(0.0,1.0);
	ratio["leftJaw"] = Dice::roll(0.0,1.0);
	ratio["rightJaw"] = Dice::roll(0.0,1.0);
}
