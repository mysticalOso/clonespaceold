/*
 * Sentient.h
 *
 *  Created on: 10 Jan 2015
 *      Author: mysticalOso
 */

#ifndef SRC_RACES_SENTIENT_H_
#define SRC_RACES_SENTIENT_H_

#include <races/Race.h>

class CameraCS;
class Interior;
class ShipCtrl;
class CloneCtrl;
class CloneBuilder;

class Sentient : public Race {
public:
	Sentient(RaceHub* hub);
	virtual ~Sentient();
	void init();
	void update();

	Clone* getCurrentClone(){ return getClone(currentClone); }

	Clone* getNextClone();

	Ship* getPlayerShip();

	void toggleCloneCtrl(bool b){ controlEnabled = b; }

	void sendReinforcements();

	void sendTeleporterShip();

	void sendScout();

	void sendMother();


	Ship* addShip(int type, float x, float y);

	void removeClone(Clone* clone);

	void birth();

	void enableDocking(bool b);

	void addStationClone();

	void reset();
private:

	void addClones(Interior* interior);

	Clone* addCloneToShip(Ship* ship, float x, float y, bool isDriver, string name);
	void updateCtrl();

	Ship* station;
	int currentClone;
	CameraCS* camera;

	Ship* startShip;

	int count;

	ShipCtrl* shipCtrl;
	CloneCtrl* cloneCtrl;
	CloneBuilder* cloneBuilder;
	bool controlEnabled;
};

#endif /* SRC_RACES_SENTIENT_H_ */
