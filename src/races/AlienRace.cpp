/*
 * AlienRace.cpp
 *
 *  Created on: 12 Jan 2015
 *      Author: mysticalOso
 */

#include <builders/CloneBuilder.h>
#include <builders/ShipBuilder.h>
#include <clone/Clone.h>
#include <core/Constants.h>
#include <core/Vessel.h>
#include <glm/vec3.hpp>
#include <hubs/CloneHub.h>
#include <hubs/RaceHub.h>
#include <interior/Interior.h>
#include <osogine/game/Game.h>
#include <osogine/utils/Dice.h>
#include <races/AlienRace.h>
#include <ship/Ship.h>
#include <string>

AlienRace::AlienRace(RaceHub* hub) : Race(hub)
{
	count = 0;
	setController("race");
	cloneBuilder = (CloneBuilder*) game->getBuilder("clone");
	first = true;
}

AlienRace::~AlienRace()
{

}

void AlienRace::update()
{
	Race::update();
//	count++;
//	if (count == 10)
//	{
//		count = 0;
//
//	}
}

void AlienRace::init()
{
	Race::init();
	for (int i = 0; i < 1; i++)
	{
	//Ship* scout = shipBuilder->buildXacadianScout();
	//scout->setPosition(100,100,0);
	}
}

Ship* AlienRace::addScout()
{
	Ship* scout = shipBuilder->buildXacadianScout();
	Ship* currentPlayer = hub->getPlayerShip();
	vec3 pos = currentPlayer->getPosition() + Dice::roll(vec3(0,HEIGHT2,0),vec3(WIDTH2*2,HEIGHT2*2,0));
	scout->setMaxHealth(5);
	scout->setPosition(pos);
	//addCloneToShip(scout,0,0,true,"xee");
	return scout;
}

Ship* AlienRace::addKiller()
{
	Ship* scout = shipBuilder->buildXacadianScout();
	Ship* currentPlayer = hub->getPlayerShip();
	vec3 pos = currentPlayer->getPosition() + Dice::roll(vec3(0,HEIGHT2,0),vec3(WIDTH2*2,HEIGHT2*2,0));
	addShip(6, pos.x, pos.y);
	//addCloneToShip(scout,0,0,true,"xee");
	return scout;
}

Ship* AlienRace::addMother()
{
	Ship* scout = shipBuilder->buildXacadianMother();
	Ship* currentPlayer = hub->getPlayerShip();
	vec3 pos = currentPlayer->getPosition() + Dice::roll(vec3(-WIDTH2,HEIGHT2*2,0),vec3(WIDTH2,HEIGHT2*2,0));
	scout->setPosition(pos);

	if (first) {scout->setMaxHealth(100000); first = false; }
	else scout->setMaxHealth(50);
	float scale = 2.0f / 47.0f;

	addCloneToShip(scout,0,240.f * scale ,true,"Xaccy");
	addCloneToShip(scout,-5,0,false,"Fredric");
	return scout;
}

void AlienRace::addShip(int type, float x, float y)
{
	Ship* ship = shipBuilder->buildShip(type, this);
	ship->setPosition(x,y,0);
	ship->setMaxHealth(50);
	addCloneToShip(ship,0,0,true,"xac");
}

void AlienRace::addCloneToShip(Ship* ship, float x, float y, bool isDriver, string name)
{
	CloneHub* cloneHub = ship->getInterior()->getCloneHub();
	Clone* clone = cloneBuilder->buildXacadienClone();
	clone->setIsDriver(isDriver);
	clone->setPosition(x,y,0.25);
	cloneHub->add(clone);
	addClone(clone);
	clone->setName(name);
	if (isDriver) ship->setDriver(clone);
}


void AlienRace::addWave(int n)
{
	Ship* currentPlayer = hub->getPlayerShip();

	if (n==1)
	{
		for (int i = 0; i < 4; i++)
		{
			Ship* ship = addMother();
			vec3 pos = currentPlayer->getPosition() + Dice::roll(vec3(-WIDTH2,HEIGHT2*4,0),vec3(WIDTH2,HEIGHT2*6,0));
			ship->setPosition(pos);
		}

		for (int i = 0; i < 8; i++)
		{
			Ship* ship = addScout();
			vec3 pos = currentPlayer->getPosition() + Dice::roll(vec3(-WIDTH2,HEIGHT2*4,0),vec3(WIDTH2,HEIGHT2*6,0));
			ship->setPosition(pos);
		}
	}
	else
	{
		for (int i = 0; i < 10; i++)
		{
			vec3 pos = currentPlayer->getPosition() + Dice::roll(vec3(-WIDTH2,HEIGHT2*4,0),vec3(WIDTH2,HEIGHT2*6,0));
			addShip(6, pos.x, pos.y);
		}
	}
}

void AlienRace::reset()
{
	Race::reset();
	first = true;
}
