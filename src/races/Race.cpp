/*
 * AlienRace.cpp
 *
 *  Created on: 10 Jan 2015
 *      Author: mysticalOso
 */

#include <builders/ShipBuilder.h>
#include <hubs/RaceHub.h>
#include <hubs/ShipHub.h>
#include <osogine/game/Game.h>
#include <races/Race.h>
#include <ship/Ship.h>
#include <space/Space.h>

Race::Race(RaceHub* hub) : Entity(hub)
{
	this->hub = hub;
	enemies = new List<Race*>();
	clones = new List<Clone*>();
	this->game = hub->getGame();
	space = 0;
	shipHub = 0;
	shipBuilder = 0;
}

Race::~Race() {
}

void Race::init()
{
	space = (Space*) game->getEntity("space");
	shipBuilder = (ShipBuilder*) game->getBuilder("ship");
	shipHub = space->getShipHub();
}

bool Race::isRace(string name)
{
	return hub->getRace(name)==this;
}

int Race::getShipCount()
{
	return shipHub->getRaceCount(this);
}

void Race::removeAllShips()
{
	shipHub->removeAllShipsOf(this);
}

void Race::removeAllClones()
{
	while( clones->size() > 0)
	{
		Clone* c = clones->get(0);
		clones->remove(c);
	}
}

void Race::reset()
{
	removeAllShips();
	removeAllClones();
}
