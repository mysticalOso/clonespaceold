/*
 * AlienRace.h
 *
 *  Created on: 10 Jan 2015
 *      Author: mysticalOso
 */

#ifndef SRC_RACES_RACE_H_
#define SRC_RACES_RACE_H_

#include <osogine/game/Entity.h>
#include <osogine/utils/List.h>

class Ship;
class Game;
class Space;
class ShipHub;
class ShipBuilder;
class RaceHub;
class Clone;

class Race: public Entity {
public:
	Race(RaceHub* hub);
	virtual ~Race();
	virtual void init();

	void addEnemy( Race* r ){ if (!enemies->contains(r)) enemies->add(r); }
	void removeEnemy( Race* r ){ enemies->remove(r); }
	bool isEnemy( Race* r ){ return enemies->contains(r); }

	void addClone(Clone* c){ clones->add(c); }
	virtual void removeClone(Clone* c){ clones->remove(c); }

	Clone* getClone(int i){ return clones->get(i); }
	int getCloneCount(){ return clones->size(); }

	bool isRace(string name);

	int getShipCount();

	void removeAllShips();
	void removeAllClones();

	virtual void reset();

protected:
	List<Clone*>* clones;
	List<Race*>* enemies;

	Game* game;
	Space* space;
	RaceHub* hub;
	ShipHub* shipHub;
	ShipBuilder* shipBuilder;
};

#endif /* SRC_RACES_RACE_H_ */
