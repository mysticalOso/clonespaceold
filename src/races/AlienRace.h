/*
 * AlienRace.h
 *
 *  Created on: 12 Jan 2015
 *      Author: mysticalOso
 */

#ifndef SRC_RACES_ALIENRACE_H_
#define SRC_RACES_ALIENRACE_H_

#include <races/Race.h>
#include <glm/fwd.hpp>

class CloneBuilder;

class AlienRace: public Race {
public:
	AlienRace(RaceHub* hub);
	virtual ~AlienRace();
	void update();

	void init();

	Ship* addScout();

	Ship* addMother();

	Ship* addKiller();

	void addShip(int type, float x, float y);

	void addWave(int n);

	void reset();


private:
	void addCloneToShip(Ship* ship, float x, float y, bool isDriver, string name);

	bool first;
	int count;
	CloneBuilder* cloneBuilder;
};

#endif /* SRC_RACES_ALIENRACE_H_ */
