/*
 * XaccyPart.h
 *
 *  Created on: 5 Mar 2015
 *      Author: Adam Nasralla
 */

#ifndef SRC_RACES_XACCYPART_H_
#define SRC_RACES_XACCYPART_H_

#include <osogine/game/TextureEntity.h>
#include <map>

class XaccyPart: public TextureEntity
{
public:
	XaccyPart(Entity* parent, string gfxName);
	~XaccyPart();
	void setSize(float size){this->size=size;}
	void setPos(vec3 pos){this->pos=pos;}
	void update();
	void setRange(float hi, float low){ this->hi = hi; this->low = low; }

private:
	void initTrail();
	void initGfx(string gfxName);
	void updateColour();
	void updateTrail();
	void updateGfx();

	map<string, TextureEntity*> parts;
	float ratio;
	float current;
	float hi;
	float low;

	string gfxName;
	List<bool> colourDir;
	EntityList* trail;
	int trailSize;
	vec3 pos;

	float size;

};

#endif /* SRC_RACES_XACCYPART_H_ */
