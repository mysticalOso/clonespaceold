/*
 * SentientFace.h
 *
 *  Created on: 19 Feb 2015
 *      Author: Adam Nasralla
 */

#ifndef SRC_RACES_SENTIENTFACE_H_
#define SRC_RACES_SENTIENTFACE_H_

#include <osogine/game/TextureEntity.h>

class SentientFace: public TextureEntity
{
public:
	SentientFace(Entity* parent);
	~SentientFace();
	virtual void update();

protected:
	virtual void initGfx();
	virtual void initTrail();
	virtual void updateTrail();
	virtual void updateColour();

	float speed;
	float friction;
	List<bool> colourDir;
	EntityList* trail;
	int trailSize;
};

#endif /* SRC_RACES_SENTIENTFACE_H_ */
