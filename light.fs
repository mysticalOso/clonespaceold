#version 330 core

in vec2 UV;
uniform vec3 lightColour;

out vec4 colour;

uniform sampler2D textureSampler;

void main()
{
	vec4 color1 = texture2D( textureSampler, UV ).rgba;
	colour = vec4(color1.a,color1.a,color1.a,color1.a) * vec4(lightColour*0.7,1);
	
}