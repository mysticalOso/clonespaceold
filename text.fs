#version 330 core

in vec2 UV;
uniform float alpha;

out vec4 colour;

uniform sampler2D textureSampler;

void main(){

	vec4 color1 = texture2D( textureSampler, UV ).rgba;
	colour = vec4(color1.r,color1.g,color1.b,color1.r*alpha);

}