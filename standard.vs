#version 330 core

layout(location = 0) in vec3 vertexPos;
layout(location = 1) in vec2 vertexUV;
layout(location = 2) in vec3 normal;

out vec2 UV;
out vec3 normalOut;

uniform mat4 MVP;
uniform mat4 M;

void main()
{	

	gl_Position =  MVP * vec4( vertexPos, 1 );
	UV = vertexUV;
	
	normalOut = vec3( M * vec4( normal, 0 ) );
	
	
}

