#version 330 core

// Input vertex data, different for all executions of this shader.
layout(location = 0) in vec3 vertexPos;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec4 colourIn;

flat out vec3 fragNormal;
flat out vec4 fragColour;

out vec3 lightDirection;
out float lightDistance;

out vec3 p1;

uniform mat4 MVP;
uniform mat4 M;

uniform vec3 lightPosition;


void main(){

    gl_Position =  MVP * vec4(vertexPos,1);
    
    vec4 p =  MVP * vec4(vertexPos,1);
    p1 = vec3(p) / p.a;
    
    lightDirection = lightPosition - vec3(M * vec4(vertexPos,1));
	lightDistance = length(lightDirection) / 6.0f;
	
	vec4 norm = M * vec4(normal,0);
   	fragNormal = norm.xyz;
    
	fragColour = colourIn;
	
}
