#version 330 core

// Input vertex data, different for all executions of this shader.
layout(location = 0) in vec3 vertexPosition_modelspace;
layout(location = 1) in vec4 vertexColour;
layout(location = 2) in vec3 velocity;
layout(location = 3) in vec3 force;
layout(location = 4) in vec3 axis;
layout(location = 5) in float spin;
layout(location = 6) in float life;
layout(location = 7) in float death;
layout(location = 8) in vec3 offset;
layout(location = 9) in vec3 normal;

// Output data ; will be interpolated for each fragment.
flat out vec4 fragColour;
flat out vec3 fragNormal;
// Values that stay constant for the whole mesh.
uniform mat4 MVP;
uniform mat4 M;
uniform float time;
uniform float alpha;

void main(){	

	// Output position of the vertex, in clip space : MVP * position
	vec3 p = vertexPosition_modelspace;
	
	vec3 p2 = axis;
	
	
	
	float theta = spin*time;
	
	vec3 u,q1,q2;
   float d;


   q1.x = p.x;
   q1.y = p.y;
   q1.z = p.z;

   u.x = p2.x;
   u.y = p2.y;
   u.z = p2.z;

   u = normalize(u);
   d = sqrt(u.y*u.y + u.z*u.z);


   if (d != 0) {
      q2.x = q1.x;
      q2.y = q1.y * u.z / d - q1.z * u.y / d;
      q2.z = q1.y * u.y / d + q1.z * u.z / d;
   } else {
      q2 = q1;
   }


   q1.x = q2.x * d - q2.z * u.x;
   q1.y = q2.y;
   q1.z = q2.x * u.x + q2.z * d;



   float ct = cos(theta);
   float st = sin(theta);
   q2.x = q1.x * ct - q1.y * st;
   q2.y = q1.x * st + q1.y * ct;
   q2.z = q1.z;


   q1.x =   q2.x * d + q2.z * u.x;
   q1.y =   q2.y;
   q1.z = - q2.x * u.x + q2.z * d;


   if (d != 0) {
      q2.x =   q1.x;
      q2.y =   q1.y * u.z / d + q1.z * u.y / d;
      q2.z = - q1.y * u.y / d + q1.z * u.z / d;
   } else {
      q2 = q1;
   }
	
	p = q2;
	
	
	p += (force*time+velocity)*time + offset;
	gl_Position =  MVP * vec4(p ,1);
	
	float a = 1;


	if (time>life)
	{
		a = (death - (time-life)) / death;
	}
	
	if (normal == vec3(0,0,-1)) fragNormal = normal;
	else
	{
		vec4 norm = M * vec4(normal,0);
   		fragNormal = norm.xyz;
	}
	
	vec4 colour = vertexColour;
	colour.a = alpha * a;
	fragColour = colour;
	
}

