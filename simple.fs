#version 330 core

// Interpolated values from the vertex shaders
in vec4 fragmentColour;

// Ouput data
out vec4 colour;

void main(){

	// Output colour = colour specified in the vertex shader, 
	// interpolated between all 3 surrounding vertices
	colour = fragmentColour;

}