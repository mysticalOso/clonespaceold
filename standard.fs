#version 330 core

in vec2 UV;
in vec3 normalOut;

out vec3 colour;

uniform sampler2D textureSampler;
uniform vec3 lightColour[8];
uniform vec3 lightDirection[8];
uniform int numLights;

void main()
{

	
	vec3 materialColour = texture( textureSampler, UV ).rgb;

	vec3 colour1 = vec3(0,0,0);
	
	for ( int i=0; i<numLights; i++ )
	{
		float cosTheta = clamp( dot( normalize( normalOut ), lightDirection[i] ), 0, 1 );
		colour1 += materialColour * lightColour[i] * cosTheta;
	}

	colour1 += materialColour * vec3(0.1,0.1,0.1);

	colour = vec3(1,1,1);
	
	//colour = colour1;



		
}