#version 330 core

flat in vec3 fragNormal;
flat in vec4 fragColour;

out vec4 colourOut;

uniform vec3 lightColour[3];
uniform vec3 lightDirection[3];
uniform int numLights;

void main()
{
	
	if (fragNormal!=vec3(0,0,-1))
	{
	float alpha = fragColour.a;
		vec4 totalColour = vec4(0,0,0,0);
		
		for ( int i=0; i<numLights; i++ )
		{
			float cosTheta = clamp( dot( normalize( fragNormal ), lightDirection[i] ), 0, 1 );
			totalColour += fragColour * vec4( lightColour[i], 1 ) * cosTheta;
		}
	
		//totalColour += fragColour * vec4(0.1,0.1,0.1,0);
		totalColour.a = alpha;
		colourOut = totalColour;
	}
	else
	{
		colourOut = fragColour;
	}
}