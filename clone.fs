#version 330 core

flat in vec3 fragNormal;
flat in vec4 fragColour;

in vec3 p1;
out vec4 colourOut;

in vec3 lightDirection;
in float lightDistance;

uniform vec3 lightColour;
uniform vec2 lightMap[256];
uniform int lightMapSize;
uniform int isBlack;

void main()
{

	bool isInside = false;
	int i;
	int j;
	vec4 totalColour = vec4(0,0,0,0);
	vec3 fNormal = normalize(fragNormal);
	int nvert = lightMapSize;
	for (i = 0, j = nvert-1; i < nvert; j = i++)
	{
		vec2 p2 = lightMap[i];
		vec2 p3 = lightMap[j];
		if ( ((p2.y>p1.y) != (p3.y>p1.y)) &&
			 (p1.x < (p3.x-p2.x) * (p1.y-p2.y) / (p3.y-p2.y) + p2.x) )
			 {
			 	isInside = !isInside;
			 }
	}
	if (isInside)
	{
		float cosTheta = clamp( dot( fNormal , normalize(lightDirection) ), 0, 1 );
		totalColour += fragColour * vec4( lightColour*3, 1 ) * cosTheta * (1-lightDistance);
	}

	if (isBlack==1)
	{
	
	vec3 light = vec3(0.5,0.5,0.8);
	totalColour += fragColour * vec4(vec3(0.06,0.05,0.05) * clamp( dot(normalize(light),fNormal), 0,1 ),1);
	light = vec3(-0.5,0.5,0.8);
	totalColour += fragColour * vec4(vec3(0.05,0.06,0.05)  * clamp( dot(normalize(light),fNormal), 0,1 ),1);
	light = vec3(0,-0.5,0.8);
	totalColour += fragColour * vec4(vec3(0.05,0.05,0.06) * clamp( dot(normalize(light),fNormal), 0,1 ),1);
	
	totalColour.a = fragColour.a;
	}

	colourOut  = totalColour;
		
}