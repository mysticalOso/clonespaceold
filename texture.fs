#version 330 core

in vec2 UV;
uniform float alpha;
uniform vec3 textureColour;
out vec4 colour;

uniform sampler2D textureSampler;

void main()
{
	vec4 color1 = texture2D( textureSampler, UV ).rgba;
	color1 = color1 * vec4(textureColour,1);
	colour = vec4(color1.r,color1.g,color1.b,alpha * color1.a);
	
}