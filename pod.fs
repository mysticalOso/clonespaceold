#version 330 core

flat in vec3 fragNormal;
flat in vec4 fragColour;

out vec4 colourOut;

uniform vec3 lightColour[3];
uniform vec3 lightDirection[3];
uniform int numLights;

void main()
{
/*
	vec4 totalColour = vec4(0,0,0,0);
	
	float alpha1 = fragColour.a;
	
	for ( int i=0; i<numLights; i++ )
	{
		float cosTheta = clamp( dot( normalize( fragNormal ), normalize(lightDirection[i]) ), 0, 1 );
		totalColour += fragColour * vec4( lightColour[i], 1 ) * cosTheta;
	}

	//totalColour += vec4(0,0,0.02,0);
	totalColour.a = alpha1;*/
	
	colourOut = fragColour;

		
}