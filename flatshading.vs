#version 330 core

// Input vertex data, different for all executions of this shader.
layout(location = 0) in vec3 vertexPos;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec4 colourIn;

flat out vec3 fragNormal;
flat out vec4 fragColour;

uniform mat4 MVP;
uniform mat4 M;


void main(){

    gl_Position =  MVP * vec4(vertexPos,1);

	vec4 norm = M * vec4(normal,0);
   	fragNormal = norm.xyz;
    
	fragColour = colourIn;
	
}
